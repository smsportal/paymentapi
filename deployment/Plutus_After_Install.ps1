function Init-Environment {
	Param (
        [Parameter(Mandatory = $true)]
		[string]$Root,
		[Parameter(Mandatory = $true)]
		[string]$Folder,
		[Parameter(Mandatory = $true)]
		[string]$Application
	)
	
	$DeploymentGroup = [Environment]::GetEnvironmentVariable("DEPLOYMENT_GROUP_NAME");
	$ServiceUser = "SMSP\smsservice";
	$ServicePassword = "M1llpark!";
	$ConfigFolderPath = "$($Root)\$($Folder)";
	$ConfigFileName = "$($Application).config";
	$ConfigFilePath = "$($ConfigFolderPath)\$ConfigFileName";
	$ValuesFilePath = "";

	if($DeploymentGroup -like "*QA*")
	{
		$DeploymentGroup = "QA";
		$ValuesFilePath = "$($ConfigFolderPath)\deploy\qa.app.config";
	}
	elseif(($DeploymentGroup -eq "Production") -or ($DeploymentGroup -like "SMSP-*"))
	{
		$DeploymentGroup = "Production";
		$ValuesFilePath = "$($ConfigFolderPath)\deploy\production.app.config";
	}
	else
	{
		$DeploymentGroup = "DEV"
		$ValuesFilePath = ".\dev\dev.app.config";
		$ConfigFilePath = "..\$($Folder)\App.config";
		$ConfigFileName = "App.config";
	}
	$ValuesFolderPath = $ValuesFilePath | Split-Path
	$ConfigFolderPath = $ConfigFilePath | Split-Path
	
	$AppName = [io.path]::GetFileNameWithoutExtension($Application);
		
	return @{
		"DeploymentGroup" = $DeploymentGroup;
		"ConfigFolderPath" = $ConfigFolderPath;
		"ApplicationFileName" = $Application;
		"ConfigFileName" = $ConfigFileName;
		"ConfigFilePath" = $ConfigFilePath;
		"ValuesFilePath" = $ValuesFilePath;
		"ValuesFolderPath" = $ValuesFolderPath;
		"Hostname" = $env:computername;
		"Root" = $Root
		"ServiceUser" = $ServiceUser;
		"ServicePassword" = $ServicePassword;
		"ServiceName" = "_$($AppName)";
	}
}

function Merge-Config {
	Param (
        [Parameter(Mandatory = $true)]
		[string]$Path, 
		[Parameter(Mandatory = $true)]
		[string]$Values, 
		[Parameter(Mandatory = $true)]
		[string]$Hostname
	) 
	$exists = Test-Path -Path $Path
	if(!$exists) {
		Write-Error "Could not find $($Path)"
	} else {
		$exists = Test-Path -Path $Values
		if(!$exists) {
			Write-Output "Could not find $($Values)"
		} else {
			Write-Output "Merging $($Path) with values from $($Values)"
			$config = (Get-Content $Path -Raw)
			$configValues = (Get-Content $Values) 
			foreach($line in $configValues) {
				if($line -like '*=*') 
				{
					$elements = $line.Split('=',2)

					$configValue = $elements[1];
					$configKey = "%%" + $elements[0] + "%%"
					$config = $config.Replace($configKey, $configValue)

					$configKey = "%" + $elements[0] + "%"
					$config = $config.Replace($configKey, $configValue)
				}
			}

			Set-Content $Path $config
			(Get-Content $Path) | ForEach-Object { $_ -replace "!HOSTNAME", $Hostname } | Set-Content $Path
		}
	}
}

function Check-Service {
	Param (
        [Parameter(Mandatory = $true)]
		[string]$Name, 
		[Parameter(Mandatory = $true)]
		[string]$Path, 
		[Parameter(Mandatory = $true)]
		[string]$User, 
		[Parameter(Mandatory = $true)]
		[string]$Password
	) 
	if (!(Get-Service "$Name" -ErrorAction SilentlyContinue))
	{
		New-Service -Name "$Name" -BinaryPathName "$Path"
		$Svc = Get-WmiObject win32_service -filter "name='$Name'"
		$Svc.Change($Null, $Null, $Null, $Null, $Null, $Null, "$User", "$Password")
		Write-Output "Service Created"
	}
	else 
	{
		Write-Output "Service already exists"
	}
}

#INSTALL
$cfg = Init-Environment -Root "C:\Live\Services" -Folder "Plutus" -Application "Plutus.exe"
Write-Output "$($cfg.DeploymentGroup) Machine:  $($cfg.Hostname) Folder:  $($cfg.ConfigFolderPath)"
Merge-Config -Path $cfg.ConfigFilePath -Values "$($cfg.ValuesFolderPath)\$($cfg.Hostname).app.config" -Hostname $cfg.Hostname
Merge-Config -Path $cfg.ConfigFilePath -Values $cfg.ValuesFilePath -Hostname $cfg.Hostname
Check-Service -Name "$($cfg.ServiceName)" -Path "$($cfg.ConfigFolderPath)\$($cfg.ApplicationFileName)" -User "$($cfg.ServiceUser)" -Password "$($cfg.ServicePassword)"