﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Moq;
using Moq.Language.Flow;
using NUnit.Framework;
using SmsPortal.Core.Time;

namespace Plutus.Tests.Base
{
    [TestFixture]
    public abstract class TestBase<T> 
    {
        private MockedInterfaceCollection interfaces;
        protected readonly MockedLogging<TestBase<T>> Log = new MockedLogging<TestBase<T>>();
        protected TestingTimeProvider Timing { get; } = new TestingTimeProvider(DateTime.Now); 
        
        public TMockedT InstanceOf<TMockedT>() 
            where TMockedT : class
            => interfaces
                .Mocked<TMockedT>()
                .Object;
        
        public void Verify<TMockedT>(Expression<Action<TMockedT>> expression, Times? times = null) 
            where TMockedT : class =>
            interfaces
                .Mocked<TMockedT>()
                .Verify(expression, times ?? Times.AtLeastOnce());
        
        public ISetup<TMockedT> Setup<TMockedT>(Expression<Action<TMockedT>> expression) 
            where TMockedT : class =>
            interfaces
                .Mocked<TMockedT>()
                .Setup(expression);
        
        public ISetup<TMockedT, TResult> Setup<TMockedT, TResult>(Expression<Func<TMockedT, TResult>> expression) 
            where TMockedT : class =>
            interfaces
                .Mocked<TMockedT>()
                .Setup(expression);
        
        public ISetup<TMockedT, Task<TResult>> Setup<TMockedT, TResult>(Expression<Func<TMockedT, Task<TResult>>> expression) 
            where TMockedT : class =>
            interfaces
                .Mocked<TMockedT>()
                .Setup(expression);
        
        public ISetup<TMockedT, IEnumerable<TResult>> Setup<TMockedT, TResult>(Expression<Func<TMockedT, IEnumerable<TResult>>> expression) 
            where TMockedT : class =>
            interfaces
                .Mocked<TMockedT>()
                .Setup(expression);
        
        public ISetup<TMockedT, List<TResult>> Setup<TMockedT, TResult>(Expression<Func<TMockedT, List<TResult>>> expression) 
            where TMockedT : class =>
            interfaces
                .Mocked<TMockedT>()
                .Setup(expression);
        
        public ISetup<TMockedT, Task<IEnumerable<TResult>>> Setup<TMockedT, TResult>(Expression<Func<TMockedT, Task<IEnumerable<TResult>>>> expression) 
            where TMockedT : class =>
            interfaces
                .Mocked<TMockedT>()
                .Setup(expression);
        
        public ISetup<TMockedT, Task<List<TResult>>> Setup<TMockedT, TResult>(Expression<Func<TMockedT, Task<List<TResult>>>> expression) 
            where TMockedT : class =>
            interfaces
                .Mocked<TMockedT>()
                .Setup(expression);

        public abstract void SetUpTestBase();

        public abstract T CreateInstance();

        public virtual void TearDownTestBase()
        {
            interfaces = null;
        }

        [SetUp]
        public void SetUp()
        {
            TimeProvider.Current = Timing;
            interfaces = new MockedInterfaceCollection();
            SetUpTestBase();
        }

        [TearDown]
        public void TearDown()
        {
            TearDownTestBase();
        }
    }
}