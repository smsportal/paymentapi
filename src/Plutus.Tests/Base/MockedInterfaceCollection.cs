﻿using System;
using System.Collections.Generic;
using Moq;

namespace Plutus.Tests.Base
{
    public class MockedInterfaceCollection : Dictionary<Type, object>, IDisposable
    {
        public Mock<T> Mocked<T>() 
            where T : class
        {
            var type = typeof(T);
            if(!ContainsKey(type))
            {
                base[type] = new Mock<T>();
            }
            return base[type] as Mock<T>;
        }

        public void Dispose()
        {
            foreach (var key in base.Keys)
            {
                base[key] = null;
            }
            base.Clear();
        }
    }
}