﻿using System;
using System.Collections.Generic;
using log4net;
using log4net.Core;
using log4net.Repository;

namespace Plutus.Tests.Base
{
    public class MockedLogging<T> : ILog, ILogger
    {
        public ILogger Logger => this;
        private readonly List<string> logged = new List<string>();

        public string[] Logged => logged.ToArray();

        private bool VisibleLogging { get; set; } = true;

        public MockedLogging<T> Reset(bool? visibleLogging = null)
        {
            logged.Clear();
            if (!visibleLogging.HasValue) return this;
            
            VisibleLogging = visibleLogging.Value;
            Console.WriteLine(VisibleLogging ? "LOGGING IS VISIBLE!" : "LOGGING IS INVISIBLE!");
            return this;
        }

        private void Log(string level, object message)
        {
            var txt = $"{level.ToUpper()}: {message}";
            logged.Add(txt);
            if(VisibleLogging) Console.WriteLine(txt);
        }
        public void Debug(object message) => Log("Debug", message);
        public void Info(object message) => Log("Info", message);
        public void Warn(object message) => Log("Warn", message);
        public void Error(object message) => Log("Error", message);
        public void Fatal(object message) => Log("Fatal", message);

        private static string Format(object message, Exception exception) => $"{message} {exception}";
        public void Debug(object message, Exception exception) => Debug(Format(message, exception));
        public void Info(object message, Exception exception) => Info(Format(message, exception));
        public void Warn(object message, Exception exception) => Warn(Format(message, exception));
        public void Error(object message, Exception exception) => Error(Format(message, exception));
        public void Fatal(object message, Exception exception) => Fatal(Format(message, exception));

        private static string Format(string format, params object[] args) => string.Format(format, args);
        public void DebugFormat(string format, params object[] args) => Debug(Format(format, args));
        public void InfoFormat(string format, params object[] args) => Info(Format(format, args));
        public void WarnFormat(string format, params object[] args) => Warn(Format(format, args));
        public void ErrorFormat(string format, params object[] args) => Error(Format(format, args));
        public void FatalFormat(string format, params object[] args) => Fatal(Format(format, args));

        public void DebugFormat(string format, object arg0) => Debug(Format(format, arg0));
        public void InfoFormat(string format, object arg0) => Info(Format(format, arg0));
        public void WarnFormat(string format, object arg0) => Warn(Format(format, arg0));
        public void ErrorFormat(string format, object arg0) => Error(Format(format, arg0));
        public void FatalFormat(string format, object arg0) => Fatal(Format(format, arg0));

        public void DebugFormat(string format, object arg0, object arg1) => Debug(Format(format, arg0, arg1));
        public void InfoFormat(string format, object arg0, object arg1) => Info(Format(format, arg0, arg1));
        public void WarnFormat(string format, object arg0, object arg1) => Warn(Format(format, arg0, arg1));
        public void ErrorFormat(string format, object arg0, object arg1) => Error(Format(format, arg0, arg1));
        public void FatalFormat(string format, object arg0, object arg1) => Fatal(Format(format, arg0, arg1));

        public void DebugFormat(string format, object arg0, object arg1, object arg2) => Debug(Format(format, arg0, arg1, arg2));
        public void InfoFormat(string format, object arg0, object arg1, object arg2) => Info(Format(format, arg0, arg1, arg2));
        public void WarnFormat(string format, object arg0, object arg1, object arg2) => Warn(Format(format, arg0, arg1, arg2));
        public void ErrorFormat(string format, object arg0, object arg1, object arg2) => Error(Format(format, arg0, arg1, arg2));
        public void FatalFormat(string format, object arg0, object arg1, object arg2) => Fatal(Format(format, arg0, arg1, arg2));

        private static string Format(IFormatProvider provider, string format, params object[] args) => string.Format(provider, format, args);
        public void DebugFormat(IFormatProvider provider, string format, params object[] args) => Debug(Format(provider,format, args));
        public void InfoFormat(IFormatProvider provider, string format, params object[] args) => Info(Format(provider,format, args));
        public void WarnFormat(IFormatProvider provider, string format, params object[] args) => Warn(Format(provider,format, args));
        public void ErrorFormat(IFormatProvider provider, string format, params object[] args) => Error(Format(provider,format, args));
        public void FatalFormat(IFormatProvider provider, string format, params object[] args) => Fatal(Format(provider,format, args));

        public bool IsDebugEnabled => IsEnabledFor(Level.Debug);
        public bool IsInfoEnabled => IsEnabledFor(Level.Info);
        public bool IsWarnEnabled => IsEnabledFor(Level.Warn);
        public bool IsErrorEnabled => IsEnabledFor(Level.Error);
        public bool IsFatalEnabled => IsEnabledFor(Level.Fatal);


        public void Log(Type callerStackBoundaryDeclaringType, Level level, object message, Exception exception) => Log(level.Name, Format(message, exception));
        public void Log(LoggingEvent logEvent) => Log(logEvent.Level.Name, logEvent.RenderedMessage);
        public bool IsEnabledFor(Level level) => true;
        public string Name => nameof(T);
        public ILoggerRepository Repository => null;
    }
}