﻿using System;
using System.Linq;

namespace Plutus.Tests.Base
{
    public static class Extensions
    {
        public static bool ContainsAll(this string txt, string[] values, StringComparison comparison = StringComparison.Ordinal)
        {
            return values.Any() && values.All(value => txt.IndexOf(value, comparison) >= 0);
        }
    }
}