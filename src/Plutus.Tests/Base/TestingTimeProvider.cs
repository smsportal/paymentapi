﻿using System;
using SmsPortal.Core.Time;

namespace Plutus.Tests.Base
{
    public class TestingTimeProvider : TimeProvider
    {
        private readonly DateTime start;
        private DateTime now;

        public TestingTimeProvider(DateTime start)
        {
            this.start = start;
            Reset(start);
        }
            
        public TestingTimeProvider Reset(DateTime? dateAndTime = null)
        {
            now = dateAndTime ?? start;
            return this;
        }

        public TestingTimeProvider Advance(TimeSpan time)
        {
            now = Now.Add(time);
            return this;
        }

        public override DateTime Now => now;
        public override DateTime UtcNow => Now.ToUniversalTime();
        public override DateTime Today => now.Date;
        
        public DateTime StartOfMonth => new DateTime(now.Year, now.Month, 1);
        public DateTime EndOfMonth => StartOfMonth.AddMonths(1).AddDays(-1);
        public DateTime EndOfTime => new DateTime(9999, 12, 31);
    }
}