﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using Plutus.Tests.Base;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;

namespace Plutus.Tests.PostPaidBilling
{
    [TestFixture]
    public class ShortCodeRentalInvoiceServiceTests : TestBase<IShortCodeRentalInvoiceService>
    {
        private const int MissingResellerSite = 150;
        private const int MissingResellerAccount = 50;
        private const int ValidResellerSite = 160;
        private const int ValidResellerAccount = 60;
        private const int MasterAccountForValidResellerSite = 61;
        private const int MissingMasterAccount = 10;
        private const int ValidMasterAccount = 20;
        private const int MasterAccountOneForPerRefCodeBilling = 21;
        private const int MasterAccountTwoForPerRefCodeBilling = 22;
        private const string OneRefCode = "RefCodeOne";
        private const int ValidSubAccountOfValidMasterAccount = 30;
        private const int InvalidSubAccountOfValidMasterAccount = 31;
        private const int ValidSubAccountOfMissingMaster = 40;
        private const int MasterAccountOneForManualBilling = 70;
        private const int MasterAccountTwoForManualBilling = 71;
        private const int MasterAccountOneForPerMasterBilling = 80;
        private const int MasterAccountTwoForPerMasterBilling = 81;
        private const int MasterWithUnsupportedBillingType = 90;
        
        private const int OldDedicatedOnValidMaster = 1;
        private const int RecentDedicatedOnValidMaster = 2;
        private const int OldKeywordOnValidSubOfValidMaster = 3;
        private const int RecentKeywordOnValidSubOfValidMaster = 4;
        private const int OldDedicatedOnMissingMaster = 5;
        private const int OldKeywordOnSubOfAnotherMaster = 6;
        private const int OldDedicatedOnValidSubOfValidMaster = 7;
        private const int OldSharedOnValidSite = 8;
        private const int OldSharedOnMissingSite = 9;
        private const int OldKeywordOnValidMaster = 10;
        private const int OldKeywordOnInvalidMaster = 11;
        private const int OldDedicatedOneForRefCodeBilling = 12;
        private const int OldDedicatedTwoForRefCodeBilling = 13;
        private const int OldDedicatedOneForManualBilling = 14;
        private const int OldDedicatedTwoForManualBilling = 15;
        private const int OldDedicatedOneForPerMasterBilling = 16;
        private const int OldDedicatedTwoForPerMasterBilling = 17;
        private const int OldDedicatedOnMasterAccountForValidResellerSite = 18;
        private const int OldDedicatedOnMasterWithUnsupportedBillingType = 19;

        private PaymentAccount retrievedPaymentAccount = null;

        private PaymentAccount CreateAccount(
            AccountKey key,
            int siteId,
            string refCode,
            string linkName,
            LineItemBreakdownType billingType) => new PaymentAccount()
        {
            AccountKey = key,
            AccountLinkName = linkName,
            Company = $"{key} Company",
            Fullname = $"{key} FullName",
            Username = $"{key} Username",
            ClientRefCode = refCode ?? string.Empty,
            BaseCurrency = new Currency("ZAR", 1),
            BillingGroupId = null,
            IsPostpaid = true,
            SiteId = siteId,
            PricingSCCurrency = "ZAR",
            PricingSCSetup = 3500,
            PricingSCMonthly = 999,
            PricingSCShared = 1500,
            PostPaidBillingType = LineItemBreakdownType.PerCostCentrePerCampaignByCountryAndMessageType,
            ShortCodeBillingType = billingType
        };

        public delegate void OutAction<in TIn,TOut>(TIn inVal, out TOut outVal);

        public void LookupAccount(AccountKey key, out PaymentAccount account)
        {
            account = null;
            switch (key.Type)
            {
                case AccountType.SmsAccount:
                    switch (key.Id)
                    {
                        case MasterAccountForValidResellerSite:
                            account =
                                CreateAccount(
                                    new AccountKey(key.Id, AccountType.SmsAccount),
                                    ValidResellerSite,
                                    $"ValidMasterOn{ValidResellerSite}",
                                    $"Account {key.Id}",
                                    LineItemBreakdownType.PerMasterAccountByUsername);
                            break;
                        case ValidSubAccountOfMissingMaster:
                        case MissingMasterAccount:
                            account = null;
                            break;
                        case InvalidSubAccountOfValidMasterAccount:
                        case ValidSubAccountOfValidMasterAccount:
                        case ValidMasterAccount:
                            account =
                                CreateAccount(
                                    new AccountKey(ValidMasterAccount, AccountType.SmsAccount),
                                    1,
                                    $"ValidMaster{ValidMasterAccount}",
                                    $"Account {key.Id}",
                                    LineItemBreakdownType.PerMasterAccountByUsername);
                            break;
                        case MasterWithUnsupportedBillingType:
                            account =
                                CreateAccount(
                                    new AccountKey(key.Id, AccountType.SmsAccount),
                                    1,
                                    "UnsupportedBillingType",
                                    $"Account {key.Id}",
                                    LineItemBreakdownType.PerMasterAccountByCampaignName);
                            break;
                        case MasterAccountOneForManualBilling:
                        case MasterAccountTwoForManualBilling:
                            account =
                                CreateAccount(
                                    new AccountKey(key.Id, AccountType.SmsAccount),
                                    1,
                                    $"UnknownOrManual",
                                    $"Account {key.Id}",
                                    LineItemBreakdownType.Unknown);
                            break;
                        case MasterAccountOneForPerMasterBilling:
                        case MasterAccountTwoForPerMasterBilling:
                            account =
                                CreateAccount(
                                    new AccountKey(key.Id, AccountType.SmsAccount),
                                    1,
                                    $"PerMaster",
                                    $"Account {key.Id}",
                                    LineItemBreakdownType.PerMasterAccountByUsername);
                            break;
                        case MasterAccountOneForPerRefCodeBilling:
                            account =
                                CreateAccount(
                                    new AccountKey(MasterAccountOneForPerRefCodeBilling, AccountType.SmsAccount),
                                    1,
                                    OneRefCode,
                                    $"Master {MasterAccountOneForPerRefCodeBilling}",
                                    LineItemBreakdownType.PerRefCodeByMasterAccountUsername);
                            break;
                        case MasterAccountTwoForPerRefCodeBilling:
                            account =
                                CreateAccount(
                                    new AccountKey(MasterAccountTwoForPerRefCodeBilling, AccountType.SmsAccount),
                                    1,
                                    OneRefCode,
                                    $"Master {MasterAccountTwoForPerRefCodeBilling}",
                                    LineItemBreakdownType.PerRefCodeByMasterAccountUsername);
                            break;
                    }

                    break;
                case AccountType.ResellerAccount:
                    switch (key.Id)
                    {
                        case MissingResellerAccount:
                            account = null;
                            break;
                        case ValidResellerAccount:
                            account =
                                CreateAccount(
                                    new AccountKey(ValidResellerAccount, AccountType.ResellerAccount),
                                    ValidResellerSite,
                                    $"RES{ValidResellerAccount}",
                                    "Valid Reseller",
                                    LineItemBreakdownType.Unknown);
                            break;
                    }

                    break;
                case AccountType.PastelCode:
                    account = null;
                    break;
                case AccountType.SiteId:
                    switch (key.Id)
                    {
                        case MissingResellerSite:
                            account = null;
                            break;
                        case ValidResellerSite:
                            account =
                                CreateAccount(
                                    new AccountKey(ValidResellerAccount, AccountType.ResellerAccount),
                                    ValidResellerSite,
                                    $"RES{ValidResellerAccount}",
                                    "Valid Reseller",
                                    LineItemBreakdownType.Unknown);
                            break;
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private readonly Dictionary<int, int> shortCodeNumbers = new Dictionary<int, int>();
        public ShortCodeRentalApplicability CreateRental(int id, int? keyWordOnId, int? loginId, int? siteId, int ageInDays)
        {
            if (!keyWordOnId.HasValue || !shortCodeNumbers.ContainsKey(keyWordOnId ?? 0))
            {
                shortCodeNumbers[id] = shortCodeNumbers.Count + 10001;
            }
            else
            {
                shortCodeNumbers[id] = shortCodeNumbers[keyWordOnId.Value];
            }

            return new ShortCodeRentalApplicability()
            {
                AllocationId = id,
                ShortCode = shortCodeNumbers[id],
                Keyword = keyWordOnId.HasValue ? $"Keyword{id}" : string.Empty,
                LoginId = loginId,
                SiteId = siteId,
                ConfigurationStartDate = keyWordOnId.HasValue && ageInDays > 20
                    ? Timing.EndOfMonth.AddYears(-1) //Keywords are billed annually so mock an appropriate date
                    : Timing.Today.AddDays(-ageInDays),
                ConfigurationEndDate = Timing.EndOfTime,
                CycleStartDate = Timing.StartOfMonth,
                CycleEndDate =
                    keyWordOnId.HasValue
                        ? Timing.EndOfMonth.AddYears(1) //Keywords are billed annually so mock an appropriate date 
                        : Timing.EndOfMonth
            };
        }

        public override void SetUpTestBase()
        {
            shortCodeNumbers.Clear();
            var rentalList = new[]
            {
                //Valid dedicated on Master
                CreateRental(OldDedicatedOnValidMaster, null, ValidMasterAccount, null, 365),
                //Recent dedicated on Master
                CreateRental(RecentDedicatedOnValidMaster, null, ValidMasterAccount, null, 7),
                //Keyword on Sub-Account for dedicated of the Master
                CreateRental(OldKeywordOnValidSubOfValidMaster, OldDedicatedOnValidMaster, ValidSubAccountOfValidMasterAccount, null, 365),
                //Keyword on Master for dedicated of the Master
                CreateRental(OldKeywordOnValidMaster, OldDedicatedOnValidMaster, ValidMasterAccount, null, 365),
                //Recent keyword on Sub-Account for dedicated of the Master
                CreateRental(RecentKeywordOnValidSubOfValidMaster, OldDedicatedOnValidMaster, ValidSubAccountOfValidMasterAccount, null, 9),
                //Valid dedicated on Invalid Master
                CreateRental(OldDedicatedOnMissingMaster, null, MissingMasterAccount, null, 365),
                //Keyword on Sub-Account for dedicated of another Master
                CreateRental(OldKeywordOnSubOfAnotherMaster, OldDedicatedOnMissingMaster, InvalidSubAccountOfValidMasterAccount, null, 365),
                //Keyword on Master for dedicated of another Master
                CreateRental(OldKeywordOnInvalidMaster, OldDedicatedOnMissingMaster, ValidMasterAccount, null, 365),
                //Valid dedicated on Sub-Account
                CreateRental(OldDedicatedOnValidSubOfValidMaster, null, ValidSubAccountOfValidMasterAccount, null, 365),
                //Valid Shared on Valid Site
                CreateRental(OldSharedOnValidSite, null, null, ValidResellerSite, 365),
                //Valid Shared on Invalid Site
                CreateRental(OldSharedOnMissingSite, null, null, MissingResellerSite, 365),
                //Valid dedicated two for Per RefCode (deliberate 2 1 order)
                CreateRental(OldDedicatedTwoForRefCodeBilling, null, MasterAccountTwoForPerRefCodeBilling, null, 365),
                //Valid dedicated one for Per RefCode (deliberate 2 1 order)
                CreateRental(OldDedicatedOneForRefCodeBilling, null, MasterAccountOneForPerRefCodeBilling, null, 365),
                //Valid dedicated one for Manual
                CreateRental(OldDedicatedOneForManualBilling, null, MasterAccountOneForManualBilling, null, 365),
                //Valid dedicated two for Manual
                CreateRental(OldDedicatedTwoForManualBilling, null, MasterAccountTwoForManualBilling, null, 365),
                //Valid dedicated one for Per Master
                CreateRental(OldDedicatedOneForPerMasterBilling, null, MasterAccountOneForPerMasterBilling, null, 365),
                //Valid dedicated two for Per Master
                CreateRental(OldDedicatedTwoForPerMasterBilling, null, MasterAccountTwoForPerMasterBilling, null, 365),
                //Valid dedicated two for Per Master
                CreateRental(OldDedicatedOnMasterAccountForValidResellerSite, null, MasterAccountForValidResellerSite, null, 365),
                //Valid dedicated for Master with Unsupported Billing Type
                CreateRental(OldDedicatedOnMasterWithUnsupportedBillingType, null, MasterWithUnsupportedBillingType, null, 365),
            };  
            
            Setup<IShortCodeRentalRepository, IEnumerable<ShortCodeRentalApplicability>>(r =>
                    r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()))
                .Returns(rentalList);

            Setup<ISiteRepository, Site>(r => r.Get(1))
                .Returns(new Site
                {
                    Id = 1,
                    Domain = "https://cp.smsportal.com",
                    Name = "SMSPortal",
                    ResellerId = 1
                });
            
            Setup<ISiteRepository, Site>(r => r.Get(ValidResellerSite))
                .Returns(new Site
                {
                    Id = ValidResellerSite,
                    Domain = "https://cp.smsportal.not",
                    Name = "Reseller",
                    ResellerId = ValidResellerAccount
                });

            PaymentAccount outAcc = null;
            Setup<IPaymentAccountRepository, bool>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out outAcc))
                .Callback(new OutAction<AccountKey,PaymentAccount>((AccountKey key, out PaymentAccount account) =>
                {
                    LookupAccount(key, out account);
                    outAcc = account;
                    retrievedPaymentAccount = account;
                }))
                .Returns(() => outAcc != null);

            Setup<IBillingGroupOrDetailsRepository, BillingDetails>(r => r.Get(It.IsAny<AccountKey>()))
                .Returns(new BillingDetails()
                {
                    CompanyName = "Billing Company",
                    Address1 = "Address 1",
                    Address2 = "Address 2",
                    City = "City",
                    StateProvince = "Province",
                    Country = "Country",
                    ZipCode = "0000",
                    CountryId = 156,
                    InvoiceEmail = "invoice@billingcompany.test",
                    RegistrationNumber = "REG1234",
                    VatNumber = "TAX9876"
                });

            Setup<ITaxConfigRepository, TaxConfig>(r => r.GetConfig(It.IsAny<int>()))
                .Returns((int countryId) => new TaxConfig()
                {
                    CountryId = countryId,
                    Description = $"Tax Country {countryId}",
                    TaxPercentage = countryId == 156 ? 10 : 0,
                    TaxType = countryId == 156 ? "VAT" : "",
                });

            IPostPaidPaymentAccountService postPaidPaymentAccountService =
                new PostPaidPaymentAccountService(
                    InstanceOf<IPaymentAccountRepository>(),
                    InstanceOf<ISiteRepository>());
            var foundAccountViaService = false;
            Setup<IPostPaidPaymentAccountService, bool>(s => s.TryGetPaymentAccount(It.IsAny<AccountKey>(), out outAcc))
                .Callback(new OutAction<AccountKey, PaymentAccount>((AccountKey key, out PaymentAccount account) =>
                {
                    foundAccountViaService = postPaidPaymentAccountService.TryGetPaymentAccount(key, out account);
                }))
                .Returns(() => foundAccountViaService);
        }

        public override IShortCodeRentalInvoiceService CreateInstance() =>
            new ShortCodeRentalInvoiceService(
                InstanceOf<IShortCodeRentalRepository>(),
                InstanceOf<ISiteRepository>(),
                InstanceOf<IBillingGroupOrDetailsRepository>(),
                InstanceOf<ITaxConfigRepository>(),
                InstanceOf<IPostPaidPaymentAccountService>(),
                Log.Reset(false));
        
        [Test]
        public void RecentSetupNotBilledButLogged()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();

            //Act
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i
                        .GetLineItems()
                        .Any(l =>
                            l.DataId == RecentKeywordOnValidSubOfValidMaster ||
                            l.DataId == RecentDedicatedOnValidMaster)
                    )
                    .ToArray();
            var relevantLogs = Log.Logged.Where(txt => 
                txt.ContainsAll(new [] {"[ShortCodeRentalApplicability", $"(#{RecentKeywordOnValidSubOfValidMaster}):", "ignored"}, StringComparison.OrdinalIgnoreCase) ||
                txt.ContainsAll(new [] {"[ShortCodeRentalApplicability", $"(#{RecentDedicatedOnValidMaster}):", "ignored"}, StringComparison.OrdinalIgnoreCase))
                .ToArray();

            Console.WriteLine("RELEVANT LOG ENTRIES:");
            foreach (var relevantLog in relevantLogs)
            {
                Console.WriteLine(relevantLog);
            }

            //Assert
            Assert.IsFalse(invoices.Any());
            Assert.AreEqual(2, relevantLogs.Length);
            Verify<IShortCodeRentalRepository>(r => r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()));
            Verify<IPaymentAccountRepository>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out retrievedPaymentAccount));
            Verify<IBillingGroupOrDetailsRepository>(r => r.Get(It.IsAny<AccountKey>()));
        }
        
        [Test]
        public void KeywordsForDedicatedForSameMasterNotBilledButLogged()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();

            //Act
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i
                        .GetLineItems()
                        .Any(l =>
                            l.DataId == OldKeywordOnValidSubOfValidMaster ||
                            l.DataId == OldKeywordOnValidMaster)
                    )
                    .ToArray();
            var relevantLogs = Log.Logged.Where(txt => 
                    txt.ContainsAll(new [] {"[ShortCodeRentalApplicability", $"(#{OldKeywordOnValidSubOfValidMaster}):", "ignored"}, StringComparison.OrdinalIgnoreCase) ||
                    txt.ContainsAll(new [] {"[ShortCodeRentalApplicability", $"(#{OldKeywordOnValidMaster}):", "ignored"}, StringComparison.OrdinalIgnoreCase))
                .ToArray(); 
            
            Console.WriteLine("RELEVANT LOG ENTRIES:");
            foreach (var relevantLog in relevantLogs)
            {
                Console.WriteLine(relevantLog);
            }
            
            //Assert
            Assert.IsFalse(invoices.Any());
            Assert.AreEqual(2, relevantLogs.Length);
            Verify<IShortCodeRentalRepository>(r => r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()));
            Verify<IPaymentAccountRepository>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out retrievedPaymentAccount));
            Verify<IBillingGroupOrDetailsRepository>(r => r.Get(It.IsAny<AccountKey>()));
        }
        
        [Test]
        public void KeywordsOnDedicatedNotBilledToMasterIsBilledToMaster()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();

            //Act
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i
                        .GetLineItems()
                        .Any(l =>
                            l.DataId == OldKeywordOnSubOfAnotherMaster || 
                            l.DataId == OldKeywordOnInvalidMaster)
                    )
                    .ToArray();
            var billedTo = 
                invoices
                    .Select(i => i.LoginId)
                    .Distinct()
                    .ToArray();

            //Assert
            Assert.IsTrue(invoices.Any());
            Assert.AreEqual(1, invoices.Length);
            Assert.AreNotEqual(ValidSubAccountOfValidMasterAccount, billedTo[0]);
            Assert.AreEqual(ValidMasterAccount, billedTo[0]);
            Assert.AreNotEqual(MissingMasterAccount, billedTo[0]);
            Verify<IShortCodeRentalRepository>(r => r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()));
            Verify<IPaymentAccountRepository>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out retrievedPaymentAccount));
            Verify<IBillingGroupOrDetailsRepository>(r => r.Get(It.IsAny<AccountKey>()));
        }
        
        [Test]
        public void DedicatedForSubsIsBilledToMaster()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();

            //Act
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i
                        .GetLineItems()
                        .Any(l =>
                            l.DataId == OldDedicatedOnValidSubOfValidMaster)
                    )
                    .ToArray();
            var billedTo = 
                invoices
                    .Select(i => i.LoginId)
                    .Distinct()
                    .ToArray();

            //Assert
            Assert.IsTrue(invoices.Any());
            Assert.AreEqual(1, invoices.Length);
            Assert.AreNotEqual(ValidSubAccountOfValidMasterAccount, billedTo[0]);
            Assert.AreEqual(ValidMasterAccount, billedTo[0]);
            Verify<IShortCodeRentalRepository>(r => r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()));
            Verify<IPaymentAccountRepository>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out retrievedPaymentAccount));
            Verify<IBillingGroupOrDetailsRepository>(r => r.Get(It.IsAny<AccountKey>()));
        }
        
        [Test]
        public void SharedForSiteIsBilledToReseller()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();

            //Act
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i
                        .GetLineItems()
                        .Any(l =>
                            l.DataId == OldSharedOnValidSite)
                    )
                    .ToArray();
            var billedTo = 
                invoices
                    .Select(i => i.ResellerId)
                    .Distinct()
                    .ToArray();

            //Assert
            Assert.IsTrue(invoices.Any());
            Assert.AreEqual(1, invoices.Length);
            Assert.AreEqual(ValidResellerAccount, billedTo[0]);
            Verify<IShortCodeRentalRepository>(r => r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()));
            Verify<IPaymentAccountRepository>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out retrievedPaymentAccount));
            Verify<IBillingGroupOrDetailsRepository>(r => r.Get(It.IsAny<AccountKey>()));
        }
        
        [Test]
        public void SingleInvoiceForRefCodeBilling()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();

            //Act
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i.Cycle.LineItemBreakdown == LineItemBreakdownType.PerRefCodeByMasterAccountUsername)
                    .ToArray();
            var billedTo = 
                invoices
                    .Select(i => i.LoginId)
                    .Distinct()
                    .ToArray();

            //Assert
            Assert.IsTrue(invoices.Any());
            Assert.AreEqual(1, invoices.Length);
            Assert.AreEqual(MasterAccountOneForPerRefCodeBilling, billedTo[0]);
            Verify<IShortCodeRentalRepository>(r => r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()));
            Verify<IPaymentAccountRepository>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out retrievedPaymentAccount));
            Verify<IBillingGroupOrDetailsRepository>(r => r.Get(It.IsAny<AccountKey>()));
        }
        
        [Test]
        public void MultipleInvoicesForUnknownOrManualBilling()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();

            //Act
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i.Cycle.LineItemBreakdown == LineItemBreakdownType.Unknown && i.LoginId > 0 && i.LoginId != ValidMasterAccount && i.LoginId != MasterWithUnsupportedBillingType)
                    .ToArray();
            var billedTo = 
                invoices
                    .Select(i => i.LoginId)
                    .Distinct()
                    .ToArray();

            //Assert
            Assert.IsTrue(invoices.Any());
            Assert.AreEqual(2, invoices.Length);
            Assert.AreEqual(2, billedTo.Length);
            Assert.True(billedTo.Any(id => id == MasterAccountOneForManualBilling));
            Assert.True(billedTo.Any(id => id == MasterAccountTwoForManualBilling));
            Verify<IShortCodeRentalRepository>(r => r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()));
            Verify<IPaymentAccountRepository>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out retrievedPaymentAccount));
            Verify<IBillingGroupOrDetailsRepository>(r => r.Get(It.IsAny<AccountKey>()));
        }
        
        
        [Test]
        public void MultipleInvoicesForPerMasterBilling()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();

            //Act
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i.Cycle.LineItemBreakdown == LineItemBreakdownType.PerMasterAccountByUsername && i.LoginId > 0 && i.LoginId != ValidMasterAccount)
                    .ToArray();
            var billedTo = 
                invoices
                    .Select(i => i.LoginId)
                    .Distinct()
                    .ToArray();

            //Assert
            Assert.IsTrue(invoices.Any());
            Assert.AreEqual(2, invoices.Length);
            Assert.AreEqual(2, billedTo.Length);
            Assert.True(billedTo.Any(id => id == MasterAccountOneForPerMasterBilling));
            Assert.True(billedTo.Any(id => id == MasterAccountTwoForPerMasterBilling));
            Verify<IShortCodeRentalRepository>(r => r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()));
            Verify<IPaymentAccountRepository>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out retrievedPaymentAccount));
            Verify<IBillingGroupOrDetailsRepository>(r => r.Get(It.IsAny<AccountKey>()));
        }
        
        [Test]
        public void UnidentifiedAccountsNotBilledButLogged()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();

            //Act
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i
                        .GetLineItems()
                        .Any(l =>
                            l.DataId == OldDedicatedOnMissingMaster ||
                            l.DataId == OldSharedOnMissingSite)
                    )
                    .ToArray(); 
            var relevantLogs = Log.Logged.Where(txt => 
                    txt.ContainsAll(new [] {"[ShortCodeRentalApplicability", $"(#{OldDedicatedOnMissingMaster}):", "could not", "payment account"}, StringComparison.OrdinalIgnoreCase) ||
                    txt.ContainsAll(new [] {"[ShortCodeRentalApplicability", $"(#{OldSharedOnMissingSite}):", "could not", "payment account"}, StringComparison.OrdinalIgnoreCase))
                .ToArray(); 
            
            Console.WriteLine("RELEVANT LOG ENTRIES:");
            foreach (var relevantLog in relevantLogs)
            {
                Console.WriteLine(relevantLog);
            }

            //Assert
            Assert.IsFalse(invoices.Any());
            Assert.AreEqual(2, relevantLogs.Length);
            Verify<IShortCodeRentalRepository>(r => r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()));
            Verify<IPaymentAccountRepository>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out retrievedPaymentAccount));
            Verify<IBillingGroupOrDetailsRepository>(r => r.Get(It.IsAny<AccountKey>()));
        }
        
        [Test]
        public void SmsAccountOnResellerSiteBilledToReseller()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();

            //Act
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i
                        .GetLineItems()
                        .Any(l =>
                            l.DataId == OldDedicatedOnMasterAccountForValidResellerSite)
                    )
                    .ToArray();
            var billedToLoginId = 
                invoices
                    .Select(i => i.LoginId)
                    .Distinct()
                    .First();
            var billedToResellerId = 
                invoices
                    .Select(i => i.ResellerId)
                    .Distinct()
                    .First();
            
            //Assert
            Assert.IsTrue(invoices.Any());
            Assert.AreEqual(1, invoices.Length);
            Assert.AreEqual(0, billedToLoginId);
            Assert.AreEqual(ValidResellerAccount, billedToResellerId);
            Verify<IShortCodeRentalRepository>(r => r.GetAllShortCodeRentalApplicability(It.IsAny<DateTime>()));
            Verify<IPaymentAccountRepository>(r => r.TryGetPaymentAccount(It.IsAny<AccountKey>(), out retrievedPaymentAccount));
            Verify<IBillingGroupOrDetailsRepository>(r => r.Get(It.IsAny<AccountKey>()));
        }

        [Test]
        public void SupportedBillingTypesIdentified()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();
            
            //Act
            var supportedShortCodeRental =
                svc.AcceptableLineItemBreakdownTypes(InvoiceLineItemType.ShortCodeRental,
                        out var defaultBreakdownShortCodeRental)
                    .ToArray();
            var supportedShortCodeKeywordRental =
                svc.AcceptableLineItemBreakdownTypes(InvoiceLineItemType.ShortCodeKeywordRental,
                    out var defaultBreakdownShortCodeKeywordRental)
                    .ToArray();

            //Assert
            Assert.IsTrue(supportedShortCodeRental.Any());
            Assert.IsTrue(supportedShortCodeKeywordRental.Any());
            Assert.AreEqual(supportedShortCodeRental.Length, supportedShortCodeKeywordRental.Length);
            Assert.AreEqual(3, supportedShortCodeRental.Length);
            Assert.AreEqual(defaultBreakdownShortCodeRental, defaultBreakdownShortCodeKeywordRental);
            Assert.AreEqual(LineItemBreakdownType.Unknown, defaultBreakdownShortCodeRental); //Manual Billing
            Assert.IsTrue(supportedShortCodeRental.Any(t => t == LineItemBreakdownType.Unknown)); 
            Assert.IsTrue(supportedShortCodeRental.Any(t => t == LineItemBreakdownType.PerRefCodeByMasterAccountUsername));
            Assert.IsTrue(supportedShortCodeRental.Any(t => t == LineItemBreakdownType.PerMasterAccountByUsername));
            Assert.IsTrue(supportedShortCodeKeywordRental.Any(t => t == LineItemBreakdownType.Unknown)); 
            Assert.IsTrue(supportedShortCodeKeywordRental.Any(t => t == LineItemBreakdownType.PerRefCodeByMasterAccountUsername));
            Assert.IsTrue(supportedShortCodeKeywordRental.Any(t => t == LineItemBreakdownType.PerMasterAccountByUsername));
        }
        
        [Test]
        public void UnsupportedBillingTypesForcedToValidBillingTypeButLogged()
        {
            //Arrange
            Timing.Reset(new DateTime(2022,03,10));
            var svc = CreateInstance();
            
            //Act
            var supportedShortCodeRental =
                svc.AcceptableLineItemBreakdownTypes(InvoiceLineItemType.ShortCodeRental,
                        out var defaultBreakdownShortCodeRental)
                    .ToArray();
            var invoices =
                svc
                    .GetAllProformaInvoices(null, null, Timing.Now)
                    .Where(i => i
                        .GetLineItems()
                        .Any(l =>
                            l.DataId == OldDedicatedOnMasterWithUnsupportedBillingType)
                    )
                    .ToArray(); 
            var relevantLogs = Log.Logged.Where(txt => 
                    txt.ContainsAll(new [] {"Billing type", $"{MasterWithUnsupportedBillingType}", "forced to", "not supported"}, StringComparison.OrdinalIgnoreCase))
                .ToArray(); 
            
            Console.WriteLine("RELEVANT LOG ENTRIES:");
            foreach (var relevantLog in relevantLogs)
            {
                Console.WriteLine(relevantLog);
            }

            //Assert
            Assert.IsTrue(invoices.Any());
            Assert.IsTrue(supportedShortCodeRental.Any());
            Assert.AreEqual(1, relevantLogs.Length);
            Assert.AreEqual(defaultBreakdownShortCodeRental, invoices[0].Cycle.LineItemBreakdown);
        }
    }
}