﻿using System;
using System.Linq;
using Moq;
using NUnit.Framework;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;

namespace Plutus.Tests
{
    [TestFixture]
    public class PaymentTests
    {
        private Mock<IPaymentRepository> mockPaymentRepository;
        private Mock<IPaymentAccountRepository> mockPaymentAccountRepository;
        private Mock<ISiteRepository> mockSiteRepository;
        private Mock<IBillingGroupOrDetailsRepository> mockBillingGroupOrDetailsRepository;
        private Mock<ITaxConfigRepository> mockTaxConfigRepository;
        private Mock<ICurrencyRepository> mockCurrencyRepository;
        private Mock<IInvoiceRepository> mockInvoiceRepository;
        private Mock<IRouteCountryRepository> mockRouteCountryRepository;
        private Mock<IHtmlPaymentFormBuilderFactory> mockHtmlPaymentFormBuilderFactory;
        
        [SetUp]
        public void SetUp()
        {
            mockPaymentRepository = new Mock<IPaymentRepository>();
            mockPaymentAccountRepository = new Mock<IPaymentAccountRepository>();
            mockSiteRepository = new Mock<ISiteRepository>();
            mockBillingGroupOrDetailsRepository = new Mock<IBillingGroupOrDetailsRepository>();
            mockTaxConfigRepository = new Mock<ITaxConfigRepository>();
            mockCurrencyRepository = new Mock<ICurrencyRepository>();
            mockInvoiceRepository = new Mock<IInvoiceRepository>();
            mockRouteCountryRepository = new Mock<IRouteCountryRepository>();
            mockHtmlPaymentFormBuilderFactory = new Mock<IHtmlPaymentFormBuilderFactory>();
            
            
            mockCurrencyRepository.Setup(r => r.GetCurrency(It.IsAny<string>()))
                .Returns((string code) =>
                {
                    switch (code)
                    {
                        case "AUD":
                            return new Currency(code, 1 / 0.0815945794147499m);
                        case "EUR":
                            return new Currency(code, 3);
                        case "ZAR":
                            return new Currency(code, 1);
                        default:
                            throw new NotImplementedException();
                    }
                });
        }

        [TearDown]
        public void TearDown()
        {
            mockPaymentRepository = null;
            mockPaymentAccountRepository = null;
            mockSiteRepository = null;
            mockBillingGroupOrDetailsRepository = null;
            mockTaxConfigRepository = null;
            mockCurrencyRepository = null;
            mockInvoiceRepository = null;
            mockRouteCountryRepository = null;
            mockHtmlPaymentFormBuilderFactory = null;
        }

        private Payment GeneratePayment(
            out decimal subTotal,
            out decimal tranFeeAmount,
            out decimal exTax,
            out decimal tax,
            out decimal total,
            AccountKey accKey,
            int siteId,
            int routeCountryId,
            int countryId,
            int quantity,
            decimal unitPrice,
            Currency pricingCurrency,
            Currency paymentCurrency,
            PaymentGatewayType gatewayType,
            decimal transactionFee,
            bool useInvoicing = false,
            Currency invoiceCurrency = null,
            decimal taxPercentage = 0,
            int? priceListId = null,
            int? billingGroupId = null,
            string clientRefCode = "TEST")
        {
            //Arrange
            var paymentGateway = gatewayType.ToKey();
            const string purchaseOrderNr = null;
            
            subTotal = Math.Round(quantity * unitPrice, 2, MidpointRounding.AwayFromZero);
            tranFeeAmount = subTotal * transactionFee / 100;
            exTax = subTotal + tranFeeAmount;
            tax = (taxPercentage <= 0) ? 0 : Math.Round( exTax * (taxPercentage / 100) , 2, MidpointRounding.AwayFromZero);
            total = exTax + tax;  
           
            var paymentGenerator = new PaymentGenerator(
                mockPaymentAccountRepository.Object,
                mockSiteRepository.Object,
                mockBillingGroupOrDetailsRepository.Object,
                mockTaxConfigRepository.Object,
                mockCurrencyRepository.Object,
                mockHtmlPaymentFormBuilderFactory.Object,
                mockInvoiceRepository.Object,
                mockRouteCountryRepository.Object);
            
            mockPaymentAccountRepository.Setup(r => r.GetPaymentAccount(It.IsAny<AccountKey>()))
                .Returns((AccountKey key) =>
                {
                    var result = new PaymentAccount()
                    {
                        AccountKey = key,
                        Username = $"User-{key}",
                        SiteId = siteId,
                        BaseCurrency = pricingCurrency,
                        PriceListId = priceListId,
                        BillingGroupId = billingGroupId,
                        ClientRefCode = clientRefCode
                    };
                    Console.WriteLine(result);
                    return result;
                });
            
            mockRouteCountryRepository.Setup(r => r.Get(It.IsAny<int>()))
                .Returns(new RouteCountry()
                {
                    Id = routeCountryId,
                    Country = new Country()
                    {
                        Id = countryId,
                        Name = $"Country {countryId}"
                    },
                    Routename = $"Route Country {routeCountryId}"
                });
            
            mockBillingGroupOrDetailsRepository.Setup(r => r.Get(It.IsAny<AccountKey>()))
                .Returns(new BillingDetails()
                {
                    CountryId = countryId
                });
            
            mockSiteRepository.Setup(r => r.Get(It.IsAny<int>()))
                .Returns((int id) =>
                {
                    var result = new Site()
                    {
                        Id = id,
                        Name = 
                            id == 1 
                                ? "SMSPortal" 
                                : id == 46 
                                    ? "SMSExpress" 
                                    : $"Site {id}",
                        EftSettings = new PaymentGatewaySettings(paymentCurrency, mockPaymentRepository.Object,
                            mockCurrencyRepository.Object)
                        {
                            Gateway = PaymentGatewayType.Eft.ToKey(),
                            CountryId = countryId,
                            TranFee = transactionFee,
                            MinTransaction = quantity,
                            MaxTransaction = quantity,
                            GatewayCurrency = paymentCurrency
                        },
                        PayPalSettings = new PaymentGatewaySettings(paymentCurrency, mockPaymentRepository.Object,
                            mockCurrencyRepository.Object)
                        {
                            Gateway = PaymentGatewayType.PayPal.ToKey(),
                            CountryId = countryId,
                            TranFee = transactionFee,
                            MinTransaction = quantity,
                            MaxTransaction = quantity,
                            GatewayCurrency = paymentCurrency
                        },
                        SagePaySettings = new PaymentGatewaySettings(paymentCurrency, mockPaymentRepository.Object,
                            mockCurrencyRepository.Object)
                        {
                            Gateway = PaymentGatewayType.NetCash.ToKey(),
                            CountryId = countryId,
                            TranFee = transactionFee,
                            MinTransaction = quantity,
                            MaxTransaction = quantity,
                            GatewayCurrency = paymentCurrency
                        },
                        StripeSettings = new PaymentGatewaySettings(paymentCurrency, mockPaymentRepository.Object,
                            mockCurrencyRepository.Object)
                        {
                            Gateway = PaymentGatewayType.Stripe.ToKey(),
                            CountryId = countryId,
                            TranFee = transactionFee,
                            MinTransaction = quantity,
                            MaxTransaction = quantity,
                            GatewayCurrency = paymentCurrency
                        },
                        TaxEnabled = (taxPercentage > 0),
                        UseAutomaticInvoicing = useInvoicing,
                        PastelRefCodes = id == 1 ? new[] {"CODE1", "CODE2", "CODE3", "CODE4"} : null
                    };
                    Console.WriteLine(result);
                    return result;
                });
            mockTaxConfigRepository.Setup(r => r.GetConfig(It.IsAny<int>()))
                .Returns(new TaxConfig()
                {
                    CountryId = countryId,
                    TaxPercentage = taxPercentage
                });
            
            //Act
            var payment = paymentGenerator.Generate(
                accKey,
                quantity,
                unitPrice,
                pricingCurrency.Code,
                paymentGateway,
                purchaseOrderNr,
                routeCountryId
            );
            
            Assert.IsNotNull(payment.TransactionId);
            Assert.AreEqual(paymentCurrency.Code, payment.Currency);
            Assert.AreEqual(quantity, payment.Credits);
            if (useInvoicing)
            {
                Assert.IsNotNull(payment.Invoice);
                Assert.IsNotNull(invoiceCurrency);
                var invoiceUnitPrice = pricingCurrency.Convert(invoiceCurrency, unitPrice, 4);
                var paymentUnitPrice = invoiceCurrency.Convert(paymentCurrency, invoiceUnitPrice, 4);
                var invoiceLine = payment.Invoice.GetLineItems().First();
                Assert.AreEqual(invoiceUnitPrice, invoiceLine.UnitPrice);
                Assert.AreEqual(paymentUnitPrice, payment.UnitPrice);
            }
            else if(!paymentCurrency.Code.Equals(pricingCurrency.Code))
            {
                Assert.IsNull(payment.Invoice);
                Assert.AreEqual(invoiceCurrency?.Convert(paymentCurrency, unitPrice, 4), payment.UnitPrice);
            }
            else if(!paymentCurrency.Code.Equals(pricingCurrency.Code))
            {
                Assert.IsNull(payment.Invoice);
                Assert.AreEqual(unitPrice, payment.UnitPrice);
            }
            Assert.AreEqual(Payment.PaymentStatusStarted, payment.Status);
            Assert.AreEqual(accKey, payment.AccountKey);
            
            Console.WriteLine(payment);
            
            return payment;
        }
        
        [Test]
        public void NoClientRef_SmsAccount_WithInvoicing()
        {
            //Arrange & Act & Assert
            const int quantity = 100;
            const decimal unitPrice = 0.0851m;
            const decimal transactionFee = 0;
            const decimal taxPercentage = 0;
            var zar = mockCurrencyRepository.Object.GetCurrency("ZAR");
            var payment = GeneratePayment(
                out var subTotal,
                out var tranFeeAmount,
                out var exTax,
                out var tax,
                out var total,
                accKey: new AccountKey(1, AccountType.SmsAccount),
                siteId: 1,
                routeCountryId: 4,
                countryId: 156,
                quantity: quantity,
                unitPrice: unitPrice,
                pricingCurrency: zar,
                paymentCurrency: zar,
                gatewayType: PaymentGatewayType.NetCash,
                transactionFee: transactionFee,
                useInvoicing: true,
                invoiceCurrency: zar,
                taxPercentage: taxPercentage,
                clientRefCode: string.Empty);

            var zarUnitPrice = zar.Convert(zar, unitPrice, 2);
            
            //Assert
            Assert.AreEqual(PaymentGatewayType.NetCash.ToKey(), payment.Gateway);
            Assert.AreEqual(zarUnitPrice, Math.Round(payment.UnitPrice, 2, MidpointRounding.AwayFromZero));
            Assert.AreEqual(transactionFee, payment.Fee);
            Assert.AreEqual(unitPrice * quantity, payment.Total);
            Assert.AreEqual(taxPercentage * unitPrice * quantity / 100, payment.TaxValue);
        }

        [Test]
        public void NoClientRef_Reseller_WithInvoicing()
        {
            //Arrange & Act & Assert
            const int quantity = 100;
            const decimal unitPrice = 0.0851m;
            const decimal transactionFee = 0;
            const decimal taxPercentage = 0;
            var zar = mockCurrencyRepository.Object.GetCurrency("ZAR");
            var payment = GeneratePayment(
                out var subTotal,
                out var tranFeeAmount,
                out var exTax,
                out var tax,
                out var total,
                accKey: new AccountKey(198, AccountType.ResellerAccount),
                siteId: 11,
                routeCountryId: 4,
                countryId: 156,
                quantity: quantity,
                unitPrice: unitPrice,
                pricingCurrency: zar,
                paymentCurrency: zar,
                gatewayType: PaymentGatewayType.NetCash,
                transactionFee: transactionFee,
                useInvoicing: true,
                invoiceCurrency: zar,
                taxPercentage: taxPercentage,
                clientRefCode: string.Empty);

            var zarUnitPrice = zar.Convert(zar, unitPrice, 2);
            
            //Assert
            Assert.AreEqual(PaymentGatewayType.NetCash.ToKey(), payment.Gateway);
            Assert.AreEqual(zarUnitPrice, Math.Round(payment.UnitPrice, 2, MidpointRounding.AwayFromZero));
            Assert.AreEqual(transactionFee, payment.Fee);
            Assert.AreEqual(unitPrice * quantity, payment.Total);
            Assert.AreEqual(taxPercentage * unitPrice * quantity / 100, payment.TaxValue);
        }
        
        
        [Test]
        public void NoClientRef_SmsAccount_WithoutInvoicing()
        {
            //Arrange & Act & Assert
            const int quantity = 100;
            const decimal unitPrice = 0.0851m;
            const decimal transactionFee = 0;
            const decimal taxPercentage = 0;
            var zar = mockCurrencyRepository.Object.GetCurrency("ZAR");
            var payment = GeneratePayment(
                out var subTotal,
                out var tranFeeAmount,
                out var exTax,
                out var tax,
                out var total,
                accKey: new AccountKey(11, AccountType.SmsAccount),
                siteId: 11,
                routeCountryId: 4,
                countryId: 156,
                quantity: quantity,
                unitPrice: unitPrice,
                pricingCurrency: zar,
                paymentCurrency: zar,
                gatewayType: PaymentGatewayType.NetCash,
                transactionFee: transactionFee,
                useInvoicing: false,
                invoiceCurrency: zar,
                taxPercentage: taxPercentage,
                clientRefCode: string.Empty);

            var zarUnitPrice = zar.Convert(zar, unitPrice, 2);
            
            //Assert
            Assert.AreEqual(PaymentGatewayType.NetCash.ToKey(), payment.Gateway);
            Assert.AreEqual(zarUnitPrice, Math.Round(payment.UnitPrice, 2, MidpointRounding.AwayFromZero));
            Assert.AreEqual(transactionFee, payment.Fee);
            Assert.AreEqual(unitPrice * quantity, payment.Total);
            Assert.AreEqual(taxPercentage * unitPrice * quantity / 100, payment.TaxValue);
        }

        [Test]
        public void NoClientRef_Reseller_WithoutInvoicing()
        {
            //Arrange & Act & Assert
            const int quantity = 100;
            const decimal unitPrice = 0.0851m;
            const decimal transactionFee = 0;
            const decimal taxPercentage = 0;
            var zar = mockCurrencyRepository.Object.GetCurrency("ZAR");
            var payment = GeneratePayment(
                out var subTotal,
                out var tranFeeAmount,
                out var exTax,
                out var tax,
                out var total,
                accKey: new AccountKey(198, AccountType.ResellerAccount),
                siteId: 11,
                routeCountryId: 4,
                countryId: 156,
                quantity: quantity,
                unitPrice: unitPrice,
                pricingCurrency: zar,
                paymentCurrency: zar,
                gatewayType: PaymentGatewayType.NetCash,
                transactionFee: transactionFee,
                useInvoicing: false,
                invoiceCurrency: zar,
                taxPercentage: taxPercentage,
                clientRefCode: string.Empty);

            var zarUnitPrice = zar.Convert(zar, unitPrice, 2);
            
            //Assert
            Assert.AreEqual(PaymentGatewayType.NetCash.ToKey(), payment.Gateway);
            Assert.AreEqual(zarUnitPrice, Math.Round(payment.UnitPrice, 2, MidpointRounding.AwayFromZero));
            Assert.AreEqual(transactionFee, payment.Fee);
            Assert.AreEqual(unitPrice * quantity, payment.Total);
            Assert.AreEqual(taxPercentage * unitPrice * quantity / 100, payment.TaxValue);
        }
        
        [Test]
        public void SmsPortal_WithInvoicing_NoPriceList_NoBillingGroup_ZarPriced_ZarInvoicing_ZarPayment_SagePay()
        {
            //Arrange & Act & Assert
            const int quantity = 100;
            const decimal unitPrice = 0.0851m;
            const decimal transactionFee = 0;
            const decimal taxPercentage = 0;
            var zar = mockCurrencyRepository.Object.GetCurrency("ZAR");
            var payment = GeneratePayment(
                out var subTotal,
                out var tranFeeAmount,
                out var exTax,
                out var tax,
                out var total,
                accKey: new AccountKey(1, AccountType.SmsAccount),
                siteId: 1,
                routeCountryId: 4,
                countryId: 156,
                quantity: quantity,
                unitPrice: unitPrice,
                pricingCurrency: zar,
                paymentCurrency: zar,
                gatewayType: PaymentGatewayType.NetCash,
                transactionFee: transactionFee,
                useInvoicing: true,
                invoiceCurrency: zar,
                taxPercentage: taxPercentage);

            var zarUnitPrice = zar.Convert(zar, unitPrice, 2);
            
            //Assert
            Assert.AreEqual(PaymentGatewayType.NetCash.ToKey(), payment.Gateway);
            Assert.AreEqual(zarUnitPrice, Math.Round(payment.UnitPrice, 2, MidpointRounding.AwayFromZero));
            Assert.AreEqual(transactionFee, payment.Fee);
            Assert.AreEqual(unitPrice * quantity, payment.Total);
            Assert.AreEqual(taxPercentage * unitPrice * quantity / 100, payment.TaxValue);
        }
        
        [Test]
        public void SmsPortal_WithInvoicing_NoPriceList_NoBillingGroup_ZarPriced_EurInvoicing_ZarPayment_SagePay()
        {
            //Arrange & Act & Assert
            const int quantity = 100;
            const decimal unitPrice = 0.0851m;
            const decimal transactionFee = 0;
            const decimal taxPercentage = 0;
            var zar = mockCurrencyRepository.Object.GetCurrency("ZAR");
            var eur = mockCurrencyRepository.Object.GetCurrency("EUR");
            var payment = GeneratePayment(
                out var subTotal,
                out var tranFeeAmount,
                out var exTax,
                out var tax,
                out var total,
                accKey: new AccountKey(1, AccountType.SmsAccount),
                siteId: 1,
                routeCountryId: 4,
                countryId: 10,
                quantity: quantity,
                unitPrice: unitPrice,
                pricingCurrency: zar,
                paymentCurrency: zar,
                gatewayType: PaymentGatewayType.NetCash,
                transactionFee: transactionFee,
                useInvoicing: true,
                invoiceCurrency: eur,
                taxPercentage: taxPercentage);

            var eurUnitPrice = eur.Convert(zar, unitPrice, 4);
            var zarUnitPrice = zar.Convert(eur, eurUnitPrice, 2);
            
            //Assert
            Assert.AreEqual(PaymentGatewayType.NetCash.ToKey(), payment.Gateway);
            Assert.AreEqual(zarUnitPrice, Math.Round(payment.UnitPrice, 2, MidpointRounding.AwayFromZero));
            Assert.AreEqual(transactionFee, payment.Fee);
            Assert.AreEqual(unitPrice * quantity, payment.Total);
            Assert.AreEqual(taxPercentage * unitPrice * quantity / 100, payment.TaxValue);
        }
        
        [Test]
        public void SmsPortal_NoPriceList_NoBillingGroup_ZarPriced_NoInvoicing_ZarPayment_SagePay()
        {
            //Arrange & Act & Assert
            const int quantity = 100;
            const decimal unitPrice = 0.085m;
            const decimal transactionFee = 0;
            const decimal taxPercentage = 0;
            var zar = mockCurrencyRepository.Object.GetCurrency("ZAR");
            var payment = GeneratePayment(
                out var subTotal,
                out var tranFeeAmount,
                out var exTax,
                out var tax,
                out var total,
                accKey: new AccountKey(1, AccountType.SmsAccount),
                siteId: 1,
                routeCountryId: 4,
                countryId: 10,
                quantity: quantity,
                unitPrice: unitPrice,
                pricingCurrency: zar,
                paymentCurrency: zar,
                gatewayType: PaymentGatewayType.NetCash,
                transactionFee: transactionFee,
                useInvoicing: false,
                taxPercentage: taxPercentage);

            //Assert
            Assert.AreEqual(PaymentGatewayType.NetCash.ToKey(), payment.Gateway);
            Assert.AreEqual(unitPrice, payment.UnitPrice);
            Assert.AreEqual(transactionFee, payment.Fee);
            Assert.AreEqual(unitPrice * quantity, payment.Total);
            Assert.AreEqual(taxPercentage * unitPrice * quantity / 100, payment.TaxValue);
        }
        
        [Test]
        public void NotSmsPortal_NoPriceList_NoBillingGroup_ZarPriced_NoInvoicing_ZarPayment_SagePay()
        {
            //Arrange & Act & Assert
            const int quantity = 100;
            const decimal unitPrice = 0.085m;
            const decimal transactionFee = 0;
            const decimal taxPercentage = 0;
            var zar = mockCurrencyRepository.Object.GetCurrency("ZAR");
            var payment = GeneratePayment(
                out var subTotal,
                out var tranFeeAmount,
                out var exTax,
                out var tax,
                out var total,
                accKey: new AccountKey(1, AccountType.SmsAccount),
                siteId: 46,
                routeCountryId: 4,
                countryId: 10,
                quantity: quantity,
                unitPrice: unitPrice,
                pricingCurrency: zar,
                paymentCurrency: zar,
                gatewayType: PaymentGatewayType.NetCash,
                transactionFee: transactionFee,
                useInvoicing: false,
                taxPercentage: taxPercentage);

            //Assert
            Assert.AreEqual(PaymentGatewayType.NetCash.ToKey(), payment.Gateway);
            Assert.AreEqual(unitPrice, payment.UnitPrice);
            Assert.AreEqual(transactionFee, payment.Fee);
            Assert.AreEqual(unitPrice * quantity, payment.Total);
            Assert.AreEqual(taxPercentage * unitPrice * quantity / 100, payment.TaxValue);
        }
        
        [Test]
        public void NotSmsPortal_NoPriceList_NoBillingGroup_AudPriced_NoInvoicing_AudPayment_PayPal()
        {
            //Arrange & Act & Assert
            const int quantity = 100;
            const decimal unitPrice = 0.085m;
            const decimal transactionFee = 0;
            const decimal taxPercentage = 0;
            var aud = mockCurrencyRepository.Object.GetCurrency("AUD");
            var payment = GeneratePayment(
                out var subTotal,
                out var tranFeeAmount,
                out var exTax,
                out var tax,
                out var total,
                accKey: new AccountKey(1, AccountType.SmsAccount),
                siteId: 46,
                routeCountryId: 4,
                countryId: 10,
                quantity: quantity,
                unitPrice: unitPrice,
                pricingCurrency: aud,
                paymentCurrency: aud,
                gatewayType: PaymentGatewayType.PayPal,
                transactionFee: transactionFee,
                useInvoicing: false,
                taxPercentage: taxPercentage);

            //Assert
            Assert.AreEqual(PaymentGatewayType.PayPal.ToKey(), payment.Gateway);
            Assert.AreEqual(unitPrice, payment.UnitPrice);
            Assert.AreEqual(transactionFee, payment.Fee);
            Assert.AreEqual(unitPrice * quantity, payment.Total);
            Assert.AreEqual(taxPercentage * unitPrice * quantity / 100, payment.TaxValue);
        }
        
        [Test]
        public void NotSmsPortal_NoPriceList_NoBillingGroup_AudPriced_NoInvoicing_AudPayment_Stripe()
        {
            //Arrange & Act & Assert
            const int quantity = 100;
            const decimal unitPrice = 0.085m;
            const decimal transactionFee = 0;
            const decimal taxPercentage = 0;
            var aud = mockCurrencyRepository.Object.GetCurrency("AUD");
            var payment = GeneratePayment(
                out var subTotal,
                out var tranFeeAmount,
                out var exTax,
                out var tax,
                out var total,
                accKey: new AccountKey(1, AccountType.SmsAccount),
                siteId: 46,
                routeCountryId: 4,
                countryId: 10,
                quantity: quantity,
                unitPrice: unitPrice,
                pricingCurrency: aud,
                paymentCurrency: aud,
                gatewayType: PaymentGatewayType.Stripe,
                transactionFee: transactionFee,
                useInvoicing: false,
                taxPercentage: taxPercentage);

            //Assert
            Assert.AreEqual(PaymentGatewayType.Stripe.ToKey(), payment.Gateway);
            Assert.AreEqual(unitPrice, payment.UnitPrice);
            Assert.AreEqual(transactionFee, payment.Fee);
            Assert.AreEqual(unitPrice * quantity, payment.Total);
            Assert.AreEqual(taxPercentage * unitPrice * quantity / 100, payment.TaxValue);
        }
    }
}