﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using log4net;
using NUnit.Framework;
using Plutus.Tests.Base;
using SmsPortal.Plutus;
using SmsPortal.Plutus.Controllers.Payments;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;

namespace Plutus.Tests.PaymentNotifications
{
    [TestFixture]
    public class GenericPaymentNotificationTests : PaymentNotificationTestBase
    {
        #region MOCK: private class PostBack & private class MockPostbackController : PaymentProcessingController 
        private class PostBack
        {
            public bool Accepted { get; set; }
            public string PaymentId { get; set; }
            public string Currency { get; set; }
            public decimal Amount { get; set; }

            public override string ToString()
            {
                return $"[PostBack {PaymentId}: Accepted={Accepted} Amount={Currency}{Amount}]";
            }
        }
        
        private class MockPostbackController : PaymentProcessingController
        {
            public MockPostbackController(
                IPaymentRepository paymentRepo,
                IPaymentAccountRepository accountRepo,
                ICreditAllocationService creditAllocator,
                ILog log = null) 
                : base(typeof(MockPostbackController), new TransactionRegistry(), paymentRepo, accountRepo, creditAllocator, log)
            {
            }

            protected override bool IsAccepted<T>(T postBack, string transactionId)
            {
                switch (postBack)
                {
                    case PostBack post:
                        return post.Accepted;
                    
                    default:
                        throw new ArgumentException($"Could not identify the acceptance data on the {typeof(T)} object");
                }
            }

            protected override string CopyPostbackTracingDataToPayment<T>(ref Payment payment, T postBack, string transactionId)
            {
                switch (postBack)
                {
                    case PostBack post:
                        payment.GatewayTransactionId = post.PaymentId;
                        return post.PaymentId;
                    
                    default:
                        throw new ArgumentException($"Could not identify the tracing data on the {typeof(T)} object");
                }
            }

            protected override decimal PostbackAmount<T>(T postBack)
            {
                switch (postBack)
                {
                    case PostBack post:
                        return post.Amount;
                    
                    default:
                        throw new ArgumentException($"Could not identify the amount on the {typeof(T)} object");
                }
            }

            protected override string PostbackDebugString<T>(T postBack)
            {
                return postBack.ToString();
            }

            public IHttpActionResult Notify(string transactionId, PostBack postback)
            {
                string postbackDebug = null;
                string paymentDebug = null;
                try
                {
                    var processed = ProcessPayment(postback, transactionId, out paymentDebug, out postbackDebug);
                    return Ok(processed);
                }
                catch (Exception e)
                {
                    Log.Error($"{GatewayName} payment exception.\r\nPayment Info: {paymentDebug}\r\nData: {postbackDebug}\r\n",
                        e);
                    return InternalServerError(e);
                }
            }
        }
        #endregion

        [SetUp]
        public override void SetUp() => base.SetUp();

        [TearDown]
        public override void TearDown() => base.TearDown();
            
        private MockPostbackController CreateMockController()
        {
            var controller = new MockPostbackController(
                MockPaymentRepository.Object,
                MockPaymentAccountRepository.Object,
                CreateCreditAllocationService(),
                MockLog.Object);
            controller.Request = GetHttpRequest(HttpMethod.Post, "http://localhost/api/mock", "MockPostback");
            return controller;
        }

        [Test]
        public void ProcessSuccessAllocateCredits()
        {
            //Arrange
            const string transactionId = "353C0299-2112-4FA2-8C3D-2677585EE3A1";
            var controller = CreateMockController();
            var invoice = CreateLinkedInvoiceAndPayment(transactionId, controller.GatewayName, Payment.PaymentStatusStarted, out var payment);
            
            var postback = new PostBack
            {
                Accepted = true,
                PaymentId = "PaymentSuccess",
                Currency = invoice.Currency,
                Amount = invoice.Total
            };

            var acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var before = acc.GetCreditBalance(acc.RouteCountryId);
            
            //Act
            var notifyOne = controller.Notify(transactionId, postback);
            var resultOne = notifyOne.ExecuteAsync(CancellationToken.None).Result;
            var processedOne = resultOne.Content.ReadAsAsync<bool>().Result;

            acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var after = acc.GetCreditBalance(acc.RouteCountryId);

            //Assert
            Assert.IsInstanceOf<IHttpActionResult>(notifyOne);
            Assert.IsInstanceOf<HttpResponseMessage>(resultOne);
            Assert.IsInstanceOf<bool>(processedOne);
            Assert.AreEqual(resultOne.StatusCode, HttpStatusCode.OK, "Incorrect Status");

            Assert.IsTrue(processedOne, "Payment not processed");
            Assert.Greater(after, before, "Credits balance");
            Assert.AreEqual(payment.Credits, after - before, "Incorrect amount");
        }
        
        [Test]
        public void ProcessFailureDoesNotAllocateCredits()
        {
            //Arrange
            const string transactionId = "353C0299-2112-4FA2-8C3D-2677585EE3A1";
            var controller = CreateMockController();
            var invoice = CreateLinkedInvoiceAndPayment(transactionId, controller.GatewayName, Payment.PaymentStatusStarted, out var payment);
            
            var postback = new PostBack
            {
                Accepted = false,
                PaymentId = "PaymentFail",
                Currency = invoice.Currency,
                Amount = invoice.Total
            };

            var acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var before = acc.GetCreditBalance(acc.RouteCountryId);
            
            //Act
            var notifyOne = controller.Notify(transactionId, postback);
            var resultOne = notifyOne.ExecuteAsync(CancellationToken.None).Result;
            var processedOne = resultOne.Content.ReadAsAsync<bool>().Result;

            acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var after = acc.GetCreditBalance(acc.RouteCountryId);

            //Assert
            Assert.IsInstanceOf<IHttpActionResult>(notifyOne);
            Assert.IsInstanceOf<HttpResponseMessage>(resultOne);
            Assert.IsInstanceOf<bool>(processedOne);
            Assert.AreEqual(resultOne.StatusCode, HttpStatusCode.OK, "Incorrect Status");
            
            Assert.IsTrue(processedOne, "Payment not processed");
            Assert.AreEqual(after, before, "Credits balance");
        }
        
        [Test]
        public void DuplicatesAreNotProcessed()
        {
            //Arrange
            const string transactionId = "353CE299-2112-4FA2-8C3D-2677585EE3A1";
            var controller = CreateMockController();
            var invoice = CreateLinkedInvoiceAndPayment(transactionId, controller.GatewayName, Payment.PaymentStatusStarted, out var payment);
            
            var postback = new PostBack
            {
                Accepted = false,
                PaymentId = "PaymentFail",
                Currency = invoice.Currency,
                Amount = invoice.Total
            };
            
            //Act
            var notifyOne = controller.Notify(transactionId, postback);
            var notifyTwo = controller.Notify(transactionId, postback);
            var resultOne = notifyOne.ExecuteAsync(CancellationToken.None).Result;
            var resultTwo = notifyTwo.ExecuteAsync(CancellationToken.None).Result;
            var processedOne = resultOne.Content.ReadAsAsync<bool>().Result;
            var processedTwo = resultTwo.Content.ReadAsAsync<bool>().Result;

            //Assert
            Assert.IsInstanceOf<IHttpActionResult>(notifyOne);
            Assert.IsInstanceOf<IHttpActionResult>(notifyTwo);
            Assert.IsInstanceOf<HttpResponseMessage>(resultOne);
            Assert.IsInstanceOf<HttpResponseMessage>(resultTwo);
            Assert.IsInstanceOf<bool>(processedOne);
            Assert.IsInstanceOf<bool>(processedTwo);
            Assert.AreEqual(resultOne.StatusCode, HttpStatusCode.OK, "Incorrect Status");
            Assert.AreEqual(resultTwo.StatusCode, HttpStatusCode.OK, "Incorrect Status");
            
            Assert.IsTrue(processedOne);
            Assert.IsFalse(processedTwo);
        }
        
        [Test]
        public void CanReprocessSuccessAfterPreviouslyFailedPayments()
        {
            //Arrange
            const string transactionId = "343C0299-2112-4FA2-8C3D-2677585EE3A1";
            var controller = CreateMockController();
            var invoice = CreateLinkedInvoiceAndPayment(transactionId, controller.GatewayName, Payment.PaymentStatusStarted, out var payment);
            
            var postback = new PostBack()
            {
                Accepted = true,
                PaymentId = "Payment1",
                Currency = invoice.Currency,
                Amount = invoice.Total
            };

            var acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var before = acc.GetCreditBalance(acc.RouteCountryId);
            
            //Act
            var notifyOne = controller.Notify(transactionId, postback);
            var resultOne = notifyOne.ExecuteAsync(CancellationToken.None).Result;
            var processedOne = resultOne.Content.ReadAsAsync<bool>().Result;

            acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var after = acc.GetCreditBalance(acc.RouteCountryId);

            //Assert
            Assert.IsInstanceOf<IHttpActionResult>(notifyOne);
            Assert.IsInstanceOf<HttpResponseMessage>(resultOne);
            Assert.IsInstanceOf<bool>(processedOne);
            Assert.AreEqual(resultOne.StatusCode, HttpStatusCode.OK, "Incorrect Status");

            Assert.IsTrue(processedOne, "Payment not processed");
            Assert.Greater(after, before, "Credits balance");
            Assert.AreEqual(payment.Credits, after - before, "Incorrect amount");
        }
    }
}