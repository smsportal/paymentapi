﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using log4net;
using Moq;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;

namespace Plutus.Tests.PaymentNotifications
{
    public abstract class PaymentNotificationTestBase
    {
        protected Mock<IPaymentRepository> MockPaymentRepository;
        protected Mock<IPaymentAccountRepository> MockPaymentAccountRepository;
        protected Mock<ILog> MockLog;
        
        private Mock<ISiteRepository> mockSiteRepository;
        private Mock<ICreditAllocationRepository> mockCreditAllocationRepository;
        private Mock<IEmailService> mockEmailService;
        private Mock<IInvoiceRepository> mockInvoiceRepo;
        private Mock<ICurrencyRepository> mockCurrencyRepository;
        private List<Guid> paymentGuids;
        private List<Guid> allocatedGuids;
        private Dictionary<int, int> accountCreditBalance;

        public virtual void SetUp()
        {
                        paymentGuids = new List<Guid>();
            allocatedGuids = new List<Guid>();
            accountCreditBalance = new Dictionary<int, int>();
            
            MockPaymentRepository = new Mock<IPaymentRepository>();
            MockPaymentAccountRepository = new Mock<IPaymentAccountRepository>();
            mockCreditAllocationRepository = new Mock<ICreditAllocationRepository>();
            mockEmailService = new Mock<IEmailService>();
            mockSiteRepository = new Mock<ISiteRepository>();
            mockInvoiceRepo = new Mock<IInvoiceRepository>();
            mockCurrencyRepository = new Mock<ICurrencyRepository>();
            MockLog = new Mock<ILog>();
            
            mockCreditAllocationRepository.Setup(r => r.SiteCreditAllocationExists(It.IsAny<int>(),It.IsAny<string>()))
                .Returns((int accountId, string transactionId) => allocatedGuids.Contains(Guid.Parse(transactionId)));

            mockCreditAllocationRepository.Setup(r => r.SiteCreditAllocation(It.IsAny<PaymentAccount>(), It.IsAny<TransactionInfo>(), It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<string>()))
                .Callback((PaymentAccount account, TransactionInfo transaction, string paymentMethod, decimal transactionFee, string payPalTransactionId) =>
                {
                    var transactionId= Guid.Parse(transaction.TransactionId);
                    if (!accountCreditBalance.ContainsKey(account.Id))
                    {
                        accountCreditBalance[account.Id] = 0;
                    }
                    if(!allocatedGuids.Contains(transactionId))
                    {
                        allocatedGuids.Add(transactionId);
                    }
                    else
                    {
                        throw new ArgumentException($"Duplicate key violation: {transactionId}");
                    }

                    accountCreditBalance[account.Id] += transaction.Credits;
                });
            
            MockPaymentAccountRepository.Setup(r => r.GetPaymentAccount(It.IsAny<AccountKey>()))
                .Returns((AccountKey key) =>
                {
                    var acc = new PaymentAccount
                    {
                        AccountKey = key,
                        SiteId = key.Id % 10,
                        BaseCurrency = Currency.BaseCurrency,
                        CreditCardAuthRequired = false,
                        FixedPricingCurrency = Currency.BaseCurrency.Code,
                        PricingSCCurrency = Currency.BaseCurrency.Code,
                        RouteCountryId = 1
                    };

                    acc.Username = $"User{acc.Id:X2}";
                    acc.ClientRefCode = $"C{acc.SiteId:X2}{acc.Id:X2}";
                    acc.Fullname = $"{acc.Username} on site {acc.SiteId}";
                    acc.EmailTo = $"{acc.Username}@site{acc.SiteId}.com";
                    
                    if (!accountCreditBalance.ContainsKey(acc.Id))
                    {
                        accountCreditBalance[acc.Id] = 0;
                    }
                    acc.AddCreditPool(new CreditPool()
                    {
                        Balance = accountCreditBalance[acc.Id],
                        Country = $"Route Country {acc}",
                        RouteCountryId = acc.RouteCountryId
                    });

                    return acc;
                });
            
            mockSiteRepository.Setup(r => r.Get(It.IsAny<int>()))
                .Returns((int id) => new Site()
                {
                    Id = id,
                    UseAutomaticInvoicing = (id == 1),
                    ContactUsEmail = $"info@site{id}.com",
                    EmailAddress = $"accounts@site{id}.com",
                    ResellerId = id + 1000,
                    Name = id == 1 ? "SMS Portal" : $"Site {id}",
                    Domain = id == 1 ? "https://smsportal.com" : $"https://site{id}.com",
                    InvoicePrefix = "INV",
                    AutoCreditSiteUsers = true,
                    EftSettings = new PaymentGatewaySettings(Currency.BaseCurrency, MockPaymentRepository.Object, mockCurrencyRepository.Object),
                    PayPalSettings = new PaymentGatewaySettings(Currency.BaseCurrency, MockPaymentRepository.Object, mockCurrencyRepository.Object),
                    SagePaySettings = new PaymentGatewaySettings(Currency.BaseCurrency, MockPaymentRepository.Object, mockCurrencyRepository.Object),
                    StripeSettings = new PaymentGatewaySettings(Currency.BaseCurrency, MockPaymentRepository.Object, mockCurrencyRepository.Object),
                    TaxEnabled = true
                });
            
            mockEmailService.Setup(r => r.SendPaymentNotification(It.IsAny<Payment>(),It.IsAny<PaymentAccount>(),It.IsAny<string>(),It.IsAny<int>()))
                .Callback((Payment payment, PaymentAccount account, string email, int siteId) =>
                {
                    var site = mockSiteRepository.Object.Get(siteId);
                    MockEmail(site.EmailAddress, email, "Payment Notification", $"{account}\r\n{payment}");
                });
            
            mockEmailService.Setup(r => r.SendCreditAllocationEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int?>()))
                .Callback((
                    string contactName, 
                    string balance, 
                    string creditsLoaded, 
                    string username,
                    string destinationEmail,
                    string country, 
                    int siteId, 
                    int? loginId) =>
                {
                    var site = mockSiteRepository.Object.Get(siteId);
                    MockEmail(site.EmailAddress, destinationEmail, "Credit Allocation", $"Hi {contactName}\r\n{creditsLoaded} credits were loaded to your account.\r\nYour balance is {balance}");
                });

            MockLog.Setup(l => l.Info(It.IsAny<object>()))
                .Callback((object toLog) => Console.WriteLine($"INFO: {toLog}"));
            
            MockLog.Setup(l => l.Warn(It.IsAny<object>()))
                .Callback((object toLog) => Console.WriteLine($"WARN: {toLog}"));

            MockLog.Setup(l => l.Error(It.IsAny<object>()))
                .Callback((object toLog) => Console.WriteLine($"ERROR: {toLog}"));
            
            MockLog.Setup(l => l.Error(It.IsAny<object>(), It.IsAny<Exception>()))
                .Callback((object toLog, Exception ex) => Console.WriteLine($"ERROR: {toLog} {ex}"));
        }

        public virtual void TearDown()
        {
            MockPaymentRepository = null;
            MockPaymentAccountRepository = null;
            mockCreditAllocationRepository = null;
            mockEmailService = null;
            mockSiteRepository = null;
            mockInvoiceRepo = null;
            paymentGuids = null;
            allocatedGuids = null;
            accountCreditBalance = null;
            MockLog = null;
        }
        
        protected void MockEmail(string from, string to, string subj, string body)
        {
            const string line = "----------------------------------------";
            Console.WriteLine($"\r\n{line}\r\nFROM: {@from}\r\nTO: {to}\r\nSUBJECT: {subj}\r\n\r\n{body}\r\n{line}\r\n");
        }

        protected HttpRequestMessage GetHttpRequest(HttpMethod method, string url, string controllerName)
        {
            var request = new HttpRequestMessage(method, new Uri(url));
            var config = new HttpConfiguration();
            var defaultApiRoute = config.Routes.MapHttpRoute("DefaultApi", "api/{Controller}/{id}");
            var routeValues = new HttpRouteValueDictionary(new { controller = controllerName });
            var routeData = new HttpRouteData(defaultApiRoute, routeValues);
            request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
            request.Properties[HttpPropertyKeys.HttpRouteDataKey] = routeData;
            return request;
        }

        protected CreditAllocationService CreateCreditAllocationService()
        {
            return new CreditAllocationService(
                mockCreditAllocationRepository.Object,
                mockEmailService.Object,
                mockSiteRepository.Object,
                mockInvoiceRepo.Object);
        }

        private DateTime? ConditionalNow(bool condition) =>
            condition ? DateTime.Now : (DateTime?)null;

        private Invoice CreateInvoice(Guid transactionId, int credits = 100, decimal unitPrice = 1)
        {
            var id = 0;
            if (paymentGuids.Contains(transactionId))
            {
                for (id = 0; id < paymentGuids.Count; id++)
                {
                    if (paymentGuids[id] == transactionId) break;
                }
            }

            var guidBytes = transactionId.ToByteArray();
            var loginId = guidBytes[4];

            var invoice = new Invoice()
            {
                Id = id,
                Created = DateTime.Now.AddMinutes(-1),
                PastelCode = $"C{loginId:x4}",
                Currency = Currency.BaseCurrency.Code,
                InvoiceNr = $"INV_{guidBytes[15]}{guidBytes[14]}{guidBytes[13]}",
                CustomerRef = $"{guidBytes[3]:x2}{guidBytes[2]:x2}{guidBytes[1]:x2}{guidBytes[0]:x2}{guidBytes[4]}",
                TaxPercentage = 10,
                LoginId = loginId,
                Status = "Unpaid",
                TransactionId = transactionId
            };

            if (credits > 0)
            {
                invoice.AddLineItem(new InvoiceLineItem()
                {
                    Code = InvoiceLineItemType.BulkSms,
                    Description = InvoiceLineItemType.BulkSms.ToDescription(),
                    Quantity = credits,
                    UnitPrice = unitPrice,
                    OptionalNote = $"Testing transaction {transactionId}"
                });
            }

            return invoice;
        }

        private Payment CreatePayment(Guid transactionId, string status, string gateway, Invoice invoice = null)
        {
            var payment = new Payment()
            {
                TransactionId = transactionId,
                AccountKey = invoice?.AccountKey ?? new AccountKey(1, AccountType.SmsAccount),
                Invoice = invoice,
                Currency = invoice?.Currency ?? Currency.BaseCurrency.Code,
                UnitPrice = invoice?.GetLineItems().FirstOrDefault()?.UnitPrice ?? 1m,
                TaxValue = invoice?.TaxAmount ?? 10m,
                Fee = invoice?.GetLineItems().FirstOrDefault(l => l.Code == InvoiceLineItemType.TransactionFee)?.LineTotal ?? 0m, 
                Credits = invoice?.GetLineItems().FirstOrDefault(l => l.Code == InvoiceLineItemType.BulkSms)?.Quantity ?? 100, 
                Total = invoice?.Total ?? 110m,
                RouteCountryId = 1,
                Created = DateTime.Now.AddMinutes(-1),
                Completed = ConditionalNow(Payment.PaymentStatusCompleted.Equals(status, StringComparison.OrdinalIgnoreCase)),
                Status = status,
                Gateway = gateway
            };

            if (invoice == null) return payment;
            
            switch (status)
            {
                case Payment.PaymentStatusStarted:
                    invoice.StartPayment(payment);
                    break;
                case Payment.PaymentStatusCompleted:
                    invoice.Close(transactionId);
                    break;
                case Payment.PaymentStatusFaulted:
                    invoice.Status = "Cancelled";
                    break;
            }
            return payment;
        }

        protected Invoice CreateLinkedInvoiceAndPayment(string transactionId, string gatewayName, string paymentStatus, out Payment payment)
        {
            var guid = Guid.Parse(transactionId);
            var createdInvoice = CreateInvoice(guid);
            var createdPayment = CreatePayment(guid, paymentStatus, gatewayName, createdInvoice);

            MockPaymentRepository.Setup(r => r.Get(It.IsAny<Guid>()))
                .Returns(() =>
                {
                    if (createdInvoice.Status == "Paid")
                    {
                        createdPayment.Status = Payment.PaymentStatusCompleted;
                    }
                    else if (!createdInvoice.Status.Contains("Unpaid"))
                    {
                        createdPayment.Status = Payment.PaymentStatusFaulted;
                    }
                    return createdPayment;
                });
            
            mockInvoiceRepo.Setup(r => r.Get(It.IsAny<string>()))
                .Returns(createdInvoice);

            payment = createdPayment;
            return createdInvoice;
        }
    }
}