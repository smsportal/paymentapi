﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using NUnit.Framework;
using Plutus.Tests.Base;
using SmsPortal.Plutus;
using SmsPortal.Plutus.Controllers;
using SmsPortal.Plutus.Models;

namespace Plutus.Tests.PaymentNotifications
{
    [TestFixture]
    public class SagePayPaymentNotificationTests : PaymentNotificationTestBase
    {

        [SetUp]
        public override void SetUp() => base.SetUp();

        [TearDown]
        public override void TearDown() => base.TearDown();
        
        private SagePayPostbackController CreateMockController()
        {
            var controller =
                new SagePayPostbackController(
                    MockPaymentAccountRepository.Object,
                    CreateCreditAllocationService(),
                    MockPaymentRepository.Object,
                    new TransactionRegistry(),
                    MockLog.Object);
            controller.Request = GetHttpRequest(HttpMethod.Post, "http://localhost/api/sagepay", "SagePayPostback");
            return controller;
        }

        [Test]
        public void ProcessSuccessAllocateCredits()
        {
            //Arrange
            const string transactionId = "A53C0291-2112-4FA2-8C3D-2677585EE3A1";
            var controller = CreateMockController();
            var invoice = CreateLinkedInvoiceAndPayment(transactionId, controller.GatewayName, Payment.PaymentStatusStarted, out var payment);
            
            var postback = new SagePayPostback()
            {
                TransactionAccepted = true,
                CardHolderIpAddr = "192.168.0.1",
                RequestTrace = "12345.123456789",
                Reference = invoice.InvoiceNr,
                Extra1 = invoice.InvoiceNr,
                Extra2 = "C|0|CC006",
                Extra3 = transactionId,
                Amount = invoice.Total
            };

            var acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var before = acc.GetCreditBalance(acc.RouteCountryId);
            
            //Act
            var notifyOne = controller.Notify(postback);
            var resultOne = notifyOne.ExecuteAsync(CancellationToken.None).Result;

            acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var after = acc.GetCreditBalance(acc.RouteCountryId);

            //Assert
            Assert.IsInstanceOf<IHttpActionResult>(notifyOne);
            Assert.IsInstanceOf<HttpResponseMessage>(resultOne);
            Assert.AreEqual(resultOne.StatusCode, HttpStatusCode.OK, "Incorrect Status");

            Assert.Greater(after, before, "Credits balance");
            Assert.AreEqual(payment.Credits, after - before, "Incorrect amount");
        }
        
        [Test]
        public void ProcessFailureDoesNotAllocateCredits()
        {
            //Arrange
            const string transactionId = "F53C0291-2112-4FA2-8C3D-2677585EE3A1";
            var controller = CreateMockController();
            var invoice = CreateLinkedInvoiceAndPayment(transactionId, controller.GatewayName, Payment.PaymentStatusStarted, out var payment);
            
            var postback = new SagePayPostback()
            {
                TransactionAccepted = false,
                CardHolderIpAddr = "192.168.0.1",
                RequestTrace = "12345.123456789",
                Reference = invoice.InvoiceNr,
                Extra1 = invoice.InvoiceNr,
                Extra2 = "C|0|CC006",
                Extra3 = transactionId,
                Amount = invoice.Total
            };
            
            var acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var before = acc.GetCreditBalance(acc.RouteCountryId);
            
            //Act
            var notifyOne = controller.Notify(postback);
            var resultOne = notifyOne.ExecuteAsync(CancellationToken.None).Result;

            acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var after = acc.GetCreditBalance(acc.RouteCountryId);

            //Assert
            Assert.IsInstanceOf<IHttpActionResult>(notifyOne);
            Assert.IsInstanceOf<HttpResponseMessage>(resultOne);
            Assert.AreEqual(resultOne.StatusCode, HttpStatusCode.OK, "Incorrect Status");
            
            Assert.AreEqual(after, before, "Credits balance");
        }

        [Test]
        public void SagePayDuplicateNotificationOnReprocessingPreviouslyFailed()
        {
            //Arrange
            const string ipAddress = "192.168.0.1";
            const string requestTrace = "12345.123456789";
            const string accountSelectionTagging = "C|0|CC006";
            const string transactionId = "343C0297-2112-4FA2-8C3D-2677585E3EAA";
            const decimal subTotal = 936.38m;
            const decimal taxValue = 93.64m; 
            const decimal total = 1030.02m;
            var controller = CreateMockController();
            var invoice = CreateLinkedInvoiceAndPayment(transactionId, controller.GatewayName, Payment.PaymentStatusFailed, out var payment);
            
            var postbackOne = new SagePayPostback()
            {
                TransactionAccepted = false,
                CardHolderIpAddr = ipAddress,
                RequestTrace = requestTrace,
                Reference = invoice.InvoiceNr,
                Extra1 = invoice.InvoiceNr,
                Extra2 = accountSelectionTagging,
                Extra3 = transactionId,
                Amount = invoice.Total
            };
            
            var postbackTwo = new SagePayPostback()
            {
                TransactionAccepted = true,
                CardHolderIpAddr = ipAddress,
                RequestTrace = requestTrace,
                Reference = invoice.InvoiceNr,
                Extra1 = invoice.InvoiceNr,
                Extra2 = accountSelectionTagging,
                Extra3 = transactionId,
                Amount = invoice.Total
            };
            
            var acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var before = acc.GetCreditBalance(acc.RouteCountryId);
            
            //Act
            //--SagePay sends the previous failed notification and the new success simultaneously--
            var processedOne = controller.Notify(postbackOne);
            var processedTwo = controller.Notify(postbackTwo);
            acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var after = acc.GetCreditBalance(acc.RouteCountryId);
            var processedOneResult = processedOne.ExecuteAsync(CancellationToken.None).Result;
            var processedTwoResult = processedTwo.ExecuteAsync(CancellationToken.None).Result;

            //Assert
            Assert.IsInstanceOf<IHttpActionResult>(processedOne);
            Assert.IsInstanceOf<IHttpActionResult>(processedTwo);
            Assert.IsInstanceOf<HttpResponseMessage>(processedOneResult);
            Assert.IsInstanceOf<HttpResponseMessage>(processedTwoResult);
            Assert.AreEqual(processedOneResult.StatusCode, HttpStatusCode.OK, "Incorrect Status - 1st notifications");
            Assert.AreEqual(processedTwoResult.StatusCode, HttpStatusCode.OK, "Incorrect Status - 2nd notifications");
            Assert.Greater(after, before, "Credits balance");
            Assert.AreEqual(payment.Credits, after - before, "Incorrect amount");
        }
    }
}