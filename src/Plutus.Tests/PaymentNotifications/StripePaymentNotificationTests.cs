﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using Moq;
using NUnit.Framework;
using Plutus.Tests.Base;
using SmsPortal.Plutus;
using SmsPortal.Plutus.Controllers;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Repositories;
using Stripe;

namespace Plutus.Tests.PaymentNotifications
{
    [TestFixture]
    public class StripePaymentNotificationTests : PaymentNotificationTestBase
    {
        private Mock<IPaymentGatewayRepository> mockPaymentGatewayRepository;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            mockPaymentGatewayRepository = new Mock<IPaymentGatewayRepository>();
        }

        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
            mockPaymentGatewayRepository = null;
        }

        private StripePostbackController CreateMockController()
        {
            var controller =
                new StripePostbackController(
                    MockPaymentRepository.Object,
                    MockPaymentAccountRepository.Object,
                    mockPaymentGatewayRepository.Object,
                    new TransactionRegistry(),
                    CreateCreditAllocationService(),
                    MockLog.Object);
            controller.Request = GetHttpRequest(HttpMethod.Post, "http://localhost/api/stripe", "StripePostback");
            return controller;
        }

        [Test]
        public void ProcessSuccessAllocateCredits()
        {
            //Arrange
            const string transactionId = "A93C0291-2112-4FA2-8C3D-2677585EE3A1";
            var controller = CreateMockController();
            var invoice = CreateLinkedInvoiceAndPayment(transactionId, controller.GatewayName, Payment.PaymentStatusStarted, out var payment);
            
            var postback = new Event()
            {
                Id = "12345",
                Type = "payment_intent.succeeded",
                Data = new EventData()
                {
                    Object = new PaymentIntent
                    {
                        Id = "123456789",
                        Currency = Currency.BaseCurrency.Code,
                        Amount = Convert.ToInt32(payment.Total * 100), //Since stripe works in cents
                        PaymentMethodId = "CreditCard",
                        Metadata = new Dictionary<string, string>()
                        {
                            ["PaymentId"] = transactionId
                        },
                        Status = "ok"
                    }
                }
            };

            var acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var before = acc.GetCreditBalance(acc.RouteCountryId);
            
            //Act
            var notifyOne = controller.Process(postback, out _, out _);
            var resultOne = notifyOne.ExecuteAsync(CancellationToken.None).Result;

            acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var after = acc.GetCreditBalance(acc.RouteCountryId);

            //Assert
            Assert.IsInstanceOf<IHttpActionResult>(notifyOne);
            Assert.IsInstanceOf<HttpResponseMessage>(resultOne);
            Assert.AreEqual(resultOne.StatusCode, HttpStatusCode.OK, "Incorrect Status");

            Assert.Greater(after, before, "Credits balance");
            Assert.AreEqual(payment.Credits, after - before, "Incorrect amount");
        }
        
        [Test]
        public void ProcessFailureDoesNotAllocateCredits()
        {
            //Arrange
            const string transactionId = "F93C0291-2112-4FA2-8C3D-2677585EE3A1";
            var controller = CreateMockController();
            var invoice = CreateLinkedInvoiceAndPayment(transactionId, controller.GatewayName, Payment.PaymentStatusStarted, out var payment);
            
            var postback = new Event()
            {
                Id = "12345",
                Type = "payment_intent.payment_failed",
                Data = new EventData()
                {
                    Object = new PaymentIntent
                    {
                        Id = "123456789",
                        Currency = Currency.BaseCurrency.Code,
                        Amount = Convert.ToInt32(payment.Total * 100), //Since stripe works in cents
                        PaymentMethodId = "CreditCard",
                        Metadata = new Dictionary<string, string>()
                        {
                            ["PaymentId"] = transactionId
                        },
                        Status = "canceled"
                    }
                }
            };
            
            var acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var before = acc.GetCreditBalance(acc.RouteCountryId);
            
            //Act
            var notifyOne = controller.Process(postback, out _, out _);
            var resultOne = notifyOne.ExecuteAsync(CancellationToken.None).Result;

            acc = MockPaymentAccountRepository.Object.GetPaymentAccount(invoice.AccountKey);
            var after = acc.GetCreditBalance(acc.RouteCountryId);

            //Assert
            Assert.IsInstanceOf<IHttpActionResult>(notifyOne);
            Assert.IsInstanceOf<HttpResponseMessage>(resultOne);
            Assert.AreEqual(resultOne.StatusCode, HttpStatusCode.OK, "Incorrect Status");
            
            Assert.AreEqual(after, before, "Credits balance");
        }
    }
}