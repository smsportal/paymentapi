﻿using System.Collections.Generic;

namespace SmsPortal.Plutus
{
	public class SpecificationResult
	{
		public bool IsSatisFied { get; set; }

		public List<FailureDetail> FailureDetails { get; set; }

		public SpecificationResult()
		{
			FailureDetails = new List<FailureDetail>();
		}
	}

	public class FailureDetail
	{
		public List<string> MemberNames { get; set; }

		public string FailureMessage { get; set; }

		public string ErrorCode { get; set; }

		public FailureDetail()
		{
			MemberNames = new List<string>();
		}
	}
}
