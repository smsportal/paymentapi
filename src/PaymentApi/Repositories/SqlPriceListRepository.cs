using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class SqlPriceListRepository: BaseSqlRepository, IPriceListRepository
    {
        private const string GetPricesSp = "Plutus_ProductPrice_List_ById_V1";
        private const string GetPriceListSp = "Plutus_PriceList_GetById_V1";
        
        public SqlPriceListRepository(string connectionString) : base(connectionString)
        {
        }

        public PriceList Get(int id)
        {
            using (var conn = GetOpenConnection())
            using (var cmd = GetStoredProcedure(GetPriceListSp, conn))
            {
                cmd.Parameters.AddWithValue("@Id", id);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var priceList = new PriceList();
                        priceList.Id = (int) reader["Id"];
                        priceList.Name = (string) reader["Name"];
                        AddPrices(priceList);
                        return priceList;
                    }
                    throw new EntityNotFoundException(typeof(PriceList), id);
                }
            }
        }

        private void AddPrices(PriceList priceList)
        {
            using (var conn = GetOpenConnection())
            using (var cmd = GetStoredProcedure(GetPricesSp, conn))
            {
                cmd.Parameters.AddWithValue("@PriceListId", priceList.Id);
                using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    var prod = new Product();
                    prod.Price = (decimal) reader["Price"];
                    prod.Code = (string) reader["Code"];
                    prod.Description = (string) reader["Description"];
                    priceList.Add(prod);
                }
            }
        }
    }
}