﻿using System.Collections.Generic;

namespace SmsPortal.Plutus.Repositories
{
    public interface IPrefixRepository
    {
	    HashSet<string> LoadSouthAfricanPrefixes();
    }
}
