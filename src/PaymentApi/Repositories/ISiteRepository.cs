﻿using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface ISiteRepository
    {
        Site Get(int id);
    }
}
