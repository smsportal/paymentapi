﻿using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
	public interface ICreditAllocationRepository
	{
		void CreditCardAllocation(PaymentAccount account, TransactionInfo transaction, string reference, string paymentMethod, decimal transactionFee, string payPalTransactionId = "");

		void SiteCreditAllocation(PaymentAccount account, TransactionInfo transaction, string paymentMethod,
			decimal transactionFee, string payPalTransactionId = "");

		void ResellerAllocation(PaymentAccount account, TransactionInfo transaction, Payment payment);
		
		void ResellerAllocation(PaymentAccount account, TransactionInfo transaction, Payment payment, string customDescription);

		void ResellerAllocation(RefundAccount account, TransactionInfo transaction, string customDescription);

		bool ResellerAllocationExists(int siteId, string transactionId);

		bool SiteCreditAllocationExists(int loginId, string reference);

		bool CreditCardAllocationExists(int loginId, string reference);

		IEnumerable<CreditAllocation> Get(int accountId, string accountType);

		void RefundEventAllocation(RefundEventAllocation allocation);
	}
}
