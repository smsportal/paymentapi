﻿using log4net;
using SmsPortal.Plutus.Models;
using System;

namespace SmsPortal.Plutus.Repositories
{
    public class TaxConfigRepository : BaseSqlRepository, ITaxConfigRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(TaxConfigRepository));
        private const string GetTaxConfigSp = "Plutus_TaxConfig_GetByCountryId_V2";

        public TaxConfigRepository (string connectionString) : base(connectionString) { }

        public TaxConfig GetConfig(int countryId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetTaxConfigSp, connection))
            {
                command.Parameters.AddWithValue("@CountryId", countryId);
                
                var reader = command.ExecuteReader();
                if (reader.Read())
                {
                    var config = new TaxConfig();
                    config.CountryId = countryId;
                    config.Description = (string)reader["CountryDesc"];
                    config.TaxType = DBNull.Value == reader["TaxType"] ? "No Tax" : (string) reader["TaxType"];
                    config.TaxPercentage = DBNull.Value == reader["TaxPercentage"] ? 0 : Convert.ToDecimal(reader["TaxPercentage"]);
                    return config;
                }

                log.Warn($"Tax Config for country with ID {countryId} not found.");
                return new TaxConfig
                {
                    CountryId = countryId,
                    Description = "Country Unknown",
                    TaxType = "No Tax",
                    TaxPercentage = 0
                };
            }
        }
    }
}
