﻿using System;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    //TODO: Implement caching
	public class CurrencyRepository : BaseSqlRepository, ICurrencyRepository
	{
		private readonly string GetConversionRatesSp = "Prefixes_GetConversionRates";
	    private readonly string GetCurrencySp = "Plutus_Currency_Get_V1";

		public CurrencyRepository(string connection) : base(connection)
		{
		}

	    public Currency GetCurrency(string currencyCode)
	    {
	        using (var connection = GetOpenConnection())
	        using (var command = GetStoredProcedure(GetCurrencySp, connection))
	        {
	            command.Parameters.AddWithValue("@CurrencyCode", currencyCode);
	            using (var reader = command.ExecuteReader())
	            {
	                if (reader.Read())
	                {
	                    var code = (string)reader["CurrencyCode"];
	                    var rate = Convert.ToDecimal(reader["Rate"]);
	                    return new Currency(code, rate);
	                }
	                throw new Exception($"Uknown currency {currencyCode}");
	            }
	        }
	    }

	    public CurrencyConverter GetCurrencyConverter(string fromCurrency, string toCurrency)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(GetConversionRatesSp, connection))
			{
				command.Parameters.AddWithValue("@fromcurr", fromCurrency);
				command.Parameters.AddWithValue("@tocurr", toCurrency);

				using (var reader = command.ExecuteReader())
				{
					var currencyConverter = new CurrencyConverter();
					for (int rows = 1; reader.Read(); rows++)
					{
						if (rows > 2)
						{
							throw new Exception($"Prefixes_GetConversionRates {fromCurrency}, {toCurrency} returned more than 2 rows.");
						}
						if ((string) reader["currencycode"] == fromCurrency)
						{
							currencyConverter.FromRate = (decimal) (double) reader["rate"];
						}
						if ((string) reader["currencycode"] == toCurrency)
						{
							currencyConverter.ToRate = (decimal) (double) reader["rate"];
						}
					}
					return currencyConverter;
				}
			}
		}
	}
}
