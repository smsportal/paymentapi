using System;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class RefundAccountRepository: BaseSqlRepository, IRefundAccountRepository
    {
        private const string GetRefundAccountSp = "Plutus_RefundAccount_GetById_V1";
        
        public RefundAccountRepository(string connectionString) : base(connectionString)
        {
        }

        public RefundAccount Get(AccountKey accountKey)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetRefundAccountSp, connection))
            {
                command.Parameters.AddWithValue("@AccountId", accountKey.Id);
                command.Parameters.AddWithValue("@AccType", accountKey.Type.ToKey());
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var acc = new RefundAccount(accountKey);
                        acc.Currency = (string) reader["BaseCurrency"];
                        acc.SiteId = (int) reader["SiteId"];
                        acc.RouteCountryId = (int) reader["RouteCountryId"];
                        acc.IsPostpaid = (bool) reader["PostPaid"];
                        acc.MasterAccountId = (int) reader["MasterLoginID"];
                        acc.Fullname = (string) reader["Fullname"];
                        acc.Username = (string) reader["Username"];
                        acc.Email = (string) reader["Email"];
                        return acc;    
                    }
                    throw new EntityNotFoundException(typeof(RefundAccount), accountKey.ToString());
                }
                
            }
        }
    }
}