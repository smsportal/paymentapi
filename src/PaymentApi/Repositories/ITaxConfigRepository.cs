﻿using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface ITaxConfigRepository
    {
        TaxConfig GetConfig(int countryId);
    }
}
