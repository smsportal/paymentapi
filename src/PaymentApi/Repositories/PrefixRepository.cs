﻿using System.Collections.Generic;

namespace SmsPortal.Plutus.Repositories
{
    public class PrefixRepository : BaseSqlRepository, IPrefixRepository
    {
	    private readonly string PrefixesSp = "Site_SAPrefixes";
	    public PrefixRepository(string connection) : base(connection)
	    {
	    }

	    public HashSet<string> LoadSouthAfricanPrefixes()
	    {
			var prefixes = new HashSet<string>();
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(PrefixesSp, connection))
			using (var reader = command.ExecuteReader())
			{
				while (reader.Read())
				{
					prefixes.Add((string) reader["SAPrefix"]);
				}
			}
		    return prefixes;
	    }
    }
}
