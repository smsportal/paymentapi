﻿using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
    public interface ILegacyInvoiceRepository
    {
	    InvoiceModel CreateInvoice(PaymentAccount paymentAccount, LegacyInvoice legacyInvoice);

	    void CloseInvoice(PaymentAccount account, string transactionId, bool sendInvoiceEmail = false,
		    string paymentReference = "");

	    CreditNoteModel CancelInvoice(PaymentAccount paymentAccount, string transactionId);

	    CreditNoteModel ExpireInvoice(PaymentAccount paymentAccount, string transactionId);

	    UpdatePaymentInvoiceResult UpdatePaymentInvoice(PaymentAccount paymentAccount, string transactionId);

	    UpdatePaymentInvoiceResult UpdateInvoiceStatus(PaymentAccount paymentAccount, string transactionId,
		    InvoiceStatus invoiceStatus, string paymentReference = "");

	    InvoiceModel LoadInvoice(PaymentAccount paymentAccount, string transactionId);

	    byte[] LoadInvoicePdf(string transactionId);

	    string GetInvoiceReferenceNumber(PaymentAccount paymentAccount);

	    InvoiceModel InsertInvoice(PaymentAccount paymentAccount, LegacyInvoice legacyInvoice);

	    void InsertInvoiceLineItem(int invoiceId, LegacyInvoiceLineItem lineItem);

	    bool CheckDuplicateInvoiceExists(string transactionId);

        InvoiceModel UpdateInvoicePoNumber(PaymentAccount account, string transactionId, string orderNumber);

	    void DeclineAuthRecord(PaymentAccount paymentAccount, string transactionId, string reason);

	    void SaveInvoiceBytes(InvoiceModel invoice);
    }
}
