﻿using System.Collections.Generic;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface ICreditNoteRepository
    {
	    CreditNoteModel InsertCreditNote(PaymentAccount paymentAccount, CreditNoteModel creditNote);
	    void SaveCreditNoteBytes(CreditNoteModel creditNote);

	    CreditNoteModel Get(string reference);

	    List<CreditNoteModel> List(ListCreditNotesRequest request);

	    byte[] GetPdf(string reference);

        long GetNextCreditNoteReferenceNumber(int siteId);

        void AddPdf(int creditNoteId, byte[] pdfBytes);
    }
}
