using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IPaymentGatewayRepository
    {
        SagePaySettings GetSagePaySettings(int resellerId);

        PayPalSettings GetPayPalSettings(int resellerId);
        
        StripeSettings GetStripeSettings(int siteId);
    }
}