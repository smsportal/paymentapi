﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
	public class TransactionRepository : BaseSqlRepository, ITransactionRepository
	{
		private const string ShortCodeBillingCycleClose = "ShortCodeBillingCycles_CloseCycle";

		public TransactionRepository(string connection) : base(connection)
		{
		}
		
		public List<LegacyInvoiceLineItem> GetShortCodeTransactions(PaymentAccount paymentAccount, ShortCodeApplicationType applicationType, DateTime from,
			DateTime to, bool isDedicated)
		{
			string storedProcedure;

			if (paymentAccount.AccountType == AccountType.ResellerAccount)
			{
				throw new Exception("Automated Short Code is only supported for SMSAccounts");
			}

			var transactions = new List<LegacyInvoiceLineItem>();
			SqlParameter accIdentifier;

			switch (applicationType)
			{
				case ShortCodeApplicationType.PerRefCode:
					storedProcedure = "ShortCodes_GetDataPerRefCode";
					accIdentifier = new SqlParameter
					{
						ParameterName = "@ClientRefCode",
						SqlDbType = SqlDbType.VarChar,
						Value = paymentAccount.ClientRefCode
					};
					break;

				case ShortCodeApplicationType.PerUser:
					storedProcedure = "ShortCodes_GetDataPerLogin";
					accIdentifier = new SqlParameter
					{
						ParameterName = "@LoginId",
						SqlDbType = SqlDbType.Int,
						Value = paymentAccount.Id
					};
					break;

				default:
					throw new Exception($"No ShortCodeApplicationType specified for authorization request. Account Id:{paymentAccount.Id} Name: {paymentAccount.Username}");
			}

			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(storedProcedure, connection))
			{
				command.Parameters.Add(accIdentifier);
				command.Parameters.AddWithValue("@BillFromDate", from);
				command.Parameters.AddWithValue("@BillToDate", to);
				command.Parameters.AddWithValue("@IsDedicated", isDedicated);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						if (!reader.HasRows)
						{
							throw new Exception($"Unable to retrieve transactions for login, Id:{paymentAccount.Id} Name:{paymentAccount.Username} SCBillingType:{applicationType.ToString()}  BillingFromDate:{from:dd/MM/yyyy} BillingToDate:{to:dd/MM/yyyy}");
						}

						transactions.Add(new LegacyInvoiceLineItem
						{
							Price = Convert.ToDecimal(reader["LineTotal"]),
							Quantity = isDedicated ? (int) reader["BillingMonths"] : 1,
							Description = (string) reader["LineItemDesc"]
						});
					}
				}
			}
			return transactions;
		}

		public void CloseShortCodeBillingCycle(ShortCodeBillingCycle billingCycle)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(ShortCodeBillingCycleClose, connection))
			{
				command.Parameters.AddWithValue("@ApplicationType", billingCycle.SCApplicationType);
				command.Parameters.AddWithValue("@IsDedicated", billingCycle.SCIsdedicated);
				command.Parameters.AddWithValue("@AccountId", billingCycle.AccountId);
				command.Parameters.AddWithValue("@ClientRefCode", billingCycle.ClientRefCode);

				command.Parameters.AddWithValue("@BillingDateFrom", billingCycle.CycleStartDate.Date);
				command.Parameters.AddWithValue("@BillingDateTo", billingCycle.CycleEndDate.Date);
				command.Parameters.AddWithValue("@InvoiceId", billingCycle.InvoiceId);
				command.Parameters.AddWithValue("@Currency", billingCycle.Currency);
				command.Parameters.AddWithValue("@SubTotal", billingCycle.SubTotal);
				command.Parameters.AddWithValue("@TaxValue", billingCycle.TaxValue);

				if (billingCycle.ShortCode > 0)
				{
					command.Parameters.AddWithValue("@ShortCode", billingCycle.ShortCode);
				}

				if (!string.IsNullOrEmpty(billingCycle.Keyword))
				{
					command.Parameters.AddWithValue("@Keyword", billingCycle.Keyword);
				}

				if (!string.IsNullOrEmpty(billingCycle.Comment))
				{
					command.Parameters.AddWithValue("@Comment", billingCycle.Comment);
				}

				command.ExecuteNonQuery();
			}
		}
	}
}
