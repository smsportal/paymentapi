﻿using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
    public interface IPostPaidEventsRepository
    {
        /// <summary>
        /// Retrieve a single proforma invoice related to a specific event
        /// </summary>
        /// <param name="accountType">The type of account</param>
        /// <param name="accountKey">The unique identifier of the account</param>
        /// <param name="eventId">The unique identifier of the event</param>
        /// <returns>A proforma invoice</returns>
        ProformaInvoice GetProformaInvoice(
            AccountKey accountKey,
            long eventId);
    }
}
