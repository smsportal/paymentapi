﻿using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
    public interface IPostPaidBillingCycleInvoiceLineRepository
    {
        IEnumerable<PostPaidBillingCycleLineItem> GetLineItems(
            AccountKey? accountKey,
            DateTime? invoiceDate,
            InvoiceLineItemType? lineItemType);
    }
}
