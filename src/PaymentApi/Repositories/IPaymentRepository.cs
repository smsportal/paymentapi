﻿using System;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IPaymentRepository
    {
        void Add(Payment payment);

        Payment Get(Guid transactionId);

        void Update(Payment payment);
    }
}
