﻿using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    /// <summary>
    /// A repository to retrieve short code rental information.
    /// </summary>
    public interface IShortCodeRentalRepository
    {
        /// <summary>
        /// Retrieve all the short code allocations that might need to be billed on the requested invoice date.
        /// </summary>
        /// <param name="invoiceDate">The invoice date. Only the date portion of this will be taken into account.</param>
        /// <returns>An enumerable collection of <c>ShortCodeRentalApplicability</c> objects related to the potential short code rental invoice lines.</returns>
        IEnumerable<ShortCodeRentalApplicability> GetAllShortCodeRentalApplicability(DateTime invoiceDate);
    }
}