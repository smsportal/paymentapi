﻿using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IInvoiceRepository
    {
        int Add(Invoice invoice);

        int Add(PostpaidInvoice invoice);

        long GetNextInvoiceNumber(int siteId);
        
        int GetNextDryInvoiceNumber(int siteId);

        void AddPdf(int invoiceId, byte[] pdfBytes);

	    List<Invoice> List(ListInvoicesRequest request);

	    Invoice Get(string reference);

	    Invoice Get(int id);

	    byte[] GetPdf(string transactionId);

        void Update(Invoice invoice);

        List<InvoiceLineItem> GetLineItems(int invoiceId);
		
        List<Invoice> List(string clientRefCode);
    }
}
