﻿using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface ISettlementDiscountRepository
    {
        SettlementDiscount GetByPastelCode(string clientRefCode);
    }
}