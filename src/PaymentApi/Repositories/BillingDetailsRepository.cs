﻿using System.Data;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Core.DatabaseExtensions;

namespace SmsPortal.Plutus.Repositories
{
	public class BillingDetailsRepository : BaseSqlRepository, IBillingDetailsRepository
	{
		private readonly string GetBillingDetailsSp = "Login_BillingDetails_GetV2";  // TODO: Create Plutus SP

		public BillingDetailsRepository(string connection) : base(connection)
		{
		}

		public bool TryGetBillingDetails(AccountKey accountKey, out BillingDetails billingDetails)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(GetBillingDetailsSp, connection))
			{
				command.Parameters.AddWithValue("@AccountId", accountKey.Id);
				command.Parameters.AddWithValue("@AccountType", accountKey.Type.ToKey());

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						billingDetails = CreateBillingDetails(reader);
						return true;
					}

					billingDetails = null;
					return false;
				}
			}
		}

		public BillingDetails GetBillingDetails(AccountKey accountKey)
		{
			if (!TryGetBillingDetails(accountKey, out var result))
			{
				throw new EntityNotFoundException(typeof(BillingDetails), accountKey.ToString());
			}

			return result;
		}

		private BillingDetails CreateBillingDetails(IDataReader reader)
		{
			return new BillingDetails
			{
                Address1 = reader.Read<string>("Address1"),
                ZipCode = reader.Read<string>("ZipCode"),
                City = reader.Read<string>("City"),
                Country = reader.Read<string>("Country"),
                StateProvince = reader.Read<string>("StateProvince"),
                CompanyName = reader.Read<string>("CompanyName"),
                OrderNumber = reader.Read<string>("OrderNumber"),
                CountryId = reader.Read<int>("CountryId"),
                Address2 = reader.Read<string>("Address2"),
                RegistrationNumber = reader.Read<string>("RegistrationNumber"),
                VatNumber = reader.Read<string>("VatNumber"),
                InvoiceEmail = reader.Read<string>("InvoiceEmail")
			};
		}

	}
}
