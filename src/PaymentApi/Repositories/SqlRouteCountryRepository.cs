using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class SqlRouteCountryRepository : BaseSqlRepository, IRouteCountryRepository
    {
        private const string GetRouteCountrySp = "Plutus_RouteCountry_GetById_V1";

        public SqlRouteCountryRepository(string connectionString) : base(connectionString)
        {
        }

        public RouteCountry Get(int routeCountryId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetRouteCountrySp, connection))
            {
                command.Parameters.AddWithValue("@RouteCountryId", routeCountryId);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var tmp = new RouteCountry();
                        tmp.Id = routeCountryId;
                        var tmpCountry = new Country();
                        tmpCountry.Id = (int) reader["CountryId"];
                        tmpCountry.Name = (string) reader["CountryDesc"];
                        tmp.Country = tmpCountry;
                        tmp.Routename = (string) reader["Name"];
                        return tmp;

                    }

                    throw new EntityNotFoundException(typeof(RouteCountry), routeCountryId);
                }
            }

        }
    }
}