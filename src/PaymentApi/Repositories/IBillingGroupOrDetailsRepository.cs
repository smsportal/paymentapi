﻿using System;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IBillingGroupOrDetailsRepository
    {
        bool TryGet(AccountKey accountKey, out BillingDetails billingDetails);
        
        BillingDetails Get(AccountKey accountKey);
    }
}