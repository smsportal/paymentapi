﻿using System;
using System.Collections.Generic;
using System.Data;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Core.DatabaseExtensions;
using AccountType = SmsPortal.Plutus.Models.Enums.AccountType;

namespace SmsPortal.Plutus.Repositories
{
    public class PaymentAccountRepository : BaseSqlRepository, IPaymentAccountRepository
    {
        private readonly ICurrencyRepository currencyRepo;
        private const string GetPaymentAccountSp = "Plutus_Account_GetMasterByAccountId_V2";
        private const string GetResellerAccountSp = "Plutus_Account_GetByResellerId_V2";
        private const string GetResellCreditPoolsSp = "Plutus_ResellerCreditPool_Get_V1";
        private const string GetPaymentAccountByRefCodeSp = "Plutus_Account_GetByClientRefCode_V1";
        private const string GetPaymentAccountByBillingGroupSp = "Plutus_Account_GetByBillingGroup_V1";

        public PaymentAccountRepository(string connection, ICurrencyRepository currencyRepo) : base(connection)
        {
            this.currencyRepo = currencyRepo;
        }
       
        public PaymentAccount GetPaymentAccount(AccountKey accountKey)
        {
            if(TryGetPaymentAccount(accountKey, out var account))
            {
                return account;
            }

            throw new EntityNotFoundException(typeof(PaymentAccount), accountKey.ToString());
        }

        public IList<PaymentAccount> GetAllByBillingGroup(int billingGroupId)
        {
            using(var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetPaymentAccountByBillingGroupSp, connection))
            {
                command.Parameters.AddWithValue("@BillingGroupId", billingGroupId);
                using (var reader = command.ExecuteReader())
                {
                    var accounts = new List<PaymentAccount>();
                    while (reader.Read())
                    {
                        accounts.Add(BuildForSmsAccount(reader));
                    }

                    return accounts;
                }
            }
        } 
        

        public bool TryGetPaymentAccount(AccountKey accountKey, out PaymentAccount account)
        {
            switch (accountKey.Type)
            {
                case AccountType.SmsAccount:
                    return TryGetForSmsAccount(accountKey, out account);
                case AccountType.ResellerAccount:
                    return TryGetForResellerAccount(accountKey, out account);
                case AccountType.SiteId:
                    return TryGetForResellerAccount(accountKey, out account);
                default:
                    throw new ArgumentOutOfRangeException(nameof(accountKey));
            }
        }

        public IList<PaymentAccount> GetAll(string clientRefCode)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetPaymentAccountByRefCodeSp, connection))
            {
                command.Parameters.AddWithValue("@ClientRefCode", clientRefCode);
                using (var reader = command.ExecuteReader())
                {
                    var accounts = new List<PaymentAccount>();
                    while (reader.Read())
                    {
                        accounts.Add(BuildForSmsAccount(reader));
                    }

                    return accounts;
                }
            }
        }

        private bool TryGetForSmsAccount(AccountKey accountKey, out PaymentAccount account)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetPaymentAccountSp, connection))
            {
                command.Parameters.AddWithValue("@AccountID", accountKey.Id);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        account = BuildForSmsAccount(reader);
                        return true;
                    }

                    account = null;
                    return false;
                }
            }
        }

        private PaymentAccount BuildForSmsAccount(IDataReader reader)
        {
            var account = new PaymentAccount();
            account.AccountKey = new AccountKey(reader.Read<int>("LoginId"), AccountType.SmsAccount);
            account.SiteId = reader.Read<int>("SiteID");
            account.BaseCurrency = currencyRepo.GetCurrency(reader.Read<string>("BaseCurrency"));
            account.Fullname = reader.Read<string>("Fullname");
            account.Username = reader.Read<string>("UserName");
            account.EmailTo = reader.Read<string>("Email");
            account.ClientRefCode = reader.Read<string>("ClientRefCode");
            account.GatewaysLive = reader.Read<bool>("GatewaysLive");
            account.Company = reader.Read<string>("Company");
            account.CreditCardAuthRequired = reader.Read("CC_Auth", () => false);
            account.FixedPricingCurrency = reader.Read<string>("fixedPricing_curr");
            account.PricingSCCurrency = reader.Read<string>("PricingSCCurrency");
            account.PricingSCSetup = reader.Read<decimal>("PricingSCSetup");
            account.PricingSCMonthly = reader.Read<decimal>("PricingSCMonthly");
            account.PricingSCShared = reader.Read<decimal>("PricingSCShared");
            account.ShortCodeExpiryEmailAddress = reader.Read<string>("SC_ExpiryEmail");
            account.IsPostpaid = reader.Read<bool>("IsPostPaid");
            account.BillingPeriod = reader.Read<BillingPeriod>("BillingCycleType");
            account.CountryOfResidence = reader.Read<int>("CountryOfResidence");
            account.RouteCountryId = reader.Read<int>("RouteCountryId");
            account.PriceListId =  reader.Read<int?>("PriceListId", () => null);
            account.BillingGroupId = reader.Read<int?>("BillingGroupId", () => null);

            var tmpCp = new CreditPool();
            tmpCp.Balance = reader.Read<int>("credits");
            tmpCp.Country = reader.Read<string>("RouteCountryDescription");
            tmpCp.RouteCountryId = reader.Read<int>("RoutecountryId");
            account.AddCreditPool(tmpCp);
            
            account.PostPaidBillingType = reader.Read<LineItemBreakdownType>("PostPaidBillingType");
            account.ShortCodeBillingType = reader.Read<LineItemBreakdownType>("ShortCodeBillingType");
            account.AccountLinkName = reader.Read<string>("AccountLinkName");

            return account;
        }

        private bool TryGetForResellerAccount(AccountKey accountKey, out PaymentAccount account)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetResellerAccountSp, connection))
            {
                switch (accountKey.Type)
                {
                    case AccountType.ResellerAccount:
                        command.Parameters.AddWithValue("@ResellerId", accountKey.Id);
                        break;
                    case AccountType.SiteId:
                        command.Parameters.AddWithValue("@SiteId", accountKey.Id);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(accountKey.Type));
                }

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        account = BuildForResellerAccount(reader, accountKey);
                        return true;
                    }

                    account = null;
                    return false;

                }
            }
        }

        private PaymentAccount BuildForResellerAccount(IDataReader reader, AccountKey accountKey)
        {
            var account = new PaymentAccount();
            var resellerId = reader.Read<int>("ResellerID");
            account.AccountKey = new AccountKey(resellerId, AccountType.ResellerAccount);
            account.SiteId = reader.Read<int>("SiteID");
            account.BaseCurrency = currencyRepo.GetCurrency(reader.Read<string>("BaseCurrency"));
            account.Fullname = reader.Read<string>("Fullname");
            account.Username = reader.Read<string>("UserName");
            account.EmailTo = reader.Read<string>("Email");
            account.ClientRefCode = reader.Read<string>("ClientRefCode");
            account.GatewaysLive = false; // TODO will need to add this to sites
            account.Company = reader.Read<string>("CompanyName");
            account.CreditCardAuthRequired = true; //TODO add column to sites.
            account.FixedPricingCurrency = "ZAR";
            account.RouteCountryId = -1;
            //TODO consider removing all of these non payment account related fields.
            account.ShortCodeExpiryEmailAddress = null;
            account.IsPostpaid = reader.Read<bool>("Postpaid");
            account.CountryOfResidence = reader.Read<int>("CountryOfResidence");
            account.BillingPeriod = BillingPeriod.None;
            account.BillingGroupId = reader.Read<int?>("BillingGroupId", () => null);

            foreach (var cp in GetCreditPools(account.SiteId))
            {
                account.AddCreditPool(cp);
            }
            
            account.PostPaidBillingType = reader.Read<LineItemBreakdownType>("PostPaidBillingType");
            account.ShortCodeBillingType = reader.Read<LineItemBreakdownType>("ShortCodeBillingType");
            account.AccountLinkName = reader.Read<string>("AccountLinkName");
            account.PricingSCCurrency = reader.Read<string>("PricingSCCurrency");
            account.PricingSCSetup = reader.Read<decimal>("PricingSCSetup");
            account.PricingSCMonthly = reader.Read<decimal>("PricingSCMonthly");
            account.PricingSCShared = reader.Read<decimal>("PricingSCShared");
            
            return account;
        }

        private IList<CreditPool> GetCreditPools(int resellerId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetResellCreditPoolsSp, connection))
            {
                command.Parameters.AddWithValue("@ResellerId", resellerId);
                using (var reader = command.ExecuteReader())
                {
                    var creditPools = new List<CreditPool>();
                    while (reader.Read())
                    {
                        var tmpCp = new CreditPool();
                        tmpCp.Balance = Convert.ToInt32(reader["credits"]);
                        tmpCp.Country = (string) reader["CountryDesc"];
                        tmpCp.RouteCountryId = (int) reader["RoutecountryId"];
                        creditPools.Add(tmpCp);
                    }

                    return creditPools;
                }
            }
        }

    }
}
