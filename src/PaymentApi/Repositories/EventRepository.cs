using System;
using System.Data;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class EventRepository : BaseSqlRepository, IEventRepository
    {
        private const string DiscountCancelledSp = "Plutus_Event_DiscountCancelledMessages";
        
        public EventRepository(string connectionString) : base(connectionString)
        {
        }

        public EventRefundResult DiscountCancelledEventMessages(int accountId, long eventId, int refundMsgCount, decimal refundValue)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(DiscountCancelledSp, connection))
            {
                command.Parameters.AddWithValue("@LoginId", accountId);
                command.Parameters.AddWithValue("@EventId", eventId);
                command.Parameters.AddWithValue("@EventRefundMessages", refundMsgCount);
                command.Parameters.AddWithValue("@EventRefundValue", refundValue);

                var createDtParam = command.Parameters.Add("@EventCreateDt", SqlDbType.DateTime2);
                var scheduleDtParam = command.Parameters.Add("@EventScheduleDt", SqlDbType.DateTime2);
                var msgTextParam = command.Parameters.Add("@EventExampleMsg", SqlDbType.VarChar, 480);
                createDtParam.Direction = ParameterDirection.Output;
                scheduleDtParam.Direction = ParameterDirection.Output;
                msgTextParam.Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();

                if(createDtParam.Value == DBNull.Value)
                {
                    return null;
                }

                return new EventRefundResult
                {
                    Created = (DateTime)createDtParam.Value,
                    Scheduled = (DateTime)scheduleDtParam.Value,
                    RefundMessageCount = refundMsgCount,
                    RefundMessageValue = refundValue,
                    ExampleMessage = (string)msgTextParam.Value
                };
            }
        }
    }
}