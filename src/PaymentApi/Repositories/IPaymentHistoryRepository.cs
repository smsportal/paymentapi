using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IPaymentHistoryRepository
    {
        IEnumerable<PaymentHistory> GetList(
            string accountType,
            int? accountId,
            string pastelCode,
            int? pageSize,
            int? pageNo);
    }
}