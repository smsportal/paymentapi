using SmsPortal.Core.DatabaseExtensions;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class SqlPaymentGatewayRepository: BaseSqlRepository, IPaymentGatewayRepository
    {
        private const string GetSagePaySettingSp = "Plutus_SagePaySettings_GetById_V1";
        private const string GetPayPalSettingsSp = "Plutus_PayPalSettings_GetById_V1";
        private const string GetStripeSettingsSp = "Plutus_StripeSettings_GetById_V1";
        private readonly string paymentGatewayUrl;
        
        public SqlPaymentGatewayRepository(string connectionString, string paymentGatewayUrl) : base(connectionString)
        {
            this.paymentGatewayUrl = paymentGatewayUrl;
        }

        public SagePaySettings GetSagePaySettings(int resellerId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetSagePaySettingSp, connection))
            {
                command.Parameters.AddWithValue("@ResellerId", resellerId);
                using (var reader = command.ExecuteReader())
                {
                    if (!reader.Read()) throw new EntityNotFoundException(typeof(SagePaySettings), resellerId);

                    var tmp = new SagePaySettings
                    {
                        PaymentGatewayUrl = paymentGatewayUrl,
                        ResellerId = resellerId,
                        VendorKey = reader.Read<string>("SagePay_VendorKey"),
                        ServiceKey = reader.Read<string>("SagePay_ServiceKey"),
                        Min = reader.Read<int>("SagePay_MinValue"),
                        Max = reader.Read<int>("SagePay_MaxValue")
                    };
                    return tmp;
                }

            }
        }

        public PayPalSettings GetPayPalSettings(int resellerId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetPayPalSettingsSp, connection))
            {
                command.Parameters.AddWithValue("@ResellerId", resellerId);
                using (var reader = command.ExecuteReader())
                {
                    if (!reader.Read()) throw new EntityNotFoundException(typeof(PayPalSettings), resellerId);

                    var tmp = new PayPalSettings
                    {
                        ResellerId = resellerId,
                        Maximum = reader.Read<int>("PayPalMaxValue"),
                        Minimum = reader.Read<int>("PayPalMinValue"),
                        Currency = reader.Read<string>("PayPalHomeCurrency"),
                        TransactionFee = reader.Read<decimal>("PayPalTransFee"),
                        MerchantId = reader.Read<string>("PayPalMerchantID")
                    };
                    return tmp;
                }
            }
        }

        public StripeSettings GetStripeSettings(int siteId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetStripeSettingsSp, connection))
            {
                command.Parameters.AddWithValue("@SiteId", siteId);
                using (var reader = command.ExecuteReader())
                {
                    if (!reader.Read()) throw new EntityNotFoundException(typeof(StripeSettings), siteId);

                    var tmp = new StripeSettings
                    {
                        SiteId = siteId,
                        ApiKey = reader.Read<string>("StripeApiKey"),
                        PublishableKey = reader.Read<string>("StripePublishableKey"),
                        WebhookSecret = reader.Read<string>("StripeWebhookSecret")
                    };
                    return tmp;
                }
            }
        }
    }
}
