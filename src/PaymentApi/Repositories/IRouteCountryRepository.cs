using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IRouteCountryRepository
    {
        RouteCountry Get(int routeCountryId);
    }
}