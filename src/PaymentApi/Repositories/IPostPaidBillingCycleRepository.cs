﻿using SmsPortal.Plutus.Models;
using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
    public interface IPostPaidBillingCycleRepository
    {
        /// <summary>
        /// Retrieve all proforma invoices of the specified filter type
        /// </summary>
        /// <param name="filterType">SMPP, STD or MOR (reverse billing)</param>
        /// <returns>A collection of proforma invoices</returns>
        IEnumerable<ProformaInvoice> GetAllProformaInvoices(PostPaidBillingCycleFilterType filterType);
        
        /// <summary>
        /// Retrieve the billing cycle details of a single invoice
        /// </summary>
        /// <param name="invoiceId">The unique identifier of the invoice</param>
        /// <returns>The details of the billing cycle</returns>
        PostPaidBillingCycle IdentifyBillingCycle(int invoiceId);
        
        /// <summary>
        /// Retrieve a single proforma invoice of the specified filter type
        /// </summary>
        /// <param name="accountType">The type of account</param>
        /// <param name="accountId">The unique identifier of the account</param>
        /// <param name="filterType">SMPP, STD or MOR (reverse billing)</param>
        /// <param name="invoiceDate">The date of the invoice</param>
        /// <returns>A proforma invoice</returns>
        ProformaInvoice GetProformaInvoice(
            AccountKey accountKey,
            PostPaidBillingCycleFilterType filterType,
            DateTime invoiceDate);
        
        /// <summary>
        /// Mark the applicability of post-paid transaction to the billing cycle
        /// </summary>
        /// <param name="invoice">The proforma invoice</param>
        /// <param name="shortCode">The short code (if any) related to the approval</param>
        /// <param name="keyword">The short code keyword (if any) related to the approval</param>
        void MarkAsPostPaid(
            ProformaInvoice invoice,
            long? shortCode = null,
            string keyword = null);
        
        /// <summary>
        /// Mark the applicability of post-paid transaction to the billing cycle as approved
        /// </summary>
        /// <param name="invoice">The proforma invoice</param>
        /// <param name="reason">The reason for the approval</param>
        /// <param name="shortCode">The short code (if any) related to the approval</param>
        /// <param name="keyword">The short code keyword (if any) related to the approval</param>
        void Approve(
            ProformaInvoice invoice,
            string reason,
            long? shortCode = null,
            string keyword = null);
        
        /// <summary>
        /// Mark the applicability of post-paid transaction to the billing cycle as declined
        /// </summary>
        /// <param name="invoice">The proforma invoice</param>
        /// <param name="reason">The reason for the decline</param>
        /// <param name="shortCode">The short code (if any) related to the decline</param>
        /// <param name="keyword">The short code keyword (if any) related to the decline</param>
        void Decline(
            ProformaInvoice invoice,
            string reason,
            long? shortCode = null,
            string keyword = null);
        
        /// <summary>
        /// Generate a single generic proforma invoice with a line item
        /// </summary>
        /// <param name="account">The payment account details</param>
        /// <param name="billingDetails">The billing details</param>
        /// <param name="period">The type of invoice period</param>
        /// <param name="line">The invoice line item</param>
        /// <param name="invoiceDate">The date of the invoice</param>
        /// <returns>A proforma invoice</returns>
        ProformaInvoice GetProformaInvoice(
            PaymentAccount account,
            BillingDetails billingDetails,
            DateTime invoiceDate,
            BillingPeriod period,
            InvoiceLineItem line);

        /// <summary>
        /// Match the provided invoices to existing invoices 
        /// </summary>
        /// <param name="invoices">The collection of invoices to match</param>
        /// <returns>The matched collection of invoices</returns>
        IEnumerable<ProformaInvoice> MatchToExisting(IEnumerable<ProformaInvoice> invoices);
    }
}
