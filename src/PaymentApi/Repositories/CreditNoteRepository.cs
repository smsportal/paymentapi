﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using AutoMapper;
using SmsPortal.Core.DatabaseExtensions;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
	public class CreditNoteRepository : BaseSqlRepository, ICreditNoteRepository
	{
	    private readonly string CreditNoteCreate = "Login_CreditNote_Create";
		private readonly string CreditNoteUpdateBytes = "Login_CreditNote_UpdateBytes";
		private readonly string CreditNoteGetReferenceNumber = "Site_CreditNoteReferenceNumber_Get";
		private readonly string CreditNoteGet = "Plutus_CreditNote_Get_V2";
		private readonly string ListCreditNotes = "Plutus_CreditNotes_List_V2";
		private readonly string GetInvoiceLineItems = "Plutus_InvoiceLineItems_Get_V2";
		private readonly string GetCreditNotePdf = "Plutus_CreditNotePdf_Get_V1";
	    private readonly string CreateCreditNotePdfSp = "Plutus_CreditNotePdf_Create_V1";


        public CreditNoteRepository(string connection) : base(connection)
		{

		}

		public CreditNoteModel InsertCreditNote(PaymentAccount paymentAccount, CreditNoteModel creditNote)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(CreditNoteCreate, connection))
			{
				switch (paymentAccount.AccountType)
				{
					case AccountType.ResellerAccount:
						command.Parameters.AddWithValue("@ResellerId", paymentAccount.Id);
						break;
					default:
						command.Parameters.AddWithValue("@LoginId", paymentAccount.Id);
						break;
				}

				command.Parameters.AddWithValue("@ReferenceNumber", creditNote.ReferenceNumber);
				command.Parameters.AddWithValue("@InvoiceReferenceNumber", creditNote.InvoiceReferenceNumber);
				command.Parameters.AddWithValue("@InvoiceId", creditNote.InvoiceId);
				command.Parameters.AddWithValue("@AccountName", creditNote.AccountName);
				command.Parameters.AddWithValue("@ClientRef", creditNote.ClientRefCode);
			    command.Parameters.AddWithValue("@CustomerRef", creditNote.CustomerRefCode);
				command.Parameters.AddWithValue("@CreatedDate", creditNote.CreatedDate);
				command.Parameters.AddWithValue("@TaxPercentage", creditNote.TaxPercentage);
				command.Parameters.AddWithValue("@TaxValue", creditNote.TaxValue);
				command.Parameters.AddWithValue("@Total", creditNote.Total);
				command.Parameters.AddWithValue("@SubTotal", creditNote.SubTotal);
				command.Parameters.AddWithValue("@Currency", creditNote.Currency);
				command.Parameters.AddWithValue("@TransactionId", creditNote.TransactionId);
				command.Parameters.Add(new SqlParameter
				{
					ParameterName = "@CreditNoteId",
					Direction = ParameterDirection.Output,
					SqlDbType = SqlDbType.Int
				});

				command.ExecuteNonQuery();
				var creditNoteId = (int) command.Parameters["@CreditNoteId"].Value;

				creditNote.CreditNoteId = creditNoteId;
			}
			return creditNote;
		}

	    public long GetNextCreditNoteReferenceNumber(int siteId)
	    {
	        using (var connection = GetOpenConnection())
	        using (var command = GetStoredProcedure(CreditNoteGetReferenceNumber, connection))
	        {
	            command.Parameters.AddWithValue("@SiteId", siteId);
	            command.Parameters.Add(new SqlParameter
	            {
	                ParameterName = "@CreditNoteReferenceNumber",
	                SqlDbType = SqlDbType.BigInt,
	                Direction = ParameterDirection.Output
	            });

	            command.ExecuteNonQuery();
	            return (long)command.Parameters["@CreditNoteReferenceNumber"].Value;
	        }
        }

	    public void AddPdf(int creditNoteId, byte[] pdfBytes)
	    {
	        using (var connection = GetOpenConnection())
	        using (var cmd = GetStoredProcedure(CreateCreditNotePdfSp, connection))
	        {
	            cmd.Parameters.AddWithValue("@CreditNoteId", creditNoteId);
	            cmd.Parameters.Add(new SqlParameter
	            {
	                ParameterName = "@Pdf",
	                SqlDbType = SqlDbType.VarBinary,
	                Value = pdfBytes
	            });
	            cmd.ExecuteNonQuery();
	        }
        }

	    public void SaveCreditNoteBytes(CreditNoteModel creditNote)
		{
			if (creditNote.CreditNoteBytes == null || creditNote.CreditNoteBytes.Length <= 0)
			{
				return;
			}

			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(CreditNoteUpdateBytes, connection))
			{
				command.Parameters.AddWithValue("@CreditNoteId", creditNote.CreditNoteId);
				command.Parameters.Add(new SqlParameter
				{
					ParameterName = "@CreditNoteBytes",
					SqlDbType = SqlDbType.VarBinary,
					Value = creditNote.CreditNoteBytes
				});

				command.ExecuteNonQuery();
			}
		}

		public CreditNoteModel Get(string reference)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(CreditNoteGet, connection))
			{
				command.Parameters.AddWithValue("@Reference", reference);

				using (var reader = command.ExecuteReader())
				{
					if (!reader.Read()) throw new EntityNotFoundException(typeof(CreditNoteModel), reference);
					var creditNote = CreateCreditNote(reader);
					return Mapper.Map<CreditNoteModel>(creditNote);
				}
			}
		}

		public List<CreditNoteModel> List(ListCreditNotesRequest request)
		{
			var list = new List<CreditNoteModel>();
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(ListCreditNotes, connection))
			{
				command.Parameters.AddWithValue("@LoginId", request.LoginId ?? (object) DBNull.Value);
				command.Parameters.AddWithValue("@From", request.From ?? (object) DBNull.Value);
				command.Parameters.AddWithValue("@To", request.To ?? (object) DBNull.Value);
				command.Parameters.AddWithValue("@Currency", request.Currency ?? (object) DBNull.Value);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						var creditNote = CreateCreditNote(reader);
						var model = Mapper.Map<CreditNoteModel>(creditNote);
						list.Add(model);
					}
				}
			}
			return list;
		}

		public byte[] GetPdf(string reference)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(GetCreditNotePdf, connection))
			{
				command.Parameters.AddWithValue("@Reference", reference);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						 return reader.Read<byte[]>("InvoiceData");
					}
				}
				throw new EntityNotFoundException(typeof(byte[]), "CreditNote {reference} pdf.");
			}
		}

		private CreditNote CreateCreditNote(SqlDataReader reader)
		{
			var creditNote = new CreditNote
			{
				Id = reader.Read<int>("Id"),
				LoginId = reader.Read<int>("LoginId", () => 0),
				ResellerId = reader.Read<int>("ResellerId", () => 0),
				InvoiceId = reader.Read<int>("InvoiceId"),
				ReferenceNumber = reader.Read<string>("ReferenceNumber"),
				InvoiceReferenceNumber = reader.Read<string>("InvoiceReferenceNumber"),
				CreatedDate = reader.Read<DateTime>("CreatedDate"),
				ClientRefCode = reader.Read<string>("ClientRef"),
				AccountName = reader.Read<string>("AccountName"),
				OrderNumber = reader.Read<string>("OrderNumber"),
				Currency = reader.Read<string>("Currency"),
				ConversionRate = reader.Read<decimal?>("ConversionRate"),
				TransactionId = reader.Read<string>("TransactionId"),
				CustomerRefCode = reader.Read<string>("CustomerRef"),
				SubTotal =  reader.Read<decimal>("SubTotal"),
				TaxValue = reader.Read<decimal>("TaxValue"),
				TaxPercentage = reader.Read<decimal>("TaxPercentage"),
				Total = reader.Read<decimal>("Total"),
				IsInternational = reader.Read<bool>("IsInternational"),
				IsPostPaid = reader.Read<bool>("IsPostPaid")
			};

			//Conversion rates of zero-to-one or one-to-one indicates no conversion
			if (creditNote.ConversionRate == 1 || creditNote.ConversionRate == 0)
			{
				creditNote.ConversionRate = null;
			}
			
			creditNote.LineItems = GetLineItems(creditNote.InvoiceId);

			return creditNote;
		}

		private List<DocumentLineItemModel> GetLineItems(int invoiceId)
		{
			var lineItems = new List<DocumentLineItemModel>();
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(GetInvoiceLineItems, connection))
			{
				command.Parameters.AddWithValue("@InvoiceId", invoiceId);

				var reader = command.ExecuteReader();

				while (reader.Read())
				{
					var lineItem = new DocumentLineItemModel
					{
						Description = reader.Read<string>("Description"),
						Quantity = reader.Read<int>("Quantity"),
						Price = reader.Read<decimal>("Price"),
						Total = reader.Read<decimal>("Total")
					};
					lineItems.Add(lineItem);
				}
			}
			return lineItems;
		}
	}
}
