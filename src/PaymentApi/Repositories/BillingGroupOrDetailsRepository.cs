﻿using System;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class BillingGroupOrDetailsRepository : IBillingGroupOrDetailsRepository
    {
        private readonly IPaymentAccountRepository paymentAccountRepository;
        private readonly IBillingDetailsRepository legacyBillingDetailsRepository;
        private readonly IBillingGroupRepository billingGroupRepository;

        public BillingGroupOrDetailsRepository(
            IPaymentAccountRepository paymentAccountRepository,
            IBillingDetailsRepository legacyBillingDetailsRepository,
            IBillingGroupRepository billingGroupRepository)
        {
            this.paymentAccountRepository = paymentAccountRepository;
            this.legacyBillingDetailsRepository = legacyBillingDetailsRepository;
            this.billingGroupRepository = billingGroupRepository;
        }
        
        public bool TryGet(AccountKey accountKey, out BillingDetails billingDetails)
        {
            var account = paymentAccountRepository.GetPaymentAccount(accountKey);
            var bgId = account.BillingGroupId;
            
            if (bgId.HasValue)
            {
                billingDetails = billingGroupRepository.Get(bgId.Value).ToBillingDetails();
                return true;
            }

            return legacyBillingDetailsRepository.TryGetBillingDetails(accountKey, out billingDetails);
        }

        public BillingDetails Get(AccountKey accountKey)
        {
            if(TryGet(accountKey, out var billingDetails))
            {
                return billingDetails;
            }
            throw new EntityNotFoundException(typeof(BillingDetails), accountKey.ToString());
        }
    }
}