﻿using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IQuoteRepository
    {
        long GetNextQuoteNumber(int siteId);
        int Add(Invoice invoice);
        void AddPdf(int quoteId, byte[] pdfBytes);
        Invoice ConvertToInvoice(string accountType, int accountId, int quoteId, string invoiceNr, string orderNumber, string status);
        byte[] GetPdf(string reference);
        Invoice Get(string reference);
        IEnumerable<Invoice> GetAllSmsAccount(DateTime? startDate, DateTime? endDate, string reference = null);
        IEnumerable<Invoice> GetAllReseller(DateTime? startDate, DateTime? endDate, string reference = null);
    }
}
