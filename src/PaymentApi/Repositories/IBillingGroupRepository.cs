﻿using System.Collections;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IBillingGroupRepository
    {
        List<BillingGroup> GetAll();
        BillingGroup GetByPastelCode(string clientRefCode);
        BillingGroup Get(int id);
        int Add(BillingGroup billingGroup);
        void Update(BillingGroup billingGroup);
        void Delete(int id);
    }
}