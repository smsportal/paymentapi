using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IRefundAccountRepository
    {
        RefundAccount Get(AccountKey accountKey);
    }
}