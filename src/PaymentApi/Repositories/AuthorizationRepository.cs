﻿using SmsPortal.Core;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class AuthorizationRepository: BaseSqlRepository, IAuthorizationRepository
    {
        private const string AddEftAuthorizationSp = "Site_EFT_Payment";

        public AuthorizationRepository(string connectionString): base(connectionString)
        {

        }

        public void Add(EftAuthorization auth)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(AddEftAuthorizationSp, connection))
            {
                command.Parameters.AddWithValue("@SiteId", auth.SiteId);
                command.Parameters.AddWithValue("@AccountType", auth.AccountType.ToKey());
                command.Parameters.AddWithValue("@AccountId", auth.AccountId);
                command.Parameters.AddWithValue("@Amount", auth.Credits);
                command.Parameters.AddWithValue("@RouteCountryID", auth.RouteCountryId);
                command.Parameters.AddWithValue("@ClientCost", auth.Total);
                command.Parameters.AddWithValue("@Ref", auth.TransactionId.ToString());
                command.Parameters.AddWithValue("@PaymentCurrency", auth.Currency);
                command.Parameters.AddWithValue("@VatAmount", auth.Vat);
                command.Parameters.AddWithValue("@BaseCurrencyCost", auth.BaseCurrencyCost);
                command.Parameters.AddWithValue("@BaseCurrency", auth.BaseCurrency);
                command.Parameters.AddWithValue("@BaseVatAmount", auth.BaseVat);
                command.Parameters.AddWithValue("@TransactionFee", auth.TransactionFee);
                command.Parameters.AddWithValue("@BaseTransactionFee", auth.TransactionFee);
                command.ExecuteNonQuery();
            }
        }
    }
}
