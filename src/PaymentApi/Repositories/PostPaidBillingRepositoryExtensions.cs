﻿using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using SmsPortal.Core;
using SmsPortal.Core.DatabaseExtensions;

namespace SmsPortal.Plutus.Repositories
{
    internal static class PostPaidBillingRepositoryExtensions
    {
        public static PostPaidBillingCycleLineItem ReadLineItem(this SqlDataReader reader, DataTable schema)
        {
            var line = new PostPaidBillingCycleLineItem();
            line.SiteId = reader.Read(schema, "SiteId", 1);
            line.AccountKey = new AccountKey(
                reader.Read(schema, "AccountId", 0),
                reader.Read(schema, "AccountType", "MA").ToEnum<AccountType>());
            line.ClientRefCode = reader.Read(schema, "ClientRefCode", "");
            line.Username = reader.Read(schema, "Username", "");
            line.Fullname = reader.Read(schema, "Fullname", "");
            line.Company = reader.Read(schema, "Company", "");
            line.Quantity = reader.Read(schema, "Quantity", 0);
            line.UnitPrice = reader.Read<decimal>(schema, "UnitPrice", 0);
            line.PaymentCurrency = reader.Read(schema, "PaymentCurrency", "ZAR");
            line.Description = reader.Read(schema, "Description", "Bulk SMS");
            line.Code = reader.Read(schema, "Code", InvoiceLineItemType.BulkSms);
            line.BillingType = reader.Read(schema, "BillingType", LineItemBreakdownType.Unknown);
            line.BillingCycle = reader.Read(schema, "BillingCycle", BillingPeriod.None);
            line.CycleStartDate = reader.Read(schema, "CycleStartDate", DateTime.Today);
            line.CycleEndDate = reader.Read(schema, "CycleEndDate", DateTime.Today);
            line.CountryId = reader.Read(schema, "CountryId", 0);
            line.MsgCount = reader.Read(schema, "MsgCount", 0);
            line.OptionalNote = reader.Read(schema, "OptionalNote", "");
            var dataId = reader.Read<long>(schema, "DataId", 0);
            line.DataId = dataId == 0 ? null : (long?)dataId;
            return line;
        }

        public static IEnumerable<ProformaInvoice> ToProformaInvoices(
            this IEnumerable<PostPaidBillingCycleLineItem> allLines,
            TaxConfig taxConfig,
            InvoiceLineItemType lineItemType,
            LineItemBreakdownType billType)
        {
            return allLines.ToProformaInvoices(taxConfig, new[] { lineItemType }, billType);
        }

        public static IEnumerable<ProformaInvoice> ToProformaInvoices(
            this IEnumerable<PostPaidBillingCycleLineItem> allLines,
            TaxConfig taxConfig,
            IEnumerable<InvoiceLineItemType> lineItemTypes,
            LineItemBreakdownType billType)
        {
            var invoiceLineItemTypes = lineItemTypes as InvoiceLineItemType[] ?? lineItemTypes.ToArray();
            return allLines
                .OrderBy(l => l.ClientRefCode)
                .ThenBy(l => l.AccountId)
                .Where(l => l.BillingType == billType && invoiceLineItemTypes.Contains(l.Code))
                .ToProformaInvoices(taxConfig, billType)
                .ToArray();
        }

        public static IEnumerable<ProformaInvoice> ToProformaInvoices(
            this IEnumerable<PostPaidBillingCycleLineItem> lines,
            TaxConfig taxConfig,
            LineItemBreakdownType billType)
        {
            var invoices
              = lines.GroupBy(
                  l => new {
                      RefCode = 
                          billType == LineItemBreakdownType.PerRefCodeByMasterAccountUsername 
                              ? l.ClientRefCode 
                              : l.AccountKey.Id.ToString(),
                      CostCentre = 
                          billType == LineItemBreakdownType.PerCostCentrePerCampaignByCountryAndMessageType 
                              ? l.Username 
                              : string.Empty,
                      Campaign = 
                          billType == LineItemBreakdownType.PerCostCentrePerCampaignByCountryAndMessageType 
                              ? l.OptionalNote 
                              : string.Empty
                  },
                  l => l,
                  (key, lns) =>
                  {
                      var linesForThisInvoice = lns as PostPaidBillingCycleLineItem[] ?? lns.ToArray();
                      var firstLineOfThisInvoice = linesForThisInvoice.First();
                      
                      var cycle = new PostPaidBillingCycle()
                      {
                          LineItemBreakdown = billType,
                          LineItemType =
                              billType == LineItemBreakdownType.PerCostCentrePerCampaignByCountryAndMessageType
                              ? InvoiceLineItemType.BulkSms 
                              : firstLineOfThisInvoice.Code,
                          BillingPeriodType = firstLineOfThisInvoice.BillingCycle,
                          CycleStartDate = firstLineOfThisInvoice.CycleStartDate,
                          CycleEndDate = firstLineOfThisInvoice.CycleEndDate,
                          MsgCount = linesForThisInvoice.Sum(l => l.MsgCount)
                      };
                      var inv = new ProformaInvoice
                      {
                          Id = 0,
                          IsInternational = firstLineOfThisInvoice.CountryId != taxConfig.CountryId,
                          TaxPercentage = firstLineOfThisInvoice.CountryId != taxConfig.CountryId ? 0 : taxConfig.TaxPercentage,
                          TaxType = firstLineOfThisInvoice.CountryId != taxConfig.CountryId ? "No Tax" : taxConfig.TaxType,
                          LoginId = (firstLineOfThisInvoice.AccountType == AccountType.ResellerAccount ? 0 : firstLineOfThisInvoice.AccountId),
                          ResellerId = (firstLineOfThisInvoice.AccountType == AccountType.ResellerAccount ? firstLineOfThisInvoice.AccountId : 0),
                          AccountName = firstLineOfThisInvoice.Company,
                          CustomerRef = string.IsNullOrEmpty(key.CostCentre) ? firstLineOfThisInvoice.Username : key.CostCentre,
                          PastelCode = firstLineOfThisInvoice.ClientRefCode,
                          Currency =  firstLineOfThisInvoice.PaymentCurrency,
                          InvoiceNr = $"Proforma Invoice for {cycle.CycleStartDate:dd MMM yyyy} - {cycle.CycleEndDate:dd MMM yyyy}",
                          Status = $"Proforma {cycle.BreakdownDescription} {cycle.ItemDescription}",
                          PurchaseOrderNr = cycle.ItemDescription,
                          BillingAddress = new BillingDetails()
                          {
                              CompanyName = firstLineOfThisInvoice.Company,
                              CountryId = firstLineOfThisInvoice.CountryId
                          },
                          Created = cycle.InvoiceDate,
                          DueDate = cycle.InvoiceDate.AddDays(7).Date,
                          Cycle = cycle
                      };
                      if (billType == LineItemBreakdownType.PerCostCentrePerCampaignByCountryAndMessageType)
                      {
                          inv.AddLineItem(new InvoiceLineItem
                          {
                              Code = InvoiceLineItemType.BulkSms,
                              Description = $"Cost Centre: {key.CostCentre}",
                              Quantity = 0,
                              UnitPrice = 0,
                              OptionalNote = firstLineOfThisInvoice.OptionalNote
                          });
                      }
                      foreach (var ln in linesForThisInvoice)
                      {
                          inv.AddLineItem(new InvoiceLineItem
                          {
                              Code = ln.Code,
                              Description = ln.Description,
                              Quantity = ln.Quantity,
                              UnitPrice = ln.UnitPrice,
                              OptionalNote =
                                  (billType == LineItemBreakdownType.PerCostCentrePerCampaignByCountryAndMessageType) 
                                      ? string.Empty 
                                      : ln.OptionalNote,
                              DataId = ln.DataId
                          });
                      }
                      return inv;
                  });
            return invoices;
        }
    }
}
