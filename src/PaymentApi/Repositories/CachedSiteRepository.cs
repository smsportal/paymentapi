﻿using Microsoft.Extensions.Caching.Memory;
using SmsPortal.Plutus.Models;
using System;

namespace SmsPortal.Plutus.Repositories
{
    public class CachedSiteRepository: ISiteRepository
    {
        private readonly ISiteRepository repository;
        private IMemoryCache cache = new MemoryCache(new MemoryCacheOptions());
        private TimeSpan cacheValidityPeriod = TimeSpan.FromMinutes(1);

        public CachedSiteRepository(ISiteRepository repository, TimeSpan validityPeriod)
        {
            this.repository = repository;
            this.cacheValidityPeriod = validityPeriod;
        }

        public Site Get(int id)
        {
            return cache.GetOrCreate(id, entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = cacheValidityPeriod;
                return repository.Get(id);
            });
        }
    }
}
