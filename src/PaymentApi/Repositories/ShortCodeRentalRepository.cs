﻿using System;
using System.Collections.Generic;
using SmsPortal.Core.DatabaseExtensions;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class ShortCodeRentalRepository : BaseSqlRepository, IShortCodeRentalRepository
    {
        private const string AllShortCodeRentalsSp = "Plutus_ShortCode_GetRentalsV2";

        public ShortCodeRentalRepository(string connectionString) 
            : base(connectionString)
        {
        }
        
        public IEnumerable<ShortCodeRentalApplicability> GetAllShortCodeRentalApplicability(DateTime invoiceDate)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(AllShortCodeRentalsSp, connection))
            {
                command.Parameters.AddWithValue("@InvoiceDate", invoiceDate.Date);
                command.CommandTimeout = 60;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var rental = new ShortCodeRentalApplicability
                        {
                            AllocationId = reader.Read<int>("AllocationId"),
                            SiteId = reader.Read<int?>("SiteId"),
                            LoginId = reader.Read<int?>("LoginId"),
                            ShortCode = reader.Read<long>("ShortCode"),
                            Keyword = reader.Read<string>("Keyword"),
                            CycleStartDate = reader.Read<DateTime>("CycleStartDate"),
                            CycleEndDate = reader.Read<DateTime>("CycleEndDate"),
                            ConfigurationStartDate = reader.Read<DateTime>("ConfigurationStartDate"),
                            ConfigurationEndDate = reader.Read<DateTime>("ConfigurationEndDate")
                        };
                        yield return rental;
                    }
                }
            }
        }
    }
}
