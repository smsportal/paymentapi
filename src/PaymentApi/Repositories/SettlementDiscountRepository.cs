﻿using SmsPortal.Core.DatabaseExtensions;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class SettlementDiscountRepository : BaseSqlRepository, ISettlementDiscountRepository
    {
        private const string GetByPastelCodeSp = "Plutus_SettlementDiscount_GetByPastelCode_V1";

        public SettlementDiscountRepository(string connectionString) 
            : base(connectionString)
        {
        }
        
        public SettlementDiscount GetByPastelCode(string clientRefCode)
        {
            using(var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetByPastelCodeSp, connection))
            {
                command.Parameters.AddWithValue("@PastelCode", clientRefCode);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return new SettlementDiscount
                        {
                            DiscountPercentage = reader.Read<decimal>("DiscountPercentage"),
                            SettlementDays = reader.Read<int>("SettlementDays")
                        };
                    }
                }
            }
            return null;
        }
    }
}