﻿using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using System;
using System.Collections.Generic;
using SmsPortal.Core;

namespace SmsPortal.Plutus.Repositories
{
    public class PostPaidBillingCycleInvoiceLineRepository : BaseSqlRepository, IPostPaidBillingCycleInvoiceLineRepository
    {
        private const string GetPostPaidBillingCycleLinesSp = "Plutus_Invoice_GetPostPaidBillingCycleLines";
        private const string GetPostPaidReverseBilledShortCodeLinesSp = "Plutus_Invoice_GetPostPaidReverseBilledShortCodeLines";
        private const string GetTrafficSummary = "Plutus_GetTrafficSummary_V1";     
        
        public PostPaidBillingCycleInvoiceLineRepository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<PostPaidBillingCycleLineItem> GetLineItems(
            AccountKey? accountKey,
            DateTime? invoiceDate,
            InvoiceLineItemType? lineItemType)
        {
            var sql = lineItemType == InvoiceLineItemType.ReverseBilledShortCodeSms
                ? GetPostPaidReverseBilledShortCodeLinesSp
                : GetPostPaidBillingCycleLinesSp;
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(sql, connection))
            {
                if (accountKey.HasValue)
                {
                    command.Parameters.AddWithValue("@AccountId", accountKey.Value.Id);
                    command.Parameters.AddWithValue("@AccountType", accountKey.Value.Type.ToKey());
                }

                if (invoiceDate.HasValue)
                {
                    command.Parameters.AddWithValue("@InvoiceDate", invoiceDate.Value);
                }

                if (lineItemType.HasValue)
                {
                    command.Parameters.AddWithValue("@InvoiceType", lineItemType.Value);
                }

                command.CommandTimeout = 15 * 60;
                using (var reader = command.ExecuteReader())
                {
                    var schema = reader.GetSchemaTable();
                    while (reader.Read())
                    {
                        yield return reader.ReadLineItem(schema);
                    }
                }
            }
        }              
    }
}
