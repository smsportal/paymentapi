﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using log4net;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Services;

namespace SmsPortal.Plutus.Repositories
{
	public class LegacyInvoiceRepository : BaseSqlRepository, ILegacyInvoiceRepository
	{
		private readonly string InvoiceUpdateBytes = "Login_Invoice_UpdateBytes";
		private readonly string InvoicePdfGet = "Invoice_Pdf_Get";
		private readonly string InvoiceCreateSp = "Login_Invoice_CreateV2";
		private readonly string InvoiceLineItemGet = "Login_InvoiceLineItem_List";
		private readonly string InvoiceCheckDuplicateSp = "Invoice_CheckDuplicates";
		private readonly string InvoiceStatusUpdateSp = "Login_Invoice_UpdateStatusV2";
		private readonly string InvoiceLineItemInsert = "Login_InvoiceLineItem_Insert";
		private readonly string InvoiceUpdateOrderNumberSp = "Invoice_UpdateOrderNumber";
		private readonly string InvoiceReferenceNumberGet = "Site_InvoiceReferenceNumber_Get";
		private readonly string InvoiceGetByTransactionId = "Login_Invoice_GetByTransactionIdV2";
		private readonly string InvoiceUpdateCreditAllocation = "Site_Invoice_Update_CreditAllocation_SiteV2";
		private readonly string DeclinePayment = "CC_Payment_DECLINEV4";

		private readonly ICurrencyRepository currencyRepository;
		private readonly IBillingGroupOrDetailsRepository billingDetailsRepository;
		private readonly ICreditNoteRepository creditNoteRepository;

		private readonly IEmailService emailService;
		private readonly ISiteRepository siteRepository;

		private const int SouthAfricaId = 156;

		private readonly ILog log = LogManager.GetLogger(typeof(LegacyInvoiceRepository));

		// Only SMSPortal has the billing enabled. Always use SMSPortal's details for invoices and emails.
		private const int SmsPortalSiteId = 1;

		public LegacyInvoiceRepository(string connectionString,
			ICurrencyRepository currencyRepository,
			IBillingGroupOrDetailsRepository billingDetailsRepository,
			ICreditNoteRepository creditNoteRepository,
			IEmailService emailService,
            ISiteRepository siteRepository) : base(connectionString)
		{
			this.currencyRepository = currencyRepository;
			this.billingDetailsRepository = billingDetailsRepository;
			this.creditNoteRepository = creditNoteRepository;
			this.emailService = emailService;
            this.siteRepository = siteRepository;
        }

		public InvoiceModel CreateInvoice(PaymentAccount paymentAccount, LegacyInvoice legacyInvoice)
		{
			try
			{
				InvoiceModel invoice;
				if (CheckDuplicateInvoiceExists(legacyInvoice.TransactionId))
				{
					invoice = LoadInvoice(paymentAccount, legacyInvoice.TransactionId);
				}
				else
				{
					invoice = InsertInvoice(paymentAccount, legacyInvoice);
				}

				UpdatePaymentInvoice(paymentAccount, invoice.TransactionId);

				var param = new CreateBillingDocumentParameter
				{
					BillingDocument = invoice
				};

				// TODO: make sure that this code did full away and the below is in fact no longer needed
				//pdfService.CreateInvoicePdfWork(param);

				return invoice;
			}
			catch (Exception e)
			{
				log.Exception($"Error creating invoice for account {paymentAccount.Username}", e);
				throw;
			}
		}

		public void CloseInvoice(PaymentAccount account, string transactionId, bool sendInvoiceEmail = false, string paymentReference = "")
		{
			var result = UpdateInvoiceStatus(account, transactionId, InvoiceStatus.Paid, paymentReference);
			UpdatePaymentInvoice(account, transactionId);

			var invoiceDebug = $"[Invoice: TransactionId={transactionId}]";
			try
			{
				if (result.Result == 1)
				{
					if (sendInvoiceEmail)
					{
						var invoice = LoadInvoice(account, transactionId);
						invoiceDebug = invoice.ToString();
						var invoicePdf = LoadInvoicePdf(transactionId);

						if (invoicePdf == null)
						{
							return;
						}

						invoice.InvoiceBytes = invoicePdf;
						emailService.SendPaidInvoiceEmail(invoice, SmsPortalSiteId);
					}
				}
				else
				{
					throw new InvalidStateException(
						"Could not close invoice",
						result.OriginalStatus,
						InvoiceStatus.Paid);
				}
			}
			catch (Exception e)
			{
				log.Exception(invoiceDebug, e);
				throw;
			}
		}

		public CreditNoteModel CancelInvoice(PaymentAccount paymentAccount, string transactionId)
		{
			return CancelInvoice(paymentAccount, transactionId, InvoiceStatus.Cancelled);
		}

		public CreditNoteModel ExpireInvoice(PaymentAccount paymentAccount, string transactionId)
		{
			return CancelInvoice(paymentAccount, transactionId, InvoiceStatus.Expired);
		}

		private CreditNoteModel CancelInvoice(PaymentAccount paymentAccount, string transactionId, InvoiceStatus status)
		{
			if (status != InvoiceStatus.Expired && status != InvoiceStatus.Cancelled)
			{
				throw new Exception("CancelInvoice - Status must be either Cancelled or Expired");
			}

			var invoice = new InvoiceModel();
			var originalStatus = InvoiceStatus.Unpaid;
			var invoiceDebug = $"[Invoice: TransactionId={transactionId}]";
			try
			{
				invoice = LoadInvoice(paymentAccount, transactionId);
				invoiceDebug = invoice.ToString();
				originalStatus = invoice.Status;
				invoice.Status = status;

				var billingDetails = billingDetailsRepository.Get(paymentAccount.AccountKey);
				var creditNote = CreditNoteModel.CreateCreditNoteModel(invoice, currencyRepository, billingDetails);
				var updateInvoiceResult = UpdateInvoiceStatus(paymentAccount, transactionId, invoice.Status);
				if (updateInvoiceResult.Result == 1)
				{
					var site = siteRepository.Get(paymentAccount.BuysFromSite);

					creditNote.ReferenceNumber = site.GetCreditNoteNr(creditNoteRepository.GetNextCreditNoteReferenceNumber(site.Id));
					creditNote = creditNoteRepository.InsertCreditNote(paymentAccount, creditNote);

					if (status == InvoiceStatus.Cancelled)
					{
						DeclineAuthRecord(paymentAccount, transactionId, "Cancelled");
					}
					else
					{
						DeclineAuthRecord(paymentAccount, transactionId, "Expired");
					}

					return creditNote;
				}

				throw new InvalidStateException(
					"Could not close invoice", 
					originalStatus, 
					status);
			}
			catch (Exception e)
			{
				log.Exception(invoiceDebug, e);
				throw;
			}
		}

		public UpdatePaymentInvoiceResult UpdatePaymentInvoice(PaymentAccount paymentAccount, string transactionId)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(InvoiceUpdateCreditAllocation, connection))
			{
				command.Parameters.AddWithValue("@SiteId", paymentAccount.SiteId);
				
				command.Parameters.AddWithValue("@AccountType", paymentAccount.AccountType.ToKey());
				
				command.Parameters.AddWithValue("@AccountId", paymentAccount.Id);
				command.Parameters.AddWithValue("@Ref", transactionId);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return new UpdatePaymentInvoiceResult
						{
							Result = (int) reader["result"],
							OriginalStatus = (string) reader["originalStatus"]
						};
					}
				}
			}
			return null;
		}

		public UpdatePaymentInvoiceResult UpdateInvoiceStatus(PaymentAccount paymentAccount, string transactionId, InvoiceStatus invoiceStatus, string paymentReference = "")
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(InvoiceStatusUpdateSp, connection))
			{
				command.Parameters.AddWithValue("@TransactionId", transactionId);
				command.Parameters.AddWithValue("@AccountId", paymentAccount.Id);
				command.Parameters.AddWithValue("@Status", invoiceStatus.ToString());
				command.Parameters.AddWithValue("@AccountType", paymentAccount.AccountType.ToKey());
				if (!string.IsNullOrEmpty(paymentReference))
				{
					command.Parameters.AddWithValue("@PaymentReference", paymentReference);
				}

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						if (!reader.HasRows)
						{
							throw new Exception($"Unable to update invoice with transactionid {transactionId} for login {paymentAccount.Username} to state {invoiceStatus}");
						}

						return new UpdatePaymentInvoiceResult
						{
							Result = (int) reader["result"],
							OriginalStatus = (string) reader["originalStatus"]
						};
					}
				}
			}
			return null;
		}

		public InvoiceModel LoadInvoice(PaymentAccount paymentAccount, string transactionId)
		{
			var invoice = new InvoiceModel();
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(InvoiceGetByTransactionId, connection))
			{
				command.Parameters.AddWithValue("@AccountId", paymentAccount.Id);
				command.Parameters.AddWithValue("@TransactionId", transactionId);
				command.Parameters.AddWithValue("@AccountType", paymentAccount.AccountType.ToKey());

				using (var reader = command.ExecuteReader())
				{
					//If invoiceDt.Rows.Count <> 1 Then
					//	Throw New Exception(String.Format("Login_Invoice_GetByTransactionId returned the incorrect number of rows for invoice with transaction id {0}", transactionId))
					//End If

					if (reader.Read())
					{
						invoice.InvoiceId = (int) reader["Id"];
						invoice.ReferenceNumber = (string) reader["ReferenceNumber"];
						invoice.AccountName = (string) reader["AccountName"];
						invoice.OrderNumber = (string) reader["OrderNumber"];
						invoice.ClientRefCode = (string) reader["ClientRef"];
						invoice.CustomerRefCode = (string) reader["CustomerRef"];
						invoice.CreatedDate = (DateTime) reader["CreatedDate"];
						invoice.DueDate = (DateTime) reader["DueDate"];
						invoice.TaxValue = Convert.ToDecimal(reader["TaxValue"]);
						invoice.TaxPercentage = Convert.ToDecimal(reader["TaxPercentage"]);
						invoice.Total = Convert.ToDecimal(reader["Total"]);
						invoice.SubTotal = Convert.ToDecimal(reader["SubTotal"]);
						invoice.PeriodBilledFromDate = reader["PeriodBilledFromDate"] == DBNull.Value
							? DateTime.Now
							: (DateTime) reader["PeriodBilledFromDate"];
						invoice.PeriodBilledToDate = reader["PeriodBilledToDate"] == DBNull.Value
							? DateTime.Now
							: (DateTime) reader["PeriodBilledToDate"];
						var status = (string)reader["Status"];
						if (status.StartsWith("PostPaid", StringComparison.OrdinalIgnoreCase))
						{
							invoice.Status = InvoiceStatus.MonthlyBilling;
						}
						else if (status.IndexOf("Declined", StringComparison.OrdinalIgnoreCase) >= 0)
						{
							invoice.Status = InvoiceStatus.UnpaidSagePay;
						}
						else if(Enum.TryParse(status, true, out InvoiceStatus invoiceStatus))
						{
							invoice.Status = invoiceStatus;
						}
						else
						{
							throw new ArgumentOutOfRangeException(nameof(status),
								$"Could not resolve '{status}' to an InvoiceStatus");
						}

						var billingCycleApplicationType = ((int)reader["BillingCycleApplicationType"]).ToString();
						if (Enum.TryParse(billingCycleApplicationType, true, out InvoiceType invoiceType))
						{
							invoice.InvoiceType = invoiceType;
						}
						else
						{
							throw new ArgumentOutOfRangeException(nameof(status),
								$"Could not resolve '{billingCycleApplicationType}' to an InvoiceType");
						}

						invoice.Currency = ((string) reader["Currency"]).Trim();

						var converter = currencyRepository.GetCurrencyConverter(invoice.Currency, "ZAR");
						var conversionRate = Math.Round(converter.Convert(1), 6);
						invoice.ConversionRate = conversionRate != 1 ? conversionRate.ToString() : string.Empty;

						invoice.ClientBillingDetails =
							billingDetailsRepository.Get(paymentAccount.AccountKey);
						invoice.TransactionId = transactionId;

						if (invoice.ClientBillingDetails.CountryId != SouthAfricaId)
						{
							invoice.IsInternational = true;
						}

						if (reader["PeriodBilledFromDate"] != DBNull.Value)
						{
							invoice.IsPostPaid = true;
						}

						invoice.LineItems = GetInvoiceLineItems(invoice.InvoiceId);
					}
				}
			}
			return invoice;
		}

		public byte[] LoadInvoicePdf(string transactionId)
		{
			using (var connecion = GetOpenConnection())
			using (var command = GetStoredProcedure(InvoicePdfGet, connecion))
			{
				command.Parameters.AddWithValue("@TransactionId", transactionId);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (byte[]) reader["InvoiceData"];
					}
				}
				return null;
			}
		}

		public string GetInvoiceReferenceNumber(PaymentAccount paymentAccount)
		{
			var site = siteRepository.Get(paymentAccount.SiteId);
			using (var connecion = GetOpenConnection())
			using (var command = GetStoredProcedure(InvoiceReferenceNumberGet, connecion))
			{
				command.Parameters.AddWithValue("@SiteId", paymentAccount.SiteId);
				command.Parameters.Add(new SqlParameter
				{
					ParameterName = "@InvoiceReferenceNumber",
					SqlDbType = SqlDbType.BigInt,
					Direction = ParameterDirection.Output
				});

				command.ExecuteReader();

				var referenceNumber = command.Parameters["@InvoiceReferenceNumber"].Value;

				return $"{site.InvoicePrefix}_{referenceNumber:D7}"; //refNumber.ToString("D7")
			}
		}

		public InvoiceModel InsertInvoice(PaymentAccount paymentAccount, LegacyInvoice legacyInvoice)
		{
			int invoiceId;
			var billingDetails = billingDetailsRepository.Get(paymentAccount.AccountKey);

			if (string.IsNullOrEmpty(legacyInvoice.OrderNumber))
			{
				legacyInvoice.OrderNumber = billingDetails.OrderNumber;
			}

			var referenceNumber = GetInvoiceReferenceNumber(paymentAccount);

			var isInternational = billingDetails.CountryId != SouthAfricaId;

			var converter = currencyRepository.GetCurrencyConverter(legacyInvoice.Currency, "ZAR");
			var zarAmountTotal = Math.Round(converter.Convert(legacyInvoice.Total), 2);
			var zarAmountSubTotal = Math.Round(converter.Convert((decimal)legacyInvoice.SubTotal), 2);
			var conversionRate = Math.Round(converter.Convert(1), 6);

			using (var connecion = GetOpenConnection())
			using (var command = GetStoredProcedure(InvoiceCreateSp, connecion))
			{
				command.Parameters.AddWithValue("@ReferenceNumber", referenceNumber);
				command.Parameters.AddWithValue("@AccountName", legacyInvoice.AccountName);
				command.Parameters.AddWithValue("@OrderNumber", legacyInvoice.OrderNumber);
				command.Parameters.AddWithValue("@ClientRef", legacyInvoice.ClientRefCode);
				command.Parameters.AddWithValue("@CustomerRef", legacyInvoice.CustomerRef);
				command.Parameters.AddWithValue("@CreatedDate", legacyInvoice.CreatedDate);
				command.Parameters.AddWithValue("@DueDate", legacyInvoice.DueDate);
				command.Parameters.AddWithValue("@TaxPercentage", legacyInvoice.TaxPercentage);
				command.Parameters.AddWithValue("@TaxValue", legacyInvoice.TaxValue);
				command.Parameters.AddWithValue("@Total", legacyInvoice.Total);
				command.Parameters.AddWithValue("@SubTotal", legacyInvoice.SubTotal);
				command.Parameters.AddWithValue("@International", isInternational);
				command.Parameters.AddWithValue("@Status", legacyInvoice.Status.ToString());
				command.Parameters.AddWithValue("@Currency", legacyInvoice.Currency);
				command.Parameters.AddWithValue("@ZarTotal", zarAmountTotal);
				command.Parameters.AddWithValue("@ZarSubTotal", zarAmountSubTotal);
				command.Parameters.AddWithValue("@TransactionId", legacyInvoice.TransactionId);
				command.Parameters.AddWithValue("@PeriodBilledFromDate", legacyInvoice.PeriodBilledFromDate);
				command.Parameters.AddWithValue("@PeriodBilledToDate", legacyInvoice.PeriodBilledToDate);
				command.Parameters.Add(new SqlParameter
				{
					ParameterName = "@InvoiceId",
					SqlDbType = SqlDbType.Int,
					Direction = ParameterDirection.Output
				});

				command.ExecuteReader();
				invoiceId = (int) command.Parameters["@InvoiceId"].Value;
			}

			var invoice = new InvoiceModel
			{
				InvoiceId = invoiceId,
				ReferenceNumber = referenceNumber,
				AccountName = legacyInvoice.AccountName,
				OrderNumber = legacyInvoice.OrderNumber,
				ClientRefCode = legacyInvoice.ClientRefCode,
				CustomerRefCode = legacyInvoice.CustomerRef,
				CreatedDate = legacyInvoice.CreatedDate,
				DueDate = legacyInvoice.DueDate,
				TaxValue = legacyInvoice.TaxValue,
				TaxPercentage = legacyInvoice.TaxPercentage,
				Total = legacyInvoice.Total,
				SubTotal = legacyInvoice.SubTotal,
				Status = legacyInvoice.Status,
				Currency = legacyInvoice.Currency,
				ConversionRate = conversionRate != 1 ? conversionRate.ToString() : string.Empty,
				TransactionId = legacyInvoice.TransactionId,
				IsInternational = legacyInvoice.IsInternational,
				IsPostPaid = legacyInvoice.IsPostPaid,
				ShowBillingPeriod = legacyInvoice.ShowBillingPeriod,
				PeriodBilledFromDate = legacyInvoice.PeriodBilledFromDate,
				PeriodBilledToDate = legacyInvoice.PeriodBilledToDate,
				InvoiceType = legacyInvoice.InvoiceType
			};

			foreach (var lineItem in legacyInvoice.LineItems)
			{
				InsertInvoiceLineItem(invoiceId, lineItem);
				invoice.LineItems.Add(new DocumentLineItemModel
				{
					Total = legacyInvoice.Total,
					Description = lineItem.Description,
					Quantity = lineItem.Quantity,
					Price = lineItem.Price
				});
			}

			invoice.ClientBillingDetails = billingDetails;
			invoice.InvoiceEmail.EmailSubject = legacyInvoice.InvoiceLegacyEmailInfo.Subject;
			invoice.InvoiceEmail.EmailBody = legacyInvoice.InvoiceLegacyEmailInfo.Body;
			invoice.InvoiceEmail.EmailRecipient = legacyInvoice.InvoiceLegacyEmailInfo.Recipient;
			return invoice;
		}

		public void InsertInvoiceLineItem(int invoiceId, LegacyInvoiceLineItem lineItem)
		{
			using (var connecion = GetOpenConnection())
			using (var command = GetStoredProcedure(InvoiceLineItemInsert, connecion))
			{
				command.Parameters.AddWithValue("@InvoiceId", invoiceId);
				command.Parameters.AddWithValue("@Code", lineItem.Description);
				command.Parameters.AddWithValue("@Description", lineItem.Description);
				command.Parameters.AddWithValue("@Quantity", lineItem.Quantity);
				command.Parameters.AddWithValue("@Price", lineItem.Price);
				command.Parameters.AddWithValue("@Total", lineItem.Total);
				command.ExecuteNonQuery();
			}
		}

		public bool CheckDuplicateInvoiceExists(string transactionId)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(InvoiceCheckDuplicateSp, connection))
			{
				command.Parameters.AddWithValue("@TransactionId", transactionId);

				using (var reader = command.ExecuteReader())
				{
					return reader.HasRows;
				}
			}
		}

		public InvoiceModel UpdateInvoicePoNumber(PaymentAccount account, string transactionId, string orderNumber)
		{
			var invoice = LoadInvoice(account, transactionId);
			if (invoice.InvoiceId > 0)
			{
				using (var connection = GetOpenConnection())
				using (var command = GetStoredProcedure(InvoiceUpdateOrderNumberSp, connection))
				{
					command.Parameters.AddWithValue("@TransactionId", transactionId);
					command.Parameters.AddWithValue("@OrderNumber", orderNumber);
					command.ExecuteNonQuery();
				}

				invoice.OrderNumber = orderNumber;
			}
			return invoice;
		}

		public void DeclineAuthRecord(PaymentAccount paymentAccount, string transactionId, string reason)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(DeclinePayment, connection))
			{
				command.Parameters.AddWithValue("@SiteID", paymentAccount.SiteId);
				command.Parameters.AddWithValue("@TransactionId", transactionId);
				command.Parameters.AddWithValue("@AccountType", paymentAccount.AccountType.ToKey());
				command.Parameters.AddWithValue("@Reason", reason);
				command.ExecuteNonQuery();
			}
		}

		public void SaveInvoiceBytes(InvoiceModel invoice)
		{
			if (invoice.InvoiceBytes != null && invoice.InvoiceBytes.Length > 0)
			{
				using (var connection = GetOpenConnection())
				using (var command = GetStoredProcedure(InvoiceUpdateBytes, connection))
				{
					command.Parameters.AddWithValue("@InvoiceId", invoice.InvoiceId);
					command.Parameters.Add(new SqlParameter
					{
						ParameterName = "@InvoiceBytes",
						Value = invoice.InvoiceBytes,
						SqlDbType = SqlDbType.Binary
					});

					command.ExecuteNonQuery();
				}
			}
		}

		private List<DocumentLineItemModel> GetInvoiceLineItems(int invoiceId)
		{
			var lineItems = new List<DocumentLineItemModel>();
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(InvoiceLineItemGet, connection))
			{
				command.Parameters.AddWithValue("@InvoiceId", invoiceId);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						lineItems.Add(new DocumentLineItemModel
							{
								Total = Convert.ToDecimal(reader["Total"]),
								Description = (string) reader["Description"],
								Price = Convert.ToDecimal(reader["Price"]),
								Quantity = (int) reader["Quantity"]
							});
					}
				}
			}
			return lineItems;
		}
	}
}
