using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;
using SmsPortal.Core.DatabaseExtensions;

namespace SmsPortal.Plutus.Repositories
{
    public class PaymentHistoryRepository: BaseSqlRepository, IPaymentHistoryRepository
    {
        private const string GetPaymentListSp = "Plutus_Payment_List";

        public PaymentHistoryRepository(string connectionString): base(connectionString)
        {
        }

        public IEnumerable<PaymentHistory> GetList(string accountType, int? accountId, string pastelCode,
            int? pageSize, int? pageNo)
        {
            var results = new List<PaymentHistory>();
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetPaymentListSp, connection))
            {
                if (accountId.HasValue)
                {
                    command.Parameters.AddWithValue("@AccountId", accountId.Value);
                    command.Parameters.AddWithValue("@AccountType", accountType);
                }
                else if (!string.IsNullOrEmpty(pastelCode))
                {
                    command.Parameters.AddWithValue("@PastelCode", pastelCode);
                }

                if (pageSize.HasValue)
                {
                    command.Parameters.AddWithValue("@PageSize", pageSize.Value);
                    if (pageNo.HasValue)
                    {
                        command.Parameters.AddWithValue("@PageNo", pageNo.Value);
                    }
                }

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var line = new PaymentHistory();
                        line.TransactionId = reader.Read<string>("TransactionId");
                        line.InvoiceId = reader.Read<int?>("InvoiceId");
                        line.InvoiceNr = reader.Read<string>("InvoiceNr");
                        line.Currency = reader.Read<string>("Currency");
                        line.TaxAmount = reader.Read<decimal>("TaxAmount");
                        line.Total = reader.Read<decimal>("Total");
                        line.Description = reader.Read<string>("Description");
                        line.Gateway = reader.Read<string>("Gateway");
                        line.GatewayTransactionId = reader.Read<string>("GatewayTransactionId");
                        line.Status = reader.Read<string>("Status");
                        line.Date = reader.Read<DateTime>("Date");
                        line.Credits = reader.Read<int?>("Credits");
                        line.RouteCountryId = reader.Read<int?>("RouteCountryId");
                        results.Add(line);
                    }
                }
            }

            return results;
        }
    }
}