﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
    public class BillingGroupRepository : BaseSqlRepository, IBillingGroupRepository
    {
        private readonly ICurrencyRepository currencyReop;
        private const string GetSp = "Plutus_BillingGroup_Get_V1";
        private const string GetByPastelCodeSp = "Plutus_BillingGroup_GetBy_PastelCode_V1";
        private const string GetAllSp = "Plutus_BillingGroup_List_V1";
        private const string AddSp = "Plutus_BillingGroup_Create_V1";
        private const string UpdateSp = "Plutus_BillingGroup_Update_V1";
        private const string DeleteSp = "Plutus_BillingGroup_Delete_V1";

        public BillingGroupRepository(string connectionString, ICurrencyRepository currencyReop) : base(connectionString)
        {
            this.currencyReop = currencyReop;
        }

        public List<BillingGroup> GetAll()
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetAllSp, connection))
            using(var reader = command.ExecuteReader())
            {
                var billingGroups = new List<BillingGroup>();
                while (reader.Read())
                {
                    billingGroups.Add(BuildBillingGroup(reader));
                }

                return billingGroups;
            }
        }

        public BillingGroup GetByPastelCode(string clientRefCode)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetByPastelCodeSp, connection))
            {
                command.Parameters.AddWithValue("@PastelCode", clientRefCode);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return BuildBillingGroup(reader);
                    }
                }

                throw new EntityNotFoundException(typeof(BillingGroup), clientRefCode);
            }
        }

        public BillingGroup Get(int id)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetSp, connection))
            {
                command.Parameters.AddWithValue("@Id", id);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return BuildBillingGroup(reader);
                    }
                }

                throw new EntityNotFoundException(typeof(BillingGroup), id);
            }
        }

        public int Add(BillingGroup billingGroup)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(AddSp, connection))
            {
                LoadBillingGroupParametersExceptId(command.Parameters, billingGroup);

                var billingGroupId = (int)command.ExecuteScalar();
                return billingGroupId;
            }
        }

        public void Update(BillingGroup billingGroup)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(UpdateSp, connection))
            {
                command.Parameters.AddWithValue("@Id", billingGroup.Id);
                LoadBillingGroupParametersExceptId(command.Parameters, billingGroup);
                if (command.ExecuteNonQuery() <= 0)
                {
                    throw new EntityNotFoundException(typeof(BillingGroup), billingGroup.Id);
                }
            }
        }

        public void Delete(int id)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(DeleteSp, connection))
            {
                command.Parameters.AddWithValue("@Id", id);
                if (command.ExecuteNonQuery() <= 0)
                {
                    throw new EntityNotFoundException(typeof(BillingGroup), id);
                }
            }
        }

        private static void LoadBillingGroupParametersExceptId(SqlParameterCollection parameters, BillingGroup bg)
        {
            parameters.AddWithValue("@PastelCode", bg.PastelCode);
            parameters.AddWithValue("@CompanyId", (object)bg.CompanyId ?? DBNull.Value);
            parameters.AddWithValue("@CompanyName", bg.CompanyName);
            parameters.AddWithValue("@PriceType", bg.PriceType);
            if (bg.Currency != null)
            {
                parameters.AddWithValue("@PriceFixedCurrency", bg.Currency.Code);    
            }
            else
            {
                parameters.AddWithValue("@PriceFixedCurrency", DBNull.Value);    
            }
            parameters.AddWithValue("@PriceListId", (object)bg.PriceListId ?? DBNull.Value);
            parameters.AddWithValue("@PriceFixedStd", (object)bg.PriceFixedStd ?? DBNull.Value);
            parameters.AddWithValue("@PriceFixedRb", (object)bg.PriceFixedRb ?? DBNull.Value);
            parameters.AddWithValue("@PriceFixedRbReply", (object)bg.PriceFixedRbReply ?? DBNull.Value);
            parameters.AddWithValue("@Address1", bg.Address1);
            parameters.AddWithValue("@Address2", (object)bg.Address2 ?? DBNull.Value);
            parameters.AddWithValue("@City",bg.City);
            parameters.AddWithValue("@StateProvince",bg.StateProvince);
            parameters.AddWithValue("@ZipPostalCode",bg.ZipPostalCode);
            parameters.AddWithValue("@CountryId",bg.CountryId);
            parameters.AddWithValue("@Email",bg.Email);
            parameters.AddWithValue("@RegistrationNr", (object)bg.RegistrationNr ?? DBNull.Value);
            parameters.AddWithValue("@VatNr", (object)bg.VatNr ?? DBNull.Value);
            parameters.AddWithValue("@ScSharedRentalBillingType",bg.ScSharedRentalBillingType);
            parameters.AddWithValue("@ScDedicatedRentalBillingType",bg.ScDedicatedRentalBillingType);
            parameters.AddWithValue("@ScRevBilledRentalBillingType",bg.ScRevBilledRentalBillingType);
            parameters.AddWithValue("@MtBillingType",bg.MtBillingType);
            parameters.AddWithValue("@MoBillingType",bg.MoBillingType);
            parameters.AddWithValue("@PostPaid",bg.PostPaid);
            parameters.AddWithValue("@BillForNoNetwork",bg.BillForNoNetworks);
        }
        
        private BillingGroup BuildBillingGroup(IDataRecord reader)
        {
            var bg = new BillingGroup();
            bg.Id = (int) reader["Id"];
            bg.PastelCode = (string) reader["PastelCode"];
            bg.CompanyId = reader["CompanyId"] == DBNull.Value ? null : (int?) reader["CompanyId"];
            bg.PriceType = (PriceType) reader["PriceType"];
            bg.PriceListId = reader["PriceListId"] == DBNull.Value ? null : (int?) reader["PriceListId"];
            bg.PriceFixedStd = reader["PriceFixedStd"] == DBNull.Value ? null : (decimal?) reader["PriceFixedStd"];
            bg.PriceFixedRb = reader["PriceFixedRb"] == DBNull.Value ? null : (decimal?) reader["PriceFixedRb"];
            bg.PriceFixedRbReply = reader["PriceFixedRbReply"] == DBNull.Value ? null : (decimal?) reader["PriceFixedRbReply"];
            bg.CompanyName = (string) reader["CompanyName"];
            bg.Address1 = (string) reader["Address1"];
            bg.Address2 = reader["Address2"] == DBNull.Value ? null : (string) reader["Address2"];
            bg.City = (string) reader["City"];
            bg.StateProvince = (string) reader["StateProvince"];
            bg.ZipPostalCode = (string) reader["ZipPostalCode"];
            bg.CountryId = (int) reader["CountryId"];
            bg.Country = reader["Country"] == DBNull.Value ? null : (string) reader["Country"];
            bg.Email = (string) reader["Email"];
            bg.RegistrationNr = reader["RegistrationNr"] == DBNull.Value ? null : (string) reader["RegistrationNr"];
            bg.VatNr = reader["VatNr"] == DBNull.Value ? null : (string) reader["VatNr"];
            bg.ScSharedRentalBillingType = (LineItemBreakdownType) reader["ScSharedRentalBillingType"];
            bg.ScDedicatedRentalBillingType = (LineItemBreakdownType) reader["ScDedicatedRentalBillingType"];
            bg.ScRevBilledRentalBillingType = (LineItemBreakdownType) reader["ScRevBilledRentalBillingType"];
            bg.MtBillingType = (LineItemBreakdownType) reader["MtBillingType"];
            bg.MoBillingType = (LineItemBreakdownType) reader["MoBillingType"];
            bg.PostPaid = (bool) reader["PostPaid"];
            bg.BillForNoNetworks = (bool) reader["BillForNoNetwork"];
            if (reader["PriceFixedCurrency"] != DBNull.Value)
            {
                var currency = currencyReop.GetCurrency((string) reader["PriceFixedCurrency"]);
                bg.Currency = currency;    
            }
            return bg;
        }
    }
}
