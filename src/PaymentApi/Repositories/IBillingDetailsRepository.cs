﻿using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
    public interface IBillingDetailsRepository
    {
        bool TryGetBillingDetails(AccountKey accountKey, out BillingDetails billingDetails);
	    BillingDetails GetBillingDetails(AccountKey accountKey);
    }
}
