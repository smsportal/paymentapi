﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using SmsPortal.CoreMetrics.Database;
using DataException = SmsPortal.Core.DatabaseExtensions.DataException;

namespace SmsPortal.Plutus.Repositories
{
    /// <summary>
    /// Provides base functionality for repository implementations that use SQL Server as the datastore.
    /// </summary>
    public abstract class BaseSqlRepository
	{
		private readonly string connectionString;
		private readonly string displayableConnectionString;

		/// <summary>
		/// Initializes a new instance of the BaseSqlDao class.
		/// </summary>
		/// <param name="connectionString">A string that contains all the details necessary to create a <c>SqlConnection</c></param>
		protected BaseSqlRepository(string connectionString)
		{
			this.connectionString = connectionString;
			displayableConnectionString = string.Join(";",
				connectionString
					.Split(';')
					.Select(p =>
					{
						if (p.StartsWith("PWD", StringComparison.OrdinalIgnoreCase) ||
						    p.StartsWith("PASSWORD", StringComparison.OrdinalIgnoreCase))
						{
							return "PWD=****";
						}

						return p;
					}));
		}

		/// <summary>
		/// Gets a open connection to the SQL Server specified in the connection string.
		/// </summary>
		/// <returns>An open <c>SqlConnection</c> object.</returns>
		protected SqlConnection GetOpenConnection()
		{
			var connection = new SqlConnection(connectionString);
			try
			{
				connection.Open();
			}
			catch (Exception e)
			{
				throw new DataException($"{e.Message}.{Environment.NewLine}CONNECTION: {displayableConnectionString}", null, null, e);
			}
			return connection;
		}

		/// <summary>
		/// Creates a new stored procedure, sets the connection and opens it.
		/// </summary>
		/// <param name="name">The name of the stored procedure.</param>
		/// <param name="connection">The connection to used.</param>
		/// <returns> A <see cref="SqlCommand" /> with connection set and open.</returns>
		protected MeteredDbCommand GetStoredProcedure(string name, SqlConnection connection)
		{
			var command = new MeteredDbCommand(name, connection);
			command.CommandType = CommandType.StoredProcedure;
			return command;
		}
    }
}
