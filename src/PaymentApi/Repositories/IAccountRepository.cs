using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IAccountRepository
    {
        IList<SmsAccount> GetSubAccounts(int masterAccountId);

        IList<SmsAccount> GetAccounts(int? billingGroupId);

        SmsAccount GetAccount(int accountId);
        
        void Update(SmsAccount account);
    }
}