using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IEventRepository
    {
        /// <summary>
        /// Discount the cancelled messages from the number of messages and credits listed on the event.
        /// </summary>
        /// <param name="accountId">The ID of the account that created the event.</param>
        /// <param name="eventId">The ID of the event to refund the credits for.</param>
        /// <param name="refundMsgCount">The number of messages being refunded.</param>
        /// <param name="refundValue">The total value of the messages being refunded (in whatever currency the account uses).</param>
        /// <returns>The result of the refund if it was successful, otherwise null.</returns>
        EventRefundResult DiscountCancelledEventMessages(int accountId, long eventId, int refundMsgCount, decimal refundValue);
    }
}