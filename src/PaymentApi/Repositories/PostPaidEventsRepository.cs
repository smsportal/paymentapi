﻿using System.Collections.Generic;
using System.Linq;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
    public class PostPaidEventsRepository : BaseSqlRepository, IPostPaidEventsInvoiceLineRepository, IPostPaidEventsRepository
    {
        private const string GetPostPaidEventLinesSp = "Plutus_Invoice_GetPostPaidEventLines";

        private readonly ITaxConfigRepository taxConfigRepository;
        private readonly IPaymentAccountRepository accRepo;
        private readonly ISiteRepository siteRepository;
        private IBillingGroupOrDetailsRepository billingDetailsRepository;

        public PostPaidEventsRepository(
            ITaxConfigRepository taxConfigRepository,
            IPaymentAccountRepository accRepo,
            ISiteRepository siteRepository,
            IBillingGroupOrDetailsRepository billingDetailsRepository,
            string connectionString)
            : base(connectionString)
        {
            this.taxConfigRepository = taxConfigRepository;
            this.accRepo = accRepo;
            this.siteRepository = siteRepository;
            this.billingDetailsRepository = billingDetailsRepository;
        }

        public IEnumerable<PostPaidBillingCycleLineItem> GetLineItems(long eventId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetPostPaidEventLinesSp, connection))
            {
                command.Parameters.AddWithValue("@EventId", eventId);
                using (var reader = command.ExecuteReader())
                {
                    var schema = reader.GetSchemaTable();
                    while (reader.Read())
                    {
                        yield return reader.ReadLineItem(schema);
                    }
                }
            }
        }

        public ProformaInvoice GetProformaInvoice(
            AccountKey accountKey,
            long eventId)
        {
            var srclines = GetLineItems(eventId);
            var acc = accRepo.GetPaymentAccount(accountKey);
            var site = siteRepository.Get(acc.SiteId);
            var resellerBillingDetails = billingDetailsRepository.Get(site.AccountKey);
            var taxConfig = taxConfigRepository.GetConfig(resellerBillingDetails.CountryId);
            return
                srclines.ToProformaInvoices(
                        taxConfig,
                        LineItemBreakdownType.PerMasterAccountByCampaignName)
                    .First(i => i.AccountKey.Equals(accountKey));
        }
    }
}
