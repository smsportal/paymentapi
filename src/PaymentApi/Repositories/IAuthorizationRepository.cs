﻿using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IAuthorizationRepository
    {
        void Add(EftAuthorization auth);
    }
}
