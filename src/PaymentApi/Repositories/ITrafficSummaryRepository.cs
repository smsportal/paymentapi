using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface ITrafficSummaryRepository
    {
        List<TrafficSummary> GetAll(int accountId, DateTime start, DateTime end);
    }
}