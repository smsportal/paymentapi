﻿using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
    public interface ITransactionRepository
    {
	    List<LegacyInvoiceLineItem> GetShortCodeTransactions(PaymentAccount paymentAccount,
		    ShortCodeApplicationType applicationType, DateTime from,
		    DateTime to, bool isDedicated);

	    void CloseShortCodeBillingCycle(ShortCodeBillingCycle billingCycle);
    }
}
