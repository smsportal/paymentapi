﻿using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface ICurrencyRepository
    {
        Currency GetCurrency(string currencyCode);

	    CurrencyConverter GetCurrencyConverter(string fromCurrency, string toCurrency);
    }
}
