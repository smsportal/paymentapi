﻿using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface ICostEstimateRepository
    {
        long GetNextCostEstimateNumber(int siteId);
        int Add(Invoice invoice, PostPaidBillingCycle cycle);
        void AddPdf(int costEstimateId, byte[] pdfBytes);
        ProformaInvoice Get(string reference);
        List<InvoiceLineItem> GetLineItems(int costEstimateId);
        byte[] GetPdf(string reference);

        Invoice ConvertToInvoice(
            string accountType,
            int accountId, 
            int costEstimateId, 
            string invoiceNr,
            string orderNumber, 
            string status);
    }
}
