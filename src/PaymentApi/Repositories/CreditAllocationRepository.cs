﻿using System;
using System.Collections.Generic;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
	public class CreditAllocationRepository : BaseSqlRepository, ICreditAllocationRepository
	{
		private const string RefundDescription = "Refund cancelled messages";
		private readonly string CreditCardAllocationSp = "Plutus_CreditCardAllocation_Create_V1";
		private readonly string SiteCreditAllocationSp = "Plutus_SiteCreditAllocation_Create_V1";
		private readonly string ResellerCreditAllocationSp = "Plutus_ResellerCreditAllocation_Create_V2";
		private readonly string RefundAllocationSp = "Plutus_Login_RefundEventAllocationV2";

		private readonly string CreditCardAuthExistsSp = "Plutus_CreditCardAuth_Get_V1";
		private readonly string SiteCreditAllocationExistsSp = "Plutus_SiteCreditAllocation_Get_V1";
		private readonly string ResellerCreditAllocationExistsSp = "Plutus_ResellerCreditAllocation_Get_V2";

		private const string ListResellerCreditAllocationSp = "Plutus_ResellerCreditAllocation_GetAll_V1";

		private readonly ICurrencyRepository currencyRepository;

		public CreditAllocationRepository(string connectionString, ICurrencyRepository currencyRepository)
			: base(connectionString)
		{
			this.currencyRepository = currencyRepository;
		}

		public void CreditCardAllocation(PaymentAccount account, TransactionInfo transaction, string reference, string paymentMethod, decimal transactionFee, string payPalTransactionId = "")
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(CreditCardAllocationSp, connection))
			{
				command.Parameters.AddWithValue("@LoginId", account.Id);
				command.Parameters.AddWithValue("@Amount", transaction.Credits);
				command.Parameters.AddWithValue("@ClientCost", transaction.Total);
				command.Parameters.AddWithValue("@Ref", transaction.TransactionId);
			    if (paymentMethod.Is(PaymentGatewayType.PayPal))
			    {
			        command.Parameters.AddWithValue("@PaymentMethod", PaymentMethodType.PayPal.ToKey());
			    }
			    else
			    {
			        command.Parameters.AddWithValue("@PaymentMethod", paymentMethod);
			    }
                command.Parameters.AddWithValue("@PaymentCurrency", transaction.Currency);
				command.Parameters.AddWithValue("@VatAmount", transaction.TaxValue);
				command.Parameters.AddWithValue("@BaseCurrencyCost", BaseAmount(transaction.Currency, account.BaseCurrency, transaction.Total));
				command.Parameters.AddWithValue("@BaseCurrency", account.BaseCurrency.Code);
				command.Parameters.AddWithValue("@BaseVatAmount", BaseAmount(transaction.Currency, account.BaseCurrency, transaction.TaxValue));
				command.Parameters.AddWithValue("@PayPalTranID", payPalTransactionId);
				command.Parameters.AddWithValue("@TransactionFee", transactionFee);
				command.Parameters.AddWithValue("@BaseTransactionFee", BaseAmount(transaction.Currency, account.BaseCurrency, transactionFee));

				command.ExecuteNonQuery();
			}
		}

		public void SiteCreditAllocation(PaymentAccount account, TransactionInfo transaction, string paymentMethod, decimal transactionFee, string payPalTransactionId = "")
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(SiteCreditAllocationSp, connection))
			{
				command.Parameters.AddWithValue("@SiteID", account.SiteId);
				command.Parameters.AddWithValue("@LoginId", account.Id);
				command.Parameters.AddWithValue("@Amount", transaction.Credits);
				command.Parameters.AddWithValue("@RouteCountryID", account.RouteCountryId);
				command.Parameters.AddWithValue("@ClientCost", transaction.Total);
				command.Parameters.AddWithValue("@Ref", transaction.TransactionId);
			    if (paymentMethod.Is(PaymentGatewayType.PayPal))
			    {
			        command.Parameters.AddWithValue("@PaymentMethod", PaymentMethodType.PayPal.ToKey());
                }
			    else
			    {
			        command.Parameters.AddWithValue("@PaymentMethod", paymentMethod);
			    }
				command.Parameters.AddWithValue("@PaymentCurrency", transaction.Currency);
				command.Parameters.AddWithValue("@VatAmount", transaction.TaxValue);
				command.Parameters.AddWithValue("@BaseCurrencyCost", BaseAmount(transaction.Currency, account.BaseCurrency, transaction.Total));
				command.Parameters.AddWithValue("@BaseCurrency", account.BaseCurrency.Code);
				command.Parameters.AddWithValue("@BaseVatAmount", BaseAmount(transaction.Currency, account.BaseCurrency, transaction.TaxValue));
				command.Parameters.AddWithValue("@PayPalTranID", payPalTransactionId);
				command.Parameters.AddWithValue("@TransactionFee", transactionFee);
				command.Parameters.AddWithValue("@BaseTransactionFee", BaseAmount(transaction.Currency, account.BaseCurrency, transactionFee));
			    command.Parameters.AddWithValue("@InvoiceId", transaction.InvoiceId);

				command.ExecuteNonQuery();
			}
		}

		public void ResellerAllocation(PaymentAccount account, TransactionInfo transaction, Payment payment)
		{
			ResellerAllocation(account, transaction, payment, null);
		}

		public void ResellerAllocation(RefundAccount account, TransactionInfo transaction, string customDescription)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(ResellerCreditAllocationSp, connection))
			{
				command.Parameters.AddWithValue("@SiteId", account.SiteId); // SiteId = SiteId & Id = ResellerId for resellers.
				command.Parameters.AddWithValue("@Credits", transaction.Credits);
				command.Parameters.AddWithValue("@RouteCountryId", account.RouteCountryId);
				command.Parameters.AddWithValue("@Cost", transaction.Total);
				command.Parameters.AddWithValue("@TransactionId", DBNull.Value);
				command.Parameters.AddWithValue("@GatewayTranId", DBNull.Value);
				command.Parameters.AddWithValue("@PaymentMethod", DBNull.Value);
				command.Parameters.AddWithValue("@PaymentCurrency", account.Currency);
				command.Parameters.AddWithValue("@VatAmount", transaction.TaxValue);
				command.Parameters.AddWithValue("@BaseCurrencyCost", BaseAmount(transaction.Currency, account.Currency, transaction.Total));
				command.Parameters.AddWithValue("@BaseCurrency", account.Currency);
				command.Parameters.AddWithValue("@BaseVatAmount", BaseAmount(transaction.Currency, account.Currency, transaction.TaxValue));
				command.Parameters.AddWithValue("@TransactionFee", 0);
				command.Parameters.AddWithValue("@BaseTransactionFee", BaseAmount(transaction.Currency, account.Currency, 0));
				command.Parameters.AddWithValue("@InvoiceId", 0);
				command.Parameters.AddWithValue("@Description", (object)customDescription ?? DBNull.Value);

				command.ExecuteNonQuery();
			}
		}

		public void RefundEventAllocation(RefundEventAllocation allocation)
		{
			if (allocation.AccountKey.Type == AccountType.SmsAccount)
			{
				RefundEventAllocation(allocation.AccountKey.Id, allocation.Credits);
			}
			else
			{
				using (var connection = GetOpenConnection())
				using (var command = GetStoredProcedure(ResellerCreditAllocationSp, connection))
				{
					command.Parameters.AddWithValue("@SiteId", allocation.AccountKey.Id);
					command.Parameters.AddWithValue("@Credits", allocation.Credits);
					command.Parameters.AddWithValue("@RouteCountryId", allocation.RouteCountryId);
					command.Parameters.AddWithValue("@Cost", 0);
					command.Parameters.AddWithValue("@TransactionId",  DBNull.Value);
					command.Parameters.AddWithValue("@GatewayTranId",  DBNull.Value);
					command.Parameters.AddWithValue("@PaymentMethod",  DBNull.Value);
					command.Parameters.AddWithValue("@PaymentCurrency", allocation.Currency);
					command.Parameters.AddWithValue("@VatAmount", 0);
					command.Parameters.AddWithValue("@BaseCurrencyCost", 0);
					command.Parameters.AddWithValue("@BaseCurrency", allocation.Currency);
					command.Parameters.AddWithValue("@BaseVatAmount", 0);
					command.Parameters.AddWithValue("@TransactionFee", 0);
					command.Parameters.AddWithValue("@BaseTransactionFee", 0);
					command.Parameters.AddWithValue("@InvoiceId", 0);
					command.Parameters.AddWithValue("@Description", "Refund cancelled messages");

					command.ExecuteNonQuery();
				}
			}
			
		}

		public void ResellerAllocation(PaymentAccount account, TransactionInfo transaction, Payment payment, string customDescription)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(ResellerCreditAllocationSp, connection))
			{
				command.Parameters.AddWithValue("@SiteId", account.SiteId); // SiteId = SiteId & Id = ResellerId for resellers.
				command.Parameters.AddWithValue("@Credits", transaction.Credits);
				command.Parameters.AddWithValue("@RouteCountryId", payment.RouteCountryId);
				command.Parameters.AddWithValue("@Cost", transaction.Total);
				command.Parameters.AddWithValue("@TransactionId", (object)transaction.TransactionId ?? DBNull.Value);
				command.Parameters.AddWithValue("@GatewayTranId", (object)payment.GatewayTransactionId ?? DBNull.Value);
				command.Parameters.AddWithValue("@PaymentMethod", (object)payment.Gateway ?? DBNull.Value); //Check if this can be used.
				command.Parameters.AddWithValue("@PaymentCurrency", transaction.Currency);
				command.Parameters.AddWithValue("@VatAmount", transaction.TaxValue);
				command.Parameters.AddWithValue("@BaseCurrencyCost", BaseAmount(transaction.Currency, account.BaseCurrency, transaction.Total));
				command.Parameters.AddWithValue("@BaseCurrency", account.BaseCurrency.Code);
				command.Parameters.AddWithValue("@BaseVatAmount", BaseAmount(transaction.Currency, account.BaseCurrency, transaction.TaxValue));
				command.Parameters.AddWithValue("@TransactionFee", payment.Fee);
				command.Parameters.AddWithValue("@BaseTransactionFee", BaseAmount(transaction.Currency, account.BaseCurrency, payment.Fee));
			    command.Parameters.AddWithValue("@InvoiceId", transaction.InvoiceId);
			    command.Parameters.AddWithValue("@Description", (object)customDescription ?? DBNull.Value);

				command.ExecuteNonQuery();
			}
		}

		public bool ResellerAllocationExists(int siteId, string transactionId)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(ResellerCreditAllocationExistsSp, connection))
			{
				command.Parameters.AddWithValue("@SiteId", siteId);
				command.Parameters.AddWithValue("@TransactionId", transactionId);

				using (var reader = command.ExecuteReader())
				{
					return reader.HasRows;
				}
			}
		}

		public bool SiteCreditAllocationExists(int loginId, string reference)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(SiteCreditAllocationExistsSp, connection))
			{
				command.Parameters.AddWithValue("@LoginId", loginId);
				command.Parameters.AddWithValue("@Ref", reference);

				using (var reader = command.ExecuteReader())
				{
					return reader.HasRows;
				}
			}
		}

		public bool CreditCardAllocationExists(int loginId, string reference)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(CreditCardAuthExistsSp, connection))
			{
				command.Parameters.AddWithValue("@LoginId", loginId);
				command.Parameters.AddWithValue("@Ref", reference);

				using (var reader = command.ExecuteReader())
				{
					return reader.HasRows;
				}
			}
		}

		public IEnumerable<CreditAllocation> Get(int accountId, string accountType)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(ListResellerCreditAllocationSp, connection))
			{
				command.Parameters.AddWithValue("@AccountId", accountId);
				using (var reader = command.ExecuteReader())
				{
					var allocations = new List<CreditAllocation>();
					while (reader.Read())
					{
						var tmp = new CreditAllocation();
						tmp.Id = (int) reader["ID"];
						tmp.AccountId = accountId;
						tmp.AccountType = accountType;
						tmp.Credits = (int) reader["Credits"];
						tmp.RouteCountryId = (int) reader["RouteCountryId"];
						tmp.Cost = (decimal) reader["Cost"];
						tmp.Allocated = (DateTime) reader["Date"];
						tmp.Description = (string) reader["Description"];
						tmp.InvoiceId = (int) reader["InvoiceId"];
						tmp.TransacitonId = (Guid) reader["TransactionId"];
						tmp.GatewayTransactionId = (string) reader["GatewayTranId"];
						allocations.Add(tmp);
					}

					return allocations;
				}
			}
		}

		private void RefundEventAllocation(int accountId, decimal refundValue)
		{
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(RefundAllocationSp, connection))
			{
				command.Parameters.AddWithValue("@LoginId", accountId);
				command.Parameters.AddWithValue("@EventRefundValue", refundValue);
				command.Parameters.AddWithValue("@Description", RefundDescription);
				command.ExecuteNonQuery();
			}
		}

		private decimal BaseAmount(string paymentCurrency, Currency baseCurrency, decimal amount)
		{
			return BaseAmount(paymentCurrency, baseCurrency.Code, amount);
		}
		
		private decimal BaseAmount(string paymentCurrency, string currencyCode, decimal amount)
		{
			var currencyConverter = currencyRepository.GetCurrencyConverter(paymentCurrency, currencyCode);
			return currencyConverter.Convert(amount);
		}
	}
}
