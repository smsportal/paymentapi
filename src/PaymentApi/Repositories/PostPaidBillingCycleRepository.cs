﻿using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using System.Linq;
using log4net;
using SmsPortal.Core;
using SmsPortal.Core.DatabaseExtensions;

namespace SmsPortal.Plutus.Repositories
{
    public class PostPaidBillingCycleRepository : BaseSqlRepository, IPostPaidBillingCycleRepository
    {
        private const string GetPostPaidBillingCycleSp = "Plutus_BillingCycle_Get";
        private const string InsertPostPaidBillingCycleSp = "Plutus_BillingCycle_Insert";
        private const string IdentifyBillingCycleSp = "Plutus_BillingCycle_Identify";

        private readonly ILog log = LogManager.GetLogger(typeof(PostPaidBillingCycleRepository));
        private readonly IPostPaidBillingCycleInvoiceLineRepository postPaidBillingLinesRepository;
        private readonly ITaxConfigRepository taxConfigRepository;
        private readonly ISiteRepository siteRepository;
        private readonly IBillingGroupOrDetailsRepository billingDetailsRepository;


        public PostPaidBillingCycleRepository(
              IPostPaidBillingCycleInvoiceLineRepository postPaidBillingLinesRepository,
              ITaxConfigRepository  taxConfigRepository,
              ISiteRepository siteRepository,
              IBillingGroupOrDetailsRepository billingDetailsRepository,
              string connectionString)
            : base(connectionString)
        {
            this.postPaidBillingLinesRepository = postPaidBillingLinesRepository;
            this.taxConfigRepository = taxConfigRepository;
            this.siteRepository = siteRepository;
            this.billingDetailsRepository = billingDetailsRepository;
        }

        public IEnumerable<ProformaInvoice> GetAllProformaInvoices(PostPaidBillingCycleFilterType filterType)
        {
            return GetAll(null, filterType);
        }

        public ProformaInvoice GetProformaInvoice(
            AccountKey accountKey,
            PostPaidBillingCycleFilterType filterType,
            DateTime invoiceDate)
        {
            return GetAll(accountKey, filterType)
                .First(i => i.Created.Date == invoiceDate);
        }

        public void MarkAsPostPaid(
            ProformaInvoice invoice,
            long? shortCode = null,
            string keyword = null)
        {
            MarkAsPostPaid(invoice, null, null, shortCode, keyword);
        }

        public void Approve(ProformaInvoice invoice,
            string reason,
            long? shortCode = null,
            string keyword = null)
        {
            MarkAsPostPaid(invoice, true, reason, shortCode, keyword);
        }

        public void Decline(
            ProformaInvoice invoice,
            string reason,
            long? shortCode = null,
            string keyword = null)
        {
            MarkAsPostPaid(invoice, false, reason, shortCode, keyword);
        }

        public ProformaInvoice GetProformaInvoice(
            PaymentAccount account,
            BillingDetails billingDetails,
            DateTime invoiceDate,
            BillingPeriod period,
            InvoiceLineItem line)
        {
            var srcLines = new List<PostPaidBillingCycleLineItem>();
            var taxConfig = taxConfigRepository.GetConfig(TaxConfig.SouthAfrica.CountryId);
            srcLines.Add(new PostPaidBillingCycleLineItem
            {
                SiteId = account.SiteId,
                AccountKey = account.AccountKey,
                ClientRefCode = account.ClientRefCode,
                Username = account.Username,
                Fullname = account.Fullname,
                Company = account.Company,
                PaymentCurrency =
                (
                    (
                        line.Code == InvoiceLineItemType.ShortCodeSetup ||
                        line.Code == InvoiceLineItemType.ShortCodeRental ||
                        line.Code == InvoiceLineItemType.ShortCodeKeywordRental
                    )
                        ? account.PricingSCCurrency
                        : account.FixedPricingCurrency
                ),
                Code = line.Code,
                UnitPrice = line.UnitPrice,
                Quantity = line.Quantity,
                Description = line.Description,
                BillingType = LineItemBreakdownType.PerMasterAccountByUsername,
                BillingCycle = BillingPeriod.None,
                CycleStartDate = invoiceDate,
                CycleEndDate = invoiceDate,
                CountryId = billingDetails.CountryId,
                OptionalNote = line.OptionalNote,
                DataId = line.DataId
            });

            return
                srcLines.ToProformaInvoices(
                        taxConfig,
                        line.Code,
                        LineItemBreakdownType.PerMasterAccountByUsername)
                    .First();
        }

        private void MarkAsPostPaid(
            ProformaInvoice invoice,
            bool? approved,
            string reason,
            long? shortCode = null,
            string keyword = null)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(InsertPostPaidBillingCycleSp, connection))
            {
                command.Parameters.AddWithValue("@AccountId",
                    invoice.LoginId == 0
                        ? invoice.ResellerId
                        : invoice.LoginId);
                command.Parameters.AddWithValue("@AccountType", invoice.LoginId == 0 ? "RS" : "MA");
                command.Parameters.AddWithValue("@ClientRefCode", invoice.CustomerRef);
                command.Parameters.AddWithValue("@InvoiceType", invoice.Cycle.LineItemType);
                command.Parameters.AddWithValue("@InvoiceId", invoice.Id);
                command.Parameters.AddWithValue("@StartDate", invoice.Cycle.CycleStartDate.Date);
                command.Parameters.AddWithValue("@EndDate", invoice.Cycle.CycleEndDate.Date);
                if (approved.HasValue)
                {
                    command.Parameters.AddWithValue("@Approved", approved);
                    command.Parameters.AddWithValue("@Comment", reason);
                }
                command.Parameters.AddWithValue("@Quantity",
                    invoice.GetLineItems()
                        .Where(l => l.Code != InvoiceLineItemType.TransactionFee)
                        .Sum(l => l.Quantity));
                command.Parameters.AddWithValue("@Currency", invoice.Currency);
                command.Parameters.AddWithValue("@Subtotal", invoice.SubTotal);
                command.Parameters.AddWithValue("@Tax", invoice.TaxAmount);
                command.Parameters.AddWithValue("@ShortCode", shortCode);
                command.Parameters.AddWithValue("@Keyword", keyword);
                command.Parameters.AddWithValue("@LineItemBreakdownType", invoice.Cycle.LineItemBreakdown);
                if (!string.IsNullOrEmpty(invoice.InvoiceNr) &&
                    !invoice.InvoiceNr.Equals("Proforma", StringComparison.OrdinalIgnoreCase))
                {
                    command.Parameters.AddWithValue("@InvoiceNumber", invoice.InvoiceNr);
                }
                command.ExecuteNonQuery();
            }
        }

        private IEnumerable<ProformaInvoice> GetAll(
            AccountKey? accountKey,
            PostPaidBillingCycleFilterType filterType)
        {
            var site = siteRepository.Get(1);
            var resellerBillingDetails = billingDetailsRepository.Get(site.AccountKey);
            var taxConfig = taxConfigRepository.GetConfig(resellerBillingDetails.CountryId);
            var results = new List<ProformaInvoice>();

            switch (filterType)
            {
                case PostPaidBillingCycleFilterType.BulkSmsUsage:
                {
                    var srcLines =
                        postPaidBillingLinesRepository.GetLineItems(
                                accountKey,
                                null,
                                null)
                            .ToArray();

                    var manInvoices =
                        srcLines
                            .ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.BulkSms,
                                LineItemBreakdownType.Unknown)
                            .ToArray();
                    var refInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.BulkSms,
                                LineItemBreakdownType.PerRefCodeByMasterAccountUsername)
                            .ToArray();
                    var usrInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.BulkSms,
                                LineItemBreakdownType.PerMasterAccountByUsername)
                            .ToArray();
                    var smppInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.BulkSms,
                                LineItemBreakdownType.PerMasterAccountByNetworkName)
                            .ToArray();
                    var costInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.BulkSms,
                                LineItemBreakdownType.PerMasterAccountByCostCentre)
                            .ToArray();
                    var cntryCmpgnInvoices =
                        srcLines.ToProformaInvoices(taxConfig,
                                new[]
                                {
                                    InvoiceLineItemType.BulkSms,
                                    InvoiceLineItemType.ReverseBilledReplySms
                                },
                                LineItemBreakdownType.PerCostCentrePerCampaignByCountryAndMessageType)
                            .ToArray();

                    results.AddRange(manInvoices);
                    results.AddRange(refInvoices);
                    results.AddRange(usrInvoices);
                    results.AddRange(smppInvoices);
                    results.AddRange(costInvoices);
                    results.AddRange(cntryCmpgnInvoices);
                    break;
                }
                case PostPaidBillingCycleFilterType.ReverseBilledReplies:
                {
                    var srcLines =
                        postPaidBillingLinesRepository.GetLineItems(
                                accountKey,
                                null,
                                InvoiceLineItemType.ReverseBilledReplySms)
                            .ToArray();

                    var manInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledReplySms,
                                LineItemBreakdownType.Unknown)
                            .ToArray();
                    var refInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledReplySms,
                                LineItemBreakdownType.PerRefCodeByMasterAccountUsername)
                            .ToArray();
                    var usrInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledReplySms,
                                LineItemBreakdownType.PerMasterAccountByUsername)
                            .ToArray();
                    var smppInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledReplySms,
                                LineItemBreakdownType.PerMasterAccountByNetworkName)
                            .ToArray();
                    var cmpgnInvoices =
                        srcLines.ToProformaInvoices(taxConfig,
                                InvoiceLineItemType.ReverseBilledReplySms,
                                LineItemBreakdownType.PerMasterAccountByCampaignName)
                            .ToArray();
                    var costInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledReplySms,
                                LineItemBreakdownType.PerMasterAccountByCostCentre)
                            .ToArray();

                    results.AddRange(manInvoices);
                    results.AddRange(refInvoices);
                    results.AddRange(usrInvoices);
                    results.AddRange(smppInvoices);
                    results.AddRange(cmpgnInvoices);
                    results.AddRange(costInvoices);
                    break;
                }
                case PostPaidBillingCycleFilterType.ReverseBilledShortCodeMessages:
                {
                    var srcLines =
                        postPaidBillingLinesRepository.GetLineItems(
                                accountKey,
                                null,
                                InvoiceLineItemType.ReverseBilledShortCodeSms)
                            .ToArray();

                    var manInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledShortCodeSms,
                                LineItemBreakdownType.Unknown)
                            .ToArray();
                    var refInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledShortCodeSms,
                                LineItemBreakdownType.PerRefCodeByMasterAccountUsername)
                            .ToArray();
                    var usrInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledShortCodeSms,
                                LineItemBreakdownType.PerMasterAccountByUsername)
                            .ToArray();
                    var smppInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledShortCodeSms,
                                LineItemBreakdownType.PerMasterAccountByNetworkName)
                            .ToArray();
                    var cmpgnInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledShortCodeSms,
                                LineItemBreakdownType.PerMasterAccountByCampaignName)
                            .ToArray();
                    var costInvoices =
                        srcLines.ToProformaInvoices(
                                taxConfig,
                                InvoiceLineItemType.ReverseBilledShortCodeSms,
                                LineItemBreakdownType.PerMasterAccountByCostCentre)
                            .ToArray();

                    results.AddRange(manInvoices);
                    results.AddRange(refInvoices);
                    results.AddRange(usrInvoices);
                    results.AddRange(smppInvoices);
                    results.AddRange(cmpgnInvoices);
                    results.AddRange(costInvoices);
                    break;
                }
            }

            return results;
        }

        public IEnumerable<ProformaInvoice> MatchToExisting(IEnumerable<ProformaInvoice> invoices)
        {
            var results = invoices as ProformaInvoice[] ?? invoices.ToArray();
            if (!results.Any()) return results;

            var accounts = results.Select(i =>
                i.ResellerId > 0
                    ? new {AccountId = i.ResellerId, AccountType = "RS"}
                    : new {AccountId = i.LoginId, AccountType = "MA"})
                .Distinct();

            foreach (var account in accounts)
            {
                var cycles = results.Where(i =>
                        i.ResellerId == account.AccountId && account.AccountType.Equals("RS") ||
                        i.LoginId == account.AccountId && account.AccountType.Equals("MA"))
                    .Select(i => i.Cycle)
                    .Distinct();
                foreach (var cycle in cycles)
                {
                    using (var connection = GetOpenConnection())
                    using (var command = GetStoredProcedure(GetPostPaidBillingCycleSp, connection))
                    {
                        if (!string.IsNullOrEmpty(account.AccountType) && account.AccountId > 0)
                        {
                            command.Parameters.AddWithValue("@AccountId", account.AccountId);
                            command.Parameters.AddWithValue("@AccountType", account.AccountType);
                        }

                        command.Parameters.AddWithValue("@InvoiceType", (int)cycle.LineItemType);
                        command.Parameters.AddWithValue("@StartDate", cycle.CycleStartDate.Date);
                        command.Parameters.AddWithValue("@EndDate", cycle.CycleEndDate.Date);

                        using (var reader = command.ExecuteReader())
                        {
                            var schema = reader.GetSchemaTable();
                            while (reader.Read())
                            {
                                if (results.Count(i => i.Id == 0) == 0)
                                {
                                    break;
                                }

                                var invId = reader.Read<int?>(schema, "InvoiceId");
                                if (!invId.HasValue) continue;

                                var loginId = reader.Read<int?>(schema, "LoginId");
                                var resellerId = reader.Read<int?>(schema, "ResellerId");
                                var invoiceType = reader.Read<int>(schema, "InvoiceType", 0);

                                var curr = reader.Read<string>(schema, "Currency");
                                var customerRef = reader.Read(schema, "CustomerRef", string.Empty);
                                var total = reader.Read<decimal>(schema, "Total", 0);

                                log.Debug($"Found invoice #{invId} (LoginId = {loginId}, ResellerId = {resellerId}, Type = {invoiceType} Total = {curr} {total}) as potential match");

                                var inv = results.FirstOrDefault(i =>
                                    i.Cycle.CycleStartDate.Date == cycle.CycleStartDate.Date &&
                                    i.Cycle.CycleEndDate.Date == cycle.CycleEndDate.Date &&
                                    i.CustomerRef.Equals(customerRef, StringComparison.OrdinalIgnoreCase) &&
                                    i.Currency.Equals(curr, StringComparison.OrdinalIgnoreCase) &&
                                    Math.Round(i.Total, 2) ==  Math.Round(total, 2) &&
                                    i.Id == 0 &&
                                    i.Cycle.LineItemType == cycle.LineItemType &&
                                    (
                                        i.LoginId == loginId && account.AccountType.Is(AccountType.SmsAccount) ||
                                        i.ResellerId == resellerId && account.AccountType.Is(AccountType.ResellerAccount)
                                    ));
                                if (inv == null) continue;

                                var invNr = reader.Read<string>(schema, "InvoiceNr");
                                if (inv.InvoiceNr.ToUpper().Contains("PROFORMA"))
                                {
                                    inv.InvoiceNr = "PROFORMA";
                                }
                                log.Debug($"Invoice {inv.InvoiceNr} for {inv.AccountName} based on {inv.Cycle.ItemDescription} {inv.Cycle.BreakdownDescription} matched to {invNr} (#{invId})");

                                var approved = reader.Read<bool?>(schema, "Approved");
                                var comment = reader.Read<string>(schema, "Comment");

                                inv.Created = reader.Read<DateTime>(schema, "SystemCreatedDate");
                                inv.Status =
                                    !approved.HasValue
                                        ? inv.Status
                                        : (bool) approved
                                            ? "Approved"
                                            : "Declined";
                                if (!string.IsNullOrWhiteSpace(comment))
                                {
                                    inv.PurchaseOrderNr = comment;
                                }

                                inv.Id = (int) invId;
                                inv.InvoiceNr = invNr;
                                if (inv.Id >= 0) continue;

                                inv.Status =
                                    $"Cost Estimate {cycle.BillingPeriodType.ToString()} {cycle.BreakdownDescription}";
                                inv.PurchaseOrderNr = "";
                            }
                        }
                    }
                }
            }

            return results;
        }

        public PostPaidBillingCycle IdentifyBillingCycle(int invoiceId)
        {
            var result = new PostPaidBillingCycle();
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(IdentifyBillingCycleSp, connection))
            {
                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);
                using (var reader = cmd.ExecuteReader())
                {
                    var schema = reader.GetSchemaTable();
                    if (!reader.Read()) return result;

                    result.BillingPeriodType = reader.Read(schema, "BillingCycle", BillingPeriod.None);
                    result.LineItemBreakdown = reader.Read(schema, "LineItemBreakdown", LineItemBreakdownType.Unknown);
                    result.LineItemType = reader.Read(schema, "LineItemType", InvoiceLineItemType.BulkSms);
                    result.CycleStartDate = reader.Read<DateTime>(schema, "DateFrom");
                    result.CycleEndDate = reader.Read<DateTime>(schema, "DateTo");
                }
            }
            return result;
        }
    }
}
