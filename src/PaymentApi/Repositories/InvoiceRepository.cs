using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SmsPortal.CoreMetrics.Database;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Core.DatabaseExtensions;

namespace SmsPortal.Plutus.Repositories
{
    public class InvoiceRepository: BaseSqlRepository, IInvoiceRepository
    {
        private const string CreateInvoiceSp = "Plutus_Invoice_Create_V2";
        private const string CreateInvoiceLineItemSp = "Plutus_Invoice_LineItem_Create_V1";
        private const string GetInvoiceNumberSp = "Plutus_Site_InvoiceReferenceNumber_Get_V1";
        private const string CreateInvoicePdfSp = "Plutus_InvoicePdf_Create_V1";
	    private const string ListInvoicesSp = "Plutus_Invoices_List_V1";
	    private const string GetInvoiceByReferenceSp = "Plutus_Invoice_Get_V2";
	    private const string GetInvoiceByIdSp = "Plutus_Invoice_GetById_V2";
	    private const string GetInvoiceLineItems = "Plutus_InvoiceLineItems_Get_V2";
	    private const string GetInvoicePdf = "Plutus_InvoicePdf_Get_V1";
        private const string UpdateInvoiceSp = "Plutus_Invoice_Update_V1";
        private const string GetDryRunNumberSp = "Plutus_DryRunNumber_Get_V1";
        private const string ListInvoicesByClientRefCodeSp = "Plutus_Invoices_List_By_ClientRef_V1";

        private readonly ICurrencyRepository currencyRepo;
        private readonly IBillingGroupOrDetailsRepository billingDetailsRepository;

        public InvoiceRepository(
            string connectionString,
            ICurrencyRepository repo,
            IBillingGroupOrDetailsRepository billingDetailsRepository) : base(connectionString)
        {
            this.currencyRepo = repo;
	        this.billingDetailsRepository = billingDetailsRepository;
        }

        public int Add(Invoice invoice)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(CreateInvoiceSp, connection))
            {
	            SetBaseProperties(invoice, cmd);
                cmd.ExecuteScalar();
                invoice.Id = (int) cmd.Parameters["@InvoiceId"].Value;
                AddLineItems(invoice);
                return invoice.Id;
            }
        }

        public int Add(PostpaidInvoice invoice)
        {
	        using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(CreateInvoiceSp, connection))
            {
	            SetBaseProperties(invoice, cmd);
	            cmd.Parameters.AddWithValue("@PeriodBilledFromDate", invoice.Cycle.CycleStartDate);
	            cmd.Parameters.AddWithValue("@PeriodBilledToDate", invoice.Cycle.CycleEndDate);
                cmd.Parameters.AddWithValue("@Approved", invoice.IsApproved);
                cmd.Parameters.AddWithValue("@ApprovalDt", (object) invoice.ApprovedOrDeclined ?? DBNull.Value);
                cmd.ExecuteScalar();
                invoice.Id = (int) cmd.Parameters["@InvoiceId"].Value;
                AddLineItems(invoice);
                return invoice.Id;
            }
        }

        private void SetBaseProperties(Invoice invoice, MeteredDbCommand cmd)
        {
	        if (invoice.ResellerId > 0)
	            {
		            cmd.Parameters.AddWithValue("@ResellerId", invoice.ResellerId);
	            }
	            else
	            {
		            cmd.Parameters.AddWithValue("@LoginId", invoice.LoginId);
	            }
                cmd.Parameters.AddWithValue("@ReferenceNumber", invoice.InvoiceNr);
                cmd.Parameters.AddWithValue("@AccountName", invoice.AccountName);
                cmd.Parameters.AddWithValue("@OrderNumber", invoice.PurchaseOrderNr ?? string.Empty);
                cmd.Parameters.AddWithValue("@ClientRef", invoice.PastelCode);
                cmd.Parameters.AddWithValue("@CustomerRef", invoice.CustomerRef);
                cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime2).Value = invoice.Created;
                cmd.Parameters.Add("@DueDate", SqlDbType.DateTime2).Value = DateTime.Now.AddDays(7);
                cmd.Parameters.AddWithValue("@TaxValue", invoice.TaxAmount);
                cmd.Parameters.AddWithValue("@TaxPercentage", invoice.TaxPercentage);
                cmd.Parameters.AddWithValue("@Total", invoice.Total);
                cmd.Parameters.AddWithValue("@SubTotal", invoice.SubTotal);
                cmd.Parameters.AddWithValue("@International", invoice.IsInternational);
                cmd.Parameters.AddWithValue("@Status", invoice.Status);
                cmd.Parameters.AddWithValue("@Currency", invoice.Currency);
                cmd.Parameters.AddWithValue("@TransactionId", (object) invoice.TransactionId ?? DBNull.Value);
                var invoiceCurrency = currencyRepo.GetCurrency(invoice.Currency);
                cmd.Parameters.AddWithValue("@ZarTotal", invoiceCurrency.Convert(Currency.BaseCurrency, invoice.Total));
                cmd.Parameters.AddWithValue("@ZarSubtotal", invoiceCurrency.Convert(Currency.BaseCurrency, invoice.SubTotal));
                cmd.Parameters.AddWithValue("@InvoiceType", invoice.GetLineItemType());
                cmd.Parameters.Add("@InvoiceId", SqlDbType.Int);
                cmd.Parameters["@InvoiceId"].Direction = ParameterDirection.Output;
        }

        private void AddLineItems(Invoice invoice)
        {
            foreach (var lineItem in invoice.GetLineItems())
            {
                using (var connection = GetOpenConnection())
                using (var command = GetStoredProcedure(CreateInvoiceLineItemSp, connection))
                {
                    command.Parameters.AddWithValue("@InvoiceId", invoice.Id);
                    command.Parameters.AddWithValue("@Code", lineItem.Code);
                    command.Parameters.AddWithValue("@Description", lineItem.Description);
                    command.Parameters.AddWithValue("@Quantity", lineItem.Quantity);
                    command.Parameters.AddWithValue("@Price", lineItem.UnitPrice);
                    command.Parameters.AddWithValue("@Total", lineItem.LineTotal);
                    command.Parameters.AddWithValue("@OptionalNote", lineItem.OptionalNote);
                    command.Parameters.AddWithValue("@DataId", lineItem.DataId);
                    command.ExecuteNonQuery();
                }
            }
        }

        public long GetNextInvoiceNumber(int siteId)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(GetInvoiceNumberSp, connection))
            {
                cmd.Parameters.AddWithValue("@SiteId", siteId);
                cmd.Parameters.Add("@InvoiceReferenceNumber", SqlDbType.BigInt);
                cmd.Parameters["@InvoiceReferenceNumber"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                return (long) cmd.Parameters["@InvoiceReferenceNumber"].Value;
            }
        }

        public int GetNextDryInvoiceNumber(int siteId)
        {
	        using (var connection = GetOpenConnection())
	        using (var cmd = GetStoredProcedure(GetDryRunNumberSp, connection))
	        {
		        return (int) cmd.ExecuteScalar();
	        }
        }

        public void AddPdf(int invoiceId, byte[] pdfBytes)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(CreateInvoicePdfSp, connection))
            {
                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);
                cmd.Parameters.Add(new SqlParameter
                                   {
                                       ParameterName = "@Pdf",
                                       SqlDbType = SqlDbType.VarBinary,
                                       Value = pdfBytes
                                   });
                cmd.ExecuteNonQuery();
            }
        }

	    public Invoice Get(string reference)
	    {
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(GetInvoiceByReferenceSp, connection))
			{
				command.Parameters.AddWithValue("@Reference", reference);

				using (var reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return CreateInvoice(reader);
					}
				}
				throw new EntityNotFoundException(typeof(Invoice), reference);
			}
	    }

	    public Invoice Get(int id)
	    {
		    using (var connection = GetOpenConnection())
		    using (var command = GetStoredProcedure(GetInvoiceByIdSp, connection))
		    {
			    command.Parameters.AddWithValue("@Id", id);
			    using (var reader = command.ExecuteReader())
			    {
				    if (reader.Read())
				    {
					    return CreateInvoice(reader);
				    }
			    }
			    throw new EntityNotFoundException(typeof(Invoice), id);
		    }
	    }

	    public byte[] GetPdf(string reference)
	    {
		    using (var connecion = GetOpenConnection())
		    using (var command = GetStoredProcedure(GetInvoicePdf, connecion))
		    {
			    command.Parameters.AddWithValue("@Reference", reference);

			    using (var reader = command.ExecuteReader())
			    {
				    if (reader.Read())
				    {
					    return (byte[])reader["InvoiceData"];
				    }
			    }
				throw new EntityNotFoundException(typeof(byte[]), "Invoice {reference} pdf.");
			}
	    }

        public void Update(Invoice invoice)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(UpdateInvoiceSp, connection))
            {
                command.Parameters.AddWithValue("@TransactionId", invoice.TransactionId);
                command.Parameters.AddWithValue("@Status", invoice.Status);
                command.Parameters.AddWithValue("@InvoiceId", invoice.Id);
                command.ExecuteNonQuery();
            }
        }

        public List<Invoice> List(string clientRefCode)
        {
	        var invoices = new List<Invoice>();
	        using (var connection = GetOpenConnection())
	        using (var command = GetStoredProcedure(ListInvoicesByClientRefCodeSp, connection))
	        {
		        command.Parameters.AddWithValue("@ClientRefCode", clientRefCode);
		        using (var reader = command.ExecuteReader())
		        {
			        while (reader.Read())
			        {
				        invoices.Add(CreateInvoice(reader));
			        }
		        }

		        return invoices;
	        }
        }


        public List<Invoice> List(ListInvoicesRequest request)
		{
			var list = new List<Invoice>();
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(ListInvoicesSp, connection))
			{
				command.Parameters.AddWithValue("@LoginId", request.LoginId ?? (object) DBNull.Value);
				command.Parameters.AddWithValue("@From", request.From ?? (object) DBNull.Value);
				command.Parameters.AddWithValue("@To", request.To ?? (object) DBNull.Value);
				command.Parameters.AddWithValue("@Currency", request.Currency ?? (object) DBNull.Value);
				command.Parameters.AddWithValue("@Status", request.Status ?? (object) DBNull.Value);
				command.Parameters.AddWithValue("@Gateway", request.Gateway ?? (object) DBNull.Value);

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						var invoice = CreateInvoice(reader);
						list.Add(invoice);
					}
				}
			}
			return list;
		}

	    private Invoice CreateInvoice(SqlDataReader reader)
	    {
			var invoice = new Invoice();
		    invoice.Id = (int)reader["Id"];
		    invoice.TaxPercentage = Convert.ToDecimal(reader["TaxPercentage"]);
		    invoice.InvoiceNr = (string)reader["ReferenceNumber"];
		    invoice.PastelCode = (string)reader["ClientRef"];
		    invoice.IsInternational = (bool)reader["International"];
		    invoice.LoginId = reader["LoginId"] == DBNull.Value ? 0 : (int)reader["LoginId"];
			invoice.ResellerId = reader["ResellerId"] == DBNull.Value ? 0 : (int)reader["ResellerId"];
		    invoice.AccountName = (string)reader["AccountName"];
		    invoice.PurchaseOrderNr = (string)reader["OrderNumber"];
		    invoice.CustomerRef = (string)reader["CustomerRef"];
		    invoice.Currency = (string)reader["Currency"];

	        if (DBNull.Value != reader["TransactionId"])
	        {
		        if (Guid.TryParse((string) reader["TransactionId"], out var tranId))
		        {
			        invoice.TransactionId = tranId;
		        }
	        }
	        invoice.Status = (string) reader["Status"];

	        invoice.ConversionRate = reader.Read<decimal?>("ConversionRate");
	        if (invoice.ConversionRate == 1)
	        {
		        invoice.ConversionRate = null; //Since 1 to 1 doesn't need a conversion rate
	        }

            if(!billingDetailsRepository.TryGet(invoice.AccountKey, out var billingDetails)) {

	            billingDetails = BillingDetails.GetTemporary();
	            billingDetails.CompanyName = invoice.AccountName;
	            billingDetails.Address1 = "Could not retrieve billing address";
            }

            invoice.BillingAddress = billingDetails;

            invoice.Created = (DateTime)reader["CreatedDate"];
		    invoice.DueDate = (DateTime)reader["DueDate"];

		    foreach (var lineItem in GetLineItems(invoice.Id))
		    {
			    invoice.AddLineItem(lineItem);
		    }
		    return invoice;
		}

	    public List<InvoiceLineItem> GetLineItems(int invoiceId)
	    {
		    var lineItems = new List<InvoiceLineItem>();
			using (var connection = GetOpenConnection())
			using (var command = GetStoredProcedure(GetInvoiceLineItems, connection))
			{
				command.Parameters.AddWithValue("@InvoiceId", invoiceId);

				var reader = command.ExecuteReader();
                var schema = reader.GetSchemaTable();
                while (reader.Read())
				{
					var lineItem = new InvoiceLineItem();
                    var code = (string)reader["Code"];
                    lineItem.Code = InvoiceLineItemType.BulkSms;
                    if (!string.IsNullOrEmpty(code))
                    {
                        if(InvoiceLineItemType.TryParse(code, out InvoiceLineItemType linType))
                        {
                            lineItem.Code = linType;
                        }
                        else if (code.StartsWith("Payment Fee", StringComparison.OrdinalIgnoreCase) || code.StartsWith("Transaction Fee", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.TransactionFee;
                        }
                        else if(code.StartsWith("Short Code Setup", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.ShortCodeSetup;
                        }
                        else if (code.StartsWith("Short Code", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.ShortCodeRental;
                        }
                        else if(code.StartsWith("Keyword", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.ShortCodeKeywordRental;
                        }
                        else if (code.StartsWith("RevBilled Short", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.ReverseBilledShortCodeSms;
                        }
                        else if (code.StartsWith("RevBilled", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.ReverseBilledReplySms;
                        }
                    }
                    lineItem.Description = (string) reader["Description"];
					lineItem.Quantity = (int) reader["Quantity"];
					lineItem.UnitPrice = Convert.ToDecimal(reader["Price"]);
                    lineItem.OptionalNote = reader.Read<string>(schema, "OptionalNote");
                    var dataId = reader.Read<long>(schema, "DataId", 0);
                    lineItem.DataId = dataId == 0 ? null : (long?)dataId;
                    lineItems.Add(lineItem);
				}
			}
		    return lineItems;
	    }

    }
}
