﻿using System;
using System.Collections.Generic;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
    public class PaymentRepository: BaseSqlRepository, IPaymentRepository
    {
        private readonly IInvoiceRepository invoiceRepository;
        private const string InsertPaymentSp = "Plutus_Payment_Create_V1";
        private const string GetPaymentSp = "Plutus_Payment_Get_V1";
        private const string UpdatePaymentSp = "Plutus_Payment_Update_V2";

        public PaymentRepository(string connectionString, IInvoiceRepository invoiceRepository): base(connectionString)
        {
            this.invoiceRepository = invoiceRepository;
        }

        public void Add(Payment payment)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(InsertPaymentSp, connection))
            {
                command.Parameters.AddWithValue("@TransactionId", payment.TransactionId);
                command.Parameters.AddWithValue("@InvoiceId", payment.InvoiceId ?? (object) DBNull.Value);
                command.Parameters.AddWithValue("@AccountId", payment.AccountId);
                command.Parameters.AddWithValue("@AccountType", payment.AccountType.ToKey());
                command.Parameters.AddWithValue("@Currency", payment.Currency);
                command.Parameters.AddWithValue("@Total", payment.Total);
                command.Parameters.AddWithValue("@Tax", payment.TaxValue);
                command.Parameters.AddWithValue("@Fee", payment.Fee);
                command.Parameters.AddWithValue("@Credits", payment.Credits);
                command.Parameters.AddWithValue("@InvoiceRefNr", payment.InvoiceRefNr ?? (object) DBNull.Value);
                command.Parameters.AddWithValue("@Description", payment.Description);
                command.Parameters.AddWithValue("@Gateway", payment.Gateway);
                command.Parameters.AddWithValue("@Status", payment.Status);
                command.Parameters.AddWithValue("@Created", payment.Created);
                command.Parameters.AddWithValue("@Completed", payment.Completed.HasValue ? (object)payment.Completed.Value : DBNull.Value);
                command.Parameters.AddWithValue("@RouteCountryId", payment.RouteCountryId ?? (object) DBNull.Value);
                command.ExecuteNonQuery();
            }
        }

        public Payment Get(Guid transactionId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetPaymentSp, connection))
            {
                command.Parameters.AddWithValue("@TransactionId", transactionId);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var payment = new Payment();
                        payment.TransactionId = (Guid) reader["TransactionId"];
                        if (reader["InvoiceId"] != DBNull.Value)
                        {
                            var invoice = invoiceRepository.Get((int) reader["InvoiceId"]);
                            payment.Invoice = invoice;

                        }

                        payment.AccountKey = new AccountKey(
                            (int) reader["AccountId"],
                            (string) reader["AccountType"]);
                        
                        payment.Currency = (string) reader["Currency"];
                        payment.Total = (decimal) reader["Total"];
                        payment.TaxValue =  (decimal) reader["Tax"];
                        payment.Fee = (decimal) reader["Fee"];
                        
                        
                        payment.Gateway = (string) reader["Gateway"];
                        payment.Status = (string) reader["Status"];
                        payment.Credits = (int) reader["Credits"];
                        payment.Created = (DateTime) reader["Created"];
                        if (reader["RouteCountryId"] != DBNull.Value)
                        {
                            payment.RouteCountryId = (int) reader["RouteCountryId"];
                        }
                        if (reader["Completed"] != DBNull.Value)
                        {
                            payment.Completed = (DateTime) reader["Completed"];
                        }
                        return payment;
                    }
                    throw new EntityNotFoundException(typeof(Payment), transactionId.ToString());
                }
            }
        }

        public void Update(Payment payment)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(UpdatePaymentSp, connection))
            {
                command.Parameters.AddWithValue("@TransactionId", payment.TransactionId);
                command.Parameters.AddWithValue("@InvoiceId", payment.InvoiceId ?? (object) DBNull.Value);
                command.Parameters.AddWithValue("@AccountId", payment.AccountId);
                command.Parameters.AddWithValue("@AccountType", payment.AccountType.ToKey());
                command.Parameters.AddWithValue("@Currency", payment.Currency);
                command.Parameters.AddWithValue("@Total", payment.Total);
                command.Parameters.AddWithValue("@Tax", payment.TaxValue);
                command.Parameters.AddWithValue("@Fee", payment.Fee);
                command.Parameters.AddWithValue("@Credits", payment.Credits);
                command.Parameters.AddWithValue("@InvoiceRefNr", payment.InvoiceRefNr ?? (object) DBNull.Value);
                command.Parameters.AddWithValue("@Description", payment.Description);
                command.Parameters.AddWithValue("@Gateway", payment.Gateway);
                command.Parameters.AddWithValue("@GatewayTranId", payment.GatewayTransactionId != null ? (object)payment.GatewayTransactionId : DBNull.Value);
                command.Parameters.AddWithValue("@Status", payment.Status);
                command.Parameters.AddWithValue("@Completed", payment.Completed.HasValue ? (object) payment.Completed.Value : DBNull.Value);
                command.Parameters.AddWithValue("@Created", payment.Created);
                command.Parameters.AddWithValue("@RouteCountryId", payment.RouteCountryId ?? (object) DBNull.Value);
                if (command.ExecuteNonQuery() == 0)
                {
                    throw new EntityNotFoundException(typeof(Payment), payment.TransactionId.ToString());
                }
            }
        }
    }
}
