using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class TrafficSummaryRepository: BaseSqlRepository, ITrafficSummaryRepository
    {
        private readonly IAccountRepository accRepo;

        public TrafficSummaryRepository(string connectionString, IAccountRepository accRepo) : base(connectionString)
        {
            this.accRepo = accRepo;
        }

        public List<TrafficSummary> GetAll(int accountId, DateTime start, DateTime end)
        {
            var summaries = new List<TrafficSummary>();
            var subAccounts = accRepo.GetSubAccounts(accountId);

            summaries.AddRange(GetSummaries(accountId, start, end));
            foreach (var subAccount in subAccounts)
            {
                summaries.AddRange(GetSummaries(subAccount.AccountId, start, end));
            }

            return summaries;
        }

        private IEnumerable<TrafficSummary> GetSummaries(int accountId, DateTime start, DateTime end)
        {
            using (var conn = GetOpenConnection())
            using (var cmd = GetStoredProcedure("Plutus_GetTrafficSummary_V1", conn))
            {
                cmd.Parameters.AddWithValue("@AccountId", accountId);
                cmd.Parameters.AddWithValue("@StartDate", start);
                cmd.Parameters.AddWithValue("@EndDate", end);
                var summary = new List<TrafficSummary>();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var tmp = new TrafficSummary();
                        tmp.AccountId = (int) reader["AccountId"];
                        tmp.Total = (long) reader["Total"];
                        tmp.NetworkId = (int) reader["NetworkId"];
                        tmp.Source = (string) reader["MsgSrc"];
                        tmp.MessageType = (string) reader["MsgType"];
                        tmp.Campaign = (string) reader["CampaignName"];
                        tmp.CostCentre = (string) reader["CustCostCentre"];
                        summary.Add(tmp);
                    }
                }

                return summary;
            }
        }
        
    }
}