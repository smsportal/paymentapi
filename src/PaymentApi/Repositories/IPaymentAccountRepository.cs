﻿using System.Collections.Generic;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IPaymentAccountRepository
    {

        bool TryGetPaymentAccount(AccountKey accountKey, out PaymentAccount account);

        PaymentAccount GetPaymentAccount(AccountKey accountKey);

        IList<PaymentAccount> GetAll(string clientRefCode);

        IList<PaymentAccount> GetAllByBillingGroup(int billingGroupId);

    }
}
