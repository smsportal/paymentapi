﻿using SmsPortal.Plutus.Models;
using System.Collections.Generic;

namespace SmsPortal.Plutus.Repositories
{
    public interface IPostPaidEventsInvoiceLineRepository
    {
        /// <summary>
        /// Retrieve an enumerable list of line items for (a) proforma invoice(s) related to a specific event
        /// </summary>
        /// <param name="eventId">The unique identifier of the event</param>
        /// <returns>The enumerable list of line items</returns>
        IEnumerable<PostPaidBillingCycleLineItem> GetLineItems(long eventId);
    }
}
