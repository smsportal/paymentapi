﻿using SmsPortal.Plutus.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Core.DatabaseExtensions;

namespace SmsPortal.Plutus.Repositories
{
    public class CostEstimateRepository : BaseSqlRepository, ICostEstimateRepository
    {
        private readonly ICurrencyRepository currencyRepository;
        private readonly IInvoiceRepository invoiceRepository;
        private const string GetCostEstimateNumberSp = "Plutus_Site_CostEstimateReferenceNumber_Get_V1";
        private const string CreateCostEstimateSp = "Plutus_CostEstimate_Create_V1";
        private const string CreateCostEstimateLineItemSp = "Plutus_CostEstimate_LineItem_Create_V1";
        private const string LinkCostEstimateToInvoiceSp = "Plutus_CostEstimate_Invoice_Link";
        private const string CreateCostEstimatePdfSp = "Plutus_CostEstimatePdf_Create_V1";
        private const string ConvertCostEstimateSp = "Plutus_CostEstimate_ToInvoice_V1";
        private const string GetCostEstimateByReferenceSp = "Plutus_CostEstimate_Get_V1";
        private const string GetCostEstimatePdfSp = "Plutus_CostEstimatePdf_Get_V1";
        private const string GetCostEstimateLineItems = "Plutus_InvoiceLineItems_Get_V2";

        public CostEstimateRepository(
            string connectionString,
            ICurrencyRepository currencyRepository,
            IInvoiceRepository invoiceRepository) 
            : base(connectionString)
        {
            this.currencyRepository = currencyRepository;
            this.invoiceRepository = invoiceRepository;
        }

        public long GetNextCostEstimateNumber(int siteId)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(GetCostEstimateNumberSp, connection))
            {
                cmd.Parameters.AddWithValue("@SiteId", siteId);
                cmd.Parameters.Add("@CostEstimateReferenceNumber", SqlDbType.BigInt);
                cmd.Parameters["@CostEstimateReferenceNumber"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                return (long) cmd.Parameters["@CostEstimateReferenceNumber"].Value;
            }
        }

        public int Add(Invoice invoice, PostPaidBillingCycle cycle)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(CreateCostEstimateSp, connection))
            {
                if (invoice.ResellerId > 0)
                {
                    cmd.Parameters.AddWithValue("@ResellerId", invoice.ResellerId);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@LoginId", invoice.LoginId);
                }

                cmd.Parameters.AddWithValue("@ReferenceNumber", invoice.InvoiceNr);
                cmd.Parameters.AddWithValue("@AccountName", invoice.AccountName);
                cmd.Parameters.AddWithValue("@ClientRef", invoice.PastelCode);
                cmd.Parameters.AddWithValue("@CustomerRef", invoice.CustomerRef);
                cmd.Parameters.AddWithValue("@CreatedDate", invoice.Created);
                cmd.Parameters.AddWithValue("@TaxValue", invoice.TaxAmount);
                cmd.Parameters.AddWithValue("@TaxPercentage", invoice.TaxPercentage);
                cmd.Parameters.AddWithValue("@Total", invoice.Total);
                cmd.Parameters.AddWithValue("@SubTotal", invoice.SubTotal);
                cmd.Parameters.AddWithValue("@International", invoice.IsInternational);
                cmd.Parameters.AddWithValue("@Currency", invoice.Currency);

                if (cycle != null)
                {
                    cmd.Parameters.AddWithValue("@PeriodBilledFromDate", cycle.CycleStartDate.Date);
                    cmd.Parameters.AddWithValue("@PeriodBilledToDate", cycle.CycleEndDate.Date);
                    cmd.Parameters.AddWithValue("@InvoiceType", (int) cycle.LineItemType);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@InvoiceType", (int) invoice.GetLineItemType());
                }

                var invoiceCurrency = currencyRepository.GetCurrency(invoice.Currency);
                cmd.Parameters.AddWithValue("@ZarTotal", invoiceCurrency.Convert(Currency.BaseCurrency, invoice.Total));
                cmd.Parameters.AddWithValue("@ZarSubtotal",
                    invoiceCurrency.Convert(Currency.BaseCurrency, invoice.SubTotal));
                cmd.Parameters.Add("@CostEstimateId", SqlDbType.Int);
                cmd.Parameters["@CostEstimateId"].Direction = ParameterDirection.Output;
                cmd.ExecuteScalar();
                var costEstimateId = (int) cmd.Parameters["@CostEstimateId"].Value;
                AddLineItems(invoice, costEstimateId);
                return costEstimateId;
            }
        }

        private void AddLineItems(Invoice invoice, int costEstimateId)
        {
            if (invoice.Id > 0)
            {
                using (var connection = GetOpenConnection())
                using (var command = GetStoredProcedure(LinkCostEstimateToInvoiceSp, connection))
                {
                    command.Parameters.AddWithValue("@CostEstimateId", costEstimateId);
                    command.Parameters.AddWithValue("@InvoiceId", invoice.Id);
                    command.ExecuteNonQuery();
                }
            }
            else
            {
                foreach (var lineItem in invoice.GetLineItems())
                {
                    using (var connection = GetOpenConnection())
                    using (var command = GetStoredProcedure(CreateCostEstimateLineItemSp, connection))
                    {
                        command.Parameters.AddWithValue("@CostEstimateId", costEstimateId);
                        command.Parameters.AddWithValue("@Code", lineItem.Code);
                        command.Parameters.AddWithValue("@Description", lineItem.Description);
                        command.Parameters.AddWithValue("@Quantity", lineItem.Quantity);
                        command.Parameters.AddWithValue("@Price", lineItem.UnitPrice);
                        command.Parameters.AddWithValue("@Total", lineItem.LineTotal);
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public void AddPdf(int costEstimateId, byte[] pdfBytes)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(CreateCostEstimatePdfSp, connection))
            {
                cmd.Parameters.AddWithValue("@CostEstimateId", costEstimateId);
                cmd.Parameters.Add(new SqlParameter
                {
                    ParameterName = "@Pdf",
                    SqlDbType = SqlDbType.VarBinary,
                    Value = pdfBytes
                });
                cmd.ExecuteNonQuery();
            }
        }

        public ProformaInvoice Get(string reference)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetCostEstimateByReferenceSp, connection))
            {
                command.Parameters.AddWithValue("@Reference", reference);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var invoice = new ProformaInvoice();
                        invoice.Cycle = new PostPaidBillingCycle();

                        invoice.Id = -(int) reader["Id"];
                        invoice.TaxPercentage = Convert.ToDecimal(reader["TaxPercentage"]);
                        invoice.InvoiceNr = (string) reader["ReferenceNumber"];
                        invoice.PastelCode = (string) reader["ClientRef"];
                        invoice.IsInternational = (bool) reader["International"];
                        invoice.LoginId = reader["LoginId"] == DBNull.Value ? 0 : (int) reader["LoginId"];
                        invoice.ResellerId = reader["ResellerId"] == DBNull.Value ? 0 : (int) reader["ResellerId"];
                        invoice.AccountName = (string) reader["AccountName"];
                        invoice.PurchaseOrderNr = (string) reader["OrderNumber"];
                        invoice.CustomerRef = (string) reader["CustomerRef"];
                        invoice.Currency = (string) reader["Currency"];

                        if (DBNull.Value != reader["TransactionId"])
                        {
                            if (Guid.TryParse((string) reader["TransactionId"], out var tranId))
                            {
                                invoice.TransactionId = tranId;
                            }
                        }

                        invoice.Status = (string) reader["Status"];

                        invoice.BillingAddress = new BillingDetails
                        {
                            CompanyName = invoice.AccountName,
                            Address1 = "Billing address not retrieved",
                        };

                        invoice.Created = (DateTime) reader["CreatedDate"];
                        if (DBNull.Value != reader["DueDate"])
                        {
                            invoice.DueDate = (DateTime) reader["DueDate"];
                        }

                        if (DBNull.Value != reader["PeriodBilledFromDate"])
                        {
                            invoice.Cycle.CycleStartDate = (DateTime) reader["PeriodBilledFromDate"];
                        }

                        if (DBNull.Value != reader["PeriodBilledToDate"])
                        {
                            invoice.Cycle.CycleEndDate = (DateTime) reader["PeriodBilledToDate"];
                        }

                        foreach (var lineItem in GetLineItems(-invoice.Id))
                        {
                            invoice.AddLineItem(lineItem);
                        }

                        return invoice;
                    }
                }

                throw new EntityNotFoundException(typeof(Invoice), reference);
            }
        }

        public List<InvoiceLineItem> GetLineItems(int costEstimateId)
        {
            var lineItems = new List<InvoiceLineItem>();
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetCostEstimateLineItems, connection))
            {
                command.Parameters.AddWithValue("@CostEstimateId", costEstimateId);

                var reader = command.ExecuteReader();
                var schema = reader.GetSchemaTable();
                while (reader.Read())
                {
                    var lineItem = new InvoiceLineItem();
                    var code = (string) reader["Code"];
                    lineItem.Code = InvoiceLineItemType.BulkSms;
                    if (!string.IsNullOrEmpty(code))
                    {
                        if (InvoiceLineItemType.TryParse(code, out InvoiceLineItemType linType))
                        {
                            lineItem.Code = linType;
                        }
                        else if (code.StartsWith("Payment Fee", StringComparison.OrdinalIgnoreCase) ||
                                 code.StartsWith("Transaction Fee", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.TransactionFee;
                        }
                        else if (code.StartsWith("Short Code Setup", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.ShortCodeSetup;
                        }
                        else if (code.StartsWith("Short Code", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.ShortCodeRental;
                        }
                        else if (code.StartsWith("Keyword", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.ShortCodeKeywordRental;
                        }
                        else if (code.StartsWith("RevBilled Short", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.ReverseBilledShortCodeSms;
                        }
                        else if (code.StartsWith("RevBilled", StringComparison.OrdinalIgnoreCase))
                        {
                            lineItem.Code = InvoiceLineItemType.ReverseBilledReplySms;
                        }
                    }

                    lineItem.Description = (string) reader["Description"];
                    lineItem.Quantity = (int) reader["Quantity"];
                    lineItem.UnitPrice = Convert.ToDecimal(reader["Price"]);
                    lineItem.OptionalNote = reader.Read<string>(schema, "OptionalNote");
                    var dataId = reader.Read<long>(schema, "DataId", 0);
                    lineItem.DataId = dataId == 0 ? null : (long?) dataId;
                    lineItems.Add(lineItem);
                }
            }

            return lineItems;
        }

        public byte[] GetPdf(string reference)
        {
            using (var connecion = GetOpenConnection())
            using (var command = GetStoredProcedure(GetCostEstimatePdfSp, connecion))
            {
                command.Parameters.AddWithValue("@Reference", reference);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return (byte[]) reader["InvoiceData"];
                    }
                }

                throw new EntityNotFoundException(typeof(byte[]), "Invoice {reference} pdf.");
            }
        }

        public Invoice ConvertToInvoice(
            string accountType, 
            int accountId, 
            int costEstimateId, 
            string invoiceNr,
            string orderNumber, 
            string status)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(ConvertCostEstimateSp, connection))
            {
                cmd.Parameters.AddWithValue("@CostEstimateId", Math.Abs(costEstimateId)); //This handles legacy negative value differentiation with invoice id
                cmd.Parameters.AddWithValue("@AccountId", accountId);
                cmd.Parameters.AddWithValue("@AccountType", accountType);
                cmd.Parameters.AddWithValue("@OrderNumber", orderNumber);
                cmd.Parameters.AddWithValue("@Status", status);
                cmd.Parameters.AddWithValue("@Reference", invoiceNr);
                cmd.Parameters.Add("@InvoiceId", SqlDbType.Int);
                cmd.Parameters["@InvoiceId"].Direction = ParameterDirection.Output;
                cmd.ExecuteScalar();
                if ((int) cmd.Parameters["@InvoiceId"].Value > 0)
                {
                    return invoiceRepository.Get(invoiceNr);
                }

                throw new ArgumentOutOfRangeException("costEstimateId",
                    $"Could not find invoice related to cost estimate {costEstimateId}");
            }
        }
    }
}
