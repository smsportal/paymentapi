using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public interface IPriceListRepository
    {
        PriceList Get(int id);
    }
}