using System;
using System.Collections.Generic;
using System.Data;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Repositories
{
    public class AccountRepository: BaseSqlRepository, IAccountRepository
    {
        private const string GetSubAccountsSp = "Plutus_SubAccounts_GetById_V1";
        private const string GetAccountsSp = "Plutus_Accounts_List_V1";
        private const string GetAccountSp = "Plutus_Account_Get_V1";
        private const string UpdateAccountSp = "Plutus_Account_Update_V1";
        
        public AccountRepository(string connectionString) : base(connectionString)
        {
        }

        public IList<SmsAccount> GetSubAccounts(int masterAccountId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetSubAccountsSp, connection))
            {
                command.Parameters.AddWithValue("@MasterAccountId", masterAccountId);
                using (var reader = command.ExecuteReader())
                {
                    var accounts = new List<SmsAccount>();
                    while (reader.Read())
                    {
                        accounts.Add(BuildAccount(reader));
                    }

                    return accounts;
                }
            }
        }

        public IList<SmsAccount> GetAccounts(int? billingGroupId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetAccountsSp, connection))
            {
                command.Parameters.AddWithValue("@BillingGroupId", billingGroupId  ?? (object) DBNull.Value);
                using (var reader = command.ExecuteReader())
                {
                    var accounts = new List<SmsAccount>();
                    while (reader.Read())
                    {
                        accounts.Add(BuildAccount(reader));
                    }

                    return accounts;
                }
            }
        }

        public SmsAccount GetAccount(int accountId)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetAccountSp, connection))
            {
                command.Parameters.AddWithValue("@AccountId", accountId);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return BuildAccount(reader);
                    }
                    throw new EntityNotFoundException(typeof(SmsAccount), accountId);
                }
            }
        }

        public void Update(SmsAccount account)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(UpdateAccountSp, connection))
            {
                command.Parameters.AddWithValue("@AccountId", account.AccountId);
                command.Parameters.AddWithValue("@BillingGroupId", account.BillingGroupId ?? (object) DBNull.Value);
                command.ExecuteNonQuery();
            }
        }

        private static SmsAccount BuildAccount(IDataRecord reader)
        {
            var account = new SmsAccount();
            account.AccountId = (int) reader["LoginId"];
            account.Username = (string) reader["LoginUsername"];
            account.Created = (DateTime) reader["DateCreated"];
            account.Activated = (bool) reader["Activated"];
            if (reader["BillingGroupId"] != DBNull.Value)
            {
                account.BillingGroupId = (int?) reader["BillingGroupId"];
            }
            return account;
        }
    }
}