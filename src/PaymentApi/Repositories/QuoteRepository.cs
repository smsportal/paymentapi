﻿using SmsPortal.Plutus.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Core.DatabaseExtensions;

namespace SmsPortal.Plutus.Repositories
{
    public class QuoteRepository : BaseSqlRepository, IQuoteRepository
    {
        private readonly ICurrencyRepository currencyRepository;
        private readonly IBillingGroupOrDetailsRepository billingDetailsRepository;
        private readonly IInvoiceRepository invoiceRepository;

        private const string GetQuoteNumberSp = "Plutus_Site_QuoteReferenceNumber_Get_V1";
        private const string GetQuoteLineItems = "Plutus_InvoiceLineItems_GetByQuoteId";
        private const string CreateQuoteSp = "Plutus_Quote_Create";
        private const string CreateQuoteLineItemSp = "Plutus_Quote_LineItem_Create";
        private const string CreateQuotePdfSp = "Plutus_QuotePdf_Create";
        private const string ConvertQuoteSp = "Plutus_Quote_ToInvoice";
        private const string GetQuotePdfSp = "Plutus_QuotePdf_Get";
        private const string GetQuoteByReferenceSp = "Plutus_Quote_Get";
        private const string GetAllQuotesForResellersSp = "Plutus_Quote_GetAllForReseller";
        private const string GetAllQuotesForSmsAccsSp = "Plutus_Quote_GetAllForSmsAccount";

        public QuoteRepository(
            string connectionString,
            ICurrencyRepository currencyRepository,
            IBillingGroupOrDetailsRepository billingDetailsRepository,
            IInvoiceRepository invoiceRepository) : base(connectionString)
        {
            this.currencyRepository = currencyRepository;
            this.billingDetailsRepository = billingDetailsRepository;
            this.invoiceRepository = invoiceRepository;
        }

        public long GetNextQuoteNumber(int siteId)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(GetQuoteNumberSp, connection))
            {
                cmd.Parameters.AddWithValue("@SiteId", siteId);
                cmd.Parameters.Add("@QuoteReferenceNumber", SqlDbType.BigInt);
                cmd.Parameters["@QuoteReferenceNumber"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                return (long)cmd.Parameters["@QuoteReferenceNumber"].Value;
            }
        }

        public int Add(Invoice invoice)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(CreateQuoteSp, connection))
            {
                if (invoice.ResellerId > 0)
                {
                    cmd.Parameters.AddWithValue("@ResellerId", invoice.ResellerId);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@LoginId", invoice.LoginId);
                }
                cmd.Parameters.AddWithValue("@ReferenceNumber", invoice.InvoiceNr);
                cmd.Parameters.AddWithValue("@AccountName", invoice.AccountName);
                cmd.Parameters.AddWithValue("@ClientRef", invoice.PastelCode);
                cmd.Parameters.AddWithValue("@CustomerRef", invoice.CustomerRef);
                cmd.Parameters.AddWithValue("@CreatedDate", invoice.Created);
                cmd.Parameters.AddWithValue("@TaxValue", invoice.TaxAmount);
                cmd.Parameters.AddWithValue("@TaxPercentage", invoice.TaxPercentage);
                cmd.Parameters.AddWithValue("@Total", invoice.Total);
                cmd.Parameters.AddWithValue("@SubTotal", invoice.SubTotal);
                cmd.Parameters.AddWithValue("@International", invoice.IsInternational);
                cmd.Parameters.AddWithValue("@PurchaseOrderNr", invoice.PurchaseOrderNr);
                cmd.Parameters.AddWithValue("@Currency", invoice.Currency);
                cmd.Parameters.AddWithValue("@InvoiceType", (int)invoice.GetLineItemType());
                if (invoice.DueDate.HasValue)
                {
                    cmd.Parameters.AddWithValue("@ValidUntil", invoice.DueDate);
                }

                var invoiceCurrency = currencyRepository.GetCurrency(invoice.Currency);
                cmd.Parameters.AddWithValue("@ZarTotal", invoiceCurrency.Convert(Currency.BaseCurrency, invoice.Total));
                cmd.Parameters.AddWithValue("@ZarSubtotal", invoiceCurrency.Convert(Currency.BaseCurrency, invoice.SubTotal));
                cmd.Parameters.Add("@QuoteId", SqlDbType.Int);
                cmd.Parameters["@QuoteId"].Direction = ParameterDirection.Output;
                cmd.ExecuteScalar();
                invoice.Id = (int)cmd.Parameters["@QuoteId"].Value;
                AddLineItems(invoice);
                return invoice.Id;
            }
        }

        private void AddLineItems(Invoice invoice)
        {
            foreach (var lineItem in invoice.GetLineItems())
            {
                using (var connection = GetOpenConnection())
                using (var command = GetStoredProcedure(CreateQuoteLineItemSp, connection))
                {
                    command.Parameters.AddWithValue("@QuoteId", invoice.Id);
                    command.Parameters.AddWithValue("@Code", lineItem.Code);
                    command.Parameters.AddWithValue("@Description", lineItem.Description);
                    command.Parameters.AddWithValue("@Quantity", lineItem.Quantity);
                    command.Parameters.AddWithValue("@Price", lineItem.UnitPrice);
                    command.Parameters.AddWithValue("@Total", lineItem.LineTotal);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void AddPdf(int quoteId, byte[] pdfBytes)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(CreateQuotePdfSp, connection))
            {
                cmd.Parameters.AddWithValue("@QuoteId", quoteId);
                cmd.Parameters.Add(new SqlParameter
                {
                    ParameterName = "@Pdf",
                    SqlDbType = SqlDbType.VarBinary,
                    Value = pdfBytes
                });
                cmd.ExecuteNonQuery();
            }
        }

        public Invoice ConvertToInvoice(string accountType, int accountId, int quoteId, string invoiceNr, string orderNumber, string status)
        {
            using (var connection = GetOpenConnection())
            using (var cmd = GetStoredProcedure(ConvertQuoteSp, connection))
            {
                cmd.Parameters.AddWithValue("@QuoteId", quoteId);
                cmd.Parameters.AddWithValue("@AccountId", accountId);
                cmd.Parameters.AddWithValue("@AccountType", accountType);
                cmd.Parameters.AddWithValue("@OrderNumber", orderNumber);
                cmd.Parameters.AddWithValue("@Status", status);
                cmd.Parameters.AddWithValue("@Reference", invoiceNr);
                cmd.Parameters.Add("@InvoiceId", SqlDbType.Int);
                cmd.Parameters["@InvoiceId"].Direction = ParameterDirection.Output;
                cmd.ExecuteScalar();
                if ((int)cmd.Parameters["@InvoiceId"].Value > 0)
                {
                    return invoiceRepository.Get(invoiceNr);
                }
                throw new ArgumentOutOfRangeException("quoteId", $"Could not find invoice related to Quote {quoteId}");
            }
        }

        public Invoice Get(string reference)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetQuoteByReferenceSp, connection))
            {
                command.Parameters.AddWithValue("@Reference", reference);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return CreateQuote(reader);
                    }
                }
                throw new EntityNotFoundException(typeof(Invoice), reference);
            }
        }

        public IEnumerable<Invoice> GetAllSmsAccount(DateTime? startDate, DateTime? endDate, string reference = null)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetAllQuotesForSmsAccsSp, connection))
            {
                if (startDate.HasValue)
                {
                    command.Parameters.AddWithValue("@StartDate", startDate);
                }
                if (endDate.HasValue)
                {
                    command.Parameters.AddWithValue("@EndDate", endDate);
                }
                if (!string.IsNullOrEmpty(reference))
                {
                    command.Parameters.AddWithValue("@Reference", reference);
                }
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return CreateQuote(reader);
                    }
                }
            }
        }

        public IEnumerable<Invoice> GetAllReseller(DateTime? startDate, DateTime? endDate, string reference = null)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetAllQuotesForResellersSp, connection))
            {
                if (startDate.HasValue)
                {
                    command.Parameters.AddWithValue("@StartDate", startDate);
                }
                if (endDate.HasValue)
                {
                    command.Parameters.AddWithValue("@EndDate", endDate);
                }
                if (!string.IsNullOrEmpty(reference))
                {
                    command.Parameters.AddWithValue("@Reference", reference);
                }
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return CreateQuote(reader);
                    }
                }
            }
        }

        public byte[] GetPdf(string reference)
        {
            using (var connecion = GetOpenConnection())
            using (var command = GetStoredProcedure(GetQuotePdfSp, connecion))
            {
                command.Parameters.AddWithValue("@Reference", reference);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return (byte[])reader["InvoiceData"];
                    }
                }
                throw new EntityNotFoundException(typeof(byte[]), "Quote {reference} pdf.");
            }
        }

        private Invoice CreateQuote(SqlDataReader reader)
        {
            var invoice = new Invoice();
            invoice.Id = (int)reader["Id"];
            invoice.LoginId = reader["LoginId"] == DBNull.Value ? 0 : (int)reader["LoginId"];
            invoice.ResellerId = reader["ResellerId"] == DBNull.Value ? 0 : (int)reader["ResellerId"];
            invoice.InvoiceNr = (string)reader["ReferenceNumber"];
            invoice.AccountName = (string)reader["AccountName"];
            invoice.PastelCode = reader["ClientRef"] == DBNull.Value ? "" : (string)reader["ClientRef"];
            invoice.CustomerRef = reader["CustomerRef"] == DBNull.Value ? "" : (string)reader["CustomerRef"];
            invoice.Created = (DateTime)reader["CreatedDate"];
            invoice.DueDate = reader["ValidUntil"] == DBNull.Value ? null : (DateTime?)reader["ValidUntil"];
            invoice.TaxPercentage = Convert.ToDecimal(reader["TaxPercentage"]);
            invoice.PurchaseOrderNr = (string)reader["PurchaseOrderNr"];
            invoice.IsInternational = (bool)reader["International"];
            invoice.Currency = (string)reader["Currency"];

            invoice.Status = "Quote";
            try
            {
                invoice.BillingAddress = billingDetailsRepository.Get(invoice.AccountKey);
            }
            catch
            {
                invoice.BillingAddress = new BillingDetails
                {
                    CompanyName = invoice.AccountName,
                    Address1 = "Could not retrieve billing address",
                };
            }

            foreach (var lineItem in GetLineItems(invoice.Id))
            {
                invoice.AddLineItem(lineItem);
            }
            return invoice;
        }

        private IEnumerable<InvoiceLineItem> GetLineItems(int quoteId)
        {
            var lineItems = new List<InvoiceLineItem>();
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetQuoteLineItems, connection))
            {
                command.Parameters.AddWithValue("@QuoteId", quoteId);

                var reader = command.ExecuteReader();
                var schema = reader.GetSchemaTable();
                while (reader.Read())
                {
                    var lineItem = new InvoiceLineItem
                    {
                        Code = InvoiceLineItemType.BulkSms,
                        Description = (string)reader["Description"],
                        Quantity = (int)reader["Quantity"],
                        UnitPrice = Convert.ToDecimal(reader["Price"]),
                        OptionalNote = reader.Read<string>(schema, "OptionalNote")
                    };

                    var dataId = reader.Read<long>(schema, "DataId", 0);
                    lineItem.DataId = dataId == 0 ? null : (long?)dataId;
                    lineItems.Add(lineItem);
                }
            }
            return lineItems;
        }
    }
}
