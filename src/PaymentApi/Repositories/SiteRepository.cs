﻿using SmsPortal.Core;
using SmsPortal.Core.DatabaseExtensions;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Repositories
{
    public class SiteRepository: BaseSqlRepository, ISiteRepository
    {
        private const string GetSiteSp = "Plutus_Site_Get_V3";
        private readonly IPaymentRepository paymentRepo;
        private readonly ICurrencyRepository currencyRepo;


        public SiteRepository(
            string connectionString, 
            IPaymentRepository paymentRepo,
            ICurrencyRepository currencyRepo): base(connectionString)
        {
            this.paymentRepo = paymentRepo;
            this.currencyRepo = currencyRepo;
        }

        public Site Get(int id)
        {
            using (var connection = GetOpenConnection())
            using (var command = GetStoredProcedure(GetSiteSp, connection))
            {
                command.Parameters.AddWithValue("@SiteId", id);
                using (var reader = command.ExecuteReader())
                {
                    if (!reader.Read()) throw new EntityNotFoundException(typeof(Site), id);
                    
                    var site = new Site
                    {
                        Id = reader.Read<int>("SiteId"),
                        ResellerId = reader.Read<int>("ResellerId"),
                        Name = reader.Read<string>("SiteName"),
                        Domain = reader.Read<string>("SiteDomain"),
                        EmailAddress = reader.Read<string>("SiteEmailFrom"),
                        ContactUsEmail = reader.Read<string>("ContactUs_Email"),
                        ContactUsMobile = reader.Read<string>("ContactUs_Mobile"),
                        CostEstimatePrefix = reader.Read<string>("CostEstimatePrefix"),
                        QuotePrefix = reader.Read<string>("QuotePrefix"),
                        InvoicePrefix = reader.Read<string>("InvoicePrefix"),
                        CreditNotePrefix = reader.Read<string>("CreditNotePrefix"),
                        UseAutomaticInvoicing = reader.Read<bool>("UsesAutomaticInvoicing"),
                        AutoCreditSiteUsers = reader.Read<bool>("AutoCreditSiteUsers"),
                        PrimaryColour = reader.Read<string>("PrimaryColour"),
                        PastelRefCodes = reader.Read("PastelRefCodes", () => string.Empty).Split('|')
                    };


                    var payPalCurrencyCode = reader.Read<string>("PayPalHomeCurrency");
                    if (!string.IsNullOrEmpty(payPalCurrencyCode))
                    {
                        var payPalCurrency = currencyRepo.GetCurrency(payPalCurrencyCode);
                        var payPal = new PaymentGatewaySettings(payPalCurrency, paymentRepo, currencyRepo)
                        {
                            Gateway = PaymentGatewayType.PayPal.ToKey(),
                            CountryId = reader.Read<int>("PayPalCountry"),
                            Enabled = reader.Read<bool>("PayPalEnabled"),
                            MinTransaction = reader.Read<int>("PayPalMinValue"),
                            MaxTransaction = reader.Read<int>("PayPalMaxValue"),
                            TranFee = reader.Read<decimal>("PayPalTransFee")
                        };
                        site.PayPalSettings = payPal;    
                    }

                    var sagePayEnabled = reader.Read<bool>("SagePay_Enabled_Accounts");
                    if (sagePayEnabled)
                    {
                        var sagePayCurrency = currencyRepo.GetCurrency("ZAR");
                        var sagePay = new PaymentGatewaySettings(sagePayCurrency, paymentRepo, currencyRepo)
                        {
                            Gateway = PaymentGatewayType.NetCash.ToKey(),
                            CountryId = reader.Read<int>("SagePay_Country"),
                            Enabled = true,
                            MinTransaction = reader.Read<int>("SagePay_MinValue"),
                            MaxTransaction = reader.Read<int>("SagePay_MaxValue")
                        };
                        site.SagePaySettings = sagePay;
                    }

                    var stripeHomeCurrencyCode = reader.Read<string>("StripeHomeCurrency");
                    if (!string.IsNullOrEmpty(stripeHomeCurrencyCode))
                    {
                        var stripeCurrency = currencyRepo.GetCurrency(stripeHomeCurrencyCode);
                        var stripe = new PaymentGatewaySettings(stripeCurrency, paymentRepo, currencyRepo)
                        {
                            Gateway = PaymentGatewayType.Stripe.ToKey(),
                            CountryId = reader.Read<int>("StripeCountryId"),
                            Enabled = reader.Read<bool>("StripeEnabled"),
                            MinTransaction = reader.Read<int>("StripeMinValue"),
                            MaxTransaction = reader.Read<int>("StripeMaxValue")
                        };
                        site.StripeSettings = stripe;
                    }

                    var eftCurrencyCode = reader.Read<string>("EftHomeCurrency");
                    if (!string.IsNullOrEmpty(eftCurrencyCode))
                    {
                        var eftCurrency = currencyRepo.GetCurrency(eftCurrencyCode);
                        var eftSettings = new PaymentGatewaySettings(eftCurrency, paymentRepo, currencyRepo)
                        {
                            Gateway = PaymentGatewayType.Eft.ToKey(),
                            CountryId = reader.Read<int>("EftCountry"),
                            Enabled = reader.Read<bool>("EnableEftForAccounts"),
                            MinTransaction = reader.Read<int>("EftMinValue"),
                            MaxTransaction = reader.Read<int>("EftMaxValue")
                        };
                        site.EftSettings = eftSettings;
                    }

                        
                    site.TaxEnabled = reader.Read<bool>("Site_Vat");
                    return site;
                }
            }
        }
    }
}
