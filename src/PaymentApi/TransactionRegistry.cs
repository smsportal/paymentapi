﻿using System.Collections.Generic;

namespace SmsPortal.Plutus
{
    public class TransactionRegistry
    {
        private readonly HashSet<string> tranIds = new HashSet<string>();
        private readonly object locker = new object();

        

        public bool TryAdd(string tranId)
        {
            lock (locker)
            {
                return tranIds.Add(tranId);
            }
        }

        public bool TryRemove(string tranId)
        {
            lock (locker)
            {
                return tranIds.Remove(tranId);
            }
        }
    }
}
