﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmsPortal.Plutus.Attributes
{
    /// <summary>
    /// Validates that a field is not null.
    /// </summary>
    public class FieldIsDestinationAddress: ValidationAttribute
    {
        protected override ValidationResult IsValid(object field, ValidationContext validationContext)
        {
            var value = field as string;

            if (value == null)
            {
                return ValidationResult.Success;
            }

            if (value.Length > 21)
            {
                throw new Exception($"The field '{validationContext.MemberName}' must be shorter than 21 characters.");
            }

            foreach (char c in value)
            {
                if (c < '0' || c > '9')
                {
                    throw new Exception($"The field '{validationContext.MemberName}' can only be numeric.");
                }
            }

            return ValidationResult.Success;
        }
    }
}
