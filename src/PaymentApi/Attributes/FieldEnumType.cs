﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace SmsPortal.Plutus.Attributes
{
    /// <summary>
    /// Validates whether a field is of the specified enum type.
    /// </summary>
    public class FieldEnumType : ValidationAttribute
    {
        private readonly Type enumType;
        private readonly string[] enumNames;
        private readonly string validValues;

        public FieldEnumType(Type enumType)
        {
            this.enumType = enumType;

            enumNames = enumType.GetEnumNames();

            var sb = new StringBuilder();
            foreach (var enumName in enumNames)
            {
                sb.Append($" {enumName},");
            }
            validValues = sb.ToString().TrimEnd(',');
        }

        protected override ValidationResult IsValid(object field, ValidationContext validationContext)
        {
            if (field == null)
            {
                return ValidationResult.Success;
            }

            if (!enumType.IsEnum)
            {
                return ValidationResult.Success;
            }

            var value = field as string;

            if (!enumNames.Contains(value))
            {
                throw new Exception(
                    $"The required parameter '{validationContext.MemberName}' has an invalid type '{value}'. Valid values are{validValues}.");
            }

            return ValidationResult.Success;
        }
    }
}
