﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmsPortal.Plutus.Attributes
{
    /// <summary>
    /// Validates that a field is not null.
    /// </summary>
    public class FieldRequired: ValidationAttribute
    {
        private readonly bool allowEmptyString;

        public FieldRequired(bool allowEmptyString = false)
        {
            this.allowEmptyString = allowEmptyString;
        }

        protected override ValidationResult IsValid(object field, ValidationContext validationContext)
        {
            if (field == null)
            {
                throw new Exception(
                    $"The required parameter '{validationContext.MemberName}' has not been specified.");
            }

            if (!allowEmptyString)
            {
                if (field is string)
                {
                    var value = (string) field;
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        throw new Exception($"The required parameter '{validationContext.MemberName}' has been specified but is empty or contains only whitespace.");
                    }
                }
            }

            return ValidationResult.Success;
        }
    }
}
