﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace SmsPortal.Plutus.Attributes
{
    /// <summary>
    /// Validates that a string or collection is smaller than a specified length.
    /// </summary>
    public class FieldMaxLength : ValidationAttribute
    {
        private readonly int maxLength;

        public FieldMaxLength(int maxLength)
        {
            this.maxLength = maxLength;
        }

        protected override ValidationResult IsValid(object field, ValidationContext validationContext)
        {
            if (field == null)
            {
                return ValidationResult.Success;
            }

            if (field is string)
            {
                var str = (string) field;

                if (str.Length > maxLength)
                {
                    throw new Exception($"The field '{validationContext.MemberName}' must be smaller than {maxLength}.");
                }

                return ValidationResult.Success;
            }


            if (field is ICollection)
            {
                var collection = (ICollection) field;

                if (collection.Count > maxLength)
                {
                    throw new Exception($"The field '{validationContext.MemberName}' must be smaller than {maxLength}.");
                }
            }

            return ValidationResult.Success;
        }
    }
}
