﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace SmsPortal.Plutus.Attributes
{
    public class CollectionCountRange : ValidationAttribute
    {
        private readonly int lowerBound;
        private readonly int upperBound;

        /// <summary>
        /// Validates that a collection has at least the minimum and does not exceed the maximum number of specified items
        /// </summary>
        /// <param name="lowerBound">The minimum number of items that the collection must have</param>
        /// <param name="upperBound">The maximum number of items that the collection must have</param>
        public CollectionCountRange(int lowerBound, int upperBound)
        {
            this.lowerBound = lowerBound;
            this.upperBound = upperBound;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var collection = (value as ICollection);
            if (collection == null)
            {
                return ValidationResult.Success;
            }

            if (collection.Count < lowerBound || collection.Count > upperBound)
            {
                throw new Exception(
                    $"The field '{validationContext.MemberName}' must have a count between the range {lowerBound} and {upperBound}.");
            }

            return ValidationResult.Success;
        }
    }
}
