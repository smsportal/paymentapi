﻿using System;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using Autofac;
using Bugle.Comms;
using log4net;
using log4net.Config;
using Metrics;
using Microsoft.Owin.Hosting;
using SmsPortal.CoreMetrics;
using SmsPortal.Plutus.Bootstrap;
using SmsPortal.Plutus.Services;

namespace SmsPortal.Plutus
{
    /// <summary>
    /// The Plutus windows service interface implementation.
    /// </summary>
	public class RestService : ServiceBase
	{
		private IProformaInvoiceService postPaidService;
		private IExpiryService expiryService;
		private IEventRefundService refundService;
		private IDisposable restServer;
		private IHeartbeatSender heartbeat;
		private readonly string baseUri = ConfigurationManager.AppSettings["BaseUri"];
		private static readonly ILog Log = LogManager.GetLogger(typeof(RestService));

		private static void Initialize()
		{
			var logCfgPath = ConfigurationManager.AppSettings["Log4netConfigFile"];
			if (logCfgPath == null)
			{
				BasicConfigurator.Configure();
				Log.Warn("Could not find file path for [Log4netConfigFile] in app.config. Using default configuration.");
			}
			else
			{
				var logCfg = new FileInfo(logCfgPath);
				if (!logCfg.Exists)
				{
					BasicConfigurator.Configure();
					Log.WarnFormat("Could not find file {0} for log4net. Using default configuration.", logCfg.FullName);
				}
				else
				{
					XmlConfigurator.ConfigureAndWatch(logCfg);
				}
			}
			AppDomain.CurrentDomain.UnhandledException += LogUnhandledException;
		}

		/// <inheritdoc />
		protected override void OnStart(string[] args)
		{
			Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
			Initialize();
			Log.Info("Starting Plutus Service");
			ConfigureMetrics();
			var webAppOptions = new StartOptions();
			webAppOptions.Urls.Add(baseUri);
			restServer = WebApp.Start<Startup>(webAppOptions);
			expiryService = Startup.Container.Resolve<IExpiryService>();
			expiryService.Start();
			refundService = Startup.Container.Resolve<IEventRefundService>();
			refundService.Start();
			postPaidService = Startup.Container.Resolve<IProformaInvoiceService>();
			postPaidService.Start();
			heartbeat = Startup.Container.Resolve<IHeartbeatSender>();
			heartbeat.Start();
			Log.Info("Successfully started Plutus Service");
		}

		/// <inheritdoc />
		protected override void OnStop()
		{
			Log.Info("Stopping Plutus Service");
			heartbeat?.Stop();
			refundService.Stop();
			expiryService.Stop();
			postPaidService.Stop();

			restServer.Dispose();
			Log.Info("Successfully stopped Plutus Service");
		}

		static void Main(string[] args)
		{
			var service = new RestService();
			if (!Environment.UserInteractive)
			{
				Run(service);
			}
			else
			{
				service.OnStart(args);
				Console.WriteLine(@"Press Enter to quit.");
				Console.ReadLine();
				service.OnStop();
			}
		}

		private static void LogUnhandledException(object sender, UnhandledExceptionEventArgs args)
		{
			var exception = args.ExceptionObject as Exception;
			Log.Fatal(exception);
			if (exception != null) throw exception;
		}

		private static void ConfigureMetrics()
		{
			Metric
				.EmptyGlobalContextConfig()
				.WithReporting(InfluxDbHelper.InfluxDbHttpDbReport())
				.WithReporting(InfluxDbHelper.InfluxDbHttpApplicationReport())
				.WithReporting(InfluxDbHelper.InfluxDbHttpRabbitMqReport())
				.WithAppCounters();
		}
	}
}
