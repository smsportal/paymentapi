﻿using System;
using System.Linq;
using System.Reflection;
using SmsPortal.Plutus.Attributes;
using Swashbuckle.Swagger;

namespace SmsPortal.Plutus
{
    public class CustomSwaggerSchemaFilter : ISchemaFilter
    {
        public void Apply(Schema schema, SchemaRegistry schemaRegistry, Type type)
        { 
            UpdateSchemaWithFieldRequiredAnnotation(schema, type);
        }

        private void UpdateSchemaWithFieldRequiredAnnotation(Schema schema, Type type)
        {
            var publicProperties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);

            var additionalRequiredParameters = publicProperties
                .Where(p => p.GetCustomAttribute<FieldRequired>() != null)
                .Select(p => char.ToLowerInvariant(p.Name[0]) + p.Name.Substring(1))  //Ensure the first character is lowercase
                .ToList();

            if (schema.required != null)
            {
                additionalRequiredParameters.AddRange(schema.required);
            }

            schema.required = additionalRequiredParameters;
        }
    }
}
