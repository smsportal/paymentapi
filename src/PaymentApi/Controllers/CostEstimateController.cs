﻿using AutoMapper;
using log4net;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;
using System.Web.Http;
using SmsPortal.Core;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Controllers
{
    [RoutePrefix("api/costestimate")]
    public class CostEstimateController : ApiController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CostEstimateController));
        private readonly ICostEstimateRepository costEstimateRepository;
        private readonly IPostPaidBillingCycleRepository postPaidBillingRepository;
        private readonly IInvoiceRepository invoiceRepository;
        private readonly ISiteRepository siteRepository;
        private readonly IPaymentAccountRepository paymentAccountRepository;
        private readonly IPdfService pdfService;
        private readonly IEmailService emailer;
        private const int SmsPortalSiteId = 1;


        public CostEstimateController (
            ICostEstimateRepository costEstimateRepository,
            IPostPaidBillingCycleRepository postPaidBillingRepository,
            IInvoiceRepository invoiceRepository,
            ISiteRepository siteRepository,
            IPaymentAccountRepository paymentAccountRepository,
            IPdfService pdfService,
            IEmailService emailer)
        {
            this.costEstimateRepository = costEstimateRepository;
            this.postPaidBillingRepository = postPaidBillingRepository;
            this.invoiceRepository = invoiceRepository;
            this.siteRepository = siteRepository;
            this.paymentAccountRepository = paymentAccountRepository;
            this.pdfService = pdfService;
            this.emailer = emailer;
        }

        [HttpPost]
        [Route("invoice")]
        public CreateInvoiceResponse Create(ConvertCostEstimateToInvoiceRequest request)
        {
            var response = new CreateInvoiceResponse();
            var accountKey = new AccountKey(request.AccountId, request.AccountType);
            var account = paymentAccountRepository.GetPaymentAccount(accountKey);

            var site = siteRepository.Get(account.SiteId);
            var invoiceNr = site.GetInvoiceNr(invoiceRepository.GetNextInvoiceNumber(account.SiteId));
            var invoice = costEstimateRepository.ConvertToInvoice(request.AccountType, request.AccountId, request.CostEstimateId, invoiceNr, request.OrderNumber, string.IsNullOrEmpty(request.Status) ? "PostPaidApproved" : request.Status);
            var cycle = postPaidBillingRepository.IdentifyBillingCycle(invoice.Id);
            var pdf = pdfService.CreatePdf(invoice, cycle);
            invoiceRepository.AddPdf(invoice.Id, pdf);

            var ppInv = Mapper.Map<ProformaInvoice>(invoice);
            ppInv.Cycle = cycle;
            postPaidBillingRepository.Approve(ppInv, cycle.ItemDescription);
            
            if ((invoice.Status ?? "").Equals("PAID", System.StringComparison.OrdinalIgnoreCase))
            {
                emailer.SendPaidInvoiceEmail(invoice, invoice.BillingAddress.InvoiceEmail, pdf, SmsPortalSiteId);
            }
            else
            {
                emailer.SendUnpaidPostPaidInvoiceCreated(invoice, pdf, SmsPortalSiteId);
            }

            response.Invoice = Mapper.Map<InvoiceDto>(invoice);
            log.Info($"Cost Estimate for {invoice.AccountName} based on {cycle.ItemDescription} {cycle.BreakdownDescription} converted to Invoice {invoice.InvoiceNr} with order number {invoice.PurchaseOrderNr}");
            return response;
        }
    }
}
