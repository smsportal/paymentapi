using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using log4net;
using Microsoft.Web.Http;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;
using SmsPortal.Plutus.v2.Model;

namespace SmsPortal.Plutus.Controllers.Invoicing.v2
{

    [ApiVersion("2.0")]
    [RoutePrefix("api/invoice")]
    public class InvoiceController: ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(InvoiceController));
        private readonly IPaymentAccountRepository accountRepo;
        private readonly ITrafficSummaryRepository trafficSummaryRepo;
        private readonly IPdfService pdfService;
        private readonly IPriceListRepository priceListRepo;
        private readonly IInvoiceRepository invoiceRepo;
        private readonly ISiteRepository siteRepo;
        private readonly IBillingGroupRepository billingGroupRepository;
        private readonly ISettlementDiscountRepository discountRepo;

        public InvoiceController(
            IPaymentAccountRepository accountRepo,
            ITrafficSummaryRepository trafficSummaryRepo,
            IPdfService pdfService,
            IPriceListRepository priceListRepo,
            IInvoiceRepository invoiceRepo,
            ISiteRepository siteRepo,
            IBillingGroupRepository billingGroupRepo,
            ISettlementDiscountRepository discountRepo)
        {
            this.accountRepo = accountRepo;
            this.trafficSummaryRepo = trafficSummaryRepo;
            this.pdfService = pdfService;
            this.priceListRepo = priceListRepo;
            this.invoiceRepo = invoiceRepo;
            this.siteRepo = siteRepo;
            this.billingGroupRepository = billingGroupRepo;
            this.discountRepo = discountRepo;
        }

        [Route]
        [HttpGet]
        public ListInvoiceResponse List([FromUri] ListInvoiceRequest request)
        {
            var invoices = invoiceRepo.List(request.ClientRefCode);
            var resp = new ListInvoiceResponse();
            resp.Invoices = invoices.Select(Mapper.Map<InvoiceDto>).ToList();
            return resp;
        }


        [Route]
        [HttpPost]
        public CreatePostPaidInvoiceResponse Post(CreatePostPaidInvoiceRequest request)
        {
            var cycle = PostPaidBillingCycle.FromMonth(request.Month);

            var billingGroup = billingGroupRepository.GetByPastelCode(request.ClientRefCode);
            if (billingGroup.PriceType != PriceType.PriceList 
                || billingGroup.PriceListId == null)
            {
                throw new InvalidOperationException(
                    $"Billing Group {billingGroup.Id} for pastel code {request.ClientRefCode} does not have/use a price list");
            }

            var accounts = accountRepo.GetAllByBillingGroup(billingGroup.Id);
            
            if (accounts.Count == 0)
            {
                return new CreatePostPaidInvoiceResponse
                {
                    ErrorMessage = $"No accounts found for Client Ref Code {request.ClientRefCode}",
                    ErrorCode = "KeyNotFoundException"
                };
            }

            var discount = discountRepo.GetByPastelCode(billingGroup.PastelCode);
            
            var priceList = priceListRepo.Get(billingGroup.PriceListId.Value);
            var invoices = accounts
                .Select(acc => BillAccountForUsage(
                    acc,
                    priceList,
                    billingGroup,
                    cycle,
                    request.DryRun,
                    discount))
                .Where(inv => inv != null)
                .ToList();

            foreach (var invoice in invoices)
            {
                invoice.Id = invoiceRepo.Add(invoice);
                var pdf = pdfService.CreatePdf(invoice, cycle);
                invoiceRepo.AddPdf(invoice.Id, pdf);
            }

            var resp = new CreatePostPaidInvoiceResponse();
            resp.InvoiceIds.AddRange(invoices.Select(inv => inv.Id));
            return resp;
        }

        private PostpaidInvoice BillAccountForUsage(
            PaymentAccount account,
            PriceList priceList,
            BillingGroup billingGroup,
            PostPaidBillingCycle cycle,
            bool dryRun,
            SettlementDiscount discount)
        {
            var nwUsages = trafficSummaryRepo.GetAll(account.Id, cycle.CycleStartDate, cycle.CycleEndDate)
                .Where(li => li.NetworkId != 0 || billingGroup.BillForNoNetworks)
                .GroupBy(summary => new {summary.NetworkId, summary.MessageType})
                .Select(grouping => new
                {
                    grouping.Key.NetworkId,
                    Total = grouping.Sum(summary => summary.Total),
                    grouping.Key.MessageType
                })
                .ToList();

            if (nwUsages.Count == 0)
            {
                Log.Info($"No traffic found for account {account} for period {cycle}. No invoice generated.");
                return null;
            }

            var site = siteRepo.Get(account.SiteId);
            var builder = new PostpaidInvoiceBuilder();
            var refNr = !dryRun
                ? site.GetInvoiceNr(invoiceRepo.GetNextInvoiceNumber(site.Id))
                : "DRY" + invoiceRepo.GetNextDryInvoiceNumber(site.Id);
            var invoice = builder.WithApproval(dryRun ? (bool?) null : true)
                .WithBillingCycle(cycle)
                .WithCurrency(account.BaseCurrency)
                .WithInvoiceNr(refNr)
                .Build(billingGroup, account, site);
            
            var lineItems = new List<InvoiceLineItem>();
            foreach (var usage in nwUsages)
            {
                var lineItem = new InvoiceLineItem
                {
                    Code = InvoiceLineItemType.BulkSms,
                    Quantity = (int) usage.Total
                };
                SmsType smsType;
                switch (usage.MessageType)
                {
                    case "SmsR":
                        smsType = SmsType.ReverseBillMobileTerminating;
                        break;
                    case "MoR":
                        smsType = SmsType.ReverseBillMobileOriginating;
                        break;
                    case "Sms":
                        smsType = SmsType.StandardRateMobileTerminating;
                        break;
                    default:
                        throw new InvalidOperationException($"Unsupported type '{usage.MessageType}' found to be billed");
                }
                lineItem.UnitPrice = priceList.GetNetworkPrice(usage.NetworkId, smsType);
                lineItem.Description = priceList.GetProductDescription(usage.NetworkId, smsType);
                lineItems.Add(lineItem);
            }

            lineItems = lineItems.OrderBy(li => li.Description).ToList();

            foreach (var li in lineItems)
            {
                invoice.AddLineItem(li);
            }

            if (discount != null)
            {
                invoice.AddLineItem(discount.ToLineItem());
            }

            return invoice;
        }
    }
}
