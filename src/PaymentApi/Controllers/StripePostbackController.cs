﻿using System;
using System.Linq;
using System.Web.Http;
using log4net;
using SmsPortal.Plutus.Controllers.Payments;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;
using Stripe;

namespace SmsPortal.Plutus.Controllers
{
    [RoutePrefix("api/stripe")]
    public class StripePostbackController : PaymentProcessingController
    {
        private readonly IPaymentGatewayRepository gatewayRepo;

        public StripePostbackController(
            IPaymentRepository paymentRepo,
            IPaymentAccountRepository accountRepo,
            IPaymentGatewayRepository gatewayRepo,
            TransactionRegistry tranRegistry,
            ICreditAllocationService creditAllocator,
            ILog log = null)
            : base(PaymentGatewayType.Stripe, tranRegistry, paymentRepo, accountRepo, creditAllocator, log)
        {
            this.gatewayRepo = gatewayRepo;
        }

        private PaymentIntent ToPaymentIntent<T>(T postBack)
        {
            switch (postBack)
            {
                case Event stripeEvent:
                    return (PaymentIntent)stripeEvent.Data.Object;

                case PaymentIntent p:
                    return p;

                default:
                    throw new ArgumentException($"Could not identify the PaymentIntent on the {typeof(T)} object");
            }
        }

        protected override bool IsAccepted<T>(T postBack, string transactionId)
        {
            if (string.IsNullOrEmpty(transactionId)) return false;

            var intent = ToPaymentIntent(postBack);
            return
                !intent.Status.Equals("canceled", StringComparison.OrdinalIgnoreCase) &&
                !intent.Status.Equals("requires_payment_method", StringComparison.OrdinalIgnoreCase);
        }

        protected override string CopyPostbackTracingDataToPayment<T>(ref Payment payment, T postBack, string transactionId)
        {
            var intent = ToPaymentIntent(postBack);
            payment.GatewayTransactionId = intent?.Id ?? string.Empty;
            return intent?.Id ?? string.Empty;
        }

        protected override decimal PostbackAmount<T>(T postBack)
        {
            var intent = ToPaymentIntent(postBack);
            return Convert.ToDecimal(intent?.Amount) / 100m; //Since stripe works in cents
        }

        protected override string PostbackDebugString<T>(T postBack)
        {
            switch (postBack)
            {
                case Event stripeEvent:
                    return "[Stripe.Event" +
                           $"\r\n\tId={stripeEvent.Id}," +
                           $" Type={stripeEvent.Type}," +
                           $"\r\n\tData={stripeEvent.Data}" +
                           "]";
                
                case PaymentIntent intent:
                    return
                        "[Stripe.PaymentIntent" +
                        $"\r\n\tId={intent.Id}," +
                        $"\r\n\tCurrency={intent.Currency}," +
                        $" Amount={intent.Amount}," +
                        $"\r\n\tPaymentMethodId={intent.PaymentMethodId}," +
                        $" PaymentId={intent}" +
                        "]";
            }

            return postBack.ToString();
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Post([FromUri] int siteId)
        {
            string postbackDebug = null;
            string paymentDebug = null;
            try
            {
                var json = Request.Content.ReadAsStringAsync().Result;
                    
                var settings = gatewayRepo.GetStripeSettings(siteId);
                var signature = Request.Headers.GetValues("Stripe-Signature").First();
                postbackDebug = $"[Stripe.Event Site: {siteId} Signature: {signature} Secret: {settings.WebhookSecret}] Body: {json}";
                
                var stripeEvent = EventUtility.ConstructEvent(json, signature, settings.WebhookSecret);
                return Process(stripeEvent, out paymentDebug, out postbackDebug);
            }
            catch (StripeException e) {
                Log.PaymentException(GatewayName, paymentDebug, postbackDebug, e);
                return BadRequest();
            }
            catch (Exception e) {
                Log.PaymentException(GatewayName, paymentDebug, postbackDebug, e);
                return InternalServerError(e);
            }
        }

        public IHttpActionResult Process(Event stripeEvent, out string paymentDebug, out string postbackDebug)
        {
            postbackDebug = null;
            paymentDebug = null;
            try
            {
                postbackDebug = PostbackDebugString(stripeEvent);

                var intent = ToPaymentIntent(stripeEvent);
                var transactionId = intent.Metadata["PaymentId"];
                
                switch (stripeEvent.Type)
                {
                    case "payment_intent.succeeded":
                        if (!ProcessSuccess(intent, transactionId, out paymentDebug, out postbackDebug))
                        {
                            Log.PaymentNotProcessed(GatewayName,paymentDebug,postbackDebug);
                        }
                        return Ok();
                    
                    case "payment_intent.canceled":
                    case "payment_intent.payment_failed":
                        if (!ProcessFailure(intent, transactionId, out paymentDebug, out postbackDebug))
                        {
                            Log.PaymentNotProcessed(GatewayName,paymentDebug,postbackDebug);
                        }
                        return Ok();
                }
                
                return NotFound();
            }
            catch (Exception e)
            {
                Log.PaymentException(GatewayName, paymentDebug, postbackDebug, e);
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Check([FromUri] int siteId)
        {
            try
            {
                var settings = gatewayRepo.GetStripeSettings(siteId);
                Log.PaymentInfo($"Config check succeeded for site: {siteId}", GatewayName);
                return Ok($"PublishableKey: {settings.PublishableKey}");
            }
            catch (Exception e) {
                Log.Exception("Check Stripe Settings",
                    new PaymentGatewayConfigurationException(GatewayName, $"Config check succeeded for site: {siteId}",
                    e));
                return InternalServerError(e);
            }
        }
    }
}
