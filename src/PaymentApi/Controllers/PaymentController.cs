﻿using System;
using System.Web.Http;
using Microsoft.Web.Http;
using SmsPortal.Core;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Controllers
{
    [ApiVersion("1.0")]
    [RoutePrefix("api")]
    public class PaymentController: ApiController
    {
        private readonly IInvoiceRepository invoiceRepo;
        private readonly IPaymentAccountRepository accRepo;
        private readonly IBillingGroupOrDetailsRepository billingDetailsRepo;
        private readonly ISiteRepository siteRepo;
        private readonly IHtmlPaymentFormBuilderFactory factory;
        private readonly IEmailService emailer; // TODO: Place the IEmail and InvoiceRepo into an InvoiceIssuer class
        private readonly IPdfService pdfService;
        private readonly IPaymentRepository paymentRepo;
        private readonly ICurrencyRepository currencyRepo;
        private readonly IRouteCountryRepository routeCountryRepo;
        
        // Only SMSPortal has the billing enabled. Always use SMSPortal's details for invoices and emails.
        private const int SmsPortalSiteId = 1;

        public PaymentController(
            IInvoiceRepository invoiceRepo, 
            IPaymentAccountRepository accRepo,
            IBillingGroupOrDetailsRepository billingDetailsRepo,
            ISiteRepository siteRepo,
            IHtmlPaymentFormBuilderFactory factory,
            IEmailService emailer,
            IPdfService pdfService,
            IPaymentRepository paymentRepo,
            ICurrencyRepository currencyRepo, 
            IRouteCountryRepository routeCountryRepo)
        {
            this.invoiceRepo = invoiceRepo;
            this.accRepo = accRepo;
            this.billingDetailsRepo = billingDetailsRepo;
            this.siteRepo = siteRepo;
            this.factory = factory;
            this.emailer = emailer;
            this.pdfService = pdfService;
            this.paymentRepo = paymentRepo;
            this.currencyRepo = currencyRepo;
            this.routeCountryRepo = routeCountryRepo;
        }

        [HttpGet]
        [Route("payment/{id}")]
        public GetPaymentResponse GetPayment(string id)
        {
            var payment = paymentRepo.Get(Guid.Parse(id));
            var account = accRepo.GetPaymentAccount(payment.AccountKey);
            payment.NotificationEmail = account.EmailTo;
            var site = siteRepo.Get(account.BuysFromSite);
            var gateway = site.GetSettings(payment.Gateway.ToLower());
            
            
            var invoice = invoiceRepo.Get(payment.InvoiceRefNr);
            var resp = new GetPaymentResponse
            {
                InvoiceNr = invoice.InvoiceNr,
                Currency = payment.Currency,
                SubTotal =  payment.Total - payment.TaxValue,
                Total = payment.Total,
                TaxAmount = payment.TaxValue,
                PaymentFee = payment.Fee,
                TaxType = invoice.TaxType,
                TransactionId = payment.TransactionId.ToString(),
                Status =  payment.Status
            };

            if (gateway.Gateway.IsOneOf(new[] {PaymentGatewayType.PayPal, PaymentGatewayType.NetCash, PaymentGatewayType.NetCashInstantEft}))
            {
                var formBuilder = factory.GetFormBuilder(site, gateway.Gateway, !account.GatewaysLive);
                resp.HtmlPaymentForm = formBuilder.GetHtml(payment);
            }

            return resp;
        }

        [HttpPost]
        [Route("payment")]
        public CreatePaymentResponse Create(CreatePaymentRequest request)
        {
            if (request.AccountType == "RS" && !request.RouteCountryId.HasValue)
            {
                throw new InvalidOperationException("Reseller accounts must specify a Route Country ID");
            }
            var smsPortalSite = siteRepo.Get(SmsPortalSiteId);
            var accountKey = new AccountKey(request.LoginId, request.AccountType);
            var account = accRepo.GetPaymentAccount(accountKey);
            var invoice = CreateInvoice(account, smsPortalSite, request);
            var gateway = smsPortalSite.GetSettings(request.Gateway);
            var payment = gateway.CreatePayment(invoice, request.Quantity);
            payment.NotificationEmail = account.EmailTo;
            invoice.StartPayment(payment);
            var formBuilder = factory.GetFormBuilder(smsPortalSite, gateway.Gateway, !account.GatewaysLive);
            invoiceRepo.Add(invoice);
            payment.Invoice = invoice;
            var htmlForm = formBuilder.GetHtml(payment);
            if (request.RouteCountryId.HasValue)
            {
                payment.RouteCountryId = request.RouteCountryId;
            }
            else
            {
                if (account.AccountType == AccountType.SmsAccount)
                {
                    payment.RouteCountryId = account.RouteCountryId;
                }
            }
            paymentRepo.Update(payment);
            var pdf = pdfService.CreatePdf(invoice);
            invoiceRepo.AddPdf(invoice.Id, pdf);
            emailer.SendUnpaidPrePaidInvoiceEmailCreated(invoice, pdf, SmsPortalSiteId);
            return new CreatePaymentResponse
                    {
                        InvoiceNr = invoice.InvoiceNr,
                        Currency = invoice.Currency,
                        SubTotal = invoice.SubTotal,
                        Total = invoice.Total,
                        TaxAmount = invoice.TaxAmount,
                        PaymentFee = payment.Fee,
                        TaxType = invoice.TaxType,
                        HtmlPaymentForm = htmlForm,
                        TransactionId = payment.TransactionId.ToString()
            };
        }

        private Invoice CreateInvoice(PaymentAccount account, Site site, CreatePaymentRequest request)
        {
            var billingDetails = billingDetailsRepo.Get(account.AccountKey);
            var currencyCode = SiteCurrency.SMSPortal.Code(billingDetails.CountryId);
            var invoiceCurrency = currencyRepo.GetCurrency(currencyCode);
            var invoice = new Invoice();
            if (billingDetails.CountryId == TaxConfig.SouthAfrica.CountryId)
            {
                invoice.TaxPercentage = TaxConfig.SouthAfrica.TaxPercentage;
                invoice.TaxType = TaxConfig.SouthAfrica.TaxType;
            }
            else
            {
                invoice.IsInternational = true;
                invoice.ConversionRate = (1 / invoiceCurrency.Rate);
            }
            invoice.Created = DateTime.Now;
            var rc = routeCountryRepo.Get(request.RouteCountryId ?? account.RouteCountryId);
            
            decimal unitPrice;
            if (request.Currency == invoiceCurrency.Code)
            {
                unitPrice = request.UnitPrice;
            }
            else
            {
                var requestCurrency = currencyRepo.GetCurrency(request.Currency);
                unitPrice = requestCurrency.Convert(invoiceCurrency, request.UnitPrice, 4);
            }
            
            invoice.AddLineItem(new InvoiceLineItem
            {
                Code = InvoiceLineItemType.BulkSms,
                Description = $"Bulk SMS Credits - {rc.Country.Name}",
                Quantity = request.Quantity,
                UnitPrice = unitPrice
            });
            if (account.AccountType == AccountType.ResellerAccount)
            {
	            invoice.ResellerId = account.Id;
            }
            else
            {
	            invoice.LoginId = account.Id;
            }
            
            invoice.InvoiceNr = site.GetInvoiceNr(invoiceRepo.GetNextInvoiceNumber(site.Id));
            invoice.AccountName = account.Company;
            invoice.PurchaseOrderNr = billingDetails.OrderNumber;
            invoice.CustomerRef = account.Username;
            invoice.Currency = invoiceCurrency.Code;
            invoice.BillingAddress = billingDetails;

            invoice.PastelCode = 
                string.IsNullOrEmpty(account.ClientRefCode) 
                    ? site.GetPastelCode(billingDetails.CountryId) 
                    : account.ClientRefCode;
            
            invoice.DueDate = DateTime.Now.AddDays(7);
            return invoice;
        }

        private void ModifyInvoiceWithTaxOrCurrencyRates()
        {
            
        }
    }
}
