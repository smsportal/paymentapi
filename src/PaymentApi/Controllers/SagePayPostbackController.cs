﻿using System;
using System.Web.Http;
using log4net;
using SmsPortal.Core.Configuration;
using SmsPortal.Plutus.Controllers.Payments;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;

namespace SmsPortal.Plutus.Controllers
{
	[RoutePrefix("api/sagepay")]
	public class SagePayPostbackController : PaymentProcessingController
	{
		public SagePayPostbackController(
			IPaymentAccountRepository accountRepo, 
			ICreditAllocationService creditAllocator,
			IPaymentRepository paymentRepo, 
			TransactionRegistry tranRegistry,
			ILog log = null)
		: base(PaymentGatewayType.NetCash, tranRegistry, paymentRepo, accountRepo, creditAllocator, log)
		{
		}
		
		protected override bool IsAccepted<T>(T postBack, string transactionId)
		{
			if (string.IsNullOrEmpty(transactionId)) return false;

			switch (postBack)
			{
				case SagePayPostback successPost:
					return successPost.TransactionAccepted;
				
				case SagePayFailurePostback _:
					return false;
				
				default:
					throw new ArgumentException($"Could not identify the TransactionAccepted on the {typeof(T)} object");
			}
		}

		protected override string CopyPostbackTracingDataToPayment<T>(ref Payment payment, T postBack, string transactionId)
		{
			switch (postBack)
			{
				case SagePayPostback successPost:
					payment.GatewayTransactionId = successPost.RequestTrace;
					return successPost.RequestTrace;
				
				case SagePayFailurePostback failurePostback:
					payment.GatewayTransactionId = failurePostback.RequestTrace;
					return failurePostback.RequestTrace;
				
				default:
					throw new ArgumentException($"Could not identify the RequestTrace on the {typeof(T)} object");
			}
		}
		
		protected override decimal PostbackAmount<T>(T postBack)
		{
			switch (postBack)
			{
				case SagePayPostback successPost:
					return successPost.Amount;
				
				case SagePayFailurePostback failurePostback:
					return failurePostback.Amount;
				
				default:
					throw new ArgumentException($"Could not identify the Amount on the {typeof(T)} object");
			}
		}
		
		protected override string PostbackDebugString<T>(T postBack)
		{
			switch (postBack)
			{
				case SagePayPostback successPost:
					return successPost.ToDebugString();
				
				case SagePayFailurePostback failurePostback:
					return failurePostback.ToDebugString();
			}

			return postBack.ToString();
		}

		[HttpPost]
		[Route("success")]
		public IHttpActionResult Success(SagePayPostback sagePayPostback)
		{
			var config = new ConfigurationManager();
			var shouldProcess = config.GetBoolean("SagePay.Process.Accept");
			if (shouldProcess)
			{
				Log.PaymentInfo("Processing Accept Post back.", GatewayName, null, PostbackDebugString(sagePayPostback));
				return Notify(sagePayPostback);
			}
			Log.PaymentDebug("Ignoring Accept Post back", GatewayName);
			return Ok();
		}

		[HttpPost]
		[Route("failure")]
		public IHttpActionResult Failure(SagePayFailurePostback sagePayPostback)
		{
			if (!sagePayPostback.TransactionAccepted)
			{
				TransactionFailed(sagePayPostback);
			}

			return Redirect("https://cp.smsportal.com/app/#/buynow/payment/fail");
		}

		[HttpPost]
		[Route("notify")]
		public IHttpActionResult Notify(SagePayPostback sagePayPostback)
		{
			string postbackDebug = null;
			string paymentDebug = null;
			try
			{
				var transactionId = sagePayPostback.Extra3;
				ProcessPayment(sagePayPostback, transactionId, out paymentDebug, out postbackDebug);
				return Ok();
			}
			catch (Exception e)
			{
				Log.PaymentException(GatewayName, paymentDebug, postbackDebug, e);
				return InternalServerError(e);
			}
		}
		
		private void TransactionFailed(SagePayFailurePostback sagePayPostback)
		{
			string postbackDebug = null;
			string paymentDebug = null;
			try
			{
				var transactionId = sagePayPostback.Extra3;
				if (!ProcessFailure(sagePayPostback, transactionId, out paymentDebug, out postbackDebug))
				{
					Log.PaymentNotProcessed(GatewayName,paymentDebug,postbackDebug);
				}
			}
			catch (Exception e)
			{
				Log.PaymentException(GatewayName, paymentDebug, postbackDebug, e);
			}
		}
	}
}
