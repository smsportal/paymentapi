﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using log4net;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;

namespace SmsPortal.Plutus.Controllers
{
    [Route("api/paypal")]
    public class PaypalIpnController: ApiController
    {
        private readonly IPaymentAccountRepository accountRepo;
        private readonly ICreditAllocationService allocator;
        private readonly IPaymentRepository paymentRepo;
        private static ILog Log = LogManager.GetLogger(typeof(PaypalIpnController));
        private const string PayPalUrl = "https://ipnpb.paypal.com/cgi-bin/webscr";
        private const string PayPalSandboxUrl = "https://ipnpb.sandbox.paypal.com/cgi-bin/webscr";

        public PaypalIpnController(
            IPaymentAccountRepository accountRepo, 
            ICreditAllocationService allocator,
            IPaymentRepository paymentRepo)
        {
            this.accountRepo = accountRepo;
            this.allocator = allocator;
            this.paymentRepo = paymentRepo;
        }

        [HttpPost]
        public HttpResponseMessage What()
        {
            ProcessAsync();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        private void ProcessAsync()
        {
            var ifDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["PayPalDebug"]);
            Request.Content.ReadAsFormDataAsync().ContinueWith(x =>
            {
                string postbackDebug = null;
                string paymentDebug = null;
                try
                {
                    using (var wc = new WebClient())
                    {
                        var nvc = x.Result;
                        nvc.Add("cmd", "_notify-validate");
                        
                        var validateUrl = ifDebug ? PayPalSandboxUrl : PayPalUrl;
                        Log.PaymentDebug($"Trying to authenticate request: {validateUrl}","PayPal");
                        var response = Encoding.ASCII.GetString(wc.UploadValues(validateUrl, nvc));
                        var ipn = new PayPalIpn(nvc);
                        postbackDebug = ipn.ToDebugString();
                        
                        if (response.Equals("VERIFIED"))
                        {
                            
                            try
                            {
                                var payment = paymentRepo.Get(ipn.TransctionId);
                                payment.GatewayTransactionId = ipn.PayPalTranId;
                                paymentDebug = payment.ToString(); 
                                var acc = accountRepo.GetPaymentAccount(payment.AccountKey);
                                if (payment.IsComplete)
                                {
                                    Log.PaymentWarn("Received possible duplicate IPN.", "PayPal", paymentDebug, postbackDebug);
                                }
                                else
                                {
                                    if (ipn.Price == payment.Total)
                                    {
                                        allocator.ProcessTransaction(acc, payment, ipn.PayPalTranId);
                                        payment.Complete();
                                        paymentRepo.Update(payment);
                                    }
                                    else
                                    {
                                        Log.PaymentWarn("Amount does not match.", "PayPal", paymentDebug, postbackDebug);
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Log.PaymentException("Paypal", paymentDebug, postbackDebug, e);
                                throw;
                            }
                            
                        }
                        else
                        {
                            Log.PaymentWarn($"IPN does not appear authentic. Response received: {response}", "PayPal", null, postbackDebug);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.PaymentException("Paypal", paymentDebug, postbackDebug, e);
                    throw;
                }
            });
        }
    }
}
