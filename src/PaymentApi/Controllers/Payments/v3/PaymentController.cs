﻿using System.Linq;
using System.Web.Http;
using Microsoft.Web.Http;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;
using SmsPortal.Plutus.v3.Model;

namespace SmsPortal.Plutus.Controllers.Payments.v3
{
    [ApiVersion("3.0")]
    [RoutePrefix("api/payment")]
    public class PaymentController : ApiController
    {
        private readonly IPaymentRepository paymentRepo;
        private readonly IInvoiceRepository invoiceRepo;
        private readonly IAuthorizationRepository authRepo;
        private readonly IEmailService emailService;
        private readonly IPdfService pdfService;
        private readonly IPaymentGenerator paymentGenerator;
        private readonly IPaymentAccountRepository paymentAccountRepository;

        // Only SMSPortal has the billing enabled. Always use SMSPortal's details for invoices and emails.
        private const int SmsPortalSiteId = 1;

        public PaymentController(
            IPaymentGenerator paymentGenerator,
            IPaymentRepository paymentRepo, 
            IInvoiceRepository invoiceRepo,
            IAuthorizationRepository authRepo,
            IEmailService emailService,
            IPdfService pdfService,
            IPaymentAccountRepository paymentAccountRepository)
        {
            this.paymentGenerator = paymentGenerator;
            this.paymentRepo = paymentRepo;
            this.invoiceRepo = invoiceRepo;
            this.authRepo = authRepo;
            this.emailService = emailService;
            this.pdfService = pdfService;
            this.paymentAccountRepository = paymentAccountRepository;
        } 
        
        [Route]
        [HttpPost]
        public CreatePaymentResponse Create(CreatePaymentRequest request)
        {
            var accountKey = new AccountKey(request.AccountId, request.AccountType);
            var payment = paymentGenerator.Generate(
                accountKey,
                request.Quantity,
                request.UnitPrice,
                request.Currency,
                request.Gateway,
                request.PurchaseOrderNr,
                request.RouteCountryId);
            var resp = new CreatePaymentResponse();
            resp.AccountId = accountKey.Id;
            resp.AccountType = accountKey.Type.ToKey();
            resp.PaymentId = payment.TransactionId.ToString();
            resp.Quantity = request.Quantity;
            resp.UnitPrice = payment.UnitPrice;
            
            if (!string.IsNullOrEmpty(request.NotificationEmailAddress))
            {
                payment.NotificationEmail = request.NotificationEmailAddress;
            }
            payment.NotificationEmail = (payment.NotificationEmail ?? string.Empty)
                .Split(new[] {',', ';'})
                .FirstOrDefault();

            if (payment.Invoice != null)
            {
                var invoice = payment.Invoice;
                resp.InvoiceNr = invoice.InvoiceNr;
                resp.UnitDescription = invoice.GetLineItems().First().Description;
                invoiceRepo.Add(invoice);
                var bytes = pdfService.CreatePdf(invoice);
                invoiceRepo.AddPdf(invoice.Id, bytes);

                if (!string.IsNullOrEmpty(request.NotificationEmailAddress) &&
                    request.NotificationEmailAddress != invoice.BillingAddress.InvoiceEmail)
                {
                    invoice.BillingAddress.InvoiceEmail =
                        $"{invoice.BillingAddress.InvoiceEmail},{request.NotificationEmailAddress}";
                }

                if (request.Gateway.Is(PaymentGatewayType.Eft))
                {
                    var paymentAcc = paymentAccountRepository.GetPaymentAccount(accountKey);
                    emailService.SendEftInvoiceCreatedEmail(paymentAcc, invoice, bytes, SmsPortalSiteId);
                }
                else
                {
                    emailService.SendUnpaidPrePaidInvoiceEmailCreated(invoice, bytes, SmsPortalSiteId);
                }
            }
            else
            {
                resp.UnitDescription = "SMS Credits";
            }
            
            paymentRepo.Add(payment);

            resp.Currency = payment.Currency;
            if (request.Gateway.Is(PaymentGatewayType.Eft))
            {
                var auth = payment.GetEftAuthorization();
                authRepo.Add(auth);
            }
            else
            {
                resp.Variables = paymentGenerator.GetVariables(payment);
            }

            resp.TransactionFee = payment.Fee;
            resp.TaxAmount = payment.TaxValue;
            resp.Total = payment.Total;
            return resp;
        }
    }
}
