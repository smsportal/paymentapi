using System;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using Microsoft.Web.Http;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;
using SmsPortal.Plutus.v2.Model;
using SmsPortal.Plutus.v2.Model.Dto;

namespace SmsPortal.Plutus.Controllers.Payments.v2
{
    [ApiVersion("2.0")]
    [RoutePrefix("api/payment")]
    public class PaymentController: ApiController
    {
        private readonly IPaymentRepository paymentRepo;
        private readonly IPaymentHistoryRepository paymentHistoryRepo;
        private readonly IInvoiceRepository invoiceRepo;
        private readonly IAuthorizationRepository authRepo;
        private readonly IEmailService emailService;
        private readonly IPdfService pdfService;
        private readonly IPaymentGenerator paymentGenerator;
        private readonly IPaymentAccountRepository paymentAccountRepository;
        
        // Only SMSPortal has the billing enabled. Always use SMSPortal's details for invoices and emails.
        private const int SmsPortalSiteId = 1;

        public PaymentController(
            IPaymentGenerator paymentGenerator,
            IPaymentRepository paymentRepo, 
            IInvoiceRepository invoiceRepo,
            IAuthorizationRepository authRepo,
            IPaymentHistoryRepository paymentHistoryRepo,
            IEmailService emailService,
            IPdfService pdfService,
            IPaymentAccountRepository paymentAccountRepository)
        {
            this.paymentGenerator = paymentGenerator;
            this.paymentRepo = paymentRepo;
            this.invoiceRepo = invoiceRepo;
            this.authRepo = authRepo;
            this.emailService = emailService;
            this.pdfService = pdfService;
            this.paymentAccountRepository = paymentAccountRepository;
            this.paymentHistoryRepo = paymentHistoryRepo;
        }

        [Route]
        [HttpPost]
        public CreatePaymentResponse Post(CreatePaymentRequest request)
        {
            var accountKey = new AccountKey(request.AccountId, request.AccountType);
            var payment = paymentGenerator.Generate(
                accountKey,
                request.Quantity,
                request.UnitPrice,
                request.Currency,
                request.Gateway,
                request.PurchaseOrderNr,
                request.RouteCountryId);
            var resp = new CreatePaymentResponse();
            resp.PaymentId = payment.TransactionId.ToString();
            if (payment.Invoice != null)
            {
                var invoice = payment.Invoice;
                resp.InvoiceNr = invoice.InvoiceNr;
                invoiceRepo.Add(invoice);
                var bytes = pdfService.CreatePdf(invoice);
                invoiceRepo.AddPdf(invoice.Id, bytes);

                if (!string.IsNullOrEmpty(request.NotificationEmailAddress) &&
                    request.NotificationEmailAddress != invoice.BillingAddress.InvoiceEmail)
                {
                    invoice.BillingAddress.InvoiceEmail =
                        $"{invoice.BillingAddress.InvoiceEmail},{request.NotificationEmailAddress}";
                }
                
                if (request.Gateway.Is(PaymentGatewayType.Eft))
                {
                    var paymentAcc = paymentAccountRepository.GetPaymentAccount(accountKey);
                    emailService.SendEftInvoiceCreatedEmail(paymentAcc, invoice, bytes, SmsPortalSiteId);
                }
                else
                {
                    emailService.SendUnpaidPrePaidInvoiceEmailCreated(invoice, bytes, SmsPortalSiteId);
                }
            }

            paymentRepo.Add(payment);

            if (request.Gateway.Is(PaymentGatewayType.Eft))
            {
                var auth = payment.GetEftAuthorization();
                authRepo.Add(auth);
            }
            else
            {
                resp.HtmlForm = paymentGenerator.GetHtml(payment);
            }

            resp.Total = payment.Total;
            return resp;
        }

        /// <summary>
        /// Request a list of payments.
        /// </summary>
        /// <param name="accountId">The unique numerical identifier of the account associated with the payments.</param>
        /// <param name="accountType">
        /// The type of account (as per  <seealso cref="SmsPortal.Plutus.Model.Dto.Enums.AccountType"/>) associated with the payments.
        /// <remarks>
        /// The account type will only be used if the <seealso cref="accountId"/> is provided.  
        /// </remarks>
        /// </param>
        /// <param name="pageSize">
        /// The maximum number of payments to return (per page).
        /// <remarks>
        /// If not provided then all applicable payments will be returned. 
        /// </remarks>
        /// </param>
        /// <param name="pageNumber">
        /// The page number of payments to return (as per PageSize).
        /// <remarks>
        /// The page number will only be used if the <seealso cref="PageSize"/> is provided.
        /// If not provided then the first page of the applicable size requested in the <seealso cref="PageSize"/> will be returned.
        /// </remarks>
        /// </param>
        /// <returns>A <c>ListPaymentHistoryResponse</c> object containing the list of requested payments.</returns>
        [Route("history/{accountId}/{accountType}")]
        [HttpGet]
        public ListPaymentHistoryResponse List(
            int? accountId,
            string accountType,
            int? pageSize = null,
            int? pageNumber = null
            )
        {
            var payments =
                paymentHistoryRepo.GetList(
                        accountType,
                        accountId,
                        null,
                        pageSize,
                        pageNumber)
                    .ToArray();

            var count = payments.Any() ?  payments.Max(h => h.TotalNumberOfResults) : 0;
            var size = pageSize ?? (count > 0 ? count : 1);

            return new ListPaymentHistoryResponse
            {
                PageNumber = pageNumber ?? 1,
                NumberOfPages = Math.Max((int) Math.Ceiling((double) count / (double) size), 1),
                Payments =
                    payments
                        .Select(Mapper.Map<PaymentDto>)
                        .ToList()
            };
        }
    }
}
