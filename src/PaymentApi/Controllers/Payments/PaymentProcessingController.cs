﻿using System;
using System.Web.Http;
using log4net;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;

namespace SmsPortal.Plutus.Controllers.Payments
{
    public abstract class PaymentProcessingController : ApiController
    {
        protected readonly ILog Log;
        private readonly TransactionRegistry tranRegistry;
        private readonly IPaymentRepository paymentRepo;
        public readonly string GatewayName;
        private readonly IPaymentAccountRepository accountRepo;
        private readonly ICreditAllocationService creditAllocator;

        protected PaymentProcessingController(
	        PaymentGatewayType gateway,
	        TransactionRegistry tranRegistry,
	        IPaymentRepository paymentRepo,
	        IPaymentAccountRepository accountRepo,
	        ICreditAllocationService creditAllocator,
	        ILog log = null)
	        : this(gateway.ToKey(), tranRegistry, paymentRepo, accountRepo, creditAllocator, log)
        {
        }
        
        protected PaymentProcessingController(
	        Type postBackController,
	        TransactionRegistry tranRegistry,
	        IPaymentRepository paymentRepo,
	        IPaymentAccountRepository accountRepo,
	        ICreditAllocationService creditAllocator,
	        ILog log = null)
	        : this(postBackController.Name.ToUpper().Replace("POSTBACK","").Replace("CONTROLLER",""), tranRegistry, paymentRepo, accountRepo, creditAllocator, log)
        {
        }

        private PaymentProcessingController(
	        string gatewayName,
	        TransactionRegistry tranRegistry,
	        IPaymentRepository paymentRepo,
	        IPaymentAccountRepository accountRepo,
	        ICreditAllocationService creditAllocator,
	        ILog log = null)
        {
	        this.tranRegistry = tranRegistry;
	        this.paymentRepo = paymentRepo;
	        this.accountRepo = accountRepo;
	        this.creditAllocator = creditAllocator;
	        GatewayName = gatewayName;
	        Log = log ?? LogManager.GetLogger(GetType());
        }

        protected abstract bool IsAccepted<T>(T postBack, string transactionId);
        protected abstract string CopyPostbackTracingDataToPayment<T>(ref Payment payment, T postBack, string transactionId);
        protected abstract decimal PostbackAmount<T>(T postBack);
        protected abstract string PostbackDebugString<T>(T postBack);

        private bool MarkAsBusy(string transactionId, bool accepted)
        {
	        return tranRegistry.TryAdd($"{transactionId}-{accepted}");
        }
        
        private void MarkAsFinished(string transactionId, bool accepted)
        {
	        tranRegistry.TryRemove($"{transactionId}-{accepted}");
        } 

        private bool ProcessPayment<T>(T postBack, string transactionId, bool accepted, Func<T, Payment, string, string> process, out string paymentDebug, out string postbackDebug)
        {
	        postbackDebug = PostbackDebugString(postBack) ?? string.Empty;
	        paymentDebug = null;

	        if (!Guid.TryParse(transactionId, out var transactionGuid))
	        {
		        paymentDebug = $"Transaction Id {transactionId} is not a valid GUID";
		        Log.PaymentWarn("GUID Parse failure", GatewayName, paymentDebug, postbackDebug);
		        return false;
	        }

	        var payment = paymentRepo.Get(transactionGuid);
	        paymentDebug = payment.ToString();
	        var successFail = (accepted ? "success" : "failure");
	        
	        if (!MarkAsBusy(transactionId, accepted))
	        {
		        Log.PaymentDebug($"Already busy processing {successFail}.", GatewayName, paymentDebug, postbackDebug);
		        return false;
	        }
	        
	        switch (payment.Status)
	        {
		        case Payment.PaymentStatusCompleted:
			        Log.PaymentDebug("Already marked as completed.", GatewayName, paymentDebug, postbackDebug);
			        MarkAsFinished(transactionId, accepted);
			        return false;

		        case Payment.PaymentStatusFailed:
			        if (accepted)
			        {
				        var reprocessingError = process.Invoke(postBack, payment, postbackDebug);
				        if (string.IsNullOrEmpty(reprocessingError)) return true;

				        Log.PaymentWarn(reprocessingError, GatewayName, paymentDebug, postbackDebug);
				        return false;
			        }

			        Log.PaymentDebug("Already marked as failed.", GatewayName, paymentDebug, postbackDebug);
			        MarkAsFinished(transactionId, false);
			        return false;
		        
		        case Payment.PaymentStatusFaulted:
			        Log.PaymentWarn("Payment faulted since it was probably cancelled by the user.", GatewayName, paymentDebug, postbackDebug);
			        payment.Faulted();
			        paymentRepo.Update(payment);
			        MarkAsFinished(transactionId, accepted);
			        return true;

		        case Payment.PaymentStatusStarted:
			        var processingError = process.Invoke(postBack, payment, postbackDebug);
			        if (string.IsNullOrEmpty(processingError)) return true;

			        Log.PaymentWarn(processingError, GatewayName, paymentDebug, postbackDebug);
			        return false;
	        }

	        Log.PaymentDebug("Ignored based on previous status.", GatewayName, paymentDebug, postbackDebug);
	        MarkAsFinished(transactionId, accepted);
	        return false;
        }

        protected bool ProcessPayment<T>(T postBack, string transactionId, out string paymentDebug, out string postbackDebug) => 
	        !IsAccepted(postBack, transactionId) 
		        ? ProcessFailure(postBack, transactionId, out paymentDebug, out postbackDebug) 
		        : ProcessSuccess(postBack, transactionId, out paymentDebug, out postbackDebug);

        protected bool ProcessSuccess<T>(T postBack, string transactionId, out string paymentDebug, out string postbackDebug)
	    {
	        return ProcessPayment(postBack, transactionId, true,
		        (data, payment, debug) =>
		        {
			        var requestTrace = CopyPostbackTracingDataToPayment(ref payment, postBack, transactionId);
			        var amount = PostbackAmount(postBack);
			        if (amount == payment.Total)
			        {
				        Log.PaymentInfo("Payment succeeded.", GatewayName, payment.ToString(), debug);
				        var account = accountRepo.GetPaymentAccount(payment.AccountKey);
				        creditAllocator.ProcessTransaction(account, payment, requestTrace);
				        payment.Complete();
				        paymentRepo.Update(payment);
				        MarkAsFinished(transactionId, true);
				        return string.Empty;
			        }
			        
			        MarkAsFinished(transactionId, true);
			        return $"Amount does not match. {payment}";
		        }
		        , out paymentDebug, out postbackDebug);
        }
        
        protected bool ProcessFailure<T>(T postBack, string transactionId, out string paymentDebug, out string postbackDebug)
        {
	        return ProcessPayment(postBack, transactionId, false,
		        (data, payment, debug) =>
		        {
			        CopyPostbackTracingDataToPayment(ref payment, postBack, transactionId);
			        Log.PaymentInfo("Payment failed gracefully.", GatewayName, payment.ToString(), debug);
			        payment.Fail();
			        paymentRepo.Update(payment);
			        MarkAsFinished(transactionId, false);
			        return string.Empty;
		        }
		        , out paymentDebug, out postbackDebug);
        }
    }
}