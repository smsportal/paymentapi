﻿using AutoMapper;
using System;
using System.Linq;
using System.Net.Mail;
using System.Web.Http;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Services;
using log4net;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Controllers
{
    [Route("api/postpaid")]
    public class PostPaidBillingCycleController : ApiController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(PostPaidBillingCycleController));
        private readonly IProformaInvoiceService proformaInvoiceService;
        private readonly IEmailService emailer;
        
        // Only SMSPortal has the billing enabled. Always use SMSPortal's details for invoices and emails.
        private const int SmsPortalSiteId = 1;

        public PostPaidBillingCycleController(
            IProformaInvoiceService proformaInvoiceService,
            IEmailService emailer)
        {
            this.proformaInvoiceService = proformaInvoiceService;
            this.emailer = emailer;
        }

        [HttpGet]
        [Route("api/postpaid/cycle/{filterType}")]
        public GetPostPaidBillingCycleProformaInvoiceListResponse GetList(string filterType)
        {
            var invoices =
                proformaInvoiceService.GetAllPostPaidProformaInvoices(
                    filterType.ToEnum<PostPaidBillingCycleFilterType>(),
                    null);
            var response = new GetPostPaidBillingCycleProformaInvoiceListResponse()
            {
                FilterType = filterType,
                Invoices = invoices.Select(Mapper.Map<InvoiceDto>).ToList()
            };
            return response;
        }
        
        [HttpPost]
        [Route("api/postpaid/start")]
        public void StartBillingCycle(StartBillingCycleRequest request)
        {
            proformaInvoiceService.BillingCycleStart = request.BillingCycleStart;
            log.Info($"Starting automated proforma invoice generation for the billing cycle starting at {proformaInvoiceService.BillingCycleStart}");
        }

        [HttpPost]
        [Route("api/postpaid/event")]
        public CreateInvoiceResponse Create(CreatePostPaidEventInvoiceRequest request)
        {
            var response = new CreateInvoiceResponse();
            var accountKey = new AccountKey(request.AccountId, request.AccountType);
            var invoice = proformaInvoiceService.GetPostPaidProformaInvoice(accountKey, request.EventId);

            var cycle = invoice.Cycle;
            var cmpgnCreated = cycle.CycleStartDate;
            var cmpgnSched = cycle.CycleEndDate;
            cycle.CycleStartDate = new DateTime(cycle.CycleStartDate.Year, cycle.CycleStartDate.Month, 1); //First of the month
            cycle.CycleEndDate = cycle.CycleStartDate.AddMonths(1).AddDays(-1); //End of the month

            var cmpgnName = invoice.GetLineItems().First().Description;
            invoice = proformaInvoiceService.AddDiscountLine(invoice);

            proformaInvoiceService.InsertInvoice(invoice, request.OrderNumber);
            proformaInvoiceService.Approve(invoice, cmpgnName);

            response.Invoice = Mapper.Map<InvoiceDto>(invoice);

            var pdf = proformaInvoiceService.CreatePdf(invoice, cycle);

            cycle.CycleStartDate = cmpgnCreated;
            cycle.CycleEndDate = cmpgnSched;

            try
            {
                emailer.SendUnpaidCampaignInvoiceCreated(invoice, cycle, request.EmailAddress, pdf, SmsPortalSiteId);
            }
            catch (Exception e)
            {
                log.Exception("api/postpaid/event", e);
            }

            log.Info($"Invoice {invoice.InvoiceNr} created for {invoice.AccountName} ({request.AccountType} {request.AccountId}) based on {cycle.ItemDescription} {cycle.BreakdownDescription}");
            return response;
        }

        [HttpPost]
        [Route("api/postpaid/shortcode")]
        public CreateInvoiceResponse Create(CreatePostPaidShortCodeSetupInvoiceRequest request)
        {
            var response = new CreateInvoiceResponse();
            var accountKey = new AccountKey(request.AccountId, request.AccountType);
            var invoice = proformaInvoiceService.GetPostPaidProformaInvoice(
                accountKey,
                request.ShortCodeUsername,
                request.SetupDate,
                request.ShortCode,
                request.Keyword,
                request.DataId);

            if (string.IsNullOrEmpty(request.Keyword))
            {
                invoice.Cycle.CycleEndDate =
                    new DateTime(invoice.Cycle.CycleStartDate.Year, invoice.Cycle.CycleStartDate.Month, 1)
                        .AddMonths(1)
                        .AddDays(-1); //End of the month
            }
            else
            {
                if (invoice.Cycle.CycleStartDate.Day > 1)
                {
                    invoice.Cycle.CycleStartDate =
                        new DateTime(invoice.Cycle.CycleStartDate.Year, invoice.Cycle.CycleStartDate.Month, 1)
                            .AddMonths(1); //1st of the next month
                }

                invoice.Cycle.CycleEndDate =
                    invoice.Cycle.CycleStartDate.AddYears(1)
                        .AddDays(-1); //End of the running year, i.e. end of the month 12 months later
            }

            proformaInvoiceService.InsertInvoice(invoice, request.OrderNumber);
            proformaInvoiceService.Approve(
                invoice,
                invoice.Cycle.ItemDescription,
                long.Parse(request.ShortCode),
                request.Keyword);

            response.Invoice = Mapper.Map<InvoiceDto>(invoice);

            var pdf = proformaInvoiceService.CreatePdf(invoice, invoice.Cycle);

            if (request.EmailAddress != null && request.EmailAddress != invoice.BillingAddress.InvoiceEmail)
            {
                invoice.BillingAddress.InvoiceEmail =
                    $"{invoice.BillingAddress.InvoiceEmail},{request.EmailAddress}";
            }

            try
            {
                emailer.SendUnpaidShortCodeSetupInvoiceCreated(invoice,
                    $"Setup {request.ShortCode} {request.Keyword}", pdf, SmsPortalSiteId);
            }
            catch (Exception e)
            {
                log.Exception("api/postpaid/shortcode", e);
            }

            log.Info($"Invoice {invoice.InvoiceNr} created for {invoice.AccountName} ({request.AccountType} {request.AccountId}) for setup of {request.ShortCode} {request.Keyword}");
            return response;
        }
        
        private bool IsValidEmailAddress(string emailAddress)
        {
            try
            {
                var m = new MailAddress(emailAddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        [HttpPost]
        [Route("api/postpaid/invoice")]
        public CreateInvoiceResponse Create(CreatePostPaidInvoiceRequest request, [FromUri] int id = 0)
        {
            ProformaInvoice invoice;
            if (id > 0)
            {
                invoice = proformaInvoiceService.GetPostPaidProformaInvoice(id);
            }
            else
            {
                var accountKey = new AccountKey(request.AccountId, request.AccountType);
                invoice = proformaInvoiceService.GetPostPaidProformaInvoice(
                    request.FilterType.ToEnum<PostPaidBillingCycleFilterType>(null),
                    accountKey,
                    request.InvoiceDate);
            }

            var response = new CreateInvoiceResponse();

            if (invoice.Id > 0 && !invoice.InvoiceNr.Equals("Proforma", StringComparison.OrdinalIgnoreCase))
            {
                //The invoice already exists, just return the result
                response.Invoice = Mapper.Map<InvoiceDto>(invoice);
                return response;
            }

            var cycle = invoice.Cycle;

            if (invoice.Id <= 0)
            {
                proformaInvoiceService.InsertInvoice(invoice, request?.OrderNumber);
            }

            proformaInvoiceService.Approve(invoice, cycle.ItemDescription);

            var pdf = proformaInvoiceService.CreatePdf(invoice, cycle);

            try
            {
                if (cycle.LineItemBreakdown ==
                    LineItemBreakdownType.PerCostCentrePerCampaignByCountryAndMessageType &&
                    IsValidEmailAddress(invoice.CustomerRef))
                {
                    //Customer Ref contains the cost centre for PerCostCentrePerCampaignByCountryAndMessageType invoices
                    //If the Customer Ref is a valid email address then also send the email to that address 
                    invoice.BillingAddress.InvoiceEmail =
                        string.Join(",",
                            new[]
                            {
                                invoice.BillingAddress.InvoiceEmail,
                                invoice.CustomerRef
                            });
                }

                emailer.SendUnpaidPostPaidInvoiceCreated(invoice, pdf, SmsPortalSiteId);
            }
            catch (Exception e)
            {
                log.Exception("api/postpaid/invoice", e);
            }

            response.Invoice = Mapper.Map<InvoiceDto>(invoice);
            log.Info($"Invoice {invoice.InvoiceNr} created for {invoice.AccountName} ({invoice.AccountKey}) based on {cycle.ItemDescription} {cycle.BreakdownDescription}");
            return response;
        }

        [HttpPost]
        [Route("api/postpaid/costestimate")]
        public CreateInvoiceResponse Quote(CreatePostPaidInvoiceRequest request, [FromUri] int id = 0)
        {
            var response = new CreateInvoiceResponse();

            ProformaInvoice invoice;
            if (id > 0)
            {
                invoice = proformaInvoiceService.GetPostPaidProformaInvoice(id);
            }
            else
            {
                var accountKey = new AccountKey(request.AccountId, request.AccountType);
                invoice = proformaInvoiceService.GetPostPaidProformaInvoice(
                    request.FilterType.ToEnum<PostPaidBillingCycleFilterType>(null),
                    accountKey,
                    request.InvoiceDate);
            }

            if (invoice.Id != 0 && !invoice.InvoiceNr.Equals("Proforma", StringComparison.OrdinalIgnoreCase))
            {
                //The invoice/cost estimate/quote already exists, just return the result
                response.Invoice = Mapper.Map<InvoiceDto>(invoice);
                return response;
            }

            var cycle = invoice.Cycle;
            proformaInvoiceService.InsertCostEstimate(invoice);

            var pdf = proformaInvoiceService.CreatePdf(invoice, cycle);

            try
            {
                emailer.SendCostEstimateCreated(invoice, pdf, SmsPortalSiteId);
            }
            catch (Exception e)
            {
                log.Exception("api/postpaid/costestimate", e);
            }

            response.Invoice = Mapper.Map<InvoiceDto>(invoice);
            log.Info($"Cost Estimate {invoice.InvoiceNr} created for {invoice.AccountName} ({request.AccountType} {request.AccountId}) based on {cycle.ItemDescription} {cycle.BreakdownDescription}");
            return response;
        }

        [HttpPost]
        [Route("api/postpaid/decline")]
        public CreateInvoiceResponse Decline(DeclinePostPaidInvoiceRequest request, [FromUri] int id = 0)
        {
            var response = new CreateInvoiceResponse();
            ProformaInvoice invoice;
            if (id > 0)
            {
                invoice = proformaInvoiceService.GetPostPaidProformaInvoice(id);
            }
            else
            {
                var accountKey = new AccountKey(request.AccountId, request.AccountType);
                invoice = proformaInvoiceService.GetPostPaidProformaInvoice(
                    request.FilterType.ToEnum<PostPaidBillingCycleFilterType>(null),
                    accountKey,
                    request.InvoiceDate);
            }

            if (invoice.Id > 0 && !invoice.InvoiceNr.Equals("Proforma", StringComparison.OrdinalIgnoreCase))
            {
                //The invoice/cost estimate/quote already exists, just return the result
                response.Invoice = Mapper.Map<InvoiceDto>(invoice);
                return response;
            }

            proformaInvoiceService.Decline(invoice, request?.Reason);

            response.Invoice = Mapper.Map<InvoiceDto>(invoice);
            log.Info($"Invoice for {invoice.AccountName} ({request.AccountType} {request.AccountId}) based on {invoice.Cycle.ItemDescription} {invoice.Cycle.BreakdownDescription} declined");
            return response;
        }
    }
}
