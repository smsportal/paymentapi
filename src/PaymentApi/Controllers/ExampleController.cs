﻿using System.Web.Http;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Controllers
{
	public class ExampleController : ApiController
	{
		public ExampleController()
		{
		}

		[Route("Get")]
		[HttpGet]
		public IHttpActionResult Get()
		{
			return Ok("OK");
		}

		[Route("Post")]
		[HttpPost]
		public IHttpActionResult Post(ExampleModel model)
		{
			return Ok($"Posted {model.Message}");
		}
	}
}
