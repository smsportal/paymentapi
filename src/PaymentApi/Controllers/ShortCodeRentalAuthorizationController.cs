﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using log4net;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Controllers
{
	[Route("api/[controller]")]
	public class ShortCodeRentalAuthorizationController : ApiController
	{
		private readonly ILog log = LogManager.GetLogger(typeof(ShortCodeRentalAuthorizationController));
		private readonly IPaymentAccountRepository paymentAccountRepository;
		private readonly ITransactionRepository transactionRepository;
		private readonly ILegacyInvoiceRepository legacyInvoiceRepository;
		private readonly IBillingGroupRepository billingGroupRepository;

		public ShortCodeRentalAuthorizationController(IPaymentAccountRepository paymentAccountRepository,
			ITransactionRepository transactionRepository,
			ILegacyInvoiceRepository legacyInvoiceRepository,
			IBillingGroupRepository billingGroupRepository)
		{
			this.paymentAccountRepository = paymentAccountRepository;
			this.transactionRepository = transactionRepository;
			this.legacyInvoiceRepository = legacyInvoiceRepository;
			this.billingGroupRepository = billingGroupRepository;
		}

		[HttpPost]
	    public IHttpActionResult AuthorizeRental([FromBody] CreateShortCodeRentalInvoiceRequest request)
	    {
            var accountKey = new AccountKey(request.AccountId, AccountType.SmsAccount);
		    var paymentAccount = paymentAccountRepository.GetPaymentAccount(accountKey);

		    InvoiceType invoiceType;
		    List<LegacyInvoiceLineItem> lineItems;

		    switch ((ShortCodeApplicationType) request.SCRental_BillingType)
		    {
				case ShortCodeApplicationType.NewShortCodeApplication:
					lineItems = GetNewShortCodeRentalApplicationData(paymentAccount, request);
					invoiceType = InvoiceType.ShortCodeRentals;
					break;
				case ShortCodeApplicationType.NewKeywordApplication:
					lineItems = GetNewKeywordRentalApplicationData(paymentAccount, request);
					invoiceType = InvoiceType.ShortCodeKeyword;
					break;
				case ShortCodeApplicationType.PerUser:
					lineItems = GetExistingScRentalApplicationTransactions(paymentAccount, request);
					invoiceType = InvoiceType.ShortCodeRentals;
					break;
				default:
					throw new Exception("Invalid ShortCodeApplicationType specified.");
			}

		    bool postpaid;
		    if (paymentAccount.BillingGroupId.HasValue)
		    {
			    var bg = billingGroupRepository.Get(paymentAccount.BillingGroupId.Value);
			    postpaid = bg.PostPaid;
		    }
		    else
		    {
			    postpaid = paymentAccount.IsPostpaid;
		    }
	        var invoice = new LegacyInvoice();
	        invoice.IsPostPaid = postpaid;
		    invoice.InvoiceType = invoiceType;
		    invoice.OrderNumber = request.OrderNumber;
		    invoice.ShowBillingPeriod = true;
		    invoice.PeriodBilledFromDate = request.PeriodBilledFromDate;
		    invoice.PeriodBilledToDate = request.PeriodBilledToDate;

		    if (string.IsNullOrEmpty(request.Email))
		    {
			    invoice.InvoiceLegacyEmailInfo.Recipient = request.Email;
		    }
			else if (string.IsNullOrEmpty(paymentAccount.ShortCodeExpiryEmailAddress))
		    {
			    invoice.InvoiceLegacyEmailInfo.Recipient = paymentAccount.ShortCodeExpiryEmailAddress;
		    }

		    var newInvoice = legacyInvoiceRepository.CreateInvoice(paymentAccount, invoice);
			CloseBillingCycle(paymentAccount, request, newInvoice);

		    return Ok();
	    }
		
	    private void CloseBillingCycle(PaymentAccount paymentAccount, CreateShortCodeRentalInvoiceRequest request,
		    InvoiceModel invoice)
	    {
		    var billingCycle = new ShortCodeBillingCycle();
		    billingCycle.SCApplicationType = (ShortCodeApplicationType) request.SCRental_BillingType;
		    billingCycle.AccountId = paymentAccount.Id;
		    billingCycle.ClientRefCode = paymentAccount.ClientRefCode;
		    billingCycle.CycleStartDate = request.PeriodBilledFromDate;
		    billingCycle.CycleEndDate = request.PeriodBilledToDate;
		    billingCycle.InvoiceId = invoice.InvoiceId;
		    billingCycle.InvoiceReference = invoice.ReferenceNumber;
		    billingCycle.Currency = invoice.Currency;
		    billingCycle.SubTotal = invoice.SubTotal;
		    billingCycle.TaxValue = invoice.TaxValue;

		    billingCycle.ShortCode = long.Parse(request.ShortCode);
		    billingCycle.Keyword = request.Keyword;
		    billingCycle.Comment = request.Comment;
		    billingCycle.SCIsdedicated = request.SCIsdedicated;
		    billingCycle.InvoiceType = invoice.InvoiceType;

		    try
		    {
			    transactionRepository.CloseShortCodeBillingCycle(billingCycle);
		    }
		    catch (Exception e)
		    {
                log.Exception("Couldn't close billing cycle.", e);
		    }
	    }

	    private List<LegacyInvoiceLineItem> GetNewShortCodeRentalApplicationData(PaymentAccount paymentAccount, CreateShortCodeRentalInvoiceRequest request)
	    {
		    var monthsBilled = MonthsBilled(request.PeriodBilledFromDate, request.PeriodBilledToDate);

			var lineItems = new List<LegacyInvoiceLineItem>();
		    lineItems.Add(new LegacyInvoiceLineItem
		    {
			    Price = paymentAccount.PricingSCMonthly,
				Quantity = monthsBilled,
				Description = $"Short Code Rental - {request.ShortCode}"
		    });
			lineItems.Add(new LegacyInvoiceLineItem
			{
				Price = paymentAccount.PricingSCSetup,
				Quantity = 1,
				Description = "Short Code Setup Fee"
			});

		    return lineItems;
	    }

	    private List<LegacyInvoiceLineItem> GetNewKeywordRentalApplicationData(PaymentAccount paymentAccount,
		    CreateShortCodeRentalInvoiceRequest request)
	    {
		    var lineItems = new List<LegacyInvoiceLineItem>();
			lineItems.Add(new LegacyInvoiceLineItem
			{
				Price = paymentAccount.PricingSCShared,
				Quantity = 1,
				Description = $"Keywords: Keyword {request.Keyword} on short code {request.ShortCode}"
			});

		    return lineItems;
	    }

	    private List<LegacyInvoiceLineItem> GetExistingScRentalApplicationTransactions(PaymentAccount paymentAccount,
		    CreateShortCodeRentalInvoiceRequest request)
	    {
		    return transactionRepository.GetShortCodeTransactions(paymentAccount, (ShortCodeApplicationType) request.SCRental_BillingType,
			    request.PeriodBilledFromDate, request.PeriodBilledToDate, request.SCIsdedicated);
	    }

	    private int MonthsBilled(DateTime start, DateTime end)
	    {
		    return (end.Year - start.Year) * 12 + end.Month - start.Month;
	    }
	}
}
