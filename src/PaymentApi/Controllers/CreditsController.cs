using System.Linq;
using System.Web.Http;
using AutoMapper;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Controllers
{
    [Route("api/credits")]
    public class CreditsController: ApiController
    {
        private readonly ICreditAllocationRepository repository;

        public CreditsController(ICreditAllocationRepository repository)
        {
            this.repository = repository;
        }
        
        [HttpGet]
        [Route("api/credits/{accountId}/{accountType}")]
        public ListCreditAllocationsResponse ListCreditAllocations(int accountId, string accountType)
        {
            var allocations = repository.Get(accountId, accountType);
            var response = new ListCreditAllocationsResponse();
            response.CreditAllocations = allocations.Select(Mapper.Map<CreditAllocationDto>).ToList();
            return response;
        }
    }
}