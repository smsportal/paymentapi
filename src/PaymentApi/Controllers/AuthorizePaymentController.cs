﻿using System.Web.Http;
using log4net;
using SmsPortal.Core;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Controllers
{
    public class AuthorizePaymentController : ApiController
	{
		private readonly ILog log = LogManager.GetLogger(typeof(AuthorizePaymentController));
		private readonly IPaymentAccountRepository paymentAccountRepository;
		private readonly ILegacyInvoiceRepository legacyInvoiceRepository;
	    private readonly ISiteRepository siteRepository;

	    public AuthorizePaymentController(
            IPaymentAccountRepository paymentAccountRepository, 
            ILegacyInvoiceRepository legacyInvoiceRepository,
            ISiteRepository siteRepository)
	    {
		    this.paymentAccountRepository = paymentAccountRepository;
		    this.legacyInvoiceRepository = legacyInvoiceRepository;
	        this.siteRepository = siteRepository;
	    }

		[Route("AuthorizePayment")]
		[HttpPost]
	    public IHttpActionResult AuthorizePayment([FromBody] AuthorizePaymentRequest request)
	    {
		    var accountKey = new AccountKey(request.PurchaseAccLoginId, request.AccountType); 
		    var account = 
			    paymentAccountRepository.GetPaymentAccount(accountKey);
	        var site = siteRepository.Get(account.SiteId);
		    if (site.UseAutomaticInvoicing)
		    {
                
			    if (request.PaymentMethod.Is(PaymentGatewayType.Eft))
			    {
					CloseInvoice(account, request.TransactionId, false);
			    }
			    else if (request.PaymentMethod.Is(PaymentGatewayType.NetCash) ||
			             request.PaymentMethod.Is(PaymentMethodType.SagePay)) //Used in old payments
			    {
				    CloseInvoice(account, request.TransactionId, true);
			    }
		    }
		    else
		    {
			    log.WarnFormat("Site {0} for account {1} is not set to use Automatic Invoicing.", 
                    site.Domain, 
                    account.Username);
		    }

		    return Ok();
	    }

		private void CloseInvoice(PaymentAccount account, string transactionId, bool sendInvoiceEmail)
		{
			legacyInvoiceRepository.CloseInvoice(account, transactionId, sendInvoiceEmail);
		}
    }
}
