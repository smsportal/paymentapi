using System;
using System.Web.Http;
using log4net;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;

namespace SmsPortal.Plutus.Controllers
{
    [RoutePrefix("api/pdf")]
    public class PdfRegenerationController : ApiController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(PdfRegenerationController));
        private readonly ISiteRepository siteRepo;
        private readonly IInvoiceRepository invoiceRepository;
        private readonly ICreditNoteRepository creditNoteRepository;
        private readonly ICostEstimateRepository costEstimateRepository;
        private readonly IQuoteRepository quoteRepository;
        private readonly IPaymentAccountRepository paymentAccountRepository;
        private readonly IBillingGroupOrDetailsRepository billingDetailsRepository;
        private readonly IPostPaidBillingCycleRepository postPaidBillingRepository;
        private readonly IPdfService pdfService;

        public PdfRegenerationController(
            ISiteRepository siteRepo,
            IBillingGroupOrDetailsRepository billingDetailsRepository,
            IPaymentAccountRepository paymentAccountRepository, 
            IQuoteRepository quoteRepository,
            ICostEstimateRepository costEstimateRepository,
            IInvoiceRepository invoiceRepository,
            ICreditNoteRepository creditNoteRepository,
            IPostPaidBillingCycleRepository postPaidBillingRepository,
            IPdfService pdfService)
        {
            this.siteRepo = siteRepo;
            this.paymentAccountRepository = paymentAccountRepository;
            this.billingDetailsRepository = billingDetailsRepository;
            this.costEstimateRepository = costEstimateRepository;
            this.invoiceRepository = invoiceRepository;
            this.creditNoteRepository = creditNoteRepository;
            this.postPaidBillingRepository = postPaidBillingRepository;
            this.pdfService = pdfService;
            this.quoteRepository = quoteRepository;
        }

        private PaymentAccount GetPaymentAccount(Invoice invoice)
        {
            return GetPaymentAccount(invoice.AccountKey);
        }

        private PaymentAccount GetPaymentAccount(CreditNoteModel invoice)
        {
            return GetPaymentAccount(invoice.AccountKey);
        }

        private PaymentAccount GetPaymentAccount(AccountKey accountKey)
        {
            var account = paymentAccountRepository.GetPaymentAccount(accountKey);
            var site = siteRepo.Get(account.SiteId);
            if (!site.IsBilledAsSmsportal)
            {
                account = paymentAccountRepository.GetPaymentAccount(site.AccountKey);
            }
            return account;
        }

        [HttpGet]
        [Route("quote/{reference}")]
        public PdfResponse GenerateQuotePdf(string reference)
        {

            try
            {
                var q = quoteRepository.Get(reference);
                //Since the normal retrieval could return a negative value to comply with the unified list of billing documents:
                q.Id = Math.Abs(q.Id);
                var account = GetPaymentAccount(q);
                var billAddr = billingDetailsRepository.Get(account.AccountKey);
                q.AccountName = billAddr.CompanyName;
                q.BillingAddress = billAddr;

                var pdfBytes = pdfService.CreatePdf(q, null, DocumentType.Quote);
                quoteRepository.AddPdf(q.Id, pdfBytes);
                log.Info($"PDF regenerated for {q} -> {pdfBytes.Length} bytes saved");

                return new PdfResponse
                {
                    PdfBytes = pdfBytes
                };
            }
            catch (Exception ex)
            {
                log.Exception("GenerateQuotePdf", ex);
                return new PdfResponse
                {
                    ErrorMessage = $"{ex.Message}\r\n{ex.StackTrace}"
                };
            }
        }
        
        [HttpGet]
        [Route("costestimate/{reference}")]
        public PdfResponse GenerateCostEstimatePdf(string reference)
        {

            try
            {
                var ce = costEstimateRepository.Get(reference);
                //Since the normal retrieval could return a negative value to comply with the unified list of billing documents:
                ce.Id = Math.Abs(ce.Id);
                var account = GetPaymentAccount(ce);
                var billAddr = billingDetailsRepository.Get(account.AccountKey);
                ce.AccountName = billAddr.CompanyName;
                ce.BillingAddress = billAddr;

                var pdfBytes = pdfService.CreatePdf(ce, ce.Cycle, DocumentType.CostEstimate);
                costEstimateRepository.AddPdf(ce.Id, pdfBytes);
                log.Info($"PDF regenerated for {ce} -> {pdfBytes.Length} bytes saved");

                return new PdfResponse
                {
                    PdfBytes = pdfBytes
                };
            }
            catch (Exception ex)
            {
                log.Exception("GenerateCostEstimatePdf", ex);
                return new PdfResponse
                {
                    ErrorMessage = $"{ex.Message}\r\n{ex.StackTrace}"
                };
            }
        }

        [HttpGet]
        [Route("invoice/{reference}")]
        public PdfResponse GenerateInvoicePdf(string reference)
        {

            try
            {
                var inv = invoiceRepository.Get(reference);
                PostPaidBillingCycle cycle = null;
                if (inv.Status.StartsWith("PostPaid", StringComparison.OrdinalIgnoreCase) ||
                    inv.Status.StartsWith("Monthly", StringComparison.OrdinalIgnoreCase))
                {
                    cycle = postPaidBillingRepository.IdentifyBillingCycle(inv.Id);
                }

                var account = GetPaymentAccount(inv);
                var billAddr = billingDetailsRepository.Get(account.AccountKey);
                inv.AccountName = billAddr.CompanyName;
                inv.BillingAddress = billAddr;

                var pdfBytes = cycle == null ? pdfService.CreatePdf(inv) : pdfService.CreatePdf(inv, cycle);
                invoiceRepository.AddPdf(inv.Id, pdfBytes);
                log.Info($"PDF regenerated for {inv} -> {pdfBytes.Length} bytes saved");

                return new PdfResponse
                {
                    PdfBytes = pdfBytes
                };
            }
            catch (Exception ex)
            {
                log.Exception("GenerateInvoicePdf", ex);
                return new PdfResponse
                {
                    ErrorMessage = $"{ex.Message}\r\n{ex.StackTrace}"
                };
            }
        }


        [HttpGet]
        [Route("creditnote/{reference}")]
        public PdfResponse GenerateCreditNotePdf(string reference)
        {

            try
            {
                var cn = creditNoteRepository.Get(reference);
                var account = GetPaymentAccount(cn);
                var billAddr = billingDetailsRepository.Get(account.AccountKey);
                cn.AccountName = billAddr.CompanyName;
                cn.ClientBillingDetails = billAddr;

                var pdfBytes = pdfService.CreatePdf(cn);
                creditNoteRepository.AddPdf(cn.CreditNoteId, pdfBytes);
                log.Info($"PDF regenerated for {cn} -> {pdfBytes.Length} bytes saved");

                return new PdfResponse
                {
                    PdfBytes = pdfBytes
                };
            }
            catch (Exception ex)
            {
                log.Exception("GenerateCreditNotePdf", ex);
                return new PdfResponse
                {
                    ErrorMessage = $"{ex.Message}\r\n{ex.StackTrace}"
                };
            }
        }
    }
}
