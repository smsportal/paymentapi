﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using AutoMapper;
using log4net;
using SmsPortal.Core;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Controllers
{
	[RoutePrefix("api/invoice")]
	public class InvoiceController : ApiController
	{
		private readonly ILog log = LogManager.GetLogger(typeof(InvoiceController));
		private readonly IPaymentAccountRepository paymentAccountRepository;
		private readonly IBillingGroupOrDetailsRepository billingDetailsRepo;
		private readonly ILegacyInvoiceRepository legacyInvoiceRepository;
		private readonly IInvoiceRepository invoiceRepository;
		private readonly ICreditNoteRepository creditNoteRepo;
		private readonly IAuthorizationRepository authRepo;
		private readonly ICurrencyRepository currencyRepo;
		private readonly ISiteRepository siteRepo;
		private readonly IPdfService pdfService;
		private readonly IEmailService emailer;
		private readonly IPaymentRepository paymentRepo;
		private readonly IRouteCountryRepository routeCountryRepo;
		private readonly IPostPaidBillingCycleRepository postPaidBillingRepository;

		// Only SMSPortal has the billing enabled. Always use SMSPortal's details for invoices and emails.
		private const int SmsPortalSiteId = 1;

		public InvoiceController(
			IPaymentAccountRepository paymentAccountRepository, 
			IBillingGroupOrDetailsRepository billingDetailsRepo,
			ILegacyInvoiceRepository legacyInvoiceRepository, 
			IInvoiceRepository invoiceRepository,
			ISiteRepository siteRepo,
			IPdfService pdfService,
			IEmailService emailer,
			ICreditNoteRepository creditNoteRepo,
			IAuthorizationRepository authRepo,
			ICurrencyRepository currencyRepo,
			IPaymentRepository paymentRepo,
			IRouteCountryRepository routeCountryRepo,
			IPostPaidBillingCycleRepository postPaidBillingRepository)
		{
			this.paymentAccountRepository = paymentAccountRepository;
			this.billingDetailsRepo = billingDetailsRepo;
			this.legacyInvoiceRepository = legacyInvoiceRepository;
			this.invoiceRepository = invoiceRepository;
			this.siteRepo = siteRepo;
			this.pdfService = pdfService;
			this.emailer = emailer;
			this.creditNoteRepo = creditNoteRepo;
			this.authRepo = authRepo;
			this.currencyRepo = currencyRepo;
			this.paymentRepo = paymentRepo;
			this.routeCountryRepo = routeCountryRepo;
			this.postPaidBillingRepository = postPaidBillingRepository;
		}
		
		public IHttpActionResult UpdateInvoiceOrderNumber(UpdateInvoicePaymentNumberRequest request)
		{
			var accountKey = new AccountKey(request.AccountId, request.AccountType);
			var account = paymentAccountRepository.GetPaymentAccount(accountKey);
			var updated = legacyInvoiceRepository.UpdateInvoicePoNumber(account, request.TransactionId, request.OrderNumber);
			var invoice = invoiceRepository.Get(updated.ReferenceNumber);
			var cycle = postPaidBillingRepository.IdentifyBillingCycle(invoice.Id);
			var pdf = cycle.CycleDays > 1 ? pdfService.CreatePdf(invoice, cycle) : pdfService.CreatePdf(invoice);
			invoiceRepository.AddPdf(invoice.Id, pdf);
			log.Info($"Order Number of Invoice {invoice.InvoiceNr} for {invoice.AccountName} updated to {invoice.PurchaseOrderNr}");
			return Ok();
		}

		[HttpPost]
		[Route("create")]
		public CreateInvoiceResponse Create(CreateInvoiceRequest request)
		{
			var accountKey = new AccountKey(request.AccountId, request.AccountType);
			var account = paymentAccountRepository.GetPaymentAccount(accountKey);
			var billingDetails = billingDetailsRepo.Get(accountKey);
			
			var smsPortalSite = siteRepo.Get(SmsPortalSiteId);
			var invoice = CreateInvoice(account, smsPortalSite, request);
			var gateway = smsPortalSite.GetSettings("eft");
			var payment = gateway.CreatePayment(invoice, request.Quantity);
			invoice.StartPayment(payment);
			invoiceRepository.Add(invoice);
			var pdf = pdfService.CreatePdf(invoice);
			payment.Invoice = invoice;
			if (request.RouteCountryId.HasValue)
			{
				payment.RouteCountryId = request.RouteCountryId;
			}
			else
			{
				if (account.AccountType == AccountType.SmsAccount)
				{
					payment.RouteCountryId = account.RouteCountryId;
				}
			}
			paymentRepo.Update(payment);
			invoiceRepository.AddPdf(invoice.Id, pdf);
			if (billingDetails.InvoiceEmail != null && billingDetails.InvoiceEmail != invoice.BillingAddress.InvoiceEmail)
			{
				invoice.BillingAddress.InvoiceEmail = $"{invoice.BillingAddress.InvoiceEmail},{billingDetails.InvoiceEmail}";
			}
			emailer.SendEftInvoiceCreatedEmail(account, invoice, pdf, SmsPortalSiteId);
			var resp = new CreateInvoiceResponse();
			resp.Invoice = Mapper.Map<InvoiceDto>(invoice);
			var auth = payment.GetEftAuthorization();
			auth.SiteId = account.SiteId;
			auth.RouteCountryId = account.RouteCountryId;
			authRepo.Add(auth);
			log.Info($"Invoice {invoice.InvoiceNr} created for {account.Username} ({account.AccountType} {account.Id})");
			return resp;
		}

		private Invoice CreateInvoice(PaymentAccount account, Site site, CreateInvoiceRequest request)
		{
			var billingDetails = billingDetailsRepo.Get(account.AccountKey);
			var currencyCode = SiteCurrency.SMSPortal.Code(billingDetails.CountryId);
			var invoiceCurrency = currencyRepo.GetCurrency(currencyCode);
			var invoice = new Invoice();
			if (billingDetails.CountryId == TaxConfig.SouthAfrica.CountryId)
			{
				invoice.TaxPercentage = TaxConfig.SouthAfrica.TaxPercentage;
				invoice.TaxType = TaxConfig.SouthAfrica.TaxType;
			}
			else
			{
				invoice.IsInternational = true;
				invoice.ConversionRate = (1 / invoiceCurrency.Rate);

			}

			invoice.Created = DateTime.Now;
			var rc = routeCountryRepo.Get(request.RouteCountryId ?? account.RouteCountryId);

			decimal unitPrice;
			if (request.Currency == invoiceCurrency.Code)
			{
				unitPrice = request.UnitPrice;
			}
			else
			{
				var requestCurrency = currencyRepo.GetCurrency(request.Currency);
				unitPrice = requestCurrency.Convert(invoiceCurrency, request.UnitPrice, 4);
			}
		

			invoice.AddLineItem(new InvoiceLineItem
			{
				Code = InvoiceLineItemType.BulkSms,
				Description = $"Bulk SMS Credits - {rc.Country.Name}",
				Quantity = request.Quantity,
				UnitPrice = unitPrice
			});
			if (account.AccountType == AccountType.ResellerAccount)
			{
				invoice.ResellerId = account.Id;
			}
			else
			{
				invoice.LoginId = account.Id;
			}
			invoice.InvoiceNr = site.GetInvoiceNr(invoiceRepository.GetNextInvoiceNumber(site.Id));
			invoice.AccountName = account.Company;
			invoice.PurchaseOrderNr = request.PurchaseOrderNumber;
			invoice.CustomerRef = account.Username;
			invoice.Currency = invoiceCurrency.Code;

			invoice.BillingAddress = billingDetails;

			invoice.PastelCode = 
				string.IsNullOrEmpty(account.ClientRefCode) 
					? site.GetPastelCode(billingDetails.CountryId) 
					: account.ClientRefCode;
			
			invoice.DueDate = DateTime.Now.AddDays(7);
			return invoice;
		}

		[HttpPost]
		[Route("cancel")]
		public CancelInvoiceResponse Cancel([FromBody] CancelInvoiceRequest request)
		{
			var accountKey = new AccountKey(request.AccountId, request.AccountType);
			var paymentAccount = paymentAccountRepository.GetPaymentAccount(accountKey);
			var creditNote = CreateCreditNote(paymentAccount, request);
			var resp = new CancelInvoiceResponse();
			resp.CreditNoteReference = creditNote.ReferenceNumber;
			log.Info($"Invoice {creditNote.InvoiceReferenceNumber} for {creditNote.AccountName} cancelled and Credit Note {creditNote.ReferenceNumber} created");
			return resp;
		}

		[HttpPut]
		[Route("update")]
		public IHttpActionResult Update([FromBody] UpdateInvoicePaymentNumberRequest request)
		{
			return UpdateInvoiceOrderNumber(request);
		}

		[HttpGet]
		[Route("{reference}")]
		public GetInvoiceResponse Get(string reference)
		{
			var invoice = invoiceRepository.Get(reference);
			var invoiceDto = Mapper.Map<InvoiceDto>(invoice);

			return new GetInvoiceResponse
			{
				Invoice = invoiceDto
			};
		}

		[HttpGet]
		[Route("pdf/{reference}")]
		public PdfResponse GetPdf(string reference)
		{
			var pdfBytes = invoiceRepository.GetPdf(reference);
			return new PdfResponse
			{
				PdfBytes = pdfBytes
			};
		}

		[HttpPost]
		[Route("list")]
		public ListInvoicesResponse List([FromBody] ListInvoicesRequest request)
		{

			var invoices = invoiceRepository.List(request);
			var invoiceDtos = Mapper.Map<List<InvoiceDto>>(invoices);
			return new ListInvoicesResponse
			{
				Invoices = invoiceDtos
			};
		}

		private CreditNoteModel CreateCreditNote(PaymentAccount account, CancelInvoiceRequest request)
		{
			var creditNote = legacyInvoiceRepository.CancelInvoice(account, request.InvoiceTransactionId);
			var pdf = pdfService.CreatePdf(creditNote);
			creditNoteRepo.AddPdf(creditNote.CreditNoteId, pdf); 
			emailer.SendInvoiceCancelledEmail(creditNote, pdf, SmsPortalSiteId);
			return creditNote;
		}
	}
}
