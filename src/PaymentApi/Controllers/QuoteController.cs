﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using log4net;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;
using System.Web.Http;
using SmsPortal.Core;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Controllers
{
    [RoutePrefix("api/quote")]
    public class QuoteController : ApiController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(QuoteController));
        private readonly IQuoteRepository quoteRepository;
        private readonly IPostPaidBillingCycleRepository postPaidBillingRepository;
        private readonly IInvoiceRepository invoiceRepository;
        private readonly ISiteRepository siteRepository;
        private readonly IPaymentAccountRepository paymentAccountRepository;
        private readonly IBillingGroupOrDetailsRepository billingDetailsRepository;
        private readonly ISiteRepository siteRepo;
        private readonly ICurrencyRepository currencyRepo;
        private readonly IPdfService pdfService;
        private readonly IEmailService emailService;
        private readonly IRouteCountryRepository routeCountryRepo;

        public QuoteController(
            IQuoteRepository quoteRepository,
            IPostPaidBillingCycleRepository postPaidBillingRepository,
            IInvoiceRepository invoiceRepository,
            ISiteRepository siteRepository,
            IPaymentAccountRepository paymentAccountRepository,
            IPdfService pdfService,
            IEmailService emailService,
            IBillingGroupOrDetailsRepository billingDetailsRepository,
            ISiteRepository siteRepo,
	        ICurrencyRepository currencyRepo,
            IRouteCountryRepository routeCountryRepo)
        {
            this.quoteRepository = quoteRepository;
            this.postPaidBillingRepository = postPaidBillingRepository;
            this.invoiceRepository = invoiceRepository;
            this.siteRepository = siteRepository;
            this.paymentAccountRepository = paymentAccountRepository;
            this.pdfService = pdfService;
            this.emailService = emailService;
            this.billingDetailsRepository = billingDetailsRepository;
            this.siteRepo = siteRepo;
            this.currencyRepo = currencyRepo;
            this.routeCountryRepo = routeCountryRepo;
        }

        [HttpPost]
        [Route("quote")]
        public CreateInvoiceResponse Quote(CreateQuoteRequest request)
        {
            var response = new CreateInvoiceResponse();
            var accountKey = new AccountKey(request.AccountId, request.AccountType);
            var account = paymentAccountRepository.GetPaymentAccount(accountKey);
            var billAddr = billingDetailsRepository.Get(account.AccountKey);
            var invoiceCurrencyCode = SiteCurrency.SMSPortal.Code(billAddr.CountryId);
            var invoiceCurrency = currencyRepo.GetCurrency(invoiceCurrencyCode);

            // Our resellers still need to get smsportal branded emails/Quote numbering whilst reseller accounts need to get approriatly branded emails.
            var site = siteRepo.Get(account.BuysFromSite);

            var rc = routeCountryRepo.Get(request.RouteCountryId ?? account.RouteCountryId);

            decimal unitPrice;
            if (request.Currency == invoiceCurrency.Code)
            {
                unitPrice = request.UnitPrice;
            }
            else
            {
                var requestCurrency = currencyRepo.GetCurrency(request.Currency);
                unitPrice = requestCurrency.Convert(invoiceCurrency, request.UnitPrice, 4);
            }

            var lineItem = new InvoiceLineItem
            {
                UnitPrice = unitPrice,
                Quantity = request.Quantity,
                Code = InvoiceLineItemType.BulkSms,
                Description = $"Bulk SMS Credits - {rc.Country.Name}",
                OptionalNote = "Quoted Lineitem"
            };
            var invoice = postPaidBillingRepository.GetProformaInvoice(account, billAddr, request.InvoiceDate, BillingPeriod.None, lineItem);

            if (billAddr.CountryId == TaxConfig.SouthAfrica.CountryId)
            {
                invoice.TaxPercentage = TaxConfig.SouthAfrica.TaxPercentage;
                invoice.TaxType = TaxConfig.SouthAfrica.TaxType;
            }
            else
            {
                invoice.IsInternational = true;
                invoice.ConversionRate = (1 / invoiceCurrency.Rate);
            }

            invoice.PurchaseOrderNr = request.OrderNumber ?? string.Empty;
            invoice.AccountName = account.Company;
            invoice.InvoiceNr = site.GetQuoteNr(quoteRepository.GetNextQuoteNumber(site.Id));
            invoice.Currency = invoiceCurrency.Code;
            invoice.BillingAddress = billAddr;
            invoice.Status = "Quote";

            quoteRepository.Add(invoice);
            var pdf = pdfService.CreatePdf(invoice, null, DocumentType.Quote);
            quoteRepository.AddPdf(invoice.Id, pdf);

            emailService.SendQuoteCreatedEmail(invoice, pdf, site.Id);

            response.Invoice = Mapper.Map<InvoiceDto>(invoice);
            log.Info($"Quote {invoice.InvoiceNr} created for {account.Username} ({account.AccountType} {account.Id})");
            return response;
        }

        [HttpPost]
        [Route("invoice")]
        public CreateInvoiceResponse Create(ConvertQuoteToInvoiceRequest request)
        {
            throw new NotImplementedException();
            // TODO: uncomment respective code and SP Code when we decide we want to be able to convert an existing quote into an Invoice.
            // var response = new CreateInvoiceResponse();
            // var account = paymentAccountRepository.GetPaymentAccount(request.AccountId, request.AccountType);
            // var site = siteRepository.Get(account.SiteId);
            // var invoiceNr = site.GetInvoiceNr(invoiceRepository.GetNextInvoiceNumber(account.SiteId));
            // var invoice = quoteRepository.ConvertToInvoice(request.AccountType, request.AccountId, request.QuoteId, invoiceNr, request.OrderNumber, string.IsNullOrEmpty(request.Status) ? "UnpaidEFT" : request.Status);
            // var pdf = pdfService.CreatePdf(invoice, null);
            // invoiceRepository.AddPdf(invoice.Id, pdf);
            //
            // var emailer = emailServiceFactory.Get(site.Id);
            // if ((invoice.Status ?? "").Equals("PAID", System.StringComparison.OrdinalIgnoreCase))
            // {
            //     emailer.SendPaidInvoiceEmail(invoice, invoice.BillingAddress.InvoiceEmail, pdf);
            // }
            // else
            // {
            //     emailer.SendUnpaidPrePaidInvoiceEmailCreated(invoice, pdf);
            // }
            //
            // response.Invoice = Mapper.Map<InvoiceDto>(invoice);
            // log.Info($"Quote for {invoice.AccountName} converted to Invoice {invoice.InvoiceNr} with order number {invoice.PurchaseOrderNr}");
            // return response;
        }

        [HttpPost]
        [Route("all/smsacc")]
        public ListInvoicesResponse GetAllForSmsAccounts(ListQuotesRequest request)
        {
            var quotes = quoteRepository.GetAllSmsAccount(request.From, request.To, request.Reference);
            var invoiceDtos = quotes.Select(Mapper.Map<InvoiceDto>).ToList();

            return new ListInvoicesResponse
            {
                Invoices = invoiceDtos
            };
        }

        [HttpPost]
        [Route("all/reseller")]
        public ListInvoicesResponse GetAllForResellers(ListQuotesRequest request)
        {
            var quotes = quoteRepository.GetAllReseller(request.From, request.To, request.Reference);
            var invoiceDtos = Mapper.Map<List<InvoiceDto>>(quotes.ToList());

            return new ListInvoicesResponse
            {
                Invoices = invoiceDtos
            };
        }

        [HttpGet]
        [Route("{reference}")]
        public GetInvoiceResponse Get(string reference)
        {
            var invoice = quoteRepository.Get(reference);
            var invoiceDto = Mapper.Map<InvoiceDto>(invoice);

            return new GetInvoiceResponse
            {
                Invoice = invoiceDto
            };
        }

        [HttpGet]
        [Route("pdf/{reference}")]
        public PdfResponse GetPdf(string reference)
        {
            var pdfBytes = quoteRepository.GetPdf(reference);
            return new PdfResponse
            {
                PdfBytes = pdfBytes
            };
        }
    }
}
