﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using log4net;
using Microsoft.Web.Http;
using SmsPortal.Core;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Controllers
{
    [ApiVersion("1.0")]
    [RoutePrefix("api/billinggroups")]
    public class BillingGroupController : ApiController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(BillingGroupController));
        private readonly IBillingGroupRepository billingGroupRepo;
        private readonly IAccountRepository accountRepository;
        private readonly ICurrencyRepository currencyRepository;

        public BillingGroupController(
            IBillingGroupRepository billingGroupRepo,
            IAccountRepository accountRepository,
            ICurrencyRepository currencyRepository)
        {
            this.billingGroupRepo = billingGroupRepo;
            this.accountRepository = accountRepository;
            this.currencyRepository = currencyRepository;
        }

        [HttpGet]
        [Route("{id}")]
        public GetBillingGroupResponse GetBillingGroup(int id)
        {
            var billingGroup = billingGroupRepo.Get(id);
            return new GetBillingGroupResponse()
            {
                BillingGroup = Mapper.Map<BillingGroupDto>(billingGroup)
            };
        }

        [HttpGet]
        [Route]
        public ListBillingGroupsResponse ListBillingGroupsByCompId(int corporateGroupId)
        {
            var billingGroups = billingGroupRepo.GetAll()
                .Where(bg => bg.CompanyId.HasValue && bg.CompanyId.Value == corporateGroupId);
            return new ListBillingGroupsResponse()
            {
                BillingGroups = Mapper.Map<List<BillingGroupDto>>(billingGroups)
            };
        }

        [HttpGet]
        [Route]
        public ListBillingGroupsResponse ListBillingGroupsByPastelCode(string pastelCode)
        {
            // Although we will always get one billing group, we return a singleton list
            // so that '.../api/billinggroups' is consistently treating query parameters
            // as a filtering operation.
            List<BillingGroup> billingGroups;
            try
            {
                billingGroups = new List<BillingGroup>() { billingGroupRepo.GetByPastelCode(pastelCode) };
            }
            catch (EntityNotFoundException _)
            {
                log.Exception("ListBillingGroupsByPastelCode", new ConfigurationErrorsException($"No billing groups for {pastelCode}"));
                billingGroups = new List<BillingGroup>() { };
            }
            
            return new ListBillingGroupsResponse()
            {
                BillingGroups = Mapper.Map<List<BillingGroupDto>>(billingGroups)
            };
        }

        [HttpGet]
        [Route]
        public ListBillingGroupsResponse ListBillingGroups()
        {
            var billingGroups = billingGroupRepo.GetAll().ToList();
            return new ListBillingGroupsResponse()
            {
                BillingGroups = Mapper.Map<List<BillingGroupDto>>(billingGroups)
            };
        }

        [HttpPost]
        [Route]
        public CreateBillingGroupResponse CreateBillingGroup([FromBody] CreateBillingGroupRequest request)
        {
            var billingGroup = new BillingGroup()
            {
                PastelCode = request.PastelCode,
                CompanyId = request.CompanyId,
                CompanyName = request.CompanyName,
                PriceType = request.PriceType.ToEnum<PriceType>(),
                PriceListId = request.PriceListId,
                PriceFixedStd = request.PriceFixedStd,
                PriceFixedRb = request.PriceFixedRb,
                PriceFixedRbReply = request.PriceFixedRbReply,
                Address1 = request.Address1,
                Address2 = request.Address2,
                City = request.City,
                StateProvince = request.StateProvince,
                ZipPostalCode = request.ZipPostalCode,
                CountryId = request.CountryId,
                Email = request.Email,
                VatNr = request.VatNr,
                RegistrationNr = request.RegistrationNr,
                ScSharedRentalBillingType = ParseBillingType(request.ScSharedRentalBillingType),
                ScDedicatedRentalBillingType = ParseBillingType(request.ScDedicatedRentalBillingType),
                ScRevBilledRentalBillingType = ParseBillingType(request.ScRevBilledRentalBillingType),
                MtBillingType = ParseBillingType(request.MtBillingType),
                MoBillingType = ParseBillingType(request.MoBillingType),
                PostPaid = request.PostPaid,
                BillForNoNetworks = request.BillForNoNetworks
            };
            if (!string.IsNullOrEmpty(request.PriceFixedCurrency))
            {
                billingGroup.Currency = currencyRepository.GetCurrency(request.PriceFixedCurrency);
            }

            var id = billingGroupRepo.Add(billingGroup);
            billingGroup.Id = id;

            return new CreateBillingGroupResponse()
            {
                BillingGroup = Mapper.Map<BillingGroupDto>(billingGroup)
            };
        }

        [HttpPut]
        [Route("{id}")]
        public UpdateBillingGroupResponse UpdateBillingGroup(int id, [FromBody] UpdateBillingGroupRequest request)
        {
            var billingGroup = new BillingGroup()
            {
                Id = id,
                PastelCode = request.PastelCode,
                CompanyId = request.CompanyId,
                CompanyName = request.CompanyName,
                PriceType = request.PriceType.ToEnum<PriceType>(),
                PriceListId = request.PriceListId,
                PriceFixedStd = request.PriceFixedStd,
                PriceFixedRb = request.PriceFixedRb,
                PriceFixedRbReply = request.PriceFixedRbReply,
                Address1 = request.Address1,
                Address2 = request.Address2,
                City = request.City,
                StateProvince = request.StateProvince,
                ZipPostalCode = request.ZipPostalCode,
                CountryId = request.CountryId,
                Email = request.Email,
                VatNr = request.VatNr,
                RegistrationNr = request.RegistrationNr,
                ScSharedRentalBillingType = ParseBillingType(request.ScSharedRentalBillingType),
                ScDedicatedRentalBillingType = ParseBillingType(request.ScDedicatedRentalBillingType),
                ScRevBilledRentalBillingType = ParseBillingType(request.ScRevBilledRentalBillingType),
                MtBillingType = ParseBillingType(request.MtBillingType),
                MoBillingType = ParseBillingType(request.MoBillingType),
                PostPaid = request.PostPaid,
                BillForNoNetworks = request.BillForNoNetworks
            };
            if (!string.IsNullOrEmpty(request.PriceFixedCurrency))
            {
                billingGroup.Currency = currencyRepository.GetCurrency(request.PriceFixedCurrency);
            }
                
            billingGroupRepo.Update(billingGroup);

            return new UpdateBillingGroupResponse();
        }

        [HttpPost]
        [Route("{id}/accounts/{accountId}")]
        public Response AddAccount([FromUri] AddAccountToBillingGroupRequest req)
        {
            var account = accountRepository.GetAccount(req.AccountId);
            if (account.BillingGroupId.HasValue)
            {
                return new AddAccountToBillingGroupResponse()
                {
                    ErrorCode = "InvalidOperationException",
                    ErrorMessage = "Account is already assigned to a billing group."
                };
            }

            var billingGroup = billingGroupRepo.Get(req.Id);
            account.BillingGroupId = billingGroup.Id;
            accountRepository.Update(account);
            return new AddAccountToBillingGroupResponse();
        }

        [HttpDelete]
        [Route("{id}")]
        public DeleteBillingGroupResponse DeleteBillingGroup(int id)
        {
            var accs = accountRepository.GetAccounts(id);
            if (accs.Count > 0)
            {
                return new DeleteBillingGroupResponse
                {
                    ErrorCode = "InvalidOperationException",
                    ErrorMessage = "Billing Group has accounts."
                };
            }
            billingGroupRepo.Delete(id);
            
            return new DeleteBillingGroupResponse();
        }

        public LineItemBreakdownType ParseBillingType(string value) 
            => value.ToEnum<LineItemBreakdownType>(LineItemBreakdownType.Unknown);

        [HttpGet]
        [Route("{billingGroupId}/accounts")]
        public ListAccountsResponse GetAccounts([FromUri] ListAccountsRequest req)
        {
            var resp = new ListAccountsResponse();
            var accounts = accountRepository.GetAccounts(req.BillingGroupId);
            resp.Accounts = accounts.Select(Mapper.Map<AccountDto>).ToList();
            return resp;
        }

    }
}
