﻿using System.Web.Http;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Controllers
{
    [RoutePrefix("api/site")]
    public class SitesController: ApiController
    {
        private readonly ISiteRepository siteRepo;

        public SitesController(ISiteRepository siteRepo)
        {
            this.siteRepo = siteRepo;
        }

        [Route("paymentgateways")]
        public ListPaymentGatewayResponse GetAllPaymentGateways([FromUri] ListPaymentGatewaysRequest request)
        {
            var site = siteRepo.Get(request.SiteId);
            var resp = new ListPaymentGatewayResponse();
            if (site.PayPalSettings != null)
            {
                resp.PaymentGateways.Add(Map(site.PayPalSettings));    
            }
            if (site.SagePaySettings != null)
            {
                resp.PaymentGateways.Add(Map(site.SagePaySettings));    
            }
            if (site.StripeSettings != null)
            {
                resp.PaymentGateways.Add(Map(site.StripeSettings));    
            }
            return resp;
        }

        private PaymentGatewayDto Map(PaymentGatewaySettings settings)
        {
            return new PaymentGatewayDto
                   {
                       CountryId = settings.CountryId,
                       Currency = settings.GatewayCurrency.Code,
                       Enabled = settings.Enabled,
                       Gateway = settings.Gateway,
                       MaxTransaction = settings.MaxTransaction,
                       MinTransaction = settings.MinTransaction,
                       TransactionFee = settings.TranFee
                   };
        }

    }
}
