﻿using System.Collections.Generic;
using System.Web.Http;
using AutoMapper;
using log4net;
using SmsPortal.Plutus.Model;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;

namespace SmsPortal.Plutus.Controllers
{
	[RoutePrefix("api/creditnote")]
	public class CreditNoteController : ApiController
    {
	    private readonly ILog log = LogManager.GetLogger(typeof(CreditNoteController));
	    private readonly ILegacyInvoiceRepository legacyInvoiceRepository;
		private readonly ICreditNoteRepository creditNoteRepository;
	    private readonly IPdfService pdfService;

	    public CreditNoteController(ICreditNoteRepository creditNoteRepository,
			ILegacyInvoiceRepository legacyInvoiceRepository,
			IPdfService pdfService)
	    {
		    this.creditNoteRepository = creditNoteRepository;
		    this.legacyInvoiceRepository = legacyInvoiceRepository;
		    this.pdfService = pdfService;
	    }

		[HttpGet]
		[Route("{reference}")]
		public GetCreditNoteResponse Get(string reference)
		{
			var creditNote = creditNoteRepository.Get(reference);
			var creditNoteDto = Mapper.Map<CreditNoteDto>(creditNote);

			return new GetCreditNoteResponse
			{
				CreditNote = creditNoteDto
			};
		}

		[HttpGet]
		[Route("pdf/{reference}")]
		public PdfResponse GetPdf(string reference)
		{
			var pdfBytes = creditNoteRepository.GetPdf(reference);
			return new PdfResponse
			{
				PdfBytes = pdfBytes
			};
		}

		[HttpPost]
		[Route("list")]
		public ListCreditNotesResponse List([FromBody] ListCreditNotesRequest request)
		{
			var creditNotes = creditNoteRepository.List(request);
			var creditNoteDtos = Mapper.Map<List<CreditNoteDto>>(creditNotes);
			return new ListCreditNotesResponse
			{
				CreditNotes = creditNoteDtos
			};
		}
	}
}
