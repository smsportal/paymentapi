﻿namespace SmsPortal.Plutus
{
	public enum MetricName
	{
		RESTResponseTime,
		RESTSendRate,
		RESTUptime,
		FTPProcessTime,
		FTPSendRate,
		FTPUptime
	}
}
