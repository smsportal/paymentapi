﻿using System.Web.Http.Filters;
using log4net;

namespace SmsPortal.Plutus
{
	public class ApiExceptionFilter : ExceptionFilterAttribute
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(ApiExceptionFilter));
		public override void OnException(HttpActionExecutedContext context)
		{
			Log.Exception("Unhandled exception", context.Exception);
		}
	}
}
