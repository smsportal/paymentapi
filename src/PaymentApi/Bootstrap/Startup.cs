﻿using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Routing;
using Autofac;
using Autofac.Integration.WebApi;
using Metrics;
using Microsoft.Web.Http;
using Microsoft.Web.Http.Routing;
using Microsoft.Web.Http.Versioning;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Owin;
using Owin.Metrics;
using SmsPortal.CoreMetrics;
using SmsPortal.Plutus.Filters;
using SmsPortal.Plutus.Handlers;
using SmsPortal.Plutus.Validation;

namespace SmsPortal.Plutus.Bootstrap
{
    public class Startup
    {
        public static IContainer Container;

        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            var constraintResolver = new DefaultInlineConstraintResolver()
            {
                ConstraintMap =
                {
                    ["apiVersion"] = typeof(ApiVersionRouteConstraint)
                }
            };
            config.MessageHandlers.Add(new OwinRouteTemplateMessageHandler());
            config.MapHttpAttributeRoutes(constraintResolver);
            config.AddApiVersioning(opt =>
            {
                opt.DefaultApiVersion = new ApiVersion(1,0);
                opt.AssumeDefaultVersionWhenUnspecified = true;
                opt.ApiVersionReader = new HeaderApiVersionReader("api-version");
            });
            SwaggerConfig.Register(config);
            ConfigureFilters(config);
            ConfigureRouting(config);
            ConfigureCorse(config);
            ConfigureFormatters(config);
            ConfigureDependencies(config);
            Metric.Config
                .WithAppCounters()
                .WithOwin(middleware => appBuilder.Use(middleware), cfg => cfg
                    .WithRequestMetricsConfig(c => c.WithAllOwinMetrics())
                    .WithMetricsEndpoint())
                .WithReporting(InfluxDbHelper.InfluxDbHttpApplicationReport())
                .WithReporting(InfluxDbHelper.InfluxDbHttpDbReport());
	        MapperConfig.ConfigureMapper();

			appBuilder.UseWebApi(config);
			config.EnsureInitialized();
        }

        private static void ConfigureDependencies(HttpConfiguration configuration)
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModule(new ServiceModule());

			Container = builder.Build();
			configuration.DependencyResolver = new AutofacWebApiDependencyResolver(Container);
		}

        private static void ConfigureFilters(HttpConfiguration configuration)
        {
            var apiExceptionFilter = new ApiExceptionFilter();
            configuration.Filters.Add(apiExceptionFilter);

            var requestValidator = new RequestValidator();
            var validationFilter = new ValidationFilter(requestValidator);
            configuration.Filters.Add(validationFilter);
            configuration.Filters.Add(new ArgumentFilter());
            configuration.Filters.Add(new MetricsFilter());
        }

        private static void ConfigureRouting(HttpConfiguration config)
        {
            //config.MapHttpAttributeRoutes();
        }

        private static void ConfigureCorse(HttpConfiguration configuration)
        {
            var cors = new EnableCorsAttribute("*", "*", "*");
            configuration.EnableCors(cors);
        }

        private static void ConfigureFormatters(HttpConfiguration configuration)
        {
            var jsonFormatter = configuration.Formatters.JsonFormatter;
            var jSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                PreserveReferencesHandling = PreserveReferencesHandling.None
            };
            jSettings.Converters.Add(new StringEnumConverter { CamelCaseText = false });
            jsonFormatter.SerializerSettings = jSettings;
        }

    }
}
