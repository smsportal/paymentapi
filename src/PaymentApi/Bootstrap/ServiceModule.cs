﻿using System.Collections.Generic;
using System.Configuration;
using Autofac;
using Bugle.Comms;
using SmsPortal.Core.RabbitMQ;
using SmsPortal.Core.RabbitMQ.Alerting;
using SmsPortal.Core.RabbitMQ.ChannelManager;
using SmsPortal.Core.RabbitMQ.Configuration.Section;
using SmsPortal.Core.RabbitMQ.Metrics;
using SmsPortal.Core.RabbitMQ.MultiBroker;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Services;
using SmsPortalConfigurationManager = SmsPortal.Core.Configuration.ConfigurationManager;

namespace SmsPortal.Plutus.Bootstrap
{
	public class ServiceModule : Module
    {
        private const string RabbitBaseTopic = "Rabbit";

		protected override void Load(ContainerBuilder builder)
		{
			var configurationManager = new SmsPortalConfigurationManager();
			var rabbitBrokerSection = (MultiBrokerConfigurationSection)ConfigurationManager.GetSection("multiBrokerConfiguration");
			var reportingConnectionString = configurationManager.GetDbString("Reporting");
			var smsWebsiteConnectionString = configurationManager.GetDbString("SmsWebsite");
			var inboundMoConnectionString = configurationManager.GetDbString("InboundMo");
			var sagePayPayNowUrl= configurationManager.GetString("SagePay.PayNow.Url"); 
			var heartbeatUrl= configurationManager.GetString("HeartbeatUrl");
			var notificationAuthToken= configurationManager.GetString("NotificationAuthToken");
			var notificationUrl = configurationManager.GetString("NotificationUrl");
			var appName = configurationManager.GetString("AppName");
			var cacheValidity = configurationManager.GetTimeSpan("CacheValidity");
			var hostname = System.Environment.MachineName;

			builder.Register(ctx => new HeartbeatSender(heartbeatUrl, notificationAuthToken))
				.As<IHeartbeatSender>()
				.ExternallyOwned()
				.SingleInstance();

			builder.Register(ctx => new NotificationSender(
					notificationUrl,
					notificationAuthToken,
					appName))
				.As<INotificationSender>()
				.ExternallyOwned()
				.SingleInstance();

			builder.Register(ctx => new RabbitAlertService(
					ctx.Resolve<INotificationSender>(), 
					appName, 
					hostname,
					new List<IMultiBroker> { ctx.ResolveNamed<IMultiBroker>("Rabbit.Multi") },
					RabbitBaseTopic))
				.As<IRabbitAlertService>()
				.SingleInstance();

			builder.Register(ctx => new TaxConfigRepository(smsWebsiteConnectionString))
				.As<ITaxConfigRepository>()
				.SingleInstance();

			builder.Register(ctx => new PostPaidBillingCycleInvoiceLineRepository(reportingConnectionString))
				.As<IPostPaidBillingCycleInvoiceLineRepository>()
				.SingleInstance();

			builder.Register(ctx => new RefundAccountRepository(smsWebsiteConnectionString))
				.As<IRefundAccountRepository>()
				.SingleInstance();

			builder
				.Register(ctx =>
					new BillingGroupOrDetailsRepository(
						ctx.Resolve<IPaymentAccountRepository>(),
						ctx.Resolve<IBillingDetailsRepository>(),
						ctx.Resolve<IBillingGroupRepository>()))
				.As<IBillingGroupOrDetailsRepository>()
				.SingleInstance();
			
			builder.Register(ctx =>
					new PostPaidEventsRepository(
						ctx.Resolve<ITaxConfigRepository>(),
						ctx.Resolve<IPaymentAccountRepository>(),
						ctx.Resolve<ISiteRepository>(),
						ctx.Resolve<IBillingGroupOrDetailsRepository>(),
						smsWebsiteConnectionString))
				.As<IPostPaidEventsInvoiceLineRepository>()
				.As<IPostPaidEventsRepository>()
				.SingleInstance();

			builder.Register(ctx =>
					new PostPaidBillingCycleRepository(
						ctx.Resolve<IPostPaidBillingCycleInvoiceLineRepository>(),
						ctx.Resolve<ITaxConfigRepository>(),
						ctx.Resolve<ISiteRepository>(),
						ctx.Resolve<IBillingGroupOrDetailsRepository>(),
						smsWebsiteConnectionString))
				.As<IPostPaidBillingCycleRepository>()
				.SingleInstance();

			builder.Register(ctx => new ShortCodeRentalRepository(inboundMoConnectionString))
				.As<IShortCodeRentalRepository>()
				.SingleInstance();

			builder.Register(ctx => new SqlPaymentGatewayRepository(smsWebsiteConnectionString, sagePayPayNowUrl))
				.As<IPaymentGatewayRepository>()
				.SingleInstance();

			builder.Register(ctx =>
					new LegacyInvoiceRepository(
						smsWebsiteConnectionString,
						ctx.Resolve<ICurrencyRepository>(),
						ctx.Resolve<IBillingGroupOrDetailsRepository>(),
						ctx.Resolve<ICreditNoteRepository>(),
						ctx.Resolve<IEmailService>(),
						ctx.Resolve<ISiteRepository>()))
				.As<ILegacyInvoiceRepository>()
				.SingleInstance();

			builder.Register(ctx =>
					new PaymentRepository(
						smsWebsiteConnectionString,
						ctx.Resolve<IInvoiceRepository>()))
				.As<IPaymentRepository>()
				.SingleInstance();

			builder.Register(ctx => new PaymentHistoryRepository(smsWebsiteConnectionString))
				.As<IPaymentHistoryRepository>()
				.SingleInstance();

			builder.Register(ctx => new AuthorizationRepository(smsWebsiteConnectionString))
				.As<IAuthorizationRepository>()
				.SingleInstance();

			builder.Register(ctx => new BillingDetailsRepository(smsWebsiteConnectionString))
				.As<IBillingDetailsRepository>()
				.SingleInstance();

			builder.Register(ctx => new BillingGroupRepository(smsWebsiteConnectionString, ctx.Resolve<ICurrencyRepository>()))
				.As<IBillingGroupRepository>()
				.SingleInstance();
			
			builder.Register(ctx =>
					new SettlementDiscountRepository(smsWebsiteConnectionString))
				.As<ISettlementDiscountRepository>()
				.SingleInstance();

			builder.Register(ctx => new CreditNoteRepository(smsWebsiteConnectionString))
				.As<ICreditNoteRepository>()
				.SingleInstance();

			builder.Register(ctx => new SqlPriceListRepository(smsWebsiteConnectionString))
				.As<IPriceListRepository>()
				.SingleInstance();

			builder.Register(ctx => new AccountRepository(smsWebsiteConnectionString))
				.As<IAccountRepository>()
				.SingleInstance();

			builder.Register(ctx => new TrafficSummaryRepository(reportingConnectionString, ctx.Resolve<IAccountRepository>()))
				.As<ITrafficSummaryRepository>()
				.SingleInstance();

			builder.Register(ctx => new CurrencyRepository(smsWebsiteConnectionString))
				.As<ICurrencyRepository>()
				.SingleInstance();

			builder.Register(ctx =>
					new QuoteRepository(
						smsWebsiteConnectionString,
						ctx.Resolve<ICurrencyRepository>(),
						ctx.Resolve<IBillingGroupOrDetailsRepository>(),
						ctx.Resolve<IInvoiceRepository>()))
				.As<IQuoteRepository>()
				.SingleInstance();

			builder.Register(ctx => new PrefixRepository(smsWebsiteConnectionString))
				.As<IPrefixRepository>()
				.SingleInstance();

			builder.Register(ctx => new TransactionRepository(smsWebsiteConnectionString))
				.As<ITransactionRepository>()
				.SingleInstance();

			builder.Register(ctx => new SqlRouteCountryRepository(smsWebsiteConnectionString))
				.As<IRouteCountryRepository>()
				.SingleInstance();

			builder.Register(ctx => new EventRepository(smsWebsiteConnectionString))
				.As<IEventRepository>()
				.SingleInstance();

			builder.Register(ctx =>
					new PaymentAccountRepository(
						smsWebsiteConnectionString,
						ctx.Resolve<ICurrencyRepository>()))
				.As<IPaymentAccountRepository>()
				.SingleInstance();

			builder.RegisterType<PaymentGenerator>()
				.As<IPaymentGenerator>();

			builder.RegisterType<PdfService>()
				.As<IPdfService>()
				.SingleInstance();

			builder.Register(ctx =>
					new InvoiceRepository(
						smsWebsiteConnectionString,
						ctx.Resolve<ICurrencyRepository>(),
						ctx.Resolve<IBillingGroupOrDetailsRepository>()))
				.As<IInvoiceRepository>()
				.SingleInstance();

			builder.Register(ctx =>
					new CostEstimateRepository(
						smsWebsiteConnectionString,
						ctx.Resolve<ICurrencyRepository>(),
						ctx.Resolve<IInvoiceRepository>()))
				.As<ICostEstimateRepository>()
				.SingleInstance();

			builder.Register(ctx =>
					new SiteRepository(
						smsWebsiteConnectionString,
						ctx.Resolve<IPaymentRepository>(),
						ctx.Resolve<ICurrencyRepository>()))
				.Named<ISiteRepository>("implementor")
				.SingleInstance();
			
			builder.Register(ctx =>
					new CachedSiteRepository(						
						ctx.ResolveNamed<ISiteRepository>("implementor"), 
						cacheValidity))
				.As<ISiteRepository>()
				.SingleInstance();

			builder.Register(ctx =>
					new CreditAllocationRepository(
						smsWebsiteConnectionString,
						ctx.Resolve<ICurrencyRepository>()))
				.As<ICreditAllocationRepository>()
				.SingleInstance();

			builder.RegisterType<CreditAllocationService>()
				.As<ICreditAllocationService>()
				.SingleInstance();

			builder.RegisterType<HtmlPaymentFormBuilderFactory>()
				.As<IHtmlPaymentFormBuilderFactory>()
				.SingleInstance();

			builder.Register(ctx => new PdfService(configurationManager.GetString("PdfService.TemplateDirectory")))
				.As<IPdfService>()
				.SingleInstance();

			builder.RegisterType<TransactionRegistry>()
				.AsSelf()
				.SingleInstance();

			builder.Register(ctx =>
				{
					var config = (ExpiryConfigurationSection) ConfigurationManager.GetSection("expiryConfiguration");
					return new ExpiryService(config,
						ctx.Resolve<IPaymentAccountRepository>(),
						ctx.Resolve<IInvoiceRepository>(),
						ctx.Resolve<ILegacyInvoiceRepository>(),
						ctx.Resolve<ICreditNoteRepository>(),
						ctx.Resolve<IPdfService>(),
						ctx.Resolve<IEmailService>());
				})
				.As<IExpiryService>()
				.SingleInstance();
			
			builder.Register(ctx =>
				{
					var broker = new MeteredRabbitBroker(new RabbitBroker(
						rabbitBrokerSection.PrimaryHostName,
						rabbitBrokerSection.PrimaryPort,
						rabbitBrokerSection.PrimaryVirtualHost,
						rabbitBrokerSection.PrimaryUsername,
						rabbitBrokerSection.PrimaryPassword
					));
					broker.Name = rabbitBrokerSection.PrimaryName;
					return broker;
				})
				.Named<IRabbitBroker>("broker.primary")
				.SingleInstance();
			
			if (!string.IsNullOrWhiteSpace(rabbitBrokerSection.SecondaryHostName))
			{
				builder.Register(ctx =>
					{
						var broker = new MeteredRabbitBroker(new RabbitBroker(
							rabbitBrokerSection.SecondaryHostName,
							rabbitBrokerSection.SecondaryPort,
							rabbitBrokerSection.SecondaryVirtualHost,
							rabbitBrokerSection.SecondaryUsername,
							rabbitBrokerSection.SecondaryPassword
						));
						broker.Name = rabbitBrokerSection.SecondaryName;
						return broker;
					})
					.Named<IRabbitBroker>("broker.secondary")
					.SingleInstance();
			}
			
			builder.Register(ctx =>
				{
					var brokers = new List<IRabbitBroker> {ctx.ResolveNamed<IRabbitBroker>("broker.primary")};
					if (!string.IsNullOrWhiteSpace(rabbitBrokerSection.SecondaryHostName))
					{
						brokers.Add(ctx.ResolveNamed<IRabbitBroker>("broker.secondary"));
					}
					var broker = new MeteredMultiBroker(new MultiBroker(brokers));
					broker.Name = rabbitBrokerSection.Name;
					return broker;
				})
				.Named<IMultiBroker>("Rabbit.Multi")
				.SingleInstance();

			builder.Register(ctx => new ChannelManager(
					ctx.ResolveNamed<IMultiBroker>("Rabbit.Multi"),
					ctx.Resolve<IRabbitAlertService>()))
				.As<IChannelManager>()
				.SingleInstance();

			builder.Register(ctx => new EmailService(
				ctx.Resolve<ISiteRepository>(),
                ctx.Resolve<IChannelManager>(),
				rabbitBrokerSection.WaitForConnections,
				configurationManager.GetString("Rabbit.routingkey.emailservice")))
				.As<IEmailService>()
				.SingleInstance();

			builder.Register(ctx =>
				{
					return new EventRefundService(
						ctx.Resolve<ICreditAllocationRepository>(), 
						ctx.Resolve<IEventRepository>(),
						ctx.Resolve<IEmailService>(),
						ctx.Resolve<ISiteRepository>(),
						ctx.Resolve<IRefundAccountRepository>(),
						ctx.Resolve<IChannelManager>(),
						configurationManager.GetString("Rabbit.EventCancelNotifyQueue"));
				})
				.As<IEventRefundService>()
				.SingleInstance();

			builder.Register(ctx =>
					new PostPaidPaymentAccountService(
						ctx.Resolve<IPaymentAccountRepository>(),
						ctx.Resolve<ISiteRepository>())
				)
				.As<IPostPaidPaymentAccountService>()
				.SingleInstance();
			
			builder.Register(ctx =>
					new ShortCodeRentalInvoiceService(
						ctx.Resolve<IShortCodeRentalRepository>(),
						ctx.Resolve<ISiteRepository>(),
						ctx.Resolve<IBillingGroupOrDetailsRepository>(),
						ctx.Resolve<ITaxConfigRepository>(),
						ctx.Resolve<IPostPaidPaymentAccountService>())
				)
				.As<IShortCodeRentalInvoiceService>()
				.SingleInstance();
			
			builder.Register(ctx =>
					new ProformaInvoiceService(
						ctx.Resolve<IPostPaidBillingCycleRepository>(),
						ctx.Resolve<IPostPaidEventsRepository>(),
						ctx.Resolve<IShortCodeRentalInvoiceService>(),
						ctx.Resolve<ISiteRepository>(),
						ctx.Resolve<IBillingGroupOrDetailsRepository>(),
						ctx.Resolve<ICostEstimateRepository>(),
						ctx.Resolve<IInvoiceRepository>(),
						ctx.Resolve<IPdfService>(),
						ctx.Resolve<ICurrencyRepository>(),
						ctx.Resolve<ISettlementDiscountRepository>(),
						ctx.Resolve<IPostPaidPaymentAccountService>())
					)
				.As<IProformaInvoiceService>()
				.SingleInstance();
		}
	}
}
