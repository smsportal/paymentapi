﻿using AutoMapper;
using SmsPortal.Core;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.v2.Model.Dto;

namespace SmsPortal.Plutus.Bootstrap
{
	public class MapperConfig
	{
		public static void ConfigureMapper()
		{
			Mapper.Initialize(cfg =>
			{
				cfg.CreateMap<InvoiceLineItem, LineItemDto>();
				cfg.CreateMap<DocumentLineItemModel, LineItemDto>()
					.ForMember(dest => dest.Quantity, c => c.MapFrom(src => src.Quantity))
					.ForMember(dest => dest.UnitPrice, c => c.MapFrom(src => src.Price))
					.ForMember(dest => dest.LineTotal, c => c.MapFrom(src => src.Total))
					.ForMember(dest => dest.Description, c => c.MapFrom(src => src.Description));

				cfg.CreateMap<BillingDetails, BillingDetailsDto>();

				cfg.CreateMap<BillingGroup, BillingGroupDto>()
					.ForMember(dest => dest.PriceType, c => c.MapFrom(src => src.PriceType.ToKey()))
					.ForMember(dest => dest.ScSharedRentalBillingType, c => c.MapFrom(src => src.ScSharedRentalBillingType.ToKey()))
					.ForMember(dest => dest.ScDedicatedRentalBillingType, c => c.MapFrom(src => src.ScDedicatedRentalBillingType.ToKey()))
					.ForMember(dest => dest.ScRevBilledRentalBillingType, c => c.MapFrom(src => src.ScRevBilledRentalBillingType.ToKey()))
					.ForMember(dest => dest.MoBillingType, c => c.MapFrom(src => src.MoBillingType.ToKey()))
					.ForMember(dest => dest.MtBillingType, c => c.MapFrom(src => src.MtBillingType.ToKey()))
					.ForMember(dest => dest.PriceFixedCurrency, c => c.MapFrom(src => src.Currency != null ? src.Currency.Code : null));

				cfg.CreateMap<Invoice, InvoiceDto>();

				cfg.CreateMap<Invoice, ProformaInvoice>();

				cfg.CreateMap<CreditNote, CreditNoteModel>()
					.ForMember(dest => dest.CreditNoteId, c => c.MapFrom(src => src.Id))
					.ForMember(dest => dest.ConversionRate, c => c.MapFrom(src => src.ConversionRate != null ? src.ConversionRate.Value.ToString("#.##") : null));
				
				cfg.CreateMap<CreditNoteModel, CreditNoteDto>();

				cfg.CreateMap<CreditAllocation, CreditAllocationDto>();

				cfg.CreateMap<SmsAccount, AccountDto>()
					.ForMember(dest => dest.Name, c => c.MapFrom(src => src.Username))
					.ForMember(dest => dest.AccountType, c => c.MapFrom(src => "sms account"));
				
				cfg.CreateMap<Payment, PaymentDto>();
				cfg.CreateMap<PaymentHistory, PaymentDto>();
			});
		}
	}
}
