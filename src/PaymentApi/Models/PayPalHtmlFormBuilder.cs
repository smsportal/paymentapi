﻿using System;

namespace SmsPortal.Plutus.Models
{
    public class PayPalHtmlFormBuilder: HtmlPaymentFormBuilderBase
    {
        private readonly string domain;

        /// <summary>
        /// The URL of the PaymentGateway, payment requests will be posted to this URL.
        /// </summary>
        public override string PaymentGatewayUrl  => "https://www.paypal.com/cgi-bin/webscr";

        /// <summary>
        /// The URL of the PaymentGateways sandbox enviornment. If the client's account is set to test then payment
        /// requests will be posted to this URL.
        /// </summary>
        public override string SandboxGatewayUrl => "https://www.sandbox.paypal.com/cgi-bin/webscr";

        /// <summary>
        /// PayPal will redirect to this URL if the user cancels the payment while on PayPal's website.
        /// </summary>
        public string CancelReturnUrl { get; }
        /// <summary>
        /// PayPal will redirect the clients browser to this URL if the payment is successful.
        /// </summary>
        public string ReturnUrl { get; }
        
        /// <summary>
        /// PayPal merchant ID provided by PayPal.
        /// </summary>
        public string MerchantId { get; }

        public PayPalHtmlFormBuilder(string merchantId, string domain)
        {
            if (string.IsNullOrEmpty(merchantId))
            {
                throw new ArgumentNullException(nameof(merchantId));
            }

            this.domain = domain;
            CancelReturnUrl = domain + "/app/#/buynow/payment/fail";
            ReturnUrl = domain + "/app/#/buynow/payment/success";
            MerchantId = merchantId;
        }
        
        protected override void OnBuild(Payment payment)
        {
            try
            {
                if (payment == null)
                {
                    throw new PaymentGatewayException(
                        "Paypal",
                        new ArgumentNullException(nameof(payment)),
                        IssueSeverity.Fatal);
                }
                AddHiddenField("cmd", "_xclick");
                AddHiddenField("amount", payment.Total.ToString()); // TODO: Make sure it is rounded properly
                AddHiddenField("cancel_return", CancelReturnUrl);
                AddHiddenField("custom", payment.TransactionId.ToString());
                AddHiddenField("return", ReturnUrl);
                AddHiddenField("rm", "2");
                AddHiddenField("cbt", "Return to merchant");
                AddHiddenField("business", MerchantId);
                AddHiddenField("item_name", "SMS Credit Purchase");
                AddHiddenField("currency_code", payment.Currency);
                AddHiddenField("button_subtype", "services");
                AddHiddenField("notify_url", "https://plutus.smsportal.com/api/paypal");
                AddHiddenField("bn", "PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest");
                AddToForm("<input type='image' src='https://www.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif' border='0' name='submit' alt='Pay now'>");
                AddToForm("<img alt = '' border = '0' src = 'https://www.paypal.com/en_US/i/scr/pixel.gif' width = '1' height = '1'>");
            }
            catch (PaymentGatewayException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new PaymentGatewayException(payment?.Gateway ?? "Paypal", e);
            }
        }
    }
}
