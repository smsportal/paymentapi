﻿namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Class used to hold billing information used for populating invoices and credit notes.
    /// </summary>
	public class BillingDetails
	{
        /// <summary>
        /// The name to appear on the invoice.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Contains client's address information
        /// </summary>
		public string Address1 { get; set; }

        /// <summary>
        /// Contains client's address information
        /// </summary>
		public string Address2 { get; set; }

        /// <summary>
        /// The city in which the client resides.
        /// </summary>
		public string City { get; set; }

        /// <summary>
        /// The client's state or province.
        /// </summary>
		public string StateProvince { get; set; }

        /// <summary>
        /// The client's country of residence.
        /// </summary>
		public string Country { get; set; }


		public int CountryId { get; set; }

        /// <summary>
        /// The client's zip code or postal address
        /// </summary>
		public string ZipCode { get; set; }

        /// <summary>
        /// The client's VAT number (optional)
        /// </summary>
		public string VatNumber { get; set; }

        /// <summary>
        /// The client's company registration number
        /// </summary>
		public string RegistrationNumber { get; set; }

        /// <summary>
        /// The email address where invoices should be sent.
        /// </summary>
		public string InvoiceEmail { get; set; }

        /// <summary>
        /// Client's optional order number.
        /// </summary>
		public string OrderNumber { get; set; }

        public static BillingDetails GetTemporary()
        {
	        return new BillingDetails
	        {
		        CompanyName = "TEMP",
		        Address1 = "No billing address provided",
		        Address2 = "",
		        City = "",
		        StateProvince = "",
		        Country = TaxConfig.Unknown.Description,
		        CountryId = TaxConfig.Unknown.CountryId,
		        ZipCode = "",
		        VatNumber = "",
		        RegistrationNumber = "",
		        InvoiceEmail = "",
		        OrderNumber = "",
	        };
        }
	}
}
