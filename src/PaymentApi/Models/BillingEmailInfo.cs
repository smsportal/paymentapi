﻿namespace SmsPortal.Plutus.Models
{
	public class BillingEmailInfo
	{
		public bool SendEmail { get; set; }
		public string EmailSubject { get; set; }
		public string EmailBody { get; set; }
		public string EmailRecipient { get; set; }
	}
}
