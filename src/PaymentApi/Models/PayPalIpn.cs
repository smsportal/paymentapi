﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace SmsPortal.Plutus.Models
{
    public class PayPalIpn
    {
        private List<string> payload;

        private NameValueCollection nvc;
        
        public decimal Price => Convert.ToDecimal(nvc[Keys.McGross]);

        public string Custom => nvc[Keys.Custom];

        public Guid TransctionId => Guid.Parse(nvc[Keys.Custom]);

        public string PayPalTranId => nvc[Keys.PayPalTranId];

        public PayPalIpn(NameValueCollection nvc)
        {
            this.nvc = nvc;
            if (!string.IsNullOrEmpty(nvc[Keys.Custom]))
            {
                
            }
        }

        private class Keys
        {
            public const string McGross = "mc_gross";

            public const string Custom = "custom";

            public const string PayPalTranId = "txn_id";


        }

        public string ToDebugString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("PayPal IPN:");
            foreach (var key in nvc.AllKeys)
            {
                sb.AppendLine($"\t{key}={nvc[key]}");
            }
            return sb.ToString();
        }
    }
}
