﻿using System;
using Org.BouncyCastle.Asn1.X509.Qualified;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Class that encapsulates currency codes and conversion rates.
    /// </summary>
    public class Currency
    {
        /// <summary>
        /// Creates and initializes a new <c>Currency</c> object with the specified code and rate.
        /// </summary>
        /// <param name="code">Three letter currency code associated with the currency.</param>
        /// <param name="rate">The current rate of conversion compared to the <c>BaseCurrency</c>.</param>
        /// <exception cref="ArgumentException">If the provided rate is negative or zero.</exception>
        public Currency(string code, decimal rate)
        {
            if (rate <= 0)
            {
                throw new ArgumentException("Must be positive and non-zero.", nameof(rate));
            }
            
            Code = code;
            Rate = rate;
        }

        /// <summary>
        /// Three letter currency code associated with the currency.
        /// </summary>
        public string Code { get; }

        /// <summary>
        /// The current rate of conversion compared to the <c>BaseCurrency</c>
        /// </summary>
        public decimal Rate { get; } 

        /// <summary>
        /// The base currency of the system. All conversions performed are with reference to the base currency.
        /// </summary>
        public static Currency BaseCurrency { get; }

        public decimal Convert(Currency target, decimal value, int decimals = 2)
        {
            return Math.Round(value / Rate * target.Rate, decimals, MidpointRounding.AwayFromZero);
        }
       
        public override string ToString()
        {
            return $"[Currency {Code}: Rate={Rate}]";
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }


        public override bool Equals(object obj)
        {
            if (!(obj is Currency))
            {
                return false;
            }

            var other = (Currency) obj;
            return Code.Equals(other.Code) && (Rate == other.Rate);
        }

        static Currency()
        {
            BaseCurrency = new Currency("ZAR", 1);
        }
    }
}
