using System;
using SmsPortal.Plutus.Models.Enums;

// ReSharper disable UnusedMember.Global
// ReSharper disable ParameterHidesMember

namespace SmsPortal.Plutus.Models
{
    public abstract class BaseInvoiceBuilder<TInvoice>: IInvoiceBuilder<TInvoice> where TInvoice : Invoice, new()
    {
        private TimeSpan dueIn = TimeSpan.FromDays(7);
        private string invoiceRefNr;
        private Currency currency;
        
        private string purchaseOrderNr;
        
        public BaseInvoiceBuilder<TInvoice> WithInvoiceNr(string invoiceReferenceNr)
        {
            invoiceRefNr = invoiceReferenceNr;
            return this;
        }

        public BaseInvoiceBuilder<TInvoice> WithCurrency(Currency currency)
        {
            this.currency = currency;
            return this;
        }

        public BaseInvoiceBuilder<TInvoice> WithPurchaseOrderNr(string purchaseOrderNr)
        {
            this.purchaseOrderNr = purchaseOrderNr;
            return this;
        }

        public BaseInvoiceBuilder<TInvoice> WithDueDate(int days)
        {
            this.dueIn = TimeSpan.FromDays(days);
            return this;
        }

        public abstract TInvoice Build(BillingGroup billingGroup, PaymentAccount account, Site site);
        
        protected TInvoice SetBaseProperties(BillingGroup billingGroup, PaymentAccount account, Site site)
        {
            var invoice = new TInvoice
            {
                InvoiceNr = invoiceRefNr,
                BillingAddress = billingGroup.ToBillingDetails(),
                AccountName = account.Company
            };
            if (!string.IsNullOrEmpty(purchaseOrderNr))
            {
                invoice.PurchaseOrderNr = purchaseOrderNr;
            }
            if (account.AccountType == AccountType.ResellerAccount)
            {
                invoice.ResellerId = account.Id;
            }
            else if (account.AccountType == AccountType.SmsAccount)
            {
                invoice.LoginId = account.Id;
            }
            else
            {
                throw new InvalidOperationException($"Account type {account.AccountType} not supported for invoices");
            }

            if (billingGroup.CountryId == TaxConfig.SouthAfrica.CountryId) //Tax decisions are based on the billing address country
            {
                invoice.Currency = Currency.BaseCurrency.Code;
                invoice.TaxPercentage = TaxConfig.SouthAfrica.TaxPercentage;
                invoice.TaxType = TaxConfig.SouthAfrica.TaxType;
            }
            else
            {
                if (currency == null)
                {
                    throw new InvalidOperationException("Foreign accounts need to set a currency before building");
                }
                invoice.Currency = currency.Code;
                invoice.ConversionRate = 1 / currency.Rate;
                invoice.IsInternational = true;
            }

            invoice.CustomerRef = account.Username;
            invoice.Created = DateTime.Now;    
            invoice.DueDate = invoice.Created.Add(dueIn);

            invoice.PastelCode =
                string.IsNullOrEmpty(billingGroup.PastelCode)
                    ? string.IsNullOrEmpty(account.ClientRefCode)
                        ? site.GetPastelCode(billingGroup.CountryId)
                        : account.ClientRefCode
                    : billingGroup.PastelCode;
            
            return invoice;
        }
    }
    
}