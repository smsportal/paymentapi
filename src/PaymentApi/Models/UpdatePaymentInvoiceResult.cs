﻿namespace SmsPortal.Plutus.Models
{
	public class UpdatePaymentInvoiceResult
	{
		public int Result { get; set; }
		public string OriginalStatus { get; set; }
	}
}
