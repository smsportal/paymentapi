﻿using System;

namespace SmsPortal.Plutus.Models
{
    public abstract class IssueSeverityException : Exception
    {
        public IssueSeverity Severity { get; }

        public IssueSeverityException(string message, Exception innerException = null, IssueSeverity issueSeverity = IssueSeverity.Error)
            : base(message, innerException)
        {
            Severity = issueSeverity;
        }
    }
}