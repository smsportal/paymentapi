﻿using System;
using System.Configuration;

namespace SmsPortal.Plutus.Models
{
    public class PaymentGatewayConfigurationException : ConfigurationErrorsException
    {
        public string Gateway { get; }

        public override string Message => $"{Gateway}: {base.Message}";

        public PaymentGatewayConfigurationException(string gateway, string message, Exception innerException)
            : base(message ?? "Configuration error.", innerException)
        {
            Gateway = gateway;
        }
    }
}