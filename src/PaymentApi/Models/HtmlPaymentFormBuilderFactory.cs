﻿using System;
using System.Configuration;
using SmsPortal.Core;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Models
{
    public class HtmlPaymentFormBuilderFactory: IHtmlPaymentFormBuilderFactory
    {
        private readonly IPaymentGatewayRepository repo;

        public HtmlPaymentFormBuilderFactory(IPaymentGatewayRepository repo)
        {
            this.repo = repo;
        }
        
        public IHtmlPaymentFormBuilder GetFormBuilder(Site site, string gateway, bool useSandbox)
        {
            HtmlPaymentFormBuilderBase builder;
            var paymentGateway = gateway.ToNullableEnum<PaymentGatewayType>();
            switch (paymentGateway)
            {
                case PaymentGatewayType.NetCash:
                case PaymentGatewayType.NetCashInstantEft:
                    var sageSettings = repo.GetSagePaySettings(site.Id);
                    if (string.IsNullOrEmpty(sageSettings?.VendorKey))
                    {
                        throw new ConfigurationErrorsException($"No {gateway} configuration for site {site.Id} ({site.Name})");
                    }
                    builder = new SagePayHtmlFormBuilder(sageSettings.VendorKey, sageSettings.ServiceKey, sageSettings.PaymentGatewayUrl);
                    break;
                
                case PaymentGatewayType.PayPal:
                    var payPalSettings = repo.GetPayPalSettings(site.Id);
                    if (string.IsNullOrEmpty(payPalSettings?.MerchantId))
                    {
                        throw new ConfigurationErrorsException($"No {gateway} configuration for site {site.Id} ({site.Name})");
                    }
                    builder = new PayPalHtmlFormBuilder(payPalSettings.MerchantId, site.Domain);
                    break;
                
                case PaymentGatewayType.Stripe:
                    var stripeSettings = repo.GetStripeSettings(site.Id);
                    if (string.IsNullOrEmpty(stripeSettings?.ApiKey))
                    {
                        throw new ConfigurationErrorsException($"No {gateway} configuration for site {site.Id} ({site.Name})");
                    }
                    builder = new StripeHtmlFormBuilder(stripeSettings);
                    break;
                
                default:
                    throw new InvalidOperationException($"No HtmlFormBuilder available for Gateway {gateway}");
            }
            builder.UseSandbox = useSandbox;
            
            return builder;
        }
    }
}
