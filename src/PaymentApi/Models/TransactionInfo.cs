﻿
namespace SmsPortal.Plutus.Models
{
	public class TransactionInfo
	{
		public string AccountType { get; set; }

		public int AccountId { get; set; }

		public string TransactionId { get; set; }

		public string Currency { get; set; }

		public decimal Total { get; set; }

		public decimal TaxValue { get; set; }

		public int Credits { get; set; }
        
        public int InvoiceId { get; set; }

		public override string ToString()
		{
		    return $"[TransactionInfo {TransactionId}: AccountId={AccountId}, Total={Total}, InvoiceId={InvoiceId}]";
		}
	}
}
