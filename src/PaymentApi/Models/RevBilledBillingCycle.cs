﻿using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
	public class RevBilledBillingCycle : BillingCycle
	{
        public override InvoiceLineItemType LineItemType
        {
            get => InvoiceLineItemType.ReverseBilledReplySms;
            set => base.LineItemType = value;
        }
    }
}
