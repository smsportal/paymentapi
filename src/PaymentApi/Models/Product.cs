namespace SmsPortal.Plutus.Models
{
    public class Product
    {
        public string Code { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }
    }
}