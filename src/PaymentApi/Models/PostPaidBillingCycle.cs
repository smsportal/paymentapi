﻿using SmsPortal.Plutus.Models.Enums;
using System;
using SmsPortal.Core;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Represents the details of a post-paid billing cycle
    /// </summary>
    public class PostPaidBillingCycle
    {
        private DateTime? strtDate;
        private DateTime? endDate;
        
        public PostPaidBillingCycle(){}

        public PostPaidBillingCycle(DateTime start, DateTime end)
        {
            strtDate = start;
            endDate = end;
        }

        /// <summary>
        /// The amount of messages in the cycles (to be used for non-billing purposes like variables in the email templates only).
        /// </summary>
        public int MsgCount { get; set; } = 0;

        /// <summary>
        /// The (main) type of line items in the invoice.
        /// </summary>
        public virtual InvoiceLineItemType LineItemType { get; set; } = InvoiceLineItemType.BulkSms;

        /// <summary>
        /// The schema/billing type used to determine the break down of the line items.
        /// </summary>
        public virtual LineItemBreakdownType LineItemBreakdown { get; set; } = LineItemBreakdownType.Unknown;

        /// <summary>
        /// A classification of billing cycle/period.
        /// </summary>
        public virtual BillingPeriod BillingPeriodType { get; set; } = BillingPeriod.None;

        /// <summary>
        /// The start date of the billing cycle/period.
        /// </summary>
        public DateTime CycleStartDate
        {
            get
            {
                return strtDate.HasValue 
                    ? Math.Abs((CycleEndDate.TimeOfDay - new TimeSpan(23,59,59)).TotalSeconds) > 0
                        ? strtDate.Value //Exact value - since the end time has an exact value
                        : strtDate.Value.Date //Start of the day
                    : CycleEndDate.Date;
            }
            internal set
            {
                strtDate = value;
            }
        }

        /// <summary>
        /// The end date of the billing cycle/period.
        /// </summary>
        public DateTime CycleEndDate
        {
            get
            {
                return endDate.HasValue 
                    ? endDate.Value.Hour > 0 || endDate.Value.Minute > 0 || endDate.Value.Second > 0
                        ? endDate.Value //Exact value
                        :  endDate.Value.Date.AddDays(1).AddMilliseconds(-1) //End of the day
                    : DateTime.Now;
            }
            internal set
            {
                endDate = value;
                if (endDate.HasValue && strtDate.HasValue)
                {
                    if (endDate.Value.AddMonths(-1).AddDays(1).Date == strtDate.Value.Date)
                    {
                        if (strtDate.Value.Day.Equals(26))
                        {
                            BillingPeriodType = BillingPeriod.Early;
                        }
                        else if (strtDate.Value.Day.Equals(1))
                        {
                            BillingPeriodType = BillingPeriod.Monthly;
                        }
                    }
                    else
                    {
                        BillingPeriodType = BillingPeriod.None;
                    }
                }
            }
        }

        /// <summary>
        /// The amount of days in the billing cycle/period.
        /// </summary>
        public int CycleDays => ((CycleEndDate.Date - CycleStartDate.Date).Days + 1);

        /// <summary>
        /// A description of the (main) type of line items in the invoice.
        /// </summary>
        public string ItemDescription
        {
            get
            {
                var baseCode = LineItemType;
                if(baseCode == InvoiceLineItemType.TransactionFee)
                {
                    baseCode = InvoiceLineItemType.BulkSms;
                }
                var desc = baseCode.ToDescription();
                return $"{CycleEndDate:MMM yyyy} {desc}";
            }
        }

        /// <summary>
        /// A description of the schema/billing type used to determine the break down of the line items.
        /// </summary>
        public string BreakdownDescription => LineItemBreakdown.ToDescription();

        /// <summary>
        /// The preferred invoice date of the billing cycle/period.
        /// </summary>
        public DateTime InvoiceDate
        {
            get
            {
                switch (LineItemType)
                {
                    case InvoiceLineItemType.BulkSms:
                    case InvoiceLineItemType.ReverseBilledReplySms:
                    case InvoiceLineItemType.ReverseBilledShortCodeSms:
                        return CycleEndDate.Date;
                    case InvoiceLineItemType.ShortCodeSetup:
                    case InvoiceLineItemType.ShortCodeKeywordRental:
                    case InvoiceLineItemType.ShortCodeRental:
                        return CycleStartDate.Date;
                    case InvoiceLineItemType.TransactionFee:
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }
            if(!obj.GetType().IsSubclassOf(typeof(PostPaidBillingCycle)) && obj.GetType() != typeof(PostPaidBillingCycle))
            {
                return false;
            }
            var cycle = (PostPaidBillingCycle)obj;
            return (CycleStartDate.Date == cycle.CycleStartDate.Date && CycleEndDate.Date == cycle.CycleEndDate.Date && BillingPeriodType == cycle.BillingPeriodType && LineItemType == cycle.LineItemType);
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            var hashCode = 1076027;
            hashCode = hashCode * -1521134295 + CycleStartDate.GetHashCode();
            hashCode = hashCode * -1521134295 + CycleEndDate.GetHashCode();
            hashCode = hashCode * -1521134295 + BillingPeriodType.GetHashCode();
            hashCode = hashCode * -1521134295 + LineItemType.GetHashCode();
            return hashCode;
        }

        public static PostPaidBillingCycle FromMonth(int month)
        {
            var now = DateTime.Now;
            var startDate = new DateTime(now.Year, month, 1);
            if (month == 12)
            {
                startDate = startDate.AddYears(-1);
            }
        
            var endDate = startDate.AddMonths(1).AddDays(-1);
            return new PostPaidBillingCycle(startDate, endDate);
        }

        public override string ToString()
        {
            return $"[PostPaidBillingCycle {ItemDescription}: Breakdown={BreakdownDescription} Start={strtDate?.ToString("yyyy/MM/dd")}, End={endDate?.ToString("yyyy/MM/dd")}]";
        }
    }
}
