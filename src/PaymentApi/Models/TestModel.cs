﻿namespace SmsPortal.Plutus.Models
{
	public class TestModel
	{
		public int value1 { get; set; }

		public string value2 { get; set; }

		public TestModel(int val1, string val2)
		{
			this.value1 = val1;
			this.value2 = val2;
		}
	}
}
