﻿namespace SmsPortal.Plutus.Models
{
    public interface IHtmlPaymentFormBuilderFactory
    {
        IHtmlPaymentFormBuilder GetFormBuilder(Site site, string gateway, bool useSandbox);
    }
}
