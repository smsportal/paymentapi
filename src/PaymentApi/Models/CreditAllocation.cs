using System;

namespace SmsPortal.Plutus.Models
{
    public class CreditAllocation
    {
        public int Id { get; set; }
        
        public int AccountId { get; set; }

        public string AccountType { get; set; }

        public int Credits { get; set; }

        public DateTime Allocated { get; set; }

        public int InvoiceId { get; set; }

        public int RouteCountryId { get; set; }

        public Guid TransacitonId { get; set; }

        public string GatewayTransactionId { get; set; }

        public string Description { get; set; }

        public decimal Cost { get; set; }
    }
}