﻿using SmsPortal.Core;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    public struct AccountKey
    {
        public AccountKey(int id, AccountType type)
        {
            Id = id;
            Type = type;
        }

        public AccountKey(int id, string type) : this(id, type.ToEnum<AccountType>())
        {
        }

        public int Id { get; }
        public AccountType Type { get; }

        public override string ToString() =>
            $"{Type.ToKey()}-{Id}";

        public bool Equals(AccountKey other)
        {
            return Id == other.Id && Type == other.Type;
        }
        
        public bool Equals(AccountKey? other)
        {
            return other != null && Equals(other.Value);
        }

        public override bool Equals(object obj)
        {
            return obj is AccountKey other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id * 397) ^ (int) Type;
            }
        }
        
        public static bool operator ==(AccountKey a, AccountKey b) 
        {
            return a.Equals(b);
        }

        public static bool operator !=(AccountKey a, AccountKey b) 
        {
            return !a.Equals(b);
        }
        
    }
}