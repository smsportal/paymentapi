﻿using Newtonsoft.Json;

namespace SmsPortal.Plutus.Models
{
    public abstract class BaseEmailParams
    {
        [JsonProperty("contact_name")]
        public string ContactName { get; set; }
    }

    /// <summary>
    /// Credit Card / PayPal / Instant EFT invoice created.
    /// </summary>
    public class InvoiceCreatedEmailParams: BaseEmailParams
    {
        [JsonProperty("invoice_ref_nr")]
        public string InvoiceReference { get; set; }
    }

    /// <summary>
    /// Invoice has expired due to non-payment by the due date.
    /// </summary>
    public class InvoiceExpiredEmailParams: BaseEmailParams
    {
        [JsonProperty("credit_note_ref_nr")]
        public string CreditNoteReference { get; set; }
        
        [JsonProperty("invoice_ref_nr")]
        public string InvoiceReference { get; set; }
    }

    /// <summary>
    /// Invoice cancelled by the client.
    /// </summary>
    public class InvoiceCancelledEmailParams: BaseEmailParams
    {
        [JsonProperty("credit_note_ref_nr")]
        public string CreditNoteReference { get; set; }

        [JsonProperty("invoice_ref_nr")]
        public string InvoiceReference { get; set; }
    }

    /// <summary>
    /// Unpaid EFT invoice created.
    /// </summary>
    public class EftInvoiceCreatedEmailParams: BaseEmailParams
    {
        [JsonProperty("invoice_ref_nr")]
        public string InvoiceReference { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }
    }

    /// <summary>
    /// Cost Estimate created.
    /// </summary>
    public class CostEstimateCreatedEmailParams : BaseEmailParams
    {
        [JsonProperty("costestimate_ref_nr")]
        public string CostEstimateReference { get; set; }

        [JsonProperty("billing_period")]
        public string BillingPeriod { get; set; }
    }

    /// <summary>
    /// Unpaid Post-Paid invoice created.
    /// </summary>
    public class UnpaidPostPaidInvoiceCreatedEmailParams: BaseEmailParams
    {
        [JsonProperty("invoice_ref_nr")]
        public string InvoiceReference { get; set; }

        [JsonProperty("billing_period")]
        public string BillingPeriod { get; set; }

        [JsonProperty("billing_type")]
        public string BillingType{ get; set; }
    }

    /// <summary>
    /// Unpaid Post-Paid invoice created.
    /// </summary>
    public class UnpaidCampaignInvoiceCreatedEmailParams : BaseEmailParams
    {
        [JsonProperty("invoice_ref_nr")]
        public string InvoiceReference { get; set; }

        [JsonProperty("campaign_name")]
        public string CampaignName { get; set; }

        [JsonProperty("campaign_created_date")]
        public string CampaignCreatedDate { get; set; }

        [JsonProperty("campaign_created_time")]
        public string CampaignCreatedTime { get; set; }

        [JsonProperty("campaign_date")]
        public string CampaignSendDate { get; set; }

        [JsonProperty("campaign_time")]
        public string CampaignSendTime { get; set; }

        [JsonProperty("campaign_quantity")]
        public string CampaignNumberOfMessages { get; set; }

        [JsonProperty("campaign_credits")]
        public string CampaignNumberOfCredits { get; set; }

        [JsonProperty("campaign_message")]
        public string ExampleMessage { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("price")]
        public string PricePerCredit { get; set; }

        [JsonProperty("total_excluding")]
        public string TotalExcl { get; set; }

        [JsonProperty("total_including")]
        public string TotalIncl { get; set; }

        [JsonProperty("total_tax")]
        public string TaxAmount { get; set; }
    }

    /// <summary>
    /// Unpaid Pre-Paid invoice created.
    /// </summary>
    public class UnpaidPrePaidInvoiceCreatedEmailParams: BaseEmailParams
    {
        [JsonProperty("invoice_ref_nr")]
        public string InvoiceReference { get; set; }
    }

    /// <summary>
    /// Resend invoice or credit note.
    /// </summary>
    public class ResendInvoiceCreditNoteEmailParams: BaseEmailParams
    {
        [JsonProperty("document_type")]
        public string DocumentType { get; set; }

        [JsonProperty("invoice_ref_nr")]
        public string InvoiceReference { get; set; }
    }

    /// <summary>
    /// Send credit allocation email parameters.
    /// </summary>
    public class CreditAllocationEmailParams : BaseEmailParams
    {
        [JsonProperty("credits_loaded")]
        public string CreditsLoaded { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("balance")]
        public string Balance { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }
    }

    public class PaymentAuthorizationParams : BaseEmailParams
    {
        [JsonProperty("account")]
        public string AccountName { get; set; }

        [JsonProperty("payment")]
        public string Payment { get; set; }

        [JsonProperty("credits")]
        public string Credits { get; set; }
    }

    public class PaymentNotificationParams : BaseEmailParams
    {
        [JsonProperty("user_name")] 
        public string Username { get; set; }
        
        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("credits_purchased")]
        public string Credits { get; set; }

        [JsonProperty("client_credit_cost")]
        public string ClientCreditCost { get; set; }

        [JsonProperty("client_transaction_cost")]
        public string ClientTransactionCost { get; set; }

        [JsonProperty("client_VAT")]
        public string ClientVat { get; set; }
        
        [JsonProperty("client_total_cost")] 
        public string ClientTotalCost { get; set; }
        
        [JsonProperty("currency")]
        public string Currency { get; set; }
    }

    /// <summary>
    /// Quote created.
    /// </summary>
    public class QuoteCreatedEmailParams : BaseEmailParams
    {
        [JsonProperty("quote_ref_nr")]
        public string QuoteReference { get; set; }
    }
	
	public class CancelRefundNotificationParams : BaseEmailParams
    {
        [JsonProperty("event_user_name")] 
        public string EventUsername { get; set; }
        
        [JsonProperty("event_created_date")]
        public string EventCreatedDate { get; set; }

        [JsonProperty("event_created_time")]
        public string EventCreatedTime { get; set; }

        [JsonProperty("event_scheduled_date")]
        public string EventScheduledDate { get; set; }

        [JsonProperty("event_scheduled_time")]
        public string EventScheduledTime { get; set; }

        [JsonProperty("refunded_messages")]
        public string RefundedMessages { get; set; }

        [JsonProperty("refunded_credits")]
        public string RefundedCredits { get; set; }

        [JsonProperty("example_message")]
        public string ExampleMessage { get; set; }
    }
}
