﻿using System.Collections.Generic;
using System.Text;

namespace SmsPortal.Plutus.Models
{
    public abstract class HtmlPaymentFormBuilderBase: IHtmlPaymentFormBuilder
    {
        private Dictionary<string,string> hiddenFields = new Dictionary<string,string>();
        private List<string> rawHtmlLines = new List<string>();

        public abstract string PaymentGatewayUrl { get; }

        public abstract string SandboxGatewayUrl { get; }

        protected abstract void OnBuild(Payment payment);

        public bool UseSandbox { get; set; }
        
        public void AddHiddenField(string name, string value)
        {
            hiddenFields[name] = value;
        }

        public void AddToForm(string rawHtml)
        {
            rawHtmlLines.Add(rawHtml);
        }

        public string GetHtml(Payment payment)
        {
            OnBuild(payment);
            var sb = new StringBuilder();
            sb.AppendFormat("<form id='paymentForm' action='{0}' method='post'>", UseSandbox ? SandboxGatewayUrl : PaymentGatewayUrl);
            foreach (var field in hiddenFields)
            {
                sb.AppendLine($"<input type='hidden' name='{field.Key}' value='{field.Value}'>");
            }
            foreach (var rawHtml in rawHtmlLines)
            {
                sb.AppendLine(rawHtml);
            }
            sb.AppendFormat("</form>");
            return sb.ToString();
        }

        public IDictionary<string, string> GetVariables(Payment payment)
        {
            OnBuild(payment);
            return hiddenFields;
        }
    }
}
