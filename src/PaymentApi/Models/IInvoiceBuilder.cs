namespace SmsPortal.Plutus.Models
{
    public interface IInvoiceBuilder<TInvoice> where TInvoice: Invoice, new()
    {
        TInvoice Build(BillingGroup billingGroup, PaymentAccount account, Site site);
    }
}