﻿
namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Represents a single proforma invoice based on a post-paid billing cycle
    /// </summary>
    public class ProformaInvoice : Invoice
    {
        public PostPaidBillingCycle Cycle { get; set; }
    }
}
