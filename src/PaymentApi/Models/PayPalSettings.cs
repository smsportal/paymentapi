namespace SmsPortal.Plutus.Models
{
    public class PayPalSettings
    {
        public int ResellerId { get; set; }
        
        public string MerchantId { get; set; }

        public string Currency { get; set; }

        public int Minimum { get; set; }

        public int Maximum { get; set; }

        public decimal TransactionFee { get; set; }
    }
}