﻿using System;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// A representation of a short code allocation with the potential to be converted into a short code rental invoice line.
    /// </summary>
    public class ShortCodeRentalApplicability
    {
        /// <summary>
        /// The unique numerical identifier of the specific short code allocation.
        /// </summary>
        public int AllocationId { get; set; }
        /// <summary>
        /// The unique numerical identifier of the site associated with the short code.
        /// If provided the details of the associated reseller account should be retrieved for billing.
        /// If a <see cref="ShortCodeRentalApplicability.LoginId"/> is also provided, then the LoginId takes precedence.  
        /// </summary>
        public int? SiteId { get; set; }
        /// <summary>
        /// The unique numerical identifier of the user account associated with the short code.
        /// If provided the details of the master account of the associated user account should be retrieved for billing.
        /// If a <see cref="ShortCodeRentalApplicability.SiteId"/> is also provided, then the LoginId takes precedence.  
        /// </summary>
        public int? LoginId { get; set; }
        /// <summary>
        /// The short code number.
        /// </summary>
        public long ShortCode { get; set; }
        /// <summary>
        /// The short keyword.
        /// If NULL or empty then the complete short code is rented as a dedicated short code.
        /// </summary>
        public string Keyword { get; set; }
        /// <summary>
        /// The first date included in the rental period.
        /// </summary>
        public DateTime CycleStartDate { get; set; }
        /// <summary>
        /// The last date included in the rental period.
        /// </summary>
        public DateTime CycleEndDate { get; set; }
        /// <summary>
        /// The first active date for the shortcode or keyword.
        /// </summary>
        public DateTime ConfigurationStartDate { get; set; }
        /// <summary>
        /// The last active date for the shortcode or keyword.
        /// </summary>
        public DateTime ConfigurationEndDate { get; set; }

        public bool IsDedicated() => string.IsNullOrEmpty(Keyword);
        
        public AccountKey? IdentifyAccount() =>
            SiteId.HasValue
                ? new AccountKey(SiteId.Value, AccountType.SiteId)
                : LoginId.HasValue
                    ? new AccountKey(LoginId.Value, AccountType.SmsAccount)
                    : (AccountKey?)null;


        private const string ShortcodeRentalTag = "Short Code Rental -";

        public static string Tag(long code) => $"{ShortcodeRentalTag} {code}";
        
        public string ToShortCodeKeywordDescription(string username)
        {
            return IsDedicated()
                    ? $"{ShortcodeRentalTag} {ShortCode} ({username})"
                    : $"{ShortcodeRentalTag} Keyword {Keyword} on {ShortCode} ({username})";
        }

        public bool WasAccountRecentlySetup() =>
            ConfigurationStartDate.Month == CycleStartDate.Month &&
            ConfigurationStartDate.Year == CycleStartDate.Year;
        
        public override string ToString() =>
            $"[ShortCodeRentalApplicability {ShortCode} {Keyword} (#{AllocationId}): LoginId={LoginId} SiteId={SiteId} Cycle={CycleStartDate:dd MMMM yyyy}-{CycleEndDate:dd MMMM yyyy}, Configured={ConfigurationStartDate:dd MMMM yyyy}-{ConfigurationEndDate:dd MMMM yyyy}]";
    }
}