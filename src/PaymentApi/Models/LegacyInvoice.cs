﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
	public class LegacyInvoice
	{
		public decimal TaxPercentage { get; set; }
		public string AccountName { get; set; }
		public string OrderNumber { get; set; }
		public string ClientRefCode { get; set; }
		public string CustomerRef { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime DueDate { get; set; }
		public string Currency { get; set; }
		public string TransactionId { get; set; }
		public InvoiceStatus Status { get; set; }
		public List<LegacyInvoiceLineItem> LineItems { get; set; }
		public LegacyEmail InvoiceLegacyEmailInfo { get; set; }
		public bool IsPostPaid { get; set; }
		public bool ShowBillingPeriod { get; set; }
		public DateTime PeriodBilledFromDate { get; set; }
		public DateTime PeriodBilledToDate { get; set; }
		public InvoiceType InvoiceType { get; set; }
		public bool IsInternational { get; set; }

		public decimal SubTotal
		{
			get
			{
				return LineItems.Sum(lineItem => lineItem.Quantity * lineItem.Price);
			}
		}

		public decimal TaxValue => SubTotal * TaxPercentage / 100;

		public decimal Total => SubTotal + TaxValue;
	}
}
