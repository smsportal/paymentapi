namespace SmsPortal.Plutus.Models
{
    public class RouteCountry
    {
        public int Id { get; set; }

        public Country Country { get; set; }

        public string Routename { get; set; }
        
    }
}