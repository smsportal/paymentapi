﻿using System.Collections.Generic;

namespace SmsPortal.Plutus.Models
{
    public interface IHtmlPaymentFormBuilder
    {
        string GetHtml(Payment payment);
        
        IDictionary<string, string> GetVariables(Payment payment);
    }
}
