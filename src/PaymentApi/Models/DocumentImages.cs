﻿using System.IO;

namespace SmsPortal.Plutus.Models
{
    public class DocumentImages
    {
        public string RootPath { get; set; }
        public string ImagesSubPath { get; set; }
        public string FontsSubPath { get; set; }
        public string LogoFilename { get; set; }

        public string ImagesPath => ImageFolder.Replace("\\", "/");
        
        private string ImageFolder =>
            string.IsNullOrEmpty(ImagesSubPath)
                ? RootPath
                : Path.Combine(RootPath, ImagesSubPath);
        public string FontsPath => FontsFolder.Replace("\\", "/");
        
        private string FontsFolder =>
            string.IsNullOrEmpty(FontsSubPath)
                ? RootPath
                : Path.Combine(RootPath, FontsSubPath);

        public string CompanyHeaderPath => Path.Combine(ImageFolder, LogoFilename);
    }
}
