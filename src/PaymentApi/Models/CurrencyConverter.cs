﻿using System;

namespace SmsPortal.Plutus.Models
{
	public class CurrencyConverter
	{
		public decimal FromRate { get; set; }
		public decimal ToRate { get; set; }

		public decimal Convert(decimal value)
		{
			return Math.Round(value / FromRate * ToRate, 2, MidpointRounding.AwayFromZero);
		}
	}
}
