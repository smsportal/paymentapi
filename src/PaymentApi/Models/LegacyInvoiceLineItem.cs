﻿namespace SmsPortal.Plutus.Models
{
	public class LegacyInvoiceLineItem
	{
		public int Quantity { get; set; }
		public decimal Price { get; set; }
		public string Description { get; set; }
		public decimal Total => Quantity * Price;
	}
}
