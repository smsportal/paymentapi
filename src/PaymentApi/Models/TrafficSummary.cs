using System;
using System.Security.Principal;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    public class TrafficSummary
    {
        public int AccountId { get; set; }
        
        public long Total { get; set; }

        public int NetworkId { get; set; }

        public string MessageType { get; set; }

        public string Source { get; set; }

        public string Campaign { get; set; }

        public string CostCentre { get; set; }
    }
}