﻿using System.IO;

namespace SmsPortal.Plutus.Models
{
    public class StripeSettings
    {
        public int SiteId { get; set; }
        public string ApiKey { get; set; }
        public string PublishableKey { get; set; }
        public string WebhookSecret { get; set; }
    }
}