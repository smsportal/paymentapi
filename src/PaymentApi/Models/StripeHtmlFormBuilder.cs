﻿using System;
using System.Collections.Generic;
using Stripe;

namespace SmsPortal.Plutus.Models
{
    public class StripeHtmlFormBuilder : HtmlPaymentFormBuilderBase
    {
        private readonly string templateFilePath;
        public StripeHtmlFormBuilder(StripeSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }
            if (string.IsNullOrEmpty(settings.ApiKey))
            {
                throw new ArgumentNullException(nameof(settings.ApiKey));
            }
            if (string.IsNullOrEmpty(settings.PublishableKey))
            {
                throw new ArgumentNullException(nameof(settings.PublishableKey));
            }
            ApiKey = settings.ApiKey;
            PublishableKey = settings.PublishableKey;
        }

        //NOTE: The URL provided here is not the actual payment gateway URL,
        //but the URL to the library used to submit the payment. 
        public override string PaymentGatewayUrl => "https://js.stripe.com/v3/"; 
        public override string SandboxGatewayUrl => PaymentGatewayUrl;
        
        public string ApiKey { get; set; }
        public string PublishableKey { get; set; }
        
        protected override void OnBuild(Payment payment)
        {
            try
            {
                if (payment == null)
                {
                    throw new PaymentGatewayException(
                        "Stripe",
                        new ArgumentNullException(nameof(payment)),
                        IssueSeverity.Fatal);
                }

                var amountInCents = Convert.ToInt64(payment.Total * 100);

                // See your keys here: https://dashboard.stripe.com/account/apikeys
                StripeConfiguration.ApiKey = UseSandbox ? "sk_test_4eC39HqLyjWDarjtT1zdp7dc" : ApiKey;
                var options = new PaymentIntentCreateOptions
                {
                    Amount = amountInCents,
                    Currency = payment.Currency,
                    PaymentMethodTypes = new List<string>()
                    {
                        "card"
                    },
                    Metadata = new Dictionary<string, string>
                    {
                        {"PaymentId", payment.TransactionId.ToString()},
                        {"InvoiceRefNr", payment.InvoiceRefNr ?? string.Empty},
                        {"AccountKey", payment.AccountKey.ToString()}
                    },
                    ReceiptEmail = payment.NotificationEmail
                };

                var service = new PaymentIntentService();
                var paymentIntent = service.Create(options);

                AddHiddenField("PublishableKey", UseSandbox ? "pk_test_TYooMQauvdEDq54NiTphI7jx" : PublishableKey);
                AddHiddenField("ClientSecret", paymentIntent.ClientSecret);
            }
            catch (PaymentGatewayException)
            {
                throw;
            }
            catch (StripeException e)
            {
                throw new PaymentGatewayException(payment?.Gateway ?? "Stripe", e, IssueSeverity.Warn);
            }
            catch (Exception e)
            {
                throw new PaymentGatewayException(payment?.Gateway ?? "Stripe", e);
            }
        }
    }
}