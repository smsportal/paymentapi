﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Principal;
using SmsPortal.Core;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Class that represents various configuration relating to payments for accounts.
    /// </summary>
    public class PaymentAccount
    {
        private readonly IDictionary<int, CreditPool> creditPools = new Dictionary<int, CreditPool>();

        /// <summary>
        /// The combined id and type of the account
        /// </summary>
        public AccountKey AccountKey { get; set; }

        /// <summary>
        /// The id of the account
        /// </summary>
        public int Id => AccountKey.Id;
        
        /// <summary>
        /// The type of the account
        /// </summary>
        public AccountType AccountType => AccountKey.Type;

        /// <summary>
        /// Which site the account belongs to.
        /// </summary>
        public int SiteId { get; set; }

        /// <summary>
        /// The ID of the <c>PriceList</c> that should be used when billing this account.
        /// </summary>
        public int? PriceListId { get; set; }

        /// <summary>
        /// The currency that the account makes payments in.
        /// </summary>
        public Currency BaseCurrency { get; set; }

        /// <summary>
        /// The username associated with the account.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The first and last name of the owner of the account. Often used in emails.
        /// </summary>
        public string Fullname { get; set; }

        /// <summary>
        /// The email address where billing documents such as invoices should be sent to. Either the billing details or as a last resort the email
        /// address provided by the user during sign up.
        /// </summary>
        public string EmailTo { get; set; }

        /// <summary>
        /// The code used by Pastel to identify this account.
        /// </summary>
        public string ClientRefCode { get; set; }

        /// <summary>
        /// The id of Billing Group for the account if it has one
        /// </summary>
        public int? BillingGroupId { get; set; }

        /// <summary>
        /// If payments made by this account should use live payment gateways or sandbox environments.
        /// </summary>
        public bool GatewaysLive { get; set; }

        /// <summary>
        /// The name of the company for this account.
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Flag indicating whether credit card payments require authorization for this account.
        /// </summary>
        public bool CreditCardAuthRequired { get; set; }

        /// <summary>
        /// The 3 letter currency code indicating the currency if the account is set to use a fixed price and not a scaled price.
        /// </summary>
        public string FixedPricingCurrency { get; set; }

        /// <summary>
        /// Whether the billing period is monthly or early.
        /// </summary>
        public BillingPeriod BillingPeriod { get; set; }

        /// <summary>
        /// The ID of the country where the accounts sends SMS traffic.
        /// </summary>
        public int RouteCountryId { get; set; }

        /// <summary>
        /// The 3 letter currency code indicating the currency for short code pricing.
        /// </summary>
        public string PricingSCCurrency { get; set; }

        /// <summary>
        /// The price the account pays for the setup of the short code.
        /// </summary>
        public decimal PricingSCSetup { get; set; }

        /// <summary>
        /// The price the account pays the monthly rental of a short code.
        /// </summary>
        public decimal PricingSCMonthly { get; set; }

        /// <summary>
        /// The price the account pays the monthly rental of a shared short code (they use a keyword on the short code0
        /// </summary>
        public decimal PricingSCShared { get; set; }

        /// <summary>
        /// The email address to use when notifying a user of a short code that is due to expire.
        /// </summary>
        public string ShortCodeExpiryEmailAddress { get; set; }

        /// <summary>
        /// Flag indicating whether the account is postpaid. Postpaid accounts send SMS traffic and pay for it at the end of the month.
        /// </summary>
        public bool IsPostpaid { get; set; }

        /// <summary>
        /// Legacy field. Country Of Residence should be available on <c>BillingDetails</c>. Not all resellers use automated
        /// invoicing and hence don't require a full billing details, but they do require Country Of Residence for tax
        /// purposes.
        /// </summary>
        public int CountryOfResidence { get; set; }
        
        /// <summary>
        /// The requested line item breakdown when generating post-paid invoices.
        /// </summary>
        public LineItemBreakdownType PostPaidBillingType { get; set; }
        
        /// <summary>
        /// The requested line item breakdown when generating shortcode invoices.
        /// </summary>
        public LineItemBreakdownType ShortCodeBillingType { get; set; }
        
        /// <summary>
        /// A descriptive name for the (original) request account.
        /// Please note that this may refer to a different account than the payment account,
        /// for example the name of the sub-account used when requesting a payment account. 
        /// </summary>
        public string AccountLinkName { get; set; }

        /// <summary>
        /// Set the <see cref="PaymentAccount.AccountLinkName"/> based on the account details provided.
        /// </summary>
        /// <param name="account">The linked account.</param>
        public void SetAccountLink(PaymentAccount account)
        {
            AccountLinkName =
                !string.IsNullOrEmpty(account.AccountLinkName)
                    ? account.AccountLinkName
                    : account.Username;
        }
        
        /// <summary>
        /// Set the shortcode pricing based on the account details provided.
        /// </summary>
        /// <param name="account">The linked account.</param>
        public void SetShortCodePricing(PaymentAccount account)
        {
            PricingSCCurrency = 
                string.IsNullOrEmpty(account.PricingSCCurrency) || account.PricingSCCurrency == "-1" 
                ? account.PricingSCCurrency
                : PricingSCCurrency;
            PricingSCSetup = 
                account.PricingSCSetup != 0 
                    ? account.PricingSCSetup 
                    : PricingSCSetup;
            PricingSCMonthly = 
                account.PricingSCMonthly != 0 
                    ? account.PricingSCMonthly 
                    : PricingSCMonthly;
            PricingSCShared = 
                account.PricingSCShared != 0 
                    ? account.PricingSCShared 
                    : PricingSCShared;
        }

        public int GetCreditBalance(int routeCountryId)
        {
            return creditPools[routeCountryId].Balance;
        }

        public CreditPool GetCreditPool(int routeCountryId)
        {
            return creditPools[routeCountryId];
        }

        public void AddCreditPool(CreditPool creditPool)
        {
            creditPools[creditPool.RouteCountryId] = creditPool;
        }

        private const int SmsPortalSiteId = 1;

        public int BuysFromSite
        {
            get
            {
                switch (AccountType)
                {
                    case AccountType.ResellerAccount:
                        return SmsPortalSiteId;
                    case AccountType.SmsAccount:
                        return SiteId;
                    default:
                        throw new InvalidOperationException(
                            $"Could not determine the site {AccountKey} buys from.");
                }
            }
        }
        public override string ToString()
        {
            return $"[PaymentAccount {AccountKey}: " +
                   $"Username={Username}, " +
                   $"ClientRefCode={ClientRefCode}, " +
                   $"SiteId={SiteId}, " +
                   $"BuysFromSite={BuysFromSite}]";
        }

        public static PaymentAccount GetTemporary(AccountKey accountKey)
        {
            return new PaymentAccount
            {
                AccountKey = accountKey,
                SiteId = 1,
                PriceListId = null,
                BaseCurrency = Currency.BaseCurrency,
                Username = "Unknown",
                Fullname = $"Unknown account {accountKey}",
                EmailTo = "accounts@smsportal.com",
                ClientRefCode = "BROKEN",
                Company = $"Unknown account {accountKey}"
            };
        }
    }
}
