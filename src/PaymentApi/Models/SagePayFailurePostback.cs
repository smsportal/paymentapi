﻿using System.Text;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Data that is returned by SagePay during an decline transaction postback.
    /// </summary>
	public class SagePayFailurePostback
	{
	    /// <summary>
	    /// True if the transaction was successful; otherwise false.
	    /// </summary>
		public bool TransactionAccepted { get; set; }

	    /// <summary>
	    /// The original IP address the request was made from.
	    /// </summary>
		public string CardHolderIpAddr { get; set; }

	    /// <summary>
	    /// Unique trace ID for the transaction.
	    /// </summary>
        public string RequestTrace { get; set; }

	    /// <summary>
	    /// The transaction ID sent to SagePay.
	    /// </summary>
		public string Reference { get; set; }

	    /// <summary>
	    /// An echo back of the m4 field. See <c>SagePayHtmlFromBuilder</c>
	    /// </summary>
		public string Extra1 { get; set; }

	    /// <summary>
	    /// An echo back of the m5 field. See <c>SagePayHtmlFromBuilder</c>
	    /// </summary>
        public string Extra2 { get; set; }

	    /// <summary>
	    /// An echo back of the m6 field. See <c>SagePayHtmlFromBuilder</c>
	    /// </summary>
        public string Extra3 { get; set; }

	    /// <summary>
	    /// The amount of the transaction.
	    /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// The reason that the transaction was declined.
        /// </summary>
		public string Reason { get; set; }
        
        public string ToDebugString()
        {
	        var sb = new StringBuilder();
	        sb.AppendLine("[SagePay Failure Notification:");
	        sb.AppendLine($"\tTransactionAccepted={TransactionAccepted}");
	        sb.AppendLine($"\tCardHolderIpAddr={CardHolderIpAddr}");
	        sb.AppendLine($"\tRequestTrace={RequestTrace}");
	        sb.AppendLine($"\tReference={Reference}");
	        sb.AppendLine($"\tExtra1={Extra1}");
	        sb.AppendLine($"\tExtra2={Extra2}");
	        sb.AppendLine($"\tExtra3={Extra3}");
	        sb.AppendLine($"\tAmount={Amount}");
	        sb.AppendLine($"\tReason={Reason}");
	        sb.AppendLine("]");
	        return sb.ToString();
        }
	}
}
