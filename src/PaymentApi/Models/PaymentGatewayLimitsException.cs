﻿using System;

namespace SmsPortal.Plutus.Models
{
    public class PaymentGatewayLimitsException : PaymentGatewayException
    {
        /// <summary>
        /// The minimum credits a payment should be in order to use this gateway.
        /// </summary>
        public int MinTransaction { get; }

        /// <summary>
        /// The maximum credits a payment should be in order to use this gateway.
        /// </summary>
        public int MaxTransaction { get; }
        
        public PaymentGatewayLimitsException(
            string gateway, int minTransaction, int maxTransaction, string type = "credits",
            Exception innerException = null,
            IssueSeverity issueSeverity = IssueSeverity.Info) 
            : base(
                gateway,
                innerException ?? new InvalidOperationException($"{gateway} supports {type} between {minTransaction} and {maxTransaction}"),
                issueSeverity)
        {
            MinTransaction = minTransaction;
            MaxTransaction = maxTransaction;
        }
    }
}