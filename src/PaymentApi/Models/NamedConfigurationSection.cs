﻿using System;
using System.Configuration;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Named configuration section item
    /// </summary>
    /// <example>
    ///     <item Name="nameditem" setting1="" setting2="" setting3=""/>
    /// </example>
    public class NamedConfigurationSection : ConfigurationSection
    {
        /// <summary>
        /// The unique name of the configuration item
        /// </summary>
        [ConfigurationProperty("Name")]
        public virtual string Name
        {
            get { return (string) this["Name"]; }
            set { this["Name"] = value; }
        }

        /// <summary>
        /// A containing section for a configuration items
        /// </summary>
        /// <typeparam name="T">The type of named configuration section item</typeparam>
        /// <example>
        ///     <container>
        ///         <item Name="nameditem1" setting1="" setting2="" setting3=""/>
        ///         <item Name="nameditem2" setting1="" setting2="" setting3=""/>
        ///         <item Name="nameditem3" setting1="" setting2="" setting3=""/>
        ///     </container>
        /// </example>
        public class Container<T> : ConfigurationElementCollection where T : NamedConfigurationSection
        {
            public new T this[string name]
            {
                get { return (T)BaseGet(name); }
            }

            public T this[int index]
            {
                get { return (T)BaseGet(index); }
            }

            protected override ConfigurationElement CreateNewElement()
            {
                return Activator.CreateInstance<T>();
            }

            protected override object GetElementKey(ConfigurationElement element)
            {
                return ((T)element).Name;
            }
        }
    }
}