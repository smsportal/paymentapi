﻿using System;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Exception that is thrown when an entity for a given ID cannot be located in the system.
    /// </summary>
    public class EntityNotFoundException: Exception
    {
        /// <summary>
        /// Creates an initializes a new <c>EntityNotFoundException</c> object with the given entity type and ID.
        /// </summary>
        /// <param name="entityType">The type of entity that could not be found.</param>
        /// <param name="id">The ID of the entity.</param>
        public EntityNotFoundException(Type entityType, int id): base($"A {entityType} with ID {id} could not be found")
        { }

        /// <summary>
        /// Creates an initializes a new <c>EntityNotFoundException</c> object with the given entity type and ID.
        /// </summary>
        /// <param name="entityType">The type of entity that could not be found.</param>
        /// <param name="id">The ID of the entity.</param>
        public EntityNotFoundException(Type entityType, long id) : base($"A {entityType} with ID {id} could not be found")
        { }

        /// <summary>
        /// Creates an initializes a new <c>EntityNotFoundException</c> object with the given entity type and ID.
        /// </summary>
        /// <param name="entityType">The type of entity that could not be found.</param>
        /// <param name="id">The ID of the entity.</param>
        public EntityNotFoundException(Type entityType, string id) : base($"A {entityType} with ID {id} could not be found")
        { }
    }
}
