namespace SmsPortal.Plutus.Models
{
    public class SagePaySettings
    {
        public string PaymentGatewayUrl { get; set; }
        
        public int ResellerId { get; set; }
        
        public string ServiceKey { get; set; }

        public string VendorKey { get; set; }

        public int Min { get; set; }

        public int Max { get; set; }
       
    }
}