﻿namespace SmsPortal.Plutus.Models
{
	public class LegacyEmail
	{
		public string Subject { get; set; }
		public string Body { get; set; }
		public string Recipient { get; set; }
	}
}
