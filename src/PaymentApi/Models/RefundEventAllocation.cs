using System.Security.Principal;

namespace SmsPortal.Plutus.Models
{
    public class RefundEventAllocation
    {
        public AccountKey AccountKey { get; set; }

        public int RouteCountryId { get; set; }

        public int Credits { get; set; }

        public string Currency { get; set; }
    }
}