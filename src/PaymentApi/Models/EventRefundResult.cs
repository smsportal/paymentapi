using System;

namespace SmsPortal.Plutus.Models
{
    public class EventRefundResult
    {
        public DateTime Created;
        public DateTime Scheduled;
        public int RefundMessageCount;
        public decimal RefundMessageValue;
        public string ExampleMessage;
    }
}