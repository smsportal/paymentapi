﻿using System;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
	public class ShortCodeBillingCycle : BillingCycle
	{
        public ShortCodeBillingCycle()
        {
            LineItemType = InvoiceLineItemType.ShortCodeRental;
        }

        public override LineItemBreakdownType LineItemBreakdown
        {
            get
            {
                if (SCApplicationType == ShortCodeApplicationType.NewShortCodeApplication || SCApplicationType == ShortCodeApplicationType.NewKeywordApplication)
                {
                    return LineItemBreakdownType.PerMasterAccountByUsername;
                }
                if (SCApplicationType == ShortCodeApplicationType.None)
                {
                    return LineItemBreakdownType.Unknown;
                }
                if (SCApplicationType == ShortCodeApplicationType.PerRefCode)
                {
                    return LineItemBreakdownType.PerRefCodeByMasterAccountUsername;
                }
                if (SCApplicationType == ShortCodeApplicationType.PerUser)
                {
                    return LineItemBreakdownType.PerMasterAccountByUsername;
                }
                return base.LineItemBreakdown;
            }
            set => base.LineItemBreakdown = value;
        }

        public override InvoiceLineItemType LineItemType
        {
            get
            {
                if(SCApplicationType == ShortCodeApplicationType.NewShortCodeApplication || SCApplicationType == ShortCodeApplicationType.NewKeywordApplication)
                {
                    return InvoiceLineItemType.ShortCodeSetup;
                }
                if(base.LineItemType == InvoiceLineItemType.ShortCodeKeywordRental)
                {
                    return InvoiceLineItemType.ShortCodeKeywordRental;
                }
                return InvoiceLineItemType.ShortCodeRental;
            }
            set => base.LineItemType = value;
        }

        public long ShortCode { get; set; }
		public string Keyword { get; set; }
		public DateTime ScExpiryDate { get; set; }
		public ShortCodeApplicationType SCApplicationType { get; set; }
		public bool SCIsdedicated { get; set; }
	}
}
