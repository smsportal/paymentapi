﻿using System;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    public class EftAuthorization
    {
        public int SiteId { get; set; }

        public AccountKey AccountKey { get; set; }
        
        public int AccountId => AccountKey.Id;
        
        public AccountType AccountType => AccountKey.Type;
        
        public int RouteCountryId { get; set; }

        public decimal Total { get; set; }

        public int Credits { get; set; }

        public Guid TransactionId { get; set; }

        public string Currency { get; set; }

        public decimal Vat { get; set; }

        public decimal BaseCurrencyCost { get; set; }

        public string BaseCurrency { get; set; }

        public decimal BaseVat { get; set; }

        public decimal TransactionFee { get; set; }

        public decimal BaseTransactionFee { get; set; }

    }
}
