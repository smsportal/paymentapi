﻿namespace SmsPortal.Plutus.Models
{
	public class DocumentLineItemModel
	{
		public int Quantity { get; set; }
		public decimal Price { get; set; }
		public string Description { get; set; }
		public decimal Total { get; set; }
        public string OptionalNote { get; set; }
    }
}
