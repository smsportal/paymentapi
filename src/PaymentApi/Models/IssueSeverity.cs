﻿namespace SmsPortal.Plutus.Models
{
    public enum IssueSeverity
    {
        Info,
        Warn,
        Error,
        Fatal
    };
}