﻿using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
	public class InvoiceModel : BillingDocument
	{
	    public InvoiceModel()
	    {
	        LineItems = new List<DocumentLineItemModel>();
	    }

		public int InvoiceId { get; set; }
		public string ReferenceNumber { get; set; }
		public string AccountName { get; set; }
		public string OrderNumber { get; set; }
		public string ClientRefCode { get; set; }
		public string CustomerRefCode { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime DueDate { get; set; }
		public decimal TaxValue { get; set; }
		public decimal TaxPercentage { get; set; }
		public decimal Total { get; set; }
		public decimal SubTotal { get; set; }
		public string Currency { get; set; }
		public string ConversionRate { get; set; }
		public InvoiceStatus Status { get; set; }
		public BillingDetails ClientBillingDetails { get; set; }

		public List<DocumentLineItemModel> LineItems { get; set; }

        public byte[] InvoiceBytes { get; set; }

        public string TransactionId { get; set; }

        // TODO: Remove this. It is used by the LegacyInvoiceRepository during email?
        public BillingEmailInfo InvoiceEmail { get; set; }

        public bool IsPostPaid { get; set; }
        
        public bool ShowBillingPeriod { get; set; }

		public DateTime PeriodBilledFromDate { get; set; }

		public DateTime PeriodBilledToDate { get; set; }

        public string PeriodBilledDescription { get; set; } = "";

        public InvoiceType InvoiceType { get; set; }

		public bool IsInternational { get; set; }
	}
}
