﻿namespace SmsPortal.Plutus.Models.Enums
{
	public enum ShortCodeApplicationType
	{
		None = -1,
		PerRefCode = 0,
		PerUser = 1,
		NewKeywordApplication = 2,
		NewShortCodeApplication = 3
	}
}
