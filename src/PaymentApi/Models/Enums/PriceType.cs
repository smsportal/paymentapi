﻿using SmsPortal.Core;

namespace SmsPortal.Plutus.Models.Enums
{
    /// <summary>
    /// The type of pricing to use for billing groups 
    /// </summary>
    public enum PriceType
    {
        [Key(Model.Dto.Enums.PriceType.Scale, "Scale pricing")]
        Scale = 0,
        [Key(Model.Dto.Enums.PriceType.Fixed, "Fixed pricing")]
        Fixed = 1,
        [Key(Model.Dto.Enums.PriceType.PriceList, "Price list")]
        PriceList = 2
    }
}
