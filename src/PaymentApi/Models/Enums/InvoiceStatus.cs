﻿namespace SmsPortal.Plutus.Models.Enums
{
	public enum InvoiceStatus
	{
		Unpaid = -1,
		UnpaidEft = 0,
		Paid = 1,
		Cancelled = 2,
		Expired = 3,
		UnpaidSagePay = 4,
		MonthlyBilling = 5,
		Authorization = 6,
		SCMonthly = 7,
		SCUnpaid = 8,
		SCPaid = 9
	}
}
