using SmsPortal.Core;

namespace SmsPortal.Plutus.Models.Enums
 {
     public enum PostPaidBillingCycleFilterType
     {
         [Key(key: Model.Dto.Enums.PostPaidBillingCycleFilterType.BulkSmsUsage)]
         BulkSmsUsage,
         [Key(key: Model.Dto.Enums.PostPaidBillingCycleFilterType.ReverseBilledReplies)]
         ReverseBilledReplies,
         [Key(key: Model.Dto.Enums.PostPaidBillingCycleFilterType.ReverseBilledShortCodeMessages)]
         ReverseBilledShortCodeMessages,
         [Key(key: Model.Dto.Enums.PostPaidBillingCycleFilterType.DedicatedShortCodeRental)]
         DedicatedShortCodeRental,
         [Key(key: Model.Dto.Enums.PostPaidBillingCycleFilterType.SharedShortCodeKeywordRental)]
         SharedShortCodeKeywordRental,
         [Key(key: "AllShortCodeRental", description: "All shortcode rentals (both dedicated and keywords)")]
         AllShortCodeRental
     }
 }