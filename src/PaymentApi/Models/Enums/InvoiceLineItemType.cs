﻿
using SmsPortal.Core;

namespace SmsPortal.Plutus.Models.Enums
{
    public enum InvoiceLineItemType
    {
        [Key(Model.Dto.Enums.InvoiceType.BulkSms, "SMS Usage")]
        BulkSms = 0,
        [Key(Model.Dto.Enums.InvoiceType.ReverseBilledReplySms, "RevBill Replies")]
        ReverseBilledReplySms = 1,
        [Key(Model.Dto.Enums.InvoiceType.ReverseBilledShortCodeSms, "RevBill Short Code")]
        ReverseBilledShortCodeSms = 2,
        [Key(Model.Dto.Enums.InvoiceType.ShortCodeSetup, "Short Code")]
        ShortCodeSetup = 3,
        [Key(Model.Dto.Enums.InvoiceType.ShortCodeKeywordRental, "Short Code")]
        ShortCodeKeywordRental = 4,
        [Key(Model.Dto.Enums.InvoiceType.ShortCodeRental, "Short Code")]
        ShortCodeRental = 5,
        //RESERVED VALUE IS NOT AN INVOICE TYPE
        TransactionFee = 6
    }
}
