﻿namespace SmsPortal.Plutus.Models.Enums
{
	public enum BillingPeriod
	{
		None = -1,
		Monthly = 0,
		Early = 1
	}
}
