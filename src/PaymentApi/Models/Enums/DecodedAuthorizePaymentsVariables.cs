﻿namespace SmsPortal.Plutus.Models.Enums
{
	public enum DecodedAuthorizePaymentsVariables
	{
		AdminUsername = 0,
		AdminPassword = 1,
		Total = 2,
		VatAmount = 3,
		Credits = 4,
		PurchaseAccLoginId = 5,
		CreatedDate = 6,
		DueDate = 7,
		Currency = 8,
		TransactionFee = 9,
		TransactionId = 10,
		Paymentmethod = 11,
		AccountType = 12,
		PaymentReference = 13
	}
}
