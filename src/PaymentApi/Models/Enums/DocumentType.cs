﻿namespace SmsPortal.Plutus.Models.Enums
{
	public enum DocumentType
	{
		Invoice = 0,
		CostEstimate = 1,
		Quote = 2
	}
}