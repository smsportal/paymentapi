﻿namespace SmsPortal.Plutus.Models.Enums
{
	public enum PastelRefCodesIndex
	{
		ZeroVat = 0,
		Vat = 1,
		ZeroVatZar = 2,
		ZeroVatEur = 3,
		ZeroVatGbp = 4,
		ZeroVatUsd = 5
	}
}
