
using SmsPortal.Core;

namespace SmsPortal.Plutus.Models.Enums
{
    public enum LineItemBreakdownType
    {
        [Key(Model.Dto.Enums.BillingType.Manual, "Manual")]
        Unknown = -1,
        [Key(Model.Dto.Enums.BillingType.PerRefCode, "Per Ref Code")]
        PerRefCodeByMasterAccountUsername = 0,
        [Key(Model.Dto.Enums.BillingType.PerUser, "Per User")]
        PerMasterAccountByUsername = 1,
        [Key(Model.Dto.Enums.BillingType.Smpp, "SMPP")]
        PerMasterAccountByNetworkName = 2,
        [Key(Model.Dto.Enums.BillingType.PerCampaign, "Per Campaign")]
        PerMasterAccountByCampaignName = 3,
        [Key(Model.Dto.Enums.BillingType.PerCostCentre, "Per Cost Centre")]
        PerMasterAccountByCostCentre = 4,
        [Key(Model.Dto.Enums.BillingType.PerCostCentrePerCampaign, "Per Campaign Per Cost Centre")]
        PerCostCentrePerCampaignByCountryAndMessageType = 5
    }
}
