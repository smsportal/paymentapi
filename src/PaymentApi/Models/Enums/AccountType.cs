using SmsPortal.Core;

namespace SmsPortal.Plutus.Models.Enums
{
    public enum AccountType
    {
        [Key(Model.Dto.Enums.AccountType.SmsAccount, description: "Customer")]
        SmsAccount = 0,
        [Key(Model.Dto.Enums.AccountType.ResellerAccount, description: "Reseller")]
        ResellerAccount,
        [Key(Model.Dto.Enums.AccountType.PastelCode, description: "Billing Group")]
        PastelCode,
        [Key("SiteId", description: "Reseller Site by SiteId")]
        SiteId
    }
}