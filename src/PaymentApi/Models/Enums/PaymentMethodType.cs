﻿using SmsPortal.Core;

namespace SmsPortal.Plutus.Models.Enums
{
    public enum PaymentMethodType
    {
        [Key(Model.Dto.Enums.PaymentGatewayType.Eft, "Electronic Fund Transfer")]
        Eft = PaymentGatewayType.Eft,
        [Key(Model.Dto.Enums.PaymentMethodType.CreditCard, "Credit Card")]
        CreditCard,
        [Key(Model.Dto.Enums.PaymentMethodType.PayPal, "https://paypal.com/")]
        PayPal,
        [Key(Model.Dto.Enums.PaymentMethodType.SagePay, "https://netcash.co.za/")]
        SagePay,
    }
}