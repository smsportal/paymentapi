namespace SmsPortal.Plutus.Models.Enums
{
    public enum SmsType
    {
        StandardRateMobileTerminating,
        ReverseBillMobileTerminating,
        ReverseBillMobileOriginating
    }
}