﻿namespace SmsPortal.Plutus.Models.Enums
{
	public enum InvoiceType
	{
		None = 0,
		PostPaid = 1,
		RevBilled = 2,
		ShortCodeRentals = 3,
		ShortCodeKeyword = 4,
		RevBilledShortCode = 5
	}
}