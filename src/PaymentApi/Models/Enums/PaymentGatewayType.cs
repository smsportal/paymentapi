﻿using System;
using SmsPortal.Core;

namespace SmsPortal.Plutus.Models.Enums
{
    public enum PaymentGatewayType
    {
        [Key(Model.Dto.Enums.PaymentGatewayType.Eft, "Electronic Fund Transfer")]
        Eft = 0,

        [Key(Model.Dto.Enums.PaymentGatewayType.PayPal, "https://paypal.com/")]
        PayPal = 10,

        [Key(Model.Dto.Enums.PaymentGatewayType.NetCash, "https://netcash.co.za/")]
        NetCash = 20,
        [Key(Model.Dto.Enums.PaymentGatewayType.SagePayEft, "https://netcash.co.za/")]
        NetCashInstantEft,
        
        [Key(Model.Dto.Enums.PaymentGatewayType.Stripe, "https://stripe.com/")]
        Stripe = 30
    }
}