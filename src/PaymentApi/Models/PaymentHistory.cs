using System;
using SmsPortal.Plutus.v2.Model.Dto;

namespace SmsPortal.Plutus.Models
{
    public class PaymentHistory
    {
        public string TransactionId { get; set; }
        public int? InvoiceId { get; set; }
        public string InvoiceNr { get; set; }
        public string Currency { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Total { get; set; }
        public string Description { get; set; }
        public string Gateway { get; set; }
        public string GatewayTransactionId { get; set; }
        public string Status { get; set; }
        public DateTime Date { get; set; }
        public int? RouteCountryId { get; set; }
        public int? Credits { get; set; }
        
        /// <summary>
        /// The total number of payment history items returned by the database query
        /// </summary>
        public int TotalNumberOfResults { get; set; }
    }
}