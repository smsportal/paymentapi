using System.Security.Principal;

namespace SmsPortal.Plutus.Models
{
    public class CreditPool
    {
        public int RouteCountryId { get; set; }

        public int Balance { get; set; }

        public string Country { get; set; }
    }
}