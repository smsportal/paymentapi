﻿using System;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Class that represent settings for how a payment gateway can be used.
    /// </summary>
    public class PaymentGatewaySettings
    {
        private readonly IPaymentRepository paymentRepo;
        private readonly ICurrencyRepository currencyRepo;

        /// <summary>
        /// Creates and intializes a new instance of the <c>PaymentGateSettings</c>.
        /// </summary>
        /// <param name="gatewayCurrency">The <c>Currency</c> that the PaymentGateway accepts. Any payment requests in a different
        /// currency will first be converted to the gateway currency.</param>
        /// <param name="paymentRepo">Provides data access for <c>Payment</c> objects.</param>
        public PaymentGatewaySettings(
            Currency gatewayCurrency, 
            IPaymentRepository paymentRepo,
            ICurrencyRepository currencyRepo)
        {
            this.paymentRepo = paymentRepo;
            this.currencyRepo = currencyRepo;
            GatewayCurrency = gatewayCurrency;
        }

        /// <summary>
        /// 
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Either PayPal, SagePay, MyGate or EFT
        /// </summary>
        public string Gateway { get; set; }

        /// <summary>
        /// The currency that the payment gateway will accept payments in.
        /// </summary>
        public Currency GatewayCurrency { get; set; }

        /// <summary>
        /// True if the system is allowed to do payments to the gateway; otherwise false.
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// The minimum credits a payment should be in order to use this gateway.
        /// </summary>
        public int MinTransaction { get; set; }

        /// <summary>
        /// The maximum credits a payment should be in order to use this gateway.
        /// </summary>
        public int MaxTransaction { get; set; }

        /// <summary>
        /// An option fee that can be added in order to use this gateway.
        /// This fee is quoted as a percentage of the sub-total excluding tax.
        /// </summary>
        public decimal TranFee { get; set; }

        public override string ToString()
        {
            return $"[PaymentGatewaySettings {Gateway}: Currency={GatewayCurrency}]";
        }

        /// <summary>
        /// Creates a <c>Payment</c> object using the settings of PaymentGateway.
        /// </summary>
        /// <param name="invoice">The invoice to create a payment for.</param>
        /// <param name="credits">The number of credits being purchased.</param>
        /// <returns></returns>
        /// <remarks>Please note that the method may add another line item to the Invoice object if the gateway has a transaction
        /// fee associated with it.</remarks>
        public Payment CreatePayment(Invoice invoice, int credits)
        {
            //TODO: Really don't like this. But old code checks min credit purchase. We can only change when we don't
            // pull the same settings from the DB
            if (!IsWithinLimits(credits))
            {
                throw new PaymentGatewayLimitsException(Gateway, MinTransaction, MaxTransaction);
            }

            var payment = new Payment();
            if (TranFee > 0)
            {
                payment.Fee = (invoice.SubTotal * TranFee / 100);
                invoice.AddLineItem(new InvoiceLineItem { Code = InvoiceLineItemType.TransactionFee, Description = "Transaction Fee", Quantity = 1, UnitPrice = payment.Fee});
            }
            
            if (!GatewayCurrency.Code.Equals(invoice.Currency))
            {
                var invoiceCurrency = currencyRepo.GetCurrency(invoice.Currency);
                payment.Total = invoiceCurrency.Convert(GatewayCurrency, invoice.Total);
                payment.TaxValue = invoiceCurrency.Convert(GatewayCurrency, invoice.TaxAmount);
            }
            else
            {
                payment.Total = invoice.Total;
                payment.TaxValue = invoice.TaxAmount;
            }
            payment.Gateway = Gateway;
            payment.Started();
            payment.Currency = GatewayCurrency.Code;
            payment.Credits = credits;
            payment.TransactionId = Guid.NewGuid();
            payment.Created = DateTime.Now;
            payment.AccountKey = invoice.AccountKey;
            
            paymentRepo.Add(payment);
            return payment;
        }

        public bool IsWithinLimits(int quantity)
        {
            return (MinTransaction <= quantity && quantity <= MaxTransaction);
        }
    }
}
