﻿using System;

namespace SmsPortal.Plutus.Models
{
    public class SagePayHtmlFormBuilder : HtmlPaymentFormBuilderBase
    {
        public override string PaymentGatewayUrl { get; }

        public override string SandboxGatewayUrl => PaymentGatewayUrl;

        public string VendorKey { get; set; }

        public string ServiceKey { get; set; }

        public SagePayHtmlFormBuilder(string vendorKey, string serviceKey, string paymentGatewayUrl)
        {
            if (string.IsNullOrEmpty(vendorKey))
            {
                throw new ArgumentNullException(nameof(vendorKey));
            }
            VendorKey = vendorKey;

            if (string.IsNullOrEmpty(serviceKey))
            {
                throw new ArgumentNullException(nameof(serviceKey));
            }
            ServiceKey = serviceKey;
            
            if (string.IsNullOrEmpty(paymentGatewayUrl))
            {
                throw new ArgumentNullException(nameof(paymentGatewayUrl));
            }
            PaymentGatewayUrl = paymentGatewayUrl;
        }
        
        protected override void OnBuild(Payment payment)
        {
            try
            {
                if (payment == null)
                {
                    throw new PaymentGatewayException(
                        "SagePay",
                        new ArgumentNullException(nameof(payment)),
                        IssueSeverity.Fatal);
                }
                AddHiddenField("m1", ServiceKey);
                AddHiddenField("m2", VendorKey);
                if (payment.Invoice != null)
                {
                    AddHiddenField("p2", payment.InvoiceRefNr);    
                }
                else
                {
                    AddHiddenField("p2", payment.TransactionId.ToString());    
                }
                
                AddHiddenField("p3", payment.Description);
                AddHiddenField("p4", payment.Total.ToString("0.00"));
                AddHiddenField("Budget", "N");
                if (payment.Invoice != null)
                {
                    AddHiddenField("m4", payment.InvoiceRefNr);    
                }
                
                if (!string.IsNullOrEmpty(payment.PastelCode))
                {
                    AddHiddenField("m5", $"C|0|{payment.PastelCode}");    
                }
                AddHiddenField("m6", payment.TransactionId.ToString());
                AddHiddenField("m9", payment.NotificationEmail);
            }
            catch (PaymentGatewayException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new PaymentGatewayException(payment?.Gateway ?? "NetCash", e);
            }
        }
    }
}
