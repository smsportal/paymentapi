using System.Collections.Generic;
using Org.BouncyCastle.Security;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    public class PriceList
    {
        private readonly IDictionary<string, Product> products = new Dictionary<string, Product>();
        private static readonly IDictionary<SmsType, string> Prefixes = new Dictionary<SmsType, string>();

        static PriceList()
        {
            Prefixes.Add(SmsType.StandardRateMobileTerminating, "STD");
            Prefixes.Add(SmsType.ReverseBillMobileTerminating, "REV");
            Prefixes.Add(SmsType.ReverseBillMobileOriginating, "RMO");
        }

        public int Id { get; set; }
        
        public string Name { get; set; }

        public void Add(Product product)
        {
            products.Add(product.Code, product);
        }

        public decimal GetNetworkPrice(int networkId, SmsType type)
        {
            var key = GetKey(networkId, type);
            if (!products.ContainsKey(key))
            {
                throw new EntityNotFoundException(typeof(Product), key);
            }
            return products[key].Price;
        }

        public string GetProductDescription(int networkId, SmsType type)
        {
            return products[GetKey(networkId, type)].Description;
        }

        private string GetKey(int networkId, SmsType type)
        {
            var prefix = Prefixes[type];
            var suffix = networkId.ToString().PadLeft(4, '0');
            return prefix + suffix;
        }
    }
}