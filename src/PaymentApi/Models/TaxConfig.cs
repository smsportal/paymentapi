namespace SmsPortal.Plutus.Models
{
    public class TaxConfig
    {
        /// <summary>
        /// The unique ID assigned by the system to the country.
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// A description of the country.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The current taxation rate of the country.
        /// </summary>
        public decimal TaxPercentage { get; set; }

        /// <summary>
        /// A description of the tax type.
        /// </summary>
        public string TaxType { get; set; }

        /// <summary>
        /// The base tax configuration of the System (where the country is unknown);
        /// </summary>
        public static TaxConfig Unknown { get; }

        /// <summary>
        /// The base tax configuration of the System (for South Africa);
        /// </summary>
        public static TaxConfig SouthAfrica { get; }

        static TaxConfig()
        {
            SouthAfrica = new TaxConfig
            {
                CountryId = 156,
                TaxPercentage = 15,
                TaxType = "VAT",
                Description = "South Africa"
            };
            Unknown = new TaxConfig
            {
                CountryId = 0,
                TaxPercentage = 0,
                TaxType = "No Tax",
                Description = "Country Unknown"
            };
        }

        public override string ToString()
        {
            return $"[TaxConfig: TaxPercentage={TaxPercentage}% TaxType='{TaxType}']";
        }
    }
}
