﻿using SmsPortal.Core;
using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    public class BillingGroup
    {
        /// <summary>
        /// Unique identifier for the <c>BillingGroup</c>
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// The Pastel Code that is assigned to the <c>BillingGroup</c> in the Sage system. This code is unique unless
        /// the billing group belongs to low volume prepaid accounts then it is a cash account and will have a
        /// Pastel Code like 'CC005' or similar.
        /// </summary>
        public string PastelCode { get; set; }
        
        /// <summary>
        /// The holding company's ID
        /// </summary>
        public int? CompanyId { get; set; }
        
        /// <summary>
        /// The name of the company to be billed
        /// </summary>
        public string CompanyName { get; set; }
        
        /// <summary>
        /// The type of pricing to use: scale, fixed or price list
        /// </summary>
        public PriceType PriceType { get; set; }
        
        /// <summary>
        /// The price list to use when the billing group when using price lists
        /// </summary>
        public int? PriceListId { get; set; }

        /// <summary>
        /// If the account is set to use Fixed Pricing then Currency is populated otherwise null. 
        /// </summary>
        public Currency Currency { get; set; }
        
        /// <summary>
        /// The price for standard rate SMSs when using fixed pricing
        /// </summary>
        public decimal? PriceFixedStd { get; set; }
        
        /// <summary>
        /// The price for reversed billed messages when using fixed pricing  
        /// </summary>
        public decimal? PriceFixedRb { get; set; }
        
        /// <summary>
        /// The price for reversed billed replies when using fixed pricing
        /// </summary>
        public decimal? PriceFixedRbReply { get; set; }
        
        /// <summary>
        /// Billing address information
        /// </summary>
        public string Address1 { get; set; }
        
        /// <summary>
        /// Billing address information
        /// </summary>
        public string Address2 { get; set; }
        
        /// <summary>
        /// Billing address information
        /// </summary>
        public string City { get; set; }
        
        /// <summary>
        /// Billing address information
        /// </summary>
        public string StateProvince { get; set; }
        
        /// <summary>
        /// Billing address information
        /// </summary>
        public string ZipPostalCode { get; set; }
        
        /// <summary>
        /// Billing address information
        /// </summary>
        public int CountryId { get; set; }
        
        /// <summary>
        /// The name of the country of the CountryId
        /// </summary>
        public string Country { get; set; }
        
        /// <summary>
        /// The email to send the invoice to.
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// VAT number for customers in South Africa
        /// </summary>
        public string VatNr { get; set; }
        
        /// <summary>
        /// Company registration number
        /// </summary>
        public string RegistrationNr { get; set; }
        
        /// <summary>
        /// The granularity of the invoices and their line items for shared shortcode rental
        /// </summary>
        public LineItemBreakdownType ScSharedRentalBillingType { get; set; }
        
        /// <summary>
        /// The granularity of the invoices and their line items for dedicated shortcode rental
        /// </summary>
        public LineItemBreakdownType ScDedicatedRentalBillingType { get; set; }
        
        /// <summary>
        /// The granularity of the invoices and their line items for reverse billed SMSs
        /// sent to a shortcode
        /// </summary>
        public LineItemBreakdownType ScRevBilledRentalBillingType { get; set; }
        
        /// <summary>
        /// The granularity of the invoices and their line items for SMSs sent
        /// </summary>
        public LineItemBreakdownType MtBillingType { get; set; }
        
        /// <summary>
        /// The granularity of the invoices and their line items for SMSs received
        /// </summary>
        public LineItemBreakdownType MoBillingType { get; set; }
        
        /// <summary>
        /// Whether the group is post-paid
        /// </summary>
        public bool PostPaid { get; set; }
        
        /// <summary>
        /// Whether to charge for messages addressed to numbers for which we have no network to forward.
        /// </summary>
        public bool BillForNoNetworks { get; set; }

        public BillingDetails ToBillingDetails()
        {
            return new BillingDetails
            {
                CompanyName = CompanyName,
                Address1 = Address1,
                Address2 = Address2,
                City = City,
                StateProvince = StateProvince,
                Country = Country,
                CountryId = CountryId,
                ZipCode = ZipPostalCode,
                VatNumber = VatNr,
                RegistrationNumber = RegistrationNr,
                InvoiceEmail = Email,
            };
        }

    }
}