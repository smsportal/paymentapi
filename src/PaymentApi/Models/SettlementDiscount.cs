﻿using System;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// An optional discount on early settlement of an invoice.
    /// </summary>
    public class SettlementDiscount
    {
        /// <summary>
        /// A discount percentage on early settlement of an invoice.
        /// </summary>
        public decimal DiscountPercentage { get; set; }
        /// <summary>
        /// The maximum amount of days deemed to be early settlement.
        /// </summary>
        public int SettlementDays { get; set; }
        
        public InvoiceLineItem ToLineItem()
        {
            var percentage = DiscountPercentage.ToString("N4");
            while (percentage.IndexOf(".", StringComparison.Ordinal) >= 0 && percentage.EndsWith("0"))
            {
                percentage = percentage.Substring(0, percentage.Length - 1);
            }

            if (percentage.EndsWith("."))
            {
                percentage = percentage.Substring(0, percentage.Length - 1);
            }
            
            return new InvoiceLineItem
            {
                Code = InvoiceLineItemType.BulkSms,
                Quantity = 0,
                UnitPrice = 0,
                DataId = null,
                Description = string.Empty,
                OptionalNote = $"{percentage}% Settlement Discount applicable if invoice is settled within {SettlementDays} Days"
            };
        }

    }
}