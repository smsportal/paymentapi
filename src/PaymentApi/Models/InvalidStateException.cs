﻿using System;

namespace SmsPortal.Plutus.Models
{
    public class InvalidStateException : IssueSeverityException
    {
        public string FailedAction { get; }
        public object OriginalStatus { get; }
        public object RequestedSate { get; }
        
        public InvalidStateException(string failedAction, object originalStatus, object requestedSate, Exception innerException = null, IssueSeverity issueSeverity = IssueSeverity.Warn) 
            : base($"{failedAction}, cannot be changed from {originalStatus} to {requestedSate}", innerException, issueSeverity)
        {
            OriginalStatus = originalStatus;
            RequestedSate = requestedSate;
            FailedAction = failedAction;
        }
    }
}