﻿using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
	public class BillingCycle : PostPaidBillingCycle
	{
		public int AccountId { get; set; }
		public string ClientRefCode { get; set; }
        public string Comment { get; set; }
		public bool Closed { get; set; }
		public bool Approved { get; set; }
		public int InvoiceId { get; set; }
		public string InvoiceReference { get; set; }
		public int Credits { get; set; }
		public decimal SubTotal { get; set; }
		public decimal TaxValue { get; set; }
		public string Currency { get; set; }
		public InvoiceType InvoiceType { get; set; }
		public BillingEmailInfo BillingEmailInfo { get; set; }
	}
}
