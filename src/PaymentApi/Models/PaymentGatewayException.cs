﻿using System;

namespace SmsPortal.Plutus.Models
{
    public class PaymentGatewayException : IssueSeverityException
    {
        public string Gateway { get; }
        
        public PaymentGatewayException(string gateway, Exception innerException, IssueSeverity issueSeverity = IssueSeverity.Error)
            : this(gateway, innerException?.Message ?? $"{gateway} error.", innerException, issueSeverity)
        {
            Gateway = gateway;
        }
        
        public PaymentGatewayException(string gateway, string message, Exception innerException = null, IssueSeverity issueSeverity = IssueSeverity.Error)
            : base(message, innerException, issueSeverity)
        {
            Gateway = gateway;
        }
    }
}