using System;
using System.Collections.Generic;
using log4net;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Models
{
    public class PaymentGenerator: IPaymentGenerator
    {
        private readonly ILog log = LogManager.GetLogger(typeof(PaymentGenerator));
        private readonly IPaymentAccountRepository accRepo;
        private readonly ISiteRepository siteRepo;
        private readonly IBillingGroupOrDetailsRepository billingDetailsRepository;
        private readonly ITaxConfigRepository taxRepo;
        private readonly ICurrencyRepository currencyRepo;
        private readonly IHtmlPaymentFormBuilderFactory factory;
        private readonly IInvoiceRepository invoiceRepo;
        private readonly IRouteCountryRepository routeCountryRepo;

        public PaymentGenerator(
            IPaymentAccountRepository accRepo,
            ISiteRepository siteRepo,
            IBillingGroupOrDetailsRepository billingDetailsRepository,
            ITaxConfigRepository taxRepo,
            ICurrencyRepository currencyRepo,
            IHtmlPaymentFormBuilderFactory factory,
            IInvoiceRepository invoiceRepo,
            IRouteCountryRepository routeCountryRepo)
        {
            this.accRepo = accRepo;
            this.siteRepo = siteRepo;
            this.billingDetailsRepository = billingDetailsRepository;
            this.taxRepo = taxRepo;
            this.currencyRepo = currencyRepo;
            this.factory = factory;
            this.invoiceRepo = invoiceRepo;
            this.routeCountryRepo = routeCountryRepo;
        }

        public Payment Generate(
            AccountKey accountKey,
            int quantity,
            decimal unitPrice,
            string currency,
            string paymentGateway,
            string purchaseOrderNr,
            int routeCountryId)
        {
            var acc = accRepo.GetPaymentAccount(accountKey);
            BillingDetails accBilling;
            try
            {
                accBilling = billingDetailsRepository.Get(acc.AccountKey);
            }
            catch (EntityNotFoundException e)
            {
                accBilling = new BillingDetails();
                accBilling.CountryId = acc.CountryOfResidence;
            }

            var payment = new Payment();
            payment.TransactionId = Guid.NewGuid();
            
            log.Debug($"{paymentGateway} Payment Requested" +
                      $" Country={accBilling.Country} ({accBilling.CountryId})," +
                      $" RouteCountryId={routeCountryId}," +
                       " [Payment:" +
                      $" TransactionId={payment.TransactionId}, " +
                      $" Quantity={quantity:N}," +
                      $" UnitPrice={unitPrice:N4}," +
                      $" Currency={currency}," +
                      $" AccountKey={accountKey}]");
            
            payment.NotificationEmail = 
                !string.IsNullOrEmpty(accBilling.InvoiceEmail)
                ? accBilling.InvoiceEmail
                : acc.EmailTo;

            var site = siteRepo.Get(acc.BuysFromSite);

            var gateway = site.GetSettings(paymentGateway);
            var taxPercent = 0m;
            var taxAmount = 0m;
            var taxType = "NONE";
            if (gateway.IsWithinLimits(quantity))
            {
                var originalCurrency = currencyRepo.GetCurrency(currency);
                var invoiceCurrencyCode = SiteCurrency.SMSPortal.Code(accBilling.CountryId);
                var invoiceCurrency = currencyRepo.GetCurrency(invoiceCurrencyCode);
                if (site.UseAutomaticInvoicing && invoiceCurrency.Code != currency)
                {
                    unitPrice = originalCurrency.Convert(invoiceCurrency, unitPrice, 4);
                }

                var subTotal = Math.Round(quantity * unitPrice, 2, MidpointRounding.AwayFromZero);
                var tranFee = subTotal * gateway.TranFee / 100;
                var exTax = subTotal + tranFee;
                var resellerBilling = billingDetailsRepository.Get(site.AccountKey);
                var total = 0m;
                if (accBilling.CountryId == resellerBilling.CountryId && site.TaxEnabled)
                {
                    var tax = taxRepo.GetConfig(resellerBilling.CountryId);
                    taxPercent = tax.TaxPercentage;
                    taxType = tax.TaxType;
                    taxAmount = Math.Round( exTax * (tax.TaxPercentage / 100) , 2, MidpointRounding.AwayFromZero);
                    total = exTax + taxAmount;
                }
                else
                {
                    total = exTax;
                }

                if (site.UseAutomaticInvoicing)
                {
                    var invoice = new Invoice();


                    var rc = routeCountryRepo.Get(routeCountryId);
                    invoice.AddLineItem(new InvoiceLineItem {
                        Code = InvoiceLineItemType.BulkSms,
                        Description = $"Bulk SMS Credits - {rc.Country.Name}",
                        Quantity = quantity,
                        UnitPrice = unitPrice
                    });
                    if (tranFee > 0)
                    {
                        invoice.AddLineItem(new InvoiceLineItem
                        {
                            Code = InvoiceLineItemType.TransactionFee,
                            Description = "Transaction Fee",
                            Quantity = 1,
                            UnitPrice = tranFee
                        });
                    }

                    invoice.Currency = SiteCurrency.SMSPortal.Code(accBilling.CountryId);
                    invoice.InvoiceNr = site.GetInvoiceNr(invoiceRepo.GetNextInvoiceNumber(site.Id));
                    invoice.AccountName = acc.Company;
                    invoice.PurchaseOrderNr = purchaseOrderNr;
                    invoice.CustomerRef = acc.Username;
                    invoice.BillingAddress = accBilling;
                    invoice.PastelCode =
                        string.IsNullOrEmpty(acc.ClientRefCode)
                            ? site.GetPastelCode(accBilling.CountryId)
                            : acc.ClientRefCode;
                    invoice.Created = DateTime.Now;
                    if (taxPercent > 0)
                    {
                        invoice.TaxPercentage = taxPercent;
                        invoice.TaxType = taxType;
                    }
                    invoice.IsInternational = accBilling.CountryId != resellerBilling.CountryId;
                    invoice.DueDate = DateTime.Now.AddDays(7);

                    switch (accountKey.Type)
                    {
                        case AccountType.SmsAccount:
                            invoice.LoginId = accountKey.Id;
                            break;
                        case AccountType.ResellerAccount:
                            invoice.ResellerId = accountKey.Id;
                            break;
                    }

                    invoice.TransactionId = payment.TransactionId;
                    payment.Invoice = invoice;
                    
                    log.Debug($"{paymentGateway} Payment Invoiced" +
                              $" TransactionId={payment.TransactionId}," +
                              $" Order={purchaseOrderNr}," +
                              $" {invoice}");
                }
                payment.Started();
                payment.AccountKey = accountKey;
                if (payment.Invoice != null && !payment.Invoice.Currency.Equals(gateway.GatewayCurrency.Code))
                {
                    payment.Invoice.ConversionRate = invoiceCurrency.Convert(gateway.GatewayCurrency, 1, 4);
                    payment.UnitPrice = invoiceCurrency.Convert(gateway.GatewayCurrency, unitPrice, 4);
                    payment.Total = invoiceCurrency.Convert(gateway.GatewayCurrency, total);
                    payment.TaxValue = invoiceCurrency.Convert(gateway.GatewayCurrency, taxAmount);
                    payment.Fee = invoiceCurrency.Convert(gateway.GatewayCurrency, tranFee);
                }
                else if(payment.Invoice == null && !gateway.GatewayCurrency.Code.Equals(currency))
                {
                    payment.UnitPrice = originalCurrency.Convert(gateway.GatewayCurrency, unitPrice, 4);
                    payment.Total = originalCurrency.Convert(gateway.GatewayCurrency, total);
                    payment.TaxValue = originalCurrency.Convert(gateway.GatewayCurrency, taxAmount);
                    payment.Fee = originalCurrency.Convert(gateway.GatewayCurrency, tranFee);
                }
                else
                {
                    payment.UnitPrice = unitPrice;
                    payment.Fee = tranFee;
                    payment.Total = total;
                    payment.TaxValue = taxAmount;
                }

                payment.Created = DateTime.Now;
                payment.Credits = quantity;
                payment.Gateway = gateway.Gateway;
                payment.Currency = gateway.GatewayCurrency.Code;
                payment.RouteCountryId = routeCountryId;
                payment.Invoice?.StartPayment(payment);
                
                log.Info($"{paymentGateway} Payment Generated {payment}" +
                         (payment.Invoice == null ? "" : $" {payment.Invoice}"));

                return payment;
            }

            throw new PaymentGatewayLimitsException(gateway.Gateway, gateway.MinTransaction, gateway.MaxTransaction);
        }

        public string GetHtml(Payment payment)
        {
            try
            {
                var account = accRepo.GetPaymentAccount(payment.AccountKey);
                var site = siteRepo.Get(account.BuysFromSite);
                var builder = factory.GetFormBuilder(site, site.GetSettings(payment.Gateway).Gateway, !account.GatewaysLive);
                return builder.GetHtml(payment);
            }
            catch (PaymentGatewayException e)
            {
                log.PaymentException(payment.Gateway, payment.ToString(), null, e);
                return $"<error>{e.Message}</error>";
            }
            catch (Exception e)
            {
                log.PaymentException(payment.Gateway, payment.ToString(), null, new PaymentGatewayException(payment.Gateway, e));
                return $"<error>{e.Message}</error>";
            }
        }

        public IDictionary<string, string> GetVariables(Payment payment)
        {
            try
            {
                var account = accRepo.GetPaymentAccount(payment.AccountKey);
                var site = siteRepo.Get(account.BuysFromSite);
                var builder = factory.GetFormBuilder(site, site.GetSettings(payment.Gateway).Gateway, !account.GatewaysLive);
                return builder.GetVariables(payment);
            }
            catch (PaymentGatewayException e)
            {
                log.PaymentException(payment.Gateway, payment.ToString(), null, e);
                return new Dictionary<string, string>()
                {
                    ["error"] = e.Message
                };
            }
            catch (Exception e)
            {
                log.PaymentException(payment.Gateway, payment.ToString(), null, new PaymentGatewayException(payment.Gateway, e));
                return new Dictionary<string, string>()
                {
                    ["error"] = e.Message
                };
            }
        }
    }
}
