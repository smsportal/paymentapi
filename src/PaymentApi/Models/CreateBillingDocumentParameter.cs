﻿namespace SmsPortal.Plutus.Models
{
	public class CreateBillingDocumentParameter
	{
		public BillingDocument BillingDocument { get; set; }
		public bool SendEmail { get; set; }
	}
}
