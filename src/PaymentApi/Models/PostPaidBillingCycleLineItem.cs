﻿using SmsPortal.Plutus.Models.Enums;
using System;

namespace SmsPortal.Plutus.Models
{
    public class PostPaidBillingCycleLineItem : InvoiceLineItem
    {
        
        private int? siteId = null;
        
        public int SiteId { get { return (!siteId.HasValue ? 1 : (int)siteId); }  set { siteId = value; } }
        
        public AccountKey AccountKey { get; set; }
        
        public int AccountId => AccountKey.Id;
        
        public AccountType AccountType => AccountKey.Type;
        
        public string ClientRefCode { get; set; }
        
        public string Username { get; set; }
        
        public string Fullname { get; set; }
        
        public string Company { get; set; }
        
        public string PaymentCurrency { get; set; }

        public LineItemBreakdownType BillingType
        {
            get => billingType == LineItemBreakdownType.PerRefCodeByMasterAccountUsername &&
                   string.IsNullOrEmpty(ClientRefCode)
                ? LineItemBreakdownType.Unknown //You cannot bill per Pastel Code if you do not have a Pastel Code
                : billingType;
            set => billingType = value;
        }

        public BillingPeriod BillingCycle { get; set; }
        
        public DateTime CycleStartDate { get; set; }
        
        public DateTime CycleEndDate { get; set; }
        
        public int CountryId { get; set; }
        
        private int msgCnt = 0;
        private LineItemBreakdownType billingType;
        public int MsgCount { get { return msgCnt <= 0 ? Quantity : msgCnt; } set { msgCnt = value; } }

        public override string ToString() =>
            $"[PostPaidBillingCycleLineItem {BillingType} {Code}: {Description}={ToPricingString()}, AccountKey={AccountKey} SiteId={SiteId} {BillingCycle} Cycle={CycleStartDate:dd MMMM yyyy}-{CycleEndDate:dd MMMM yyyy}, OptionalNote={OptionalNote}]";

        protected virtual string ToPricingString() =>
            $"{Quantity}x{UnitPrice}={PaymentCurrency}{LineTotal}";
    }
}
