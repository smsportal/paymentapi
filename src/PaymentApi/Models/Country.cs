namespace SmsPortal.Plutus.Models
{
    public class Country
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Prefix { get; set; }
    }
}