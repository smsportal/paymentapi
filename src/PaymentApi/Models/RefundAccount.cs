namespace SmsPortal.Plutus.Models
{
    public class RefundAccount
    {
        
        public AccountKey AccountKey { get; }

        public RefundAccount(AccountKey accountKey)
        {
            AccountKey = accountKey;
        }
        
        public string Currency { get; set; }

        public int SiteId { get; set; }

        public int RouteCountryId { get; set; }

        public bool IsPostpaid { get; set; }

        public int MasterAccountId { get; set; }

        public bool IsMaster => MasterAccountId == 0;

        public string Fullname { get; set; }

        public string Email { get; set; }
        
        public string Username { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is RefundAccount)
            {
                var other = (RefundAccount) obj;
                return other.AccountKey == AccountKey;
            }

            return false;
        }

        protected bool Equals(RefundAccount other)
        {
            return AccountKey.Equals(other.AccountKey);
        }

        public override int GetHashCode()
        {
            return AccountKey.GetHashCode();
        }
    }
}