﻿using System;
using System.Configuration;
using SmsPortal.Core;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Contains details about the business' website and operations.
    /// </summary>
    public class Site
    {        
        /// <summary>
        /// Indicates whether this site should be considered as one of SMSPortal's sites.  (or it's our smsfactory.co.za account (siteId = 21))
        /// </summary>
        public bool IsSmsportal => Domain.EndsWith("smsportal.com", StringComparison.InvariantCultureIgnoreCase);

        /// <summary>
        /// Indicates whether this site should be considered as one of SMSPortal's sites.  (or it's our smsfactory.co.za account (siteId = 21))
        /// </summary>
        public bool IsBilledAsSmsportal => Domain.EndsWith("smsportal.com", StringComparison.InvariantCultureIgnoreCase) || Id == 21;

        /// <summary>
        /// The ID of the reseller's website.
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// The ID of the reseller itself.
        /// </summary>
        public int ResellerId { get; set; }

        /// <summary>
        /// The Pastel Codes used by the reseller for client's that don't have their own Pastel Code. Based on currency being used.
        /// </summary>
        public string[] PastelRefCodes { get; set; }
        
        /// <summary>
        /// The user friendly display name of the website/business.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The domain that the webiste is hosted on.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// The prefix that is used by the business when issuing invoices for the Invoice Reference Number
        /// </summary>
        public string InvoicePrefix { get; set; }

        /// <summary>
        /// The prefix that is used by the business when issuing credit notes for the Credit Note Reference Number
        /// </summary>
        public string CreditNotePrefix { get; set; }

        /// <summary>
        /// The prefix that is used by the business when issuing cost estimates for the Cost Estimates Reference Number
        /// </summary>
        public string CostEstimatePrefix { get; set; }

        /// <summary>
        /// The prefix that is used by the business when issuing quotes for the quote Reference Number
        /// </summary>
        public string QuotePrefix { get; set; }

        /// <summary>
        /// True if the business uses the system to do invoicing; otherwise false.
        /// </summary>
        public bool UseAutomaticInvoicing { get; set; }

		/// <summary>
		/// Automatically assign credits to site users.
		/// </summary>
		public bool AutoCreditSiteUsers { get; set; }

        /// <summary>
        /// Details that allow users to contact someone should there be a problem with payments.
        /// </summary>
        public string ContactUsEmail { get; set; }

        /// <summary>
        /// The mobile contact number.
        /// </summary>
        public string ContactUsMobile { get; set; }

        /// <summary>
        /// The email address that should be used as the From address when sending out emails on behalf of the business.
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// The subject of the email sent to the account with an invoice if post paid. Can be configured under the reseller's site settings.
        /// </summary>
        public string PostPaidAuthSubject { get; set; }

        /// <summary>
        /// The contents of the body of the email sent to the account with an invoice if post paid. Can be configured under the reseller's site settings.
        /// </summary>
        public string PostPaidAuthBody { get; set; }

        /// <summary>
        /// Settings for payments to be processed via EFT.
        /// </summary>
        public PaymentGatewaySettings EftSettings { get; set; }

        /// <summary>
        /// Payment gateway settings for PayPal.
        /// </summary>
        public PaymentGatewaySettings PayPalSettings { get; set; }

        /// <summary>
        /// Payment gateway settings for SagePay.
        /// </summary>
        public PaymentGatewaySettings SagePaySettings { get; set; }
        
        /// <summary>
        /// Payment gateway settings for Stripe.
        /// </summary>
        public PaymentGatewaySettings StripeSettings { get; set; }

        public bool TaxEnabled { get; set; }

        public string PrimaryColour { get; set; }

        /// <summary>
        /// Returns the <c>PaymentGatewaySettings</c> for the specified gateway.
        /// </summary>
        /// <param name="gateway">The payment gateway type, either paypal or sagepay.</param>
        /// <exception cref="ConfigurationErrorsException">Raise when the request gateway is not configured for the site.</exception>
        /// <exception cref="ArgumentException">Raised when the request gateway is unsupported.</exception>
        /// <returns>A <c>PaymentGatewaySettings</c> object.</returns>
        public PaymentGatewaySettings GetSettings(string gateway)
        {
            var paymentGateway = gateway.ToNullableEnum<PaymentGatewayType>();
            switch (paymentGateway)
            {
                case PaymentGatewayType.Eft:
                    if (EftSettings == null)
                    {
                        throw new ConfigurationErrorsException($"No {gateway} configuration for site {Id} ({Name})");
                    }
                    return EftSettings;
                
                case PaymentGatewayType.PayPal:
                    if (PayPalSettings == null)
                    {
                        throw new ConfigurationErrorsException($"No {gateway} configuration for site {Id} ({Name})");
                    }
                    return PayPalSettings;
                
                case PaymentGatewayType.NetCash:
                case PaymentGatewayType.NetCashInstantEft:
                    if (SagePaySettings == null)
                    {
                        throw new ConfigurationErrorsException($"No {gateway} configuration for site {Id} ({Name})");
                    }
                    return SagePaySettings;
                
                case PaymentGatewayType.Stripe:
                    if (StripeSettings == null)
                    {
                        throw new ConfigurationErrorsException($"No {gateway} configuration for site {Id} ({Name})");
                    }
                    return StripeSettings;
                
                default:
                    throw new ArgumentException("Unknown payment gateway", nameof(gateway));
            }
        }

        public string GetCostEstimateNr(long nr)
        {
            return $"{CostEstimatePrefix}_{nr:D7}";
        }
        public string GetQuoteNr(long nr)
        {
            return $"{QuotePrefix}_{nr:D7}";
        }

        public string GetInvoiceNr(long nr)
        {
            return $"{InvoicePrefix}_{nr:D7}";
        }

        public string GetCreditNoteNr(long nr)
        {
            return $"{CreditNotePrefix}_{nr:D7}";
        }
        
        public AccountKey AccountKey => new AccountKey(ResellerId, AccountType.ResellerAccount);

        
        /// <summary>
        /// Returns the Pastel Code of the account if it has been assigned one. If not then if the country
        /// of residence is South Africa return the VAT customer Pastel Code. If not local then returns
        /// the ZERO VAT Pastel Code.
        /// </summary>
        /// <param name="countryId">The ID that represents this account's country of residence according to their billing
        /// details.</param>
        /// <value>A string representing the Pastel code.</value>
        public string GetPastelCode(int countryId)
        {
            return countryId == TaxConfig.SouthAfrica.CountryId 
                ? PastelRefCodes[(int) PastelRefCodesIndex.Vat] 
                : PastelRefCodes[(int) PastelRefCodesIndex.ZeroVatEur];
        }

        private string UsableCodes =>
            (PastelRefCodes?.Length ?? 0) <= (int) PastelRefCodesIndex.ZeroVatEur
                ? string.Empty
                : string.Join("|", PastelRefCodes ?? new string[] {});
        
        public override string ToString()
        {
            return $"[Site {Id}: Name={Name} Domain={Domain} PastelRefCodes={UsableCodes}]";
        }
        
    }
}
