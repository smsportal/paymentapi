using System;

namespace SmsPortal.Plutus.Models
{
    public class SmsAccount
    {
        public int AccountId { get; set; }

        public string Username { get; set; }

        public int? BillingGroupId { get; set; }

        public DateTime Created { get; set; }

        public bool Activated { get; set; }
    }
}