﻿using SmsPortal.Core;

namespace SmsPortal.Plutus.Models
{
    public class TaggedPostPaidBillingCycleLineItem<T> : PostPaidBillingCycleLineItem
    {
        public T Tag { get; set; }
        
        public override string ToString() =>
            $"{Tag}=>[{BillingType.ToDescription()} {AccountKey}: {ToPricingString()}]";
    }
}