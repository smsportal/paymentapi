﻿using System;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Models
{
    public class Payment
    {
        public const string PaymentStatusCompleted = "COMPLETED";
        public const string PaymentStatusFailed = "FAILED";
        public const string PaymentStatusFaulted = "FAULTED";
        public const string PaymentStatusStarted = "STARTED";
        
        /// <summary>
        /// Our ID used to identify the transaction.
        /// </summary>
        public Guid TransactionId { get; set; }
        
        /// <summary>
        /// ID used by the remote payment gateway to identify the transaction.
        /// </summary>
        public string GatewayTransactionId { get; set; }

        public int? InvoiceId => Invoice?.Id;

        public AccountKey AccountKey { get; set; }
        public int AccountId => AccountKey.Id;
        public AccountType AccountType => AccountKey.Type;

        public string PastelCode => Invoice?.PastelCode;
        
        public string Currency { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal Total { get; set; }

        public decimal TaxValue { get; set; }

        public decimal Fee { get; set; }
        
        public int Credits { get; set; }
        
        public string InvoiceRefNr => Invoice?.InvoiceNr;

        public string Description => Invoice == null ? "Credit Purchase" : $"{Invoice.CustomerRef} ({Invoice.InvoiceNr})";

        public string Gateway { get; set; }

        public string Status { get; set; }

        public DateTime Created { get; set; }

        public bool IsComplete => Completed.HasValue;

        public DateTime? Completed { get; set; }

        public string NotificationEmail { get; set; }

        public int? RouteCountryId { get; set; }

        public override string ToString()
        {
            return
                "[Payment:" +
                $" TransactionId={TransactionId}, Status={Status},\r\n\tGatewayTransactionId={GatewayTransactionId}," +
                $"\r\n\tAccountKey={AccountKey}," +
                (Invoice == null ? string.Empty : $" PastelCode={PastelCode}, Invoice={InvoiceRefNr},") +
                $"\r\n\tCurrency={Currency}, Total={Total}" +
                "]";
        }

        public void Complete()
        {
            Completed = DateTime.Now;
            Status = PaymentStatusCompleted;
        }

        public void Fail()
        {
            Completed = DateTime.Now;
            Status = PaymentStatusFailed;
        }

        public void Faulted()
        {
            Completed = DateTime.Now;
            Status = PaymentStatusFaulted;
        }
        
        public void Started()
        {
            Status = PaymentStatusStarted;
        }
        
        public Invoice Invoice { set; get; }
        
        public EftAuthorization GetEftAuthorization()
        {
            return new EftAuthorization
            {
                AccountKey = AccountKey,
                Total = Total,
                Credits = Credits,
                TransactionId = TransactionId,
                Currency = Currency,
                Vat = TaxValue,
                BaseCurrencyCost = Total,
                BaseCurrency = Currency,
                BaseVat = TaxValue,
                TransactionFee = Fee,
                BaseTransactionFee = Fee
            };
        }
    }
}
