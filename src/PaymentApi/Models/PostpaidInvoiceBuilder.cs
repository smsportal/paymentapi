namespace SmsPortal.Plutus.Models
{
    public class PostpaidInvoiceBuilder: BaseInvoiceBuilder<PostpaidInvoice>
    {
        private bool? isApproved;
        private PostPaidBillingCycle billingCycle;
        
        public PostpaidInvoiceBuilder WithApproval(bool? isApproved)
        {
            this.isApproved = isApproved;
            return this;
        }

        public PostpaidInvoiceBuilder WithBillingCycle(PostPaidBillingCycle cycle)
        {
            billingCycle = cycle;
            return this;
        }
        
        public override PostpaidInvoice Build(BillingGroup billingGroup, PaymentAccount account, Site site)
        {
            var tmp = SetBaseProperties(billingGroup, account, site);
            if (billingCycle != null)
            {
                tmp.Cycle = billingCycle;
                tmp.Created = billingCycle.CycleEndDate.Date;
            }
            tmp.IsApproved = isApproved;
            return tmp;
        }
    }
}