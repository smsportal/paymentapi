﻿using SmsPortal.Plutus.Models.Enums;
using System;
using System.Collections.Generic;
using SmsPortal.Core;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Class that represents an invoice. Line items can be added to the invoice.
    /// </summary>
    public partial class Invoice
    {
        private List<InvoiceLineItem> lineItems = new List<InvoiceLineItem>();

        /// <summary>
        /// The unique ID assigned by the system to the invoice.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// A unique ID assigned by the system to the invoice using the site's accounting system's numbering system.
        /// </summary>
        public string InvoiceNr { get; set; }

        /// <summary>
        /// The amount of the invoice before tax.
        /// </summary>
        public decimal SubTotal { get; private set; }

        /// <summary>
        /// The percentage of tax that should be added to the subtotal amount.
        /// </summary>
        public decimal TaxPercentage { get; set; }

        /// <summary>
        /// The amount of tax applicable on the invoice.
        /// </summary>
        public decimal TaxAmount => Math.Round((TaxPercentage / 100) * SubTotal, 2, MidpointRounding.AwayFromZero);

        /// <summary>
        /// The total amount due on the invoice.
        /// </summary>
        public decimal Total => SubTotal + TaxAmount;

        /// <summary>
        /// The type of tax being paid, VAT, GST etc.
        /// </summary>
        public string TaxType { get; set; } = "NONE";

        /// <summary>
        /// The code used to post the invoice to Pastel.
        /// </summary>
        public string PastelCode { get; set; }

        /// <summary>
        /// True if the client's country of residence differs from the site's country of residence; otherwise false.
        /// </summary>
        public bool IsInternational { get; set; }

        /// <summary>
        /// If the account making a purchase is a SMS account then this value will be set. Otherwise 0
        /// </summary>
        public int LoginId { get; set; }
        
        /// <summary>
        /// If the account making a purchase is a Reseller then this value will be set. Otherwise 0
        /// </summary>
        public int ResellerId { get; set; }

        /// <summary>
        /// The name of the company/individual making payment. Take from the account's billing details.
        /// </summary>
        public string AccountName { get; set; }
        
        /// <summary>
        /// Optional value provided by the client for their accounting department's purposes.
        /// </summary>
        public string PurchaseOrderNr { get; set; }

        /// <summary>
        /// Contains the name of the Login who created the invoice or to whom the invoice will be sent.
        /// </summary>
        public string CustomerRef { get; set; }

        /// <summary>
        /// The currency that the invoice is denominated in.
        /// </summary>
        public string Currency { get; set; }
        
        /// <summary>
        /// The current status of the invoice. Defaults to Unpaid.
        /// </summary>
        public string Status { get; set; } = "Unpaid";

        /// <summary>
        /// Details to appear on the invoice.
        /// </summary>
        public BillingDetails BillingAddress { get; set; }

        /// <summary>
        /// When the invoice was created.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// The date by which the invoice should be paid if present.
        /// </summary>
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// If the invoice is not in the same currency as the business operates in then the conversion rate used to calculate the invoice amounts.
        /// </summary>
        public decimal? ConversionRate { get; set; }

        /// <summary>
        /// If payment has already been initiated or complete then the transaction ID corresponding to that payment; otherwise <c>NULL</c>
        /// </summary>
        public Guid? TransactionId { get; set; }
        
        /// <summary>
        /// Adds a <c>InvoiceLineItem</c> to this instance adjusting the subtotal and total fields appropriately.
        /// </summary>
        /// <param name="lineItem">The line item to add to the invoice.</param>
        public void AddLineItem(InvoiceLineItem lineItem)
        {
            lineItems.Add(lineItem);
            SubTotal += lineItem.LineTotal;
        }

        public void StartPayment(Payment payment)
        {
            if (payment.Gateway.Is(PaymentGatewayType.NetCash) ||
                payment.Gateway.Is(PaymentMethodType.SagePay)) //Used in old payments
            {
                Status = "UnpaidSagePay";
            }

            if (payment.Gateway.Is(PaymentGatewayType.PayPal))
            {
                Status = "Unpaid";
            }

            if (payment.Gateway.Is(PaymentGatewayType.Eft))
            {
                Status = "UnpaidEft";
            }

            TransactionId = payment.TransactionId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionId">The ID of the transaction that closes the invoice.</param>
        public void Close(Guid transactionId)
        {
            if (Status == "Paid" ||
                Status == "Cancelled" ||
                Status == "Expired")
            {
                throw new InvalidOperationException($"Cannot close Invoice {InvoiceNr} because it is currenty in {Status} status");
            }

            Status = "Paid";
            TransactionId = transactionId;
        }

        public AccountKey AccountKey
        {
            get {
                if (LoginId > 0)
                {
                    return new AccountKey(LoginId, AccountType.SmsAccount);
                }
                if (ResellerId > 0)
                {
                    return new AccountKey(ResellerId, AccountType.ResellerAccount);
                }

                throw new InvalidOperationException("Expected either loginId or resellerId to be be present");
            }
        }

        public InvoiceLineItemType GetLineItemType()
        {
            return lineItems.Count > 0 ? lineItems[0].Code : InvoiceLineItemType.BulkSms;  
        }

        /// <summary>
        /// Gets a deep copy of the <c>InvoiceLineItem</c> objects in this instace.
        /// </summary>
        /// <returns></returns>
        public IList<InvoiceLineItem> GetLineItems()
        {
            var tmp = new List<InvoiceLineItem>();
            foreach (var lineItem in lineItems)
            {
                var tmpLineItem = new InvoiceLineItem();
                tmpLineItem.UnitPrice = lineItem.UnitPrice;
                tmpLineItem.Quantity = lineItem.Quantity;
                tmpLineItem.Code = lineItem.Code;
                tmpLineItem.Description = lineItem.Description;
                tmpLineItem.OptionalNote = lineItem.OptionalNote;
                tmpLineItem.DataId = lineItem.DataId;
                tmp.Add(tmpLineItem);
            }
            return tmp;
        }

        public override string ToString()
        {
            var accountKey = AccountKey;
            return $"[Invoice {InvoiceNr}: " +
                   $"Id={Id}, " +
                   $"TransactionId={TransactionId}, " +
                   $"AccountKey={accountKey}, " +
                   $"Total={Currency}{Total}]";
        }
    }
}
