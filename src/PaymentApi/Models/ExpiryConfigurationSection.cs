﻿using System;
using System.Collections;
using System.Configuration;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Configuration section for invoice expiry.
    /// The example shows a configuration for checking for
    /// invoices every 5 minutes, and expiring any invoices
    /// with status UnpaidEFT that are seven days overdue.
    /// <example>
    ///     <expiryConfiguration Interval="00:05:00">
    ///         <expiry status="UnpaidEft" overdue="7.00:00:00" />
    ///     </expiryConfiguration>
    /// </example>
    /// </summary>
    /// <inheritdoc cref="ConfigurationSection" />
    public class ExpiryConfigurationSection : ConfigurationSection, IEnumerable
    {
        /// <summary>
        /// What interval should the status be examined?
        /// </summary>
        [ConfigurationProperty("Interval", DefaultValue = "00:01:00")]
        public TimeSpan Interval
        {
            get { return (TimeSpan)this["Interval"]; }
            set { this["Interval"] = value; }
        }

        /// <summary>
        /// A single configuration item for invoice expiry.
        /// </summary>
        /// <inheritdoc />
        public class ExpiryConfigurationItem: NamedConfigurationSection
        {
            /// <inheritdoc />
            public override string Name
            {
                get => Status;
                set => Status = value;
            }

            /// <summary>
            /// The status of items to expire.
            /// </summary>
            [ConfigurationProperty("status", IsRequired = true)]
            public string Status
            {
                get { return (string) this["status"]; }
                set { this["status"] = value; }
            }

            /// <summary>
            /// Expire when overdue by.
            /// </summary>
            [ConfigurationProperty("overdue", DefaultValue = "00:01:00")]
            public TimeSpan Overdue
            {
                get { return (TimeSpan)this["overdue"]; }
                set { this["overdue"] = value; }
            }
        }

        #region NamedConfigurationSection.Container<ExpiryConfigurationItem> Pages AS ConfigurationCollection(AddItemName = "expiry")
        /// <summary>
        /// The list of expiry configuration elements. 
        /// </summary>
        [ConfigurationProperty("", IsDefaultCollection = true, IsRequired = true)]
        [ConfigurationCollection(typeof(NamedConfigurationSection.Container<ExpiryConfigurationItem>), AddItemName = "expiry")]
        public NamedConfigurationSection.Container<ExpiryConfigurationItem> ExpiryList
        {
            get { return (NamedConfigurationSection.Container<ExpiryConfigurationItem>)this[""]; }
        }

        /// <inheritdoc />
        public IEnumerator GetEnumerator()
        {
            return ExpiryList.GetEnumerator();
        }
        #endregion
    }
}
