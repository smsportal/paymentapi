using System;

namespace SmsPortal.Plutus.Models
{
    public class PostpaidInvoice : ProformaInvoice
    {
        public DateTime? ApprovedOrDeclined { get; private set; }

        private bool? isApproved;

        public bool? IsApproved
        {
            get => isApproved;
            set
            {
                isApproved = value;
                if (value.HasValue)
                {
                    ApprovedOrDeclined = DateTime.Now;
                    Status = value.Value ? "PostPaidApproved" : "PostPaidDeclined";
                }
                else
                {
                    ApprovedOrDeclined = null;
                    Status = "PostPaidUnpaid";
                }
            }
        }
    }
}