﻿namespace SmsPortal.Plutus.Models
{
    public class SiteCurrency
    {
        
        public int LocalCountryId { get; set; }
        
        public string LocalCurrencyCode { get; set; }

        public string ForeignCurrencyCode { get; set; }

        // TODO This will be changed to retrieved from a new repository
        public static readonly SiteCurrency SMSPortal = new SiteCurrency()
        {
            LocalCountryId = TaxConfig.SouthAfrica.CountryId,
            LocalCurrencyCode = "ZAR",
            ForeignCurrencyCode = "EUR"
        };
        
        public string Code(int countryId) => countryId == LocalCountryId ? LocalCurrencyCode : ForeignCurrencyCode;
    }
}