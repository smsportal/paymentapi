﻿using SmsPortal.Plutus.Models.Enums;
using System;
using System.Runtime.InteropServices;

namespace SmsPortal.Plutus.Models
{
    /// <summary>
    /// Represents a single line item on an invoice
    /// </summary>
    public class InvoiceLineItem
    {
        /// <summary>
        /// The unit price
        /// </summary>
        public decimal UnitPrice { get; set; }
        /// <summary>
        /// The quantity of units
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// The subtotal for the line rounded to 2 decimal places.
        /// </summary>
        public decimal LineTotal => Math.Round(UnitPrice * Quantity, 2, MidpointRounding.AwayFromZero);
        /// <summary>
        /// The code classifying the type units
        /// </summary>
        public InvoiceLineItemType Code { get; set; } = InvoiceLineItemType.BulkSms;
        /// <summary>
        /// A description for the line (normally based on the type of units)
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// An optional note
        /// </summary>
        public string OptionalNote { get; set; } = null;
        /// <summary>
        /// An optional unique identifier of the related data item
        /// </summary>
        public long? DataId { get; set; } = null;

        public override string ToString()
        {
            var describe = Description ?? "NA";
            if (describe.Length > 20)
            {
                describe = describe.Substring(0, 20) + "...";
            }
            return $"[InvoiceLineItem: Quantity={Quantity}, UnitPrice={UnitPrice}, Description='{describe}']";
        }
    }
}
