namespace SmsPortal.Plutus.Models
{
    public interface IPaymentGenerator : IHtmlPaymentFormBuilder
    {
        Payment Generate(
            AccountKey accountKey,
            int quantity, 
            decimal unitPrice, 
            string currency,
            string paymentGateway, 
            string purchaseOrderNumber,
            int routeCountryId);
    }
}