﻿using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Models
{
	public class CreditNoteModel : BillingDocument
	{
	    public CreditNoteModel()
	    {
            LineItems = new List<DocumentLineItemModel>();
	    }

	    /// <summary>
	    /// If the account making a purchase is a SMS account then this value will be set. Otherwise 0
	    /// </summary>
	    public int LoginId { get; set; }

	    public int ResellerId { get; set; }

		/// <summary>
		/// The unique ID assigned by the system to the credit note.
		/// </summary>
		public int CreditNoteId { get; set; }

		/// <summary>
		/// The ID of the invoice associated with this credit note.
		/// </summary>
		public int InvoiceId { get; set; }

		/// <summary>
		/// Credit note reference number.
		/// </summary>
		public string ReferenceNumber { get; set; }

		/// <summary>
		/// Reference number of the invoice associated with this credit note.
		/// </summary>
		public string InvoiceReferenceNumber { get; set; }

		/// <summary>
		/// The name of the account on the invoice.
		/// </summary>
		public string AccountName { get; set; }

		/// <summary>
		/// The code used to post the credit note to Pastel.
		/// </summary>
		public string ClientRefCode { get; set; }

		/// <summary>
		/// Contains the name of the Login who created the invoice or to whom the invoice was sent.
		/// </summary>
		public string CustomerRefCode { get; set; }

		/// <summary>
		/// When the credit note was created.
		/// </summary>
		public DateTime CreatedDate { get; set; }

		/// <summary>
		/// The amount of tax applicable on the credit note.
		/// </summary>
		public decimal TaxValue { get; set; }

		/// <summary>
		/// The percentage of tax that should be added to the subtotal amount.
		/// </summary>
		public decimal TaxPercentage { get; set; }

		/// <summary>
		/// The total amount on the credit note.
		/// </summary>
		public decimal Total { get; set; }

		/// <summary>
		/// The amount on the credit note before tax.
		/// </summary>
		public decimal SubTotal { get; set; }

		/// <summary>
		/// The currency that the credit note is denominated in.
		/// </summary>
		public string Currency { get; set; }

		/// <summary>
		/// If the credit note is not in the same currency as the business operates in then the conversion rate used to calculate the credit note amounts.
		/// </summary>
		public string ConversionRate { get; set; }

		/// <summary>
		/// The client's billing details.
		/// </summary>
		public BillingDetails ClientBillingDetails { get; set; }

		/// <summary>
		/// The line items on the credit note.
		/// </summary>
		public List<DocumentLineItemModel> LineItems { get; set; }

		/// <summary>
		/// The pdf bytes of the credit note.
		/// </summary>
		public byte[] CreditNoteBytes { get; set; }

		/// <summary>
		/// The transaction ID GUID.
		/// </summary>
		public string TransactionId { get; set; }

		/// <summary>
		/// The status of the invoice associated with the credit note.
		/// </summary>
		public InvoiceStatus InvoiceStatus { get; set; }
		
		public AccountKey AccountKey
		{
			get {
				if (LoginId > 0)
				{
					return new AccountKey(LoginId, AccountType.SmsAccount);
				}
				if (ResellerId > 0)
				{
					return new AccountKey(ResellerId, AccountType.ResellerAccount);
				}

				throw new InvalidOperationException("Expected either loginId or resellerId to be be present");
			}
		}
		
		/// <summary>
		/// True if the client's country of residence differs from the site's country of residence; otherwise false.
		/// </summary>
		public bool IsInternational { get; set; }
		
		/// <summary>
		/// True if the invoice has a post-paid invoice related status, or had a billing period specified.
		/// </summary>
		public bool IsPostPaid { get; set; }

		public static CreditNoteModel CreateCreditNoteModel(InvoiceModel invoice, ICurrencyRepository currencyRepository, BillingDetails billingDetails)
		{
			var creditNote = new CreditNoteModel
			{
				CreditNoteId = -1,
				InvoiceId = invoice.InvoiceId,
				InvoiceReferenceNumber = invoice.ReferenceNumber,
				AccountName = invoice.AccountName,
				ClientRefCode = invoice.ClientRefCode,
				CustomerRefCode = invoice.CustomerRefCode,
				CreatedDate = DateTime.Now,
				TaxValue = invoice.TaxValue,
				TaxPercentage = invoice.TaxPercentage,
				Total = invoice.Total,
				SubTotal = invoice.SubTotal,
				Currency = invoice.Currency,
				IsInternational = invoice.IsInternational,
				IsPostPaid = invoice.IsPostPaid
			};

			var converter = currencyRepository.GetCurrencyConverter(invoice.Currency, "ZAR");
			var conversionRate = Math.Round(converter.Convert(1), 6);	// <-- \/ this is also done elsewhere, maybe add a method to the currencyRepo
			creditNote.ConversionRate = conversionRate != 1 ? conversionRate.ToString() : string.Empty;

			creditNote.ClientBillingDetails = billingDetails;

			foreach (var lineItem in invoice.LineItems)
			{
				creditNote.LineItems.Add(new DocumentLineItemModel
				{
					Description = lineItem.Description,
					Price = lineItem.Price,
					Quantity = lineItem.Quantity,
					Total = lineItem.Total
				});
			}

			creditNote.TransactionId = Guid.NewGuid().ToString();
			creditNote.InvoiceStatus = invoice.Status;

			return creditNote;
		}
		
		public override string ToString()
		{
			var accountKey = AccountKey;
			return $"[CreditNote {ReferenceNumber}: " +
			       $"InvoiceNr={InvoiceReferenceNumber}, " +
			       $"AccountKey={accountKey} " +
			       $"Total={Currency}{Total}]";
		}
	}
}
