﻿using System;
using System.Collections.Generic;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace SmsPortal.Plutus.Filters
{
	[AttributeUsage(AttributeTargets.Method)]
	public class ArgumentFilter : ActionFilterAttribute
	{
		private readonly Func<Dictionary<string, object>, bool> validate;

		public ArgumentFilter() : this(arguments => arguments.ContainsValue(null))
		{

		}

		public ArgumentFilter(Func<Dictionary<string, object>, bool> checkCondition)
		{
			validate = checkCondition;
		}

		public override void OnActionExecuting(HttpActionContext actionContext)
		{
			//if (validate(actionContext.ActionArguments))
			//{
			//	var info = new
			//	{
			//		StatusCode = (int)HttpStatusCode.BadRequest,
			//		StatusMessage = "The requests' argument(s) cannot be null",
			//		Reason = "Validation Exception"
			//	};
			//	actionContext.Response = actionContext.Request.CreateResponse(
			//		HttpStatusCode.BadRequest, info);
			//}
		}
	}
}
