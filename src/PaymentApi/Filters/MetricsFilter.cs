﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Metrics;

namespace SmsPortal.Plutus.Filters
{
	/// <summary>
	/// Filter that facilitates recording processing times for API calls using the Metrics library.
	/// </summary>
	public class MetricsFilter : ActionFilterAttribute
	{
		private readonly Timer responseTimer = Metric.Timer(MetricName.RESTResponseTime.ToString(), Unit.Requests,
			SamplingType.ExponentiallyDecaying, TimeUnit.Milliseconds);
		private readonly string responseTimerKey = "TimerContext";

		public override void OnActionExecuting(HttpActionContext actionContext)
		{
			//actionContext.Request.Properties[responseTimerKey] = responseTimer.NewContext();

			//base.OnActionExecuting(actionContext);
		}

		public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
		{
			//var timerContext = actionExecutedContext.Request.Properties[responseTimerKey];
			//((TimerContext?)timerContext)?.Dispose();

			//base.OnActionExecuted(actionExecutedContext);
		}
	}
}
