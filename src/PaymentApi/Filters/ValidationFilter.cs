﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using SmsPortal.Plutus.Validation;

namespace SmsPortal.Plutus.Filters
{
	public class ValidationFilter : ActionFilterAttribute
	{
		private readonly IObjectValidator validator;

		public ValidationFilter(IObjectValidator validator)
		{
			this.validator = validator;
		}

		public override void OnActionExecuting(HttpActionContext actionContext)
		{
			if (!actionContext.ModelState.IsValid)
			{
				var validationResults = new List<ValidationResult>();

				foreach (var input in actionContext.ActionArguments)
				{
					var results = validator.Validate(input.Value);
					validationResults.AddRange(results);
				}

				if (validationResults.Count > 0)
				{
					var errorMessage = BuildErrorMessage(actionContext, validationResults);
					throw new ValidationException(errorMessage);
				}
			}
		}

		private string BuildErrorMessage(HttpActionContext actionContext, IEnumerable<ValidationResult> validationResults)
		{
			var errorMessageBuilder = new StringBuilder();

			foreach (var validationResult in validationResults)
			{
				errorMessageBuilder.Append($"{validationResult.ErrorMessage}{Environment.NewLine}");
			}

			return errorMessageBuilder.ToString();
		}

		private string FormatControllerName(string controllerName)
		{
			var stripVersionRegex = new Regex(@"((v|V)\d+\.)|(v|V)\d+");
			var strippedOfVersion = stripVersionRegex.Replace(controllerName, "");
			return strippedOfVersion;
		}
	}
}
