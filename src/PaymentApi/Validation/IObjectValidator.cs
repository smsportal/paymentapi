﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmsPortal.Plutus.Validation
{
	/// <summary>
	/// <c>IObjectValidator</c> used to validate objects using Data Annotations
	/// </summary>
	public interface IObjectValidator
	{
		/// <summary>
		/// Validates the object and returns the results
		/// </summary>
		/// <param name="value">The object to validate</param>
		/// <returns>List of <c>ValidationResult</c></returns>
		IEnumerable<ValidationResult> Validate(object value);
	}
}
