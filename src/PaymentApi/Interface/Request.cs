﻿using SmsPortal.Core.Messaging;

namespace SmsPortal.Plutus.Interface
{
	/// <summary>
	/// The base request object 
	/// </summary>
	public abstract class Request
	{
		/// <summary>
		/// The channel that the send came in on
		/// </summary>
		public Channel Channel { get; private set; }
	}
}
