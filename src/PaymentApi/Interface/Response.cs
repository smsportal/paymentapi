﻿namespace SmsPortal.Plutus.Interface
{
	/// <summary>
	/// The Base Response
	/// </summary>
	public abstract class Response
	{
		/// <summary>
		/// The Error Message for the request if an error occured
		/// </summary>
		public string ErrorMessage { get; set; }

		/// <summary>
		/// The Error Code for the request if an error occured
		/// </summary>
		public string ErrorCode { get; set; }
	}
}
