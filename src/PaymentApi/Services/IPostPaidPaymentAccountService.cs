using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Services
{
    public interface IPostPaidPaymentAccountService
    {
        /// <summary>
        /// Attempt to retrieve the details of a payment account
        /// </summary>
        /// <param name="accountKey">The unique key associated with the account</param>
        /// <param name="account">A variable to receive the account details as an instance of <c>PaymentAccount</c></param>
        /// <returns>A boolean indicating success</returns>
        bool TryGetPaymentAccount(AccountKey accountKey, out PaymentAccount account);
        
        /// <summary>
        /// Retrieve the details of the request payment account, or a temporary substitute indicating missing billing details 
        /// </summary>
        /// <param name="accountKey">The unique key associated with the account</param>
        /// <returns>The account details as an instance of <c>PaymentAccount</c></returns>
        PaymentAccount GetPaymentAccount(AccountKey accountKey);
    }
}