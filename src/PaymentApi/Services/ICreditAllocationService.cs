﻿using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Services
{
	public interface ICreditAllocationService
	{
	    void ProcessTransaction(PaymentAccount account, Payment payment, string gatewayTranId);

	}
}
