using System;

namespace SmsPortal.Plutus.Services
{
    public interface IEventRefundService : IDisposable
    {
        void Start();
        void Stop();
    }
}