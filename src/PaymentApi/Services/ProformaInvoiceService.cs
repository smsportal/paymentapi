using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using log4net;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Services
{
    public class ProformaInvoiceService : IProformaInvoiceService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ProformaInvoiceService));
        private readonly IPostPaidBillingCycleRepository postPaidBillingRepository;
        private readonly IPostPaidEventsRepository postPaidEventsRepository;
        private readonly IShortCodeRentalInvoiceService shortCodeRentalInvoiceService;
        private readonly IBillingGroupOrDetailsRepository billingDetailsRepository;
        private readonly ICostEstimateRepository costEstimateRepository;
        private readonly IInvoiceRepository invoiceRepository;
        private readonly ISiteRepository siteRepo;
        private readonly IPdfService pdfService;
        private readonly ICurrencyRepository currencyRepository;
        private readonly ISettlementDiscountRepository discountRepo;
        private readonly IPostPaidPaymentAccountService postPaidPaymentAccountService;
        private readonly Timer timer;
        private bool isProcessing = false;

        public ProformaInvoiceService(
            IPostPaidBillingCycleRepository postPaidBillingRepository,
            IPostPaidEventsRepository postPaidEventsRepository,
            IShortCodeRentalInvoiceService shortCodeRentalInvoiceService,
            ISiteRepository siteRepo,
            IBillingGroupOrDetailsRepository billingDetailsRepository,
            ICostEstimateRepository costEstimateRepository,
            IInvoiceRepository invoiceRepository,
            IPdfService pdfService,
            ICurrencyRepository currencyRepository,
            ISettlementDiscountRepository discountRepo,
            IPostPaidPaymentAccountService postPaidPaymentAccountService)
        {
            this.postPaidBillingRepository = postPaidBillingRepository;
            this.postPaidEventsRepository = postPaidEventsRepository;
            this.shortCodeRentalInvoiceService = shortCodeRentalInvoiceService;
            this.billingDetailsRepository = billingDetailsRepository;
            this.costEstimateRepository = costEstimateRepository;
            this.invoiceRepository = invoiceRepository;
            this.siteRepo = siteRepo;
            this.pdfService = pdfService;
            this.currencyRepository = currencyRepository;
            this.discountRepo = discountRepo;
            this.postPaidPaymentAccountService = postPaidPaymentAccountService;
            timer = new Timer(TimeSpan.FromSeconds(1).TotalMilliseconds);
            timer.Elapsed += CheckForNewBillingCycle;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        public IEnumerable<ProformaInvoice> GetAllPostPaidProformaInvoices(PostPaidBillingCycleFilterType filterType, DateTime? invoiceDate)
        {
            log.Debug($"{filterType} post-paid invoice list requested");
            IEnumerable<ProformaInvoice> invoices;
            switch (filterType)
            {
                case PostPaidBillingCycleFilterType.DedicatedShortCodeRental:
                    invoices = shortCodeRentalInvoiceService.GetAllProformaInvoices(InvoiceLineItemType.ShortCodeRental, null, invoiceDate).ToArray();
                    break;

                case PostPaidBillingCycleFilterType.SharedShortCodeKeywordRental:
                    invoices = shortCodeRentalInvoiceService.GetAllProformaInvoices(InvoiceLineItemType.ShortCodeKeywordRental, null, invoiceDate).ToArray();
                    break;
                
                case PostPaidBillingCycleFilterType.AllShortCodeRental:
                    invoices = shortCodeRentalInvoiceService.GetAllProformaInvoices(null, null, invoiceDate).ToArray();
                    break;

                default:
                    invoices = postPaidBillingRepository.GetAllProformaInvoices(filterType).ToArray();
                    break;
            }

            var matched = postPaidBillingRepository.MatchToExisting(invoices);
            log.Info($"{filterType} post-paid invoice list of {invoices.Count()} items matched with {invoices.Count(i => i.Id != 0)} existing invoices");
            return matched;
        }

        public ProformaInvoice GetPostPaidProformaInvoice(int invoiceId)
        {
            var invoice = invoiceRepository.Get(invoiceId);
            var result = new ProformaInvoice()
            {
                Id = invoice.Id,
                InvoiceNr = invoice.InvoiceNr,
                TaxPercentage = invoice.TaxPercentage,
                TaxType = invoice.TaxType,
                PastelCode = invoice.PastelCode,
                IsInternational = invoice.IsInternational,
                LoginId = invoice.LoginId,
                ResellerId = invoice.ResellerId,
                AccountName = invoice.AccountName,
                PurchaseOrderNr = invoice.PurchaseOrderNr,
                CustomerRef = invoice.CustomerRef,
                Currency = invoice.Currency,
                Status = invoice.Status,
                BillingAddress = invoice.BillingAddress,
                Created = invoice.Created,
                DueDate = invoice.DueDate,
                ConversionRate = invoice.ConversionRate,
                TransactionId = invoice.TransactionId,
            };
            var lines = invoice.GetLineItems();
            foreach (var line in lines)
            {
                result.AddLineItem(line);
            }

            result.Cycle = postPaidBillingRepository.IdentifyBillingCycle(invoiceId);
            var accountType = invoice.LoginId == 0 ? "RS" : "MA";
            var accountId = invoice.LoginId == 0 ? invoice.ResellerId : invoice.LoginId;

            log.Info($"{result.Status} invoice for {accountType} {accountId} on {invoice.Created} retrieved from database as #{invoice.Id}");
            return result;
        }

        public ProformaInvoice GetPostPaidProformaInvoice(
            PostPaidBillingCycleFilterType filterType,
            AccountKey accountKey,
            DateTime invoiceDate)
        {
            log.Debug($"{filterType} post-paid invoice for {accountKey} on {invoiceDate} requested");
            ProformaInvoice invoice;
            switch (filterType)
            {
                case PostPaidBillingCycleFilterType.DedicatedShortCodeRental:
                    invoice = shortCodeRentalInvoiceService.GetProformaInvoice(
                        accountKey,
                        invoiceDate,
                        InvoiceLineItemType.ShortCodeRental);
                    break;

                case PostPaidBillingCycleFilterType.SharedShortCodeKeywordRental:
                    invoice = shortCodeRentalInvoiceService.GetProformaInvoice(
                        accountKey,
                        invoiceDate,
                        InvoiceLineItemType.ShortCodeKeywordRental);
                    break;

                default:
                    invoice = postPaidBillingRepository.GetProformaInvoice(
                        accountKey,
                        filterType,
                        invoiceDate);
                    break;
            }


            if (invoice.IsInternational)
            {
                var currency = currencyRepository.GetCurrency(invoice.Currency);
                invoice.ConversionRate = (1 / currency.Rate);

            }

            var matched = postPaidBillingRepository.MatchToExisting(new[] {invoice}).First();
            log.Info($"{filterType} post-paid invoice for {accountKey} on {invoiceDate} {(invoice.Id != 0 ? "" : "not ")}matched with existing invoice {(invoice.Id == 0 ? "" : "#" + invoice.Id)}");
            return matched;
        }

        public ProformaInvoice GetPostPaidProformaInvoice(
            AccountKey accountKey,
            string shortCodeUsername,
            DateTime setupDate,
            string shortCode,
            string keyword,
            long? dataId)
        {
            var isDedicated = string.IsNullOrEmpty(keyword);
            var account = GetPaymentAccount(accountKey);
            var billingAddress = billingDetailsRepository.Get(accountKey);
            var invoice = postPaidBillingRepository.GetProformaInvoice(
                account,
                billingAddress,
                setupDate,
                BillingPeriod.None,
                new InvoiceLineItem
                {
                    Code = isDedicated
                        ? InvoiceLineItemType.ShortCodeSetup
                        : InvoiceLineItemType.ShortCodeKeywordRental,
                    UnitPrice = isDedicated
                        ? account.PricingSCSetup
                        : account.PricingSCShared,
                    Quantity = 1,
                    Description = isDedicated
                        ? $"Short Code Setup - {shortCode} ({shortCodeUsername})"
                        : $"Short Code Rental - Keyword {keyword} on {shortCode} ({shortCodeUsername})",
                    DataId = dataId
                });

            var type = isDedicated ? "SC SETUP" : "SCKW";
            log.Info($"{type} post-paid invoice for {accountKey} on {setupDate} requested");
            return invoice;
        }

        public ProformaInvoice GetPostPaidProformaInvoice(
            AccountKey accountKey,
            long eventId)
        {
            var invoice = postPaidEventsRepository.GetProformaInvoice(accountKey, eventId);
            log.Info($"STD post-paid invoice for {accountKey} for event #{eventId} requested");
            return invoice;
        }

        private PaymentAccount GetPaymentAccount(AccountKey accountKey) =>
            postPaidPaymentAccountService.GetPaymentAccount(accountKey);

        private string GetInvoiceNr(PaymentAccount account)
        {
            var site = siteRepo.Get(account.BuysFromSite);
            return site.GetInvoiceNr(invoiceRepository.GetNextInvoiceNumber(site.Id));
        }

        private string GetCostEstimateNr(PaymentAccount account)
        {
            var site = siteRepo.Get(account.BuysFromSite);
            return site.GetCostEstimateNr(costEstimateRepository.GetNextCostEstimateNumber(site.Id));
        }

        public void InsertCostEstimate(ProformaInvoice invoice)
        {
            var account = GetPaymentAccount(invoice.AccountKey);
            var billAddr = billingDetailsRepository.Get(account.AccountKey);

            invoice.InvoiceNr = GetCostEstimateNr(account);
            invoice.PurchaseOrderNr = string.Empty;
            invoice.AccountName = billAddr.CompanyName;
            invoice.BillingAddress = billAddr;
            invoice.Status = "CostEstimate";
            invoice.TransactionId = Guid.NewGuid();

            invoice.Id = -costEstimateRepository.Add(invoice, invoice.Cycle);
            log.Info($"Cost Estimate {invoice.InvoiceNr} created for {invoice.AccountName} ({account.AccountType} {account.Id}) based on {invoice.Cycle.ItemDescription} {invoice.Cycle.BreakdownDescription} as #{-invoice.Id}");
        }

        public void InsertProformaInvoice(
            ProformaInvoice invoice,
            string orderNumber)
        {
            var account = GetPaymentAccount(invoice.AccountKey);
            invoice.InvoiceNr = "Proforma";
            invoice.PurchaseOrderNr = orderNumber ?? string.Empty;
            invoice.Status = "PostPaidProforma";
            var invoiceType = invoice.GetLineItemType();
            invoice.Id = Insert(invoice, account);
            postPaidBillingRepository.MarkAsPostPaid(invoice);
            log.Info($"{invoiceType} Proforma Invoice created for {invoice.AccountName} ({account.AccountType} {account.Id}) based on {invoice.Cycle.ItemDescription} {invoice.Cycle.BreakdownDescription} as #{invoice.Id}");
        }

        public void InsertInvoice(
            ProformaInvoice invoice,
            string orderNumber)
        {
            var account = GetPaymentAccount(invoice.AccountKey);
            invoice.InvoiceNr = GetInvoiceNr(account);
            invoice.PurchaseOrderNr = orderNumber ?? string.Empty;
            invoice.Status = "PostPaidApproved";
            invoice.Id = Insert(invoice, account);
            log.Info($"Invoice {invoice.InvoiceNr} created for {invoice.AccountName} ({account.AccountType} {account.Id}) based on {invoice.Cycle.ItemDescription} {invoice.Cycle.BreakdownDescription} as #{invoice.Id}");
        }

        private int Insert(
            ProformaInvoice invoice,
            PaymentAccount account)
        {
            invoice.Id = 0;
            invoice.TransactionId = Guid.NewGuid();

            if(billingDetailsRepository.TryGet(account.AccountKey, out var billingAddress))
            {
                invoice.AccountName = billingAddress.CompanyName;
            }
            else
            {
                log.Debug(
                    $"A {typeof(BillingDetails)} with ID {account.AccountKey} could not be found..." +
                    " Since this is a post-paid invoice we will proceed with temporary details.");
                billingAddress = BillingDetails.GetTemporary();
                billingAddress.CompanyName = invoice.AccountName;
            }
            invoice.BillingAddress = billingAddress;

            return invoiceRepository.Add(invoice);
        }

        public byte[] CreatePdf(Invoice invoice, PostPaidBillingCycle cycle)
        {
            byte[] pdf;
            if (invoice.InvoiceNr.Equals("Proforma", StringComparison.OrdinalIgnoreCase))
            {
                pdf = pdfService.CreatePdf(invoice, cycle);
                log.Debug($"PDF for Invoice {invoice.InvoiceNr} (#{invoice.Id}) containing {pdf.Length} bytes not saved");
            }
            else if (invoice.Id < 0)
            {
                pdf = pdfService.CreatePdf(invoice, cycle, DocumentType.CostEstimate);
                costEstimateRepository.AddPdf(invoice.Id, pdf);
                log.Debug($"PDF for Cost Estimate {invoice.InvoiceNr} (#{invoice.Id}) containing {pdf.Length} bytes saved");
            }
            else
            {
                pdf = pdfService.CreatePdf(invoice, cycle);
                invoiceRepository.AddPdf(invoice.Id, pdf);
                log.Debug($"PDF for Invoice {invoice.InvoiceNr} (#{invoice.Id}) containing {pdf.Length} bytes saved");
            }

            return pdf;
        }

        public void Approve(ProformaInvoice invoice, string reason, long? shortCode = null, string keyword = null)
        {
            if (invoice.InvoiceNr.Equals("Proforma", StringComparison.OrdinalIgnoreCase))
            {
                invoice.InvoiceNr = GetInvoiceNr(GetPaymentAccount(invoice.AccountKey));
                log.Debug($"Number for Invoice #{invoice.Id} changed to {invoice.InvoiceNr}");
            }
            invoice.Status = "PostPaidApproved";
            postPaidBillingRepository.Approve(invoice, reason, shortCode, keyword);
            log.Debug($"Invoice {invoice.InvoiceNr} (#{invoice.Id}) approved");
        }

        public void Decline(ProformaInvoice invoice, string reason, long? shortCode = null, string keyword = null)
        {
            if (invoice.Id <= 0)
            {
                invoice.InvoiceNr = "Declined";
                var account = GetPaymentAccount(invoice.AccountKey);
                invoice.Id = Insert(invoice, account);
                log.Info($"Invoice {invoice.InvoiceNr} created for {invoice.AccountName} ({account.AccountType} {account.Id}) based on {invoice.Cycle.ItemDescription} {invoice.Cycle.BreakdownDescription} as #{invoice.Id}");
            }

            if (invoice.InvoiceNr.Equals("Proforma", StringComparison.OrdinalIgnoreCase))
            {
                invoice.InvoiceNr = "Declined";
                log.Debug($"Number for Invoice #{invoice.Id} changed to {invoice.InvoiceNr}");
            }
            invoice.Status = "PostPaidDeclined";
            postPaidBillingRepository.Decline(invoice, reason, shortCode, keyword);
            log.Debug($"Invoice #{invoice.Id} declined");
        }

        /// <summary>
        /// Start checking for the start of a new post-paid billing cycle for automatically generated proforma invoices.
        /// </summary>
        public void Start()
        {
            timer.Start();
        }

        /// <summary>
        /// Stop checking for the start of a new post-paid billing cycle for automatically generated proforma invoices.
        /// </summary>
        public void Stop()
        {
            timer.Stop();
        }

        /// <summary>
        /// The first day of post-paid billing cycle for automatically generated proforma invoices.
        /// </summary>
        public DateTime? BillingCycleStart { get; set; }


        public ProformaInvoice AddDiscountLine(ProformaInvoice proformaInvoice)
        {
            var discount = discountRepo.GetByPastelCode(proformaInvoice.PastelCode);
            if (discount != null)
            {
                proformaInvoice.AddLineItem(discount.ToLineItem());
            }

            return proformaInvoice;
        }
        
        /// <summary>
        /// Check for the start of a new post-paid billing cycle for automatically generated proforma invoices.
        /// </summary>
        private void CheckForNewBillingCycle(object sender, ElapsedEventArgs e)
        {
            if (isProcessing) return;
            if(!BillingCycleStart.HasValue) return;
            var startDate = BillingCycleStart.Value.Date;
            var nextMonth = BillingCycleStart.Value.Date.AddMonths(1);
            var success = true;

            isProcessing = true;
            var automaticallyGeneratedProformaInvoiceTypes = new[]
            {
                //Post-Paid in arrears:
                PostPaidBillingCycleFilterType.BulkSmsUsage,
                PostPaidBillingCycleFilterType.ReverseBilledReplies,
                PostPaidBillingCycleFilterType.ReverseBilledShortCodeMessages,
                //Post-Paid in advance
                PostPaidBillingCycleFilterType.AllShortCodeRental
            };
            foreach (var filterType in automaticallyGeneratedProformaInvoiceTypes)
            {
                try
                {
                    var proformaInvoices =
                        GetAllPostPaidProformaInvoices(filterType,
                                filterType == PostPaidBillingCycleFilterType.AllShortCodeRental
                                    ? nextMonth //Provided since it can be manipulated
                                    : (DateTime?)null /*Not provided since it is based on the availability of previously collected data*/)
                            .ToArray();
                    log.Debug($"{proformaInvoices.Length} x {filterType} potential proforma invoices for the billing cycle starting at {BillingCycleStart:dd MMM yyyy}");
                    var inAdvance =
                    (
                        filterType == PostPaidBillingCycleFilterType.AllShortCodeRental ||
                        filterType == PostPaidBillingCycleFilterType.DedicatedShortCodeRental ||
                        filterType == PostPaidBillingCycleFilterType.SharedShortCodeKeywordRental
                    );
                    foreach (var proformaInvoice in proformaInvoices)
                    {
                        if (proformaInvoice.Id != 0)
                        {
                            log.Info($"{proformaInvoice.GetLineItemType()} Proforma Invoice for {proformaInvoice.AccountName} already exists {proformaInvoice}");
                            continue;
                        }
                        
                        if (proformaInvoice.Cycle.CycleStartDate.Date != startDate && !inAdvance)
                        {
                            log.Info($"{proformaInvoice.GetLineItemType()} Proforma Invoice for {proformaInvoice.AccountName} skipped since {proformaInvoice.Cycle} is not in arrears");
                            continue;
                        }

                        if (proformaInvoice.Cycle.CycleStartDate.Date != nextMonth && inAdvance)
                        {
                            log.Info($"{proformaInvoice.GetLineItemType()} Proforma Invoice for {proformaInvoice.AccountName} skipped since {proformaInvoice.Cycle} is not in advance");
                            continue;
                        }
                        
                        try
                        {
                            var invoice = AddDiscountLine(proformaInvoice);
                            InsertProformaInvoice(invoice, string.Empty);
                        }
                        catch (Exception exception)
                        {
                            log.Exception("CheckForNewBillingCycle", exception);
                        }
                    }
                }
                catch (Exception exception)
                {
                    log.Exception("CheckForNewBillingCycle", exception);
                    success = false;
                }
            }

            if (success)
            {
                BillingCycleStart = null;
            }
            isProcessing = false;
        }
    }
}
