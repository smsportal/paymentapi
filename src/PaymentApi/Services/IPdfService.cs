﻿using System.Net;
using System.Net.Http;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Services
{
    public interface IPdfService
    {
	    byte[] CreatePdf(CreditNoteModel creditNote);

	    byte[] CreatePdf(Invoice invoice);

        byte[] CreatePdf(Invoice invoice, PostPaidBillingCycle cycle);

        byte[] CreatePdf(Invoice invoice, PostPaidBillingCycle cycle, DocumentType docType);

        HttpResponseMessage CreateResponse(byte[] bytes, string filename, HttpStatusCode statusCode);
    }
}
