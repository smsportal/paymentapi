using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Services
{
    public interface IProformaInvoiceService
    {
        IEnumerable<ProformaInvoice> GetAllPostPaidProformaInvoices(
            PostPaidBillingCycleFilterType filterType,
            DateTime? invoiceDate);
        
        ProformaInvoice GetPostPaidProformaInvoice(int invoiceId);
        
        ProformaInvoice GetPostPaidProformaInvoice(
            PostPaidBillingCycleFilterType filterType, 
            AccountKey accountKey,
            DateTime invoiceDate);

        ProformaInvoice GetPostPaidProformaInvoice(
            AccountKey accountKey,
            long eventId);

        ProformaInvoice GetPostPaidProformaInvoice(
            AccountKey accountKey,
            string shortCodeUsername,
            DateTime setupDate,
            string shortCode,
            string keyword,
            long? dataId);
        
        void InsertCostEstimate(ProformaInvoice invoice);
        
        void InsertProformaInvoice(
            ProformaInvoice invoice,
            string orderNumber);

        void InsertInvoice(
            ProformaInvoice invoice,
            string orderNumber);

        byte[] CreatePdf(Invoice invoice, PostPaidBillingCycle cycle);

        void Approve(
            ProformaInvoice invoice,
            string reason,
            long? shortCode = null,
            string keyword = null);
        
        void Decline(
            ProformaInvoice invoice,
            string reason,
            long? shortCode = null,
            string keyword = null);
        
        DateTime? BillingCycleStart { get; set; }
        
        void Start();

        void Stop();

        ProformaInvoice AddDiscountLine(ProformaInvoice proformaInvoice);
    }
}