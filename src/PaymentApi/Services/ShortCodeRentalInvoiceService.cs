﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using SmsPortal.Core.Time;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Services
{
    public class ShortCodeRentalInvoiceService : IShortCodeRentalInvoiceService
    {
        private readonly ILog log = null;
        private readonly IShortCodeRentalRepository shortCodeRentalRepository;
        private readonly ISiteRepository siteRepository;
        private readonly IPostPaidPaymentAccountService postPaidPaymentAccountService;
        private readonly IBillingGroupOrDetailsRepository billingDetailsRepository;
        private readonly ITaxConfigRepository taxConfigRepository;

        public ShortCodeRentalInvoiceService(
            IShortCodeRentalRepository shortCodeRentalRepository,
            ISiteRepository siteRepository,
            IBillingGroupOrDetailsRepository billingDetailsRepository,
            ITaxConfigRepository taxConfigRepository,
            IPostPaidPaymentAccountService postPaidPaymentAccountService,
            ILog log = null)
        {
            this.shortCodeRentalRepository = shortCodeRentalRepository;
            this.siteRepository = siteRepository;
            this.billingDetailsRepository = billingDetailsRepository;
            this.taxConfigRepository = taxConfigRepository;
            this.postPaidPaymentAccountService = postPaidPaymentAccountService;
            this.log = log ?? LogManager.GetLogger(typeof(ProformaInvoiceService));
        }

        private IEnumerable<PostPaidBillingCycleLineItem> GetLineItems(DateTime invoiceDate)
        {
            var accounts = new Dictionary<AccountKey?, PaymentAccount>();
            var perAccount = new Dictionary<AccountKey, List<TaggedPostPaidBillingCycleLineItem<ShortCodeRentalApplicability>>>();
            var rentals = shortCodeRentalRepository.GetAllShortCodeRentalApplicability(invoiceDate).ToArray();
            var acceptable = AcceptableLineItemBreakdownTypes(InvoiceLineItemType.ShortCodeRental, out var fallBack).ToArray();
            
            //First identify and retrieve all the accounts 
            foreach (var rental in rentals)
            {
                var accKey = rental.IdentifyAccount();
                if (!accKey.HasValue) continue;

                if (accounts.ContainsKey(accKey)) continue;
                
                try
                {
                    if (postPaidPaymentAccountService.TryGetPaymentAccount(accKey.Value, out var account))
                    {
                        if (!acceptable.Contains(account.ShortCodeBillingType))
                        {
                            log.Warn($"Billing type for {accKey.Value} forced to {fallBack} since {account.ShortCodeBillingType} is not supported");
                            account.ShortCodeBillingType = fallBack;
                        }
                        accounts[accKey.Value] = account;
                    }
                }
                catch (Exception e)
                {
                    log.Warn($"Could not retrieve the payment account for {accKey.Value}", e);
                }
            }

            //Then work out the billing
            foreach (var rental in rentals)
            {
                var accKey = rental.IdentifyAccount();
                if (!accKey.HasValue || !accounts.ContainsKey(accKey.Value))
                {
                    log.Warn($"{rental} could not be associated with a payment account");
                    continue;
                }
                
                var paymentAccount = accounts[accKey.Value];
                if (!perAccount.ContainsKey(paymentAccount.AccountKey))
                {
                    perAccount[paymentAccount.AccountKey] = new List<TaggedPostPaidBillingCycleLineItem<ShortCodeRentalApplicability>>();
                }

                if (rental.WasAccountRecentlySetup())
                {
                    log.Info($"{rental} ignored since it was configured in the same billing cycle");
                    continue;
                }

                var dedicated = rental.IsDedicated();
                perAccount[paymentAccount.AccountKey].Add(new TaggedPostPaidBillingCycleLineItem<ShortCodeRentalApplicability>()
                {
                    SiteId = paymentAccount.SiteId,
                    AccountKey = paymentAccount.AccountKey,
                    ClientRefCode = paymentAccount.ClientRefCode,
                    Username = paymentAccount.Username,
                    Fullname = paymentAccount.Fullname,
                    Company = paymentAccount.Company,
                    PaymentCurrency = paymentAccount.PricingSCCurrency,
                    //NOTE: ShortCodeBillingType and not PostPaidBillingType since we are doing shortcode rental invoices
                    BillingType = paymentAccount.ShortCodeBillingType,
                    BillingCycle = BillingPeriod.Monthly,
                    CycleStartDate = rental.CycleStartDate,
                    CycleEndDate = rental.CycleEndDate,
                    MsgCount = 0,
                    Quantity = 1,
                    UnitPrice = dedicated
                        ? paymentAccount.PricingSCMonthly
                        : paymentAccount.PricingSCShared,
                    Code = dedicated
                        ? InvoiceLineItemType.ShortCodeRental
                        : InvoiceLineItemType.ShortCodeKeywordRental,
                    Description = rental.ToShortCodeKeywordDescription(paymentAccount.AccountLinkName),
                    Tag = rental,
                    DataId = rental.AllocationId 
                });
            }
            
            //Then remove the keywords on already billed dedicated shortcodes
            return perAccount.SelectMany(acc => acc.Value
                .Where(line =>
                    {
                        if (line.Description.StartsWith(ShortCodeRentalApplicability.Tag(line.Tag.ShortCode)))
                        {
                            log.Debug($"{line} included as dedicated shortcode billing");
                            return true;
                        }

                        if (acc.Value.Any(l => l.Description.StartsWith(ShortCodeRentalApplicability.Tag(line.Tag.ShortCode))))
                        {
                            log.Info($"{line} ignored as a sub-account of the master that was billed for the dedicated shortcode");
                            return false;
                        }

                        log.Debug($"{line} included as an unrelated account");
                        return true;
                    }
                )
                .OrderBy(l => l.Description)
                .Select(l => l as PostPaidBillingCycleLineItem)
                .ToList()
            );
        }

        public IEnumerable<ProformaInvoice> GetAllProformaInvoices(
            InvoiceLineItemType? lineItemType = null,
            AccountKey? accountKey = null,
            DateTime? invoiceDate = null)
        {
            invoiceDate = invoiceDate ?? TimeProvider.Current.Today;
            var srcLines =
                GetLineItems(invoiceDate ?? TimeProvider.Current.Today)
                    .Where(l => !lineItemType.HasValue || l.Code == lineItemType)
                    .Where(l => !accountKey.HasValue || l.AccountKey == accountKey)
                    .ToArray();

            var site = siteRepository.Get(1);
            var resellerBillingDetails = billingDetailsRepository.Get(site.AccountKey);
            var taxConfig = taxConfigRepository.GetConfig(resellerBillingDetails.CountryId);

            if(lineItemType.HasValue) return GetAllProformaInvoices(srcLines, lineItemType.Value, taxConfig);
            
            var results = new List<ProformaInvoice>();
            results.AddRange(GetAllProformaInvoices(srcLines, InvoiceLineItemType.ShortCodeRental, taxConfig));
            results.AddRange(GetAllProformaInvoices(srcLines, InvoiceLineItemType.ShortCodeKeywordRental, taxConfig));
            return results;
        }
        
        private IEnumerable<ProformaInvoice> GetAllProformaInvoices(
            PostPaidBillingCycleLineItem[] srcLines,
            InvoiceLineItemType lineItemType,
            TaxConfig taxConfig)
        {
            InvoiceLineItemType invoiceType; 
            switch (lineItemType)
            {
                case InvoiceLineItemType.ShortCodeKeywordRental:
                case InvoiceLineItemType.ShortCodeRental:
                {
                    invoiceType = lineItemType; 
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException(nameof(lineItemType),
                        $"{lineItemType} is not a short code rental line item type");
            }
            
            var billingAddresses = new Dictionary<AccountKey?, BillingDetails>();
            var addressedSrcLines =
                srcLines
                    .GroupBy(l => l.AccountKey)
                    .SelectMany(grouping =>
                    {
                        var accKey = grouping.Key;
                        try
                        {
                            var billingAddress = billingDetailsRepository.Get(accKey);
                            billingAddresses[accKey] = billingAddress;
                            return grouping.Select(l =>
                            {
                                l.Company = billingAddress.CompanyName;
                                l.CountryId = billingAddress.CountryId;
                                return l;
                            });
                        }
                        catch (Exception e)
                        {
                            log.Warn($"Could not retrieve the billing address for {accKey}", e);
                            return grouping.Select(l => l); //Unchanged
                        }
                    })
                    .ToArray();
            
            return GetAllAcceptableLineItemBreakdownProformaInvoices(addressedSrcLines, taxConfig, invoiceType)
                .Select(inv =>
                {
                    if (billingAddresses.ContainsKey(inv.AccountKey))
                    {
                        inv.BillingAddress = billingAddresses[inv.AccountKey];
                    }
                    return inv;
                });
        }

        public ProformaInvoice GetProformaInvoice(
            AccountKey accountKey,
            DateTime invoiceDate,
            InvoiceLineItemType lineItemType)
        {
            return GetAllProformaInvoices(
                    lineItemType,
                    accountKey,
                    invoiceDate)
                .First();
        }
        
        public IEnumerable<LineItemBreakdownType> AcceptableLineItemBreakdownTypes(InvoiceLineItemType invoiceType, out LineItemBreakdownType defaultBreakdownType)
        {
            defaultBreakdownType = LineItemBreakdownType.Unknown;
            return new[]
            {
                LineItemBreakdownType.Unknown,
                LineItemBreakdownType.PerRefCodeByMasterAccountUsername,
                LineItemBreakdownType.PerMasterAccountByUsername
            };
        }

        private IEnumerable<ProformaInvoice> GetAllAcceptableLineItemBreakdownProformaInvoices(
            PostPaidBillingCycleLineItem[] srcLines,
            TaxConfig taxConfig,
            InvoiceLineItemType invoiceType)
        {
            var results = new List<ProformaInvoice>();
            var acceptable = AcceptableLineItemBreakdownTypes(invoiceType, out _).ToArray();
            foreach (var breakdownType in acceptable)
            {
                var invoices =
                    srcLines
                        .ToProformaInvoices(
                            taxConfig,
                            invoiceType,
                            breakdownType)
                        .ToArray();

                results.AddRange(invoices);
            }

            var unSupported = srcLines.Where(l => !acceptable.Contains(l.BillingType)); 
            foreach (var line in unSupported)
            {
                log.Debug($"{line} ignored since {line.BillingType} is not supported for shortcode billing");
            }
            return results;
        }
    }
}