﻿using System;
using log4net;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Services
{
	public class CreditAllocationService : ICreditAllocationService
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(CreditAllocationService));

		private readonly ICreditAllocationRepository creditAllocationRepository;
		private readonly ISiteRepository siteRepository;
		private readonly IInvoiceRepository invoiceRepo;
		private readonly IEmailService emailService;
		private const int SmsPortalSiteId = 1;
		
		public CreditAllocationService(
			ICreditAllocationRepository creditAllocationRepository, 
			IEmailService emailService,
			ISiteRepository siteRepository, 
			IInvoiceRepository invoiceRepo)
		{
			this.creditAllocationRepository = creditAllocationRepository;
			this.siteRepository = siteRepository;
			this.invoiceRepo = invoiceRepo;
			this.emailService = emailService;
		}

		public void ProcessTransaction(PaymentAccount account, Payment payment, string gatewayTranId)
		{
			var transaction = GetTransaction(payment);
			if (account.AccountType == AccountType.ResellerAccount)
			{
				if (!creditAllocationRepository.ResellerAllocationExists(account.SiteId, transaction.TransactionId))
				{
					creditAllocationRepository.ResellerAllocation(account, transaction, payment);
					CloseInvoice(payment, account);
					SendCreditsAllocatedEmail(payment, account);
				}
			}
			else
			{
				if (creditAllocationRepository.SiteCreditAllocationExists(account.Id, transaction.TransactionId))
				{
					Log.Warn("Ignoring allocation request. Allocation already exists for transaction " +
					         $"{transaction.TransactionId} for account {account.Username}");
					return;

				}
				
				var site = siteRepository.Get(account.SiteId);
				SendPaymentNotification(payment, account, site);
			
				if (site.AutoCreditSiteUsers)
				{
					creditAllocationRepository.SiteCreditAllocation(account, transaction, payment.Gateway, payment.Fee, gatewayTranId);
					CloseInvoice(payment, account);
					SendCreditsAllocatedEmail(payment, account);
					return;
				}

				if (account.CreditCardAuthRequired)
				{
					if (!creditAllocationRepository.CreditCardAllocationExists(account.Id, transaction.TransactionId))
					{
						creditAllocationRepository.CreditCardAllocation(account, transaction, transaction.TransactionId, payment.Gateway, payment.Fee, gatewayTranId);
						//DO NOT CloseInvoice(payment, account); SINCE THE AUTHORIZATION WILL TAKE CARE OF THAT  
						SendWaitingAutorizationEmail(payment, account);
					}
				}
				else
				{
					creditAllocationRepository.SiteCreditAllocation(account, transaction, payment.Gateway, payment.Fee, gatewayTranId);
					CloseInvoice(payment, account);
					SendCreditsAllocatedEmail(payment, account);
				}
			}
		}

		private TransactionInfo GetTransaction(Payment payment)
		{
			var transaction = new TransactionInfo();
			transaction.TransactionId = payment.TransactionId.ToString();
			transaction.Total = payment.Total;
			transaction.Currency = payment.Currency;
			transaction.Credits = payment.Credits;
			transaction.TaxValue = payment.TaxValue;
			if (payment.InvoiceId.HasValue)
			{
				transaction.InvoiceId = payment.InvoiceId.Value;
			}

			return transaction;
		}

		private void SendPaymentNotification(Payment payment, PaymentAccount account, Site site)
		{
			try
			{
				if (!(string.IsNullOrEmpty(site.ContactUsEmail) || site.IsSmsportal))
				{
					emailService.SendPaymentNotification(payment, account, site.ContactUsEmail, site.Id);				
				}
			}
			catch (Exception e)
			{
				Log.Exception($"Couldn't send payment notification to reseller [{site.Name}] for payment [[{payment.TransactionId}].", e);
			}
		}

		private void CloseInvoice(Payment payment, PaymentAccount account)
		{
			if (!string.IsNullOrEmpty(payment.InvoiceRefNr))
			{
				var invoice = invoiceRepo.Get(payment.InvoiceRefNr);
				invoice.Close(payment.TransactionId);
				invoiceRepo.Update(invoice);    
			}
		}

		private void SendWaitingAutorizationEmail(Payment payment, PaymentAccount account)
		{
			emailService.SendPaymentAuthorizationEmail(account, payment, account.BuysFromSite);
		}

	    private void SendCreditsAllocatedEmail(Payment payment, PaymentAccount account)
	    {
			if (!payment.RouteCountryId.HasValue)
			{
				Log.Warn("Couldn't determine route country ID. Cannot send email");
			}
			else
			{
				var creditPool = account.GetCreditPool(payment.RouteCountryId.Value);
				emailService.SendCreditAllocationEmail(account.Fullname, 
					(creditPool.Balance + payment.Credits).ToString(), 
					payment.Credits.ToString(), 
					account.Username,
					account.EmailTo, 
					creditPool.Country,
					account.BuysFromSite,
					account.Id);
			}
		}
	}
}
