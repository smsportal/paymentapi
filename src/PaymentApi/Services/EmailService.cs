﻿using System;
using EmailerCommunication.Contract;
using log4net;
using Newtonsoft.Json;
using SmsPortal.Plutus.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SmsPortal.Core.RabbitMQ;
using SmsPortal.Core.RabbitMQ.ChannelManager;
using SmsPortal.Core.RabbitMQ.Json;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Services
{
    public class EmailService : IEmailService
    {
        private readonly object channelLock = new object();
        private readonly IChannelManager channelManager;
        private readonly string routingKey;
        private readonly ISiteRepository siteRepository;
        private readonly CancellationTokenSource tokenSource;
        private readonly bool waitForConnections;
        private bool isDisposed;

        public const string CreditAllocationTemplate = "CreditAllocation";
        public const string PaymentAuthorizationTemplate = "PaymentAuthorization";
        public const string PaymentNotificationTemplate = "PaymentNotification";
        public const string EventCancelNotificationTemplate = "EventCancelledEventRefundNotification";
        public const string QuoteCreatedNotificationTemplate = "QuoteCreatedNotification";
        public const string InvoicePaidTemplate = "InvoicePaid";
        public const string InvoiceUnpaidPrePaidTemplate = "InvoiceUnpaidPrePaid";
        public const string InvoiceCreatedTemplate = "InvoiceCreated";
        public const string InvoiceUnpaidEftTemplate = "InvoiceUnpaidEft";
        public const string InvoiceUnpaidPostPaidTemplate = "InvoiceUnpaidPostPaid";
        public const string InvoiceUnpaidCampaignTemplate = "InvoiceUnpaidCampaign";
        public const string CostEstimateTemplate = "CostEstimate";
        public const string CreditNoteExpiredInvoiceTemplate = "CreditNoteExpiredInvoice";
        public const string CreditNoteCancelledInvoiceTemplate = "CreditNoteCancelledInvoice";
        
        private ISendingRabbitChannel channel;
        private const string DelimiterString = ",;";
        private const string DateFormat = "dd MMM yyyy";
        private const string DateMonthlyFormat = "MMM yyyy";
        private const string TimeFormat = "HH:mm";
        private const string SpacedIntegerFormat = "### ### ##0";
        private const string SpacedDecimalFormat2 = "### ### ##0.00";
        private const string SpacedDecimalFormat4 = "### ### ##0.0000";
        private const string DenseDecimalFormat = "0.##";
        private const string Source = "Plutus";
        private const int SmsPortal = 1;

        private ILog Log => LogManager.GetLogger(GetType());
        
        public EmailService(
            ISiteRepository siteRepository, 
            IChannelManager channelManager,
            bool waitForConnections,
            string routingKey)
        {
            this.routingKey = routingKey;
            this.siteRepository = siteRepository;
            this.channelManager = channelManager;
            this.waitForConnections = waitForConnections;
            this.tokenSource = new CancellationTokenSource();
        }

        public void SendCreditAllocationEmail(
            string contactName, 
            string balance, 
            string creditsLoaded, 
            string username, 
            string destinationEmail,
            string country, 
            int siteId, 
            int? loginId = null)
        {
            var destination = new EmailAddressDto(destinationEmail);
            var parameters = new CreditAllocationEmailParams
            {
                ContactName = contactName,
                Balance = balance,
                CreditsLoaded = creditsLoaded,
                UserName = username,
                Country = country
            };
            Enqueue(destination, CreditAllocationTemplate, parameters, siteId, loginId);
        }

        public void SendCancelRefundEmail(
            RefundAccount refundAccount, 
            RefundAccount cancellingAccount, 
            EventRefundResult evt, 
            int siteId)
        {
            var to = new EmailAddressDto(refundAccount.Email);
            var parameters = new CancelRefundNotificationParams
            {
                ContactName = refundAccount.Fullname,
                EventUsername = cancellingAccount.Username,
                EventCreatedDate = evt.Created.ToString(DateFormat),
                EventCreatedTime = evt.Created.ToString(TimeFormat),
                EventScheduledDate = evt.Scheduled.ToString(DateFormat),
                EventScheduledTime = evt.Scheduled.ToString(TimeFormat),
                RefundedMessages = evt.RefundMessageCount.ToString(SpacedIntegerFormat).Trim(),
                RefundedCredits = evt.RefundMessageValue.ToString(SpacedIntegerFormat).Trim(),
                ExampleMessage = evt.ExampleMessage
            };
            Enqueue(to, EventCancelNotificationTemplate, parameters, siteId, refundAccount.AccountKey.Id);
        }

        public void SendQuoteCreatedEmail(Invoice invoice, 
            byte[] pdfBytes, 
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"Quote_{invoice.InvoiceNr}.pdf",
                Data = pdfBytes
            };
            var to = new EmailAddressDto(invoice.BillingAddress.InvoiceEmail);
            var parameters = new QuoteCreatedEmailParams
            {
                QuoteReference = invoice.InvoiceNr,
                ContactName = invoice.CustomerRef
            };
            Enqueue(new List<Attachment>{ attachment }, new List<EmailAddressDto>{ to }, QuoteCreatedNotificationTemplate, parameters, siteId, invoice.LoginId);
        }

        public void SendPaymentAuthorizationEmail(PaymentAccount account, 
            Payment payment, 
            int siteId)
        {
            var site = siteRepository.Get(siteId);
            var to = new EmailAddressDto(site.IsSmsportal ? "info@smsportal.com" : site.ContactUsEmail);
            var parameters = new PaymentAuthorizationParams
            {
                Payment = payment.Total.ToString(DenseDecimalFormat),
                AccountName = account.Username,
                Credits = payment.Credits.ToString()
            };
            Enqueue(to, PaymentAuthorizationTemplate, parameters, siteId, account.Id);
        }

        public void SendPaymentNotification(Payment payment, 
            PaymentAccount account, 
            string destination, 
            int siteId)
        {
            var to = new EmailAddressDto(destination);
            var subTotal = payment.Total - payment.Fee - payment.TaxValue;
            var parameters = new PaymentNotificationParams()
            {
                PaymentMethod = payment.Gateway,
                Credits = payment.Credits.ToString(),
                ClientCreditCost = subTotal.ToString(DenseDecimalFormat),
                ClientVat = payment.TaxValue.ToString(DenseDecimalFormat),
                ClientTransactionCost = payment.Fee.ToString(DenseDecimalFormat),
                ClientTotalCost = payment.Total.ToString(DenseDecimalFormat),
                Username = account.Username,
                Currency = payment.Currency
            };
            Enqueue(to, PaymentNotificationTemplate, parameters, siteId);
        }
        
        public void SendInvoiceExpiredEmail(CreditNoteModel creditNote, 
            byte[] pdfBytes,
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"CreditNote_{creditNote.ReferenceNumber}.pdf",
                Data = pdfBytes
            };
            var destinations = ToEmailAddressDtos(creditNote.ClientBillingDetails.InvoiceEmail);
            var parameters = new InvoiceExpiredEmailParams
            {
                ContactName = creditNote.CustomerRefCode,
                CreditNoteReference = creditNote.ReferenceNumber,
                InvoiceReference = creditNote.InvoiceReferenceNumber
            };
            Enqueue(new List<Attachment> { attachment }, destinations, CreditNoteExpiredInvoiceTemplate, parameters, siteId, creditNote.LoginId);
        }

        public void SendInvoiceCancelledEmail(CreditNoteModel creditNote, 
            byte[] pdfBytes,
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"CreditNote_{creditNote.ReferenceNumber}.pdf",
                Data = pdfBytes
            };
            var destinations = ToEmailAddressDtos(creditNote.ClientBillingDetails.InvoiceEmail);
            var parameters = new InvoiceCancelledEmailParams
            {
                ContactName = creditNote.CustomerRefCode,
                CreditNoteReference = creditNote.ReferenceNumber,
                InvoiceReference = creditNote.InvoiceReferenceNumber
            };
            Enqueue(new List<Attachment> { attachment }, destinations, CreditNoteCancelledInvoiceTemplate, parameters, siteId, creditNote.LoginId);
        }

        public void SendEftInvoiceCreatedEmail(PaymentAccount paymentAccount, 
            Invoice invoice, 
            byte[] pdfBytes,
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"Invoice_{invoice.InvoiceNr}.pdf",
                Data = pdfBytes
            };
            var destinations = ToEmailAddressDtos(invoice.BillingAddress.InvoiceEmail);
            var parameters = new EftInvoiceCreatedEmailParams
            {
                InvoiceReference = invoice.InvoiceNr,
                ContactName = paymentAccount.Fullname,
                UserName = paymentAccount.Username
            };
            Enqueue(new List<Attachment> { attachment }, destinations, InvoiceUnpaidEftTemplate, parameters, siteId, invoice.LoginId);
        }

        public void SendUnpaidShortCodeSetupInvoiceCreated(Invoice invoice, 
            string shortCodeDesc,
            byte[] pdfBytes,
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"Invoice_{invoice.InvoiceNr}.pdf",
                Data = pdfBytes
            };
            var destinations = ToEmailAddressDtos(invoice.BillingAddress.InvoiceEmail);
            var parameters = new UnpaidPostPaidInvoiceCreatedEmailParams
            {
                InvoiceReference = invoice.InvoiceNr,
                ContactName = invoice.CustomerRef,
                BillingPeriod = invoice.Created.ToString(DateFormat),
                BillingType = shortCodeDesc
            };
            Enqueue(new List<Attachment> { attachment }, destinations, InvoiceUnpaidPostPaidTemplate, parameters, siteId, invoice.LoginId);
        }

        public void SendUnpaidPostPaidInvoiceCreated(Invoice invoice, 
            byte[] pdfBytes,
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"Invoice_{invoice.InvoiceNr}.pdf",
                Data = pdfBytes
            };
            var destinations = ToEmailAddressDtos(invoice.BillingAddress.InvoiceEmail);
            var parameters = new UnpaidPostPaidInvoiceCreatedEmailParams
            {
                InvoiceReference = invoice.InvoiceNr,
                ContactName = invoice.CustomerRef,
                BillingPeriod = invoice.Created.ToString(DateMonthlyFormat),
                BillingType = string.Empty
            };
            Enqueue(new List<Attachment> { attachment }, destinations, InvoiceUnpaidPostPaidTemplate, parameters, siteId, invoice.LoginId);
        }

        public void SendUnpaidPrePaidInvoiceEmailCreated(Invoice invoice, 
            byte[] pdfBytes, 
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"Invoice_{invoice.InvoiceNr}.pdf",
                Data = pdfBytes
            };
            var destinations = ToEmailAddressDtos(invoice.BillingAddress.InvoiceEmail);
            var parameters = new UnpaidPrePaidInvoiceCreatedEmailParams
            {
                InvoiceReference = invoice.InvoiceNr,
                ContactName = invoice.CustomerRef
            };
            Enqueue(new List<Attachment> { attachment }, destinations, InvoiceUnpaidPrePaidTemplate, parameters, siteId, invoice.LoginId);
        }

        public void SendCostEstimateCreated(Invoice invoice, 
            byte[] pdfBytes,
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"CostEstimate_{invoice.InvoiceNr}.pdf",
                Data = pdfBytes
            };
            var destinations = ToEmailAddressDtos(invoice.BillingAddress.InvoiceEmail);
            var parameters = new CostEstimateCreatedEmailParams
            {
                CostEstimateReference = invoice.InvoiceNr,
                ContactName = invoice.CustomerRef,
                BillingPeriod = invoice.Created.ToString(DateMonthlyFormat)
            };
            Enqueue(new List<Attachment> { attachment }, destinations, CostEstimateTemplate, parameters, siteId, invoice.LoginId);
        }

        public void SendPaidInvoiceEmail(Invoice invoice, 
            string email, 
            byte[] pdfBytes, 
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"Invoice_{invoice.InvoiceNr}.pdf",
                Data = pdfBytes
            };
            var destinations = ToEmailAddressDtos(email);
            var parameters = new InvoiceCreatedEmailParams
            {
                InvoiceReference = invoice.InvoiceNr,
                ContactName = invoice.CustomerRef
            };
            Enqueue(new List<Attachment> { attachment }, destinations, InvoicePaidTemplate, parameters, siteId, invoice.LoginId);
        }

        public void SendPaidInvoiceEmail(InvoiceModel invoice,
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"Invoice_{invoice.ReferenceNumber}.pdf",
                Data = invoice.InvoiceBytes
            };
            var destinations = ToEmailAddressDtos(invoice.ClientBillingDetails.InvoiceEmail);
            var parameters = new InvoiceCreatedEmailParams
            {
                InvoiceReference = invoice.ReferenceNumber,
                ContactName = invoice.AccountName
            };
            Enqueue(new List<Attachment> { attachment }, destinations, InvoicePaidTemplate, parameters, siteId);
        }

        public void SendUnpaidCampaignInvoiceCreated(Invoice invoice, 
            PostPaidBillingCycle cmpgn,
            string email, 
            byte[] pdfBytes,
            int siteId)
        {
            var attachment = new Attachment
            {
                AttachmentName = $"Invoice_{invoice.InvoiceNr}.pdf",
                Data = pdfBytes
            };
            var destinations = ToEmailAddressDtos($"{invoice.BillingAddress.InvoiceEmail},{email}");
            var lines = invoice.GetLineItems();
            var first = lines.First();
            var parameters = new UnpaidCampaignInvoiceCreatedEmailParams
            {
                InvoiceReference = invoice.InvoiceNr,
                ContactName = invoice.CustomerRef,
                CampaignName = first.Description,
                CampaignCreatedDate = cmpgn.CycleStartDate.ToString(DateFormat),
                CampaignCreatedTime = cmpgn.CycleStartDate.ToString(TimeFormat),
                CampaignSendDate = cmpgn.CycleEndDate.ToString(DateFormat),
                CampaignSendTime = cmpgn.CycleEndDate.ToString(TimeFormat),
                CampaignNumberOfMessages = cmpgn.MsgCount.ToString(SpacedIntegerFormat).Trim(),
                CampaignNumberOfCredits = lines.Sum(l => l.Quantity).ToString(SpacedIntegerFormat).Trim(),
                ExampleMessage = first.OptionalNote ?? string.Empty,
                Currency = invoice.Currency,
                PricePerCredit = first.UnitPrice.ToString(SpacedDecimalFormat4).Trim(),
                TotalExcl = invoice.SubTotal.ToString(SpacedDecimalFormat2).Trim(),
                TaxAmount = invoice.TaxAmount.ToString(SpacedDecimalFormat2).Trim(),
                TotalIncl = invoice.Total.ToString(SpacedDecimalFormat2).Trim(),
            };
            Enqueue(new List<Attachment> { attachment }, destinations, InvoiceUnpaidCampaignTemplate, parameters, siteId, invoice.LoginId);
        }

        /// <summary>
        /// Creates a new <c>EmailMessageDto</c> and queues the message for sending.
        /// </summary>
        /// <param name="destination">Destination address of the email.</param>
        /// <param name="templateType">The name of the email template.</param>
        /// <param name="parameters">Parameters to populate the email template.</param>
        /// <param name="loginId">LoginId of the client receiving the email.</param>
        /// <param name="siteId">site of who is sending the email.</param>
        private void Enqueue(EmailAddressDto destination, 
            string templateType, 
            BaseEmailParams parameters, 
            int siteId, 
            int? loginId = null)
        {
            Enqueue(new List<Attachment>(), new List<EmailAddressDto> { destination }, templateType, parameters, siteId, loginId);
        }

        /// <summary>
        /// Creates a new <c>EmailMessageDto</c> and queues the message for sending.
        /// </summary>
        /// <param name="attachments">Email attachments.</param>
        /// <param name="destinations">Destination addresses of the email.</param>
        /// <param name="templateType">The name of the email template.</param>
        /// <param name="parameters">Paramters to populate the email template.</param>
        /// <param name="loginId">LoginId of the client receiving the email.</param>
        /// <param name="siteId">site of who is sending the email.</param>
        private void Enqueue(List<Attachment> attachments,
            List<EmailAddressDto> destinations,
            string templateType,
            BaseEmailParams parameters,
            int siteId,
            int? loginId = null)
        {
            var site = siteRepository.Get(siteId);
            var fromAddress = GetFromAddress(site);
            if (fromAddress != null)
            {
                var emailMessageDto = new EmailMessageDto
                {
                    Attachments = attachments,
                    LoginId = loginId,
                    SiteId = site.Id,
                    Source = Source,
                    Destinations = destinations,
                    FromAddress = fromAddress,
                    BccAddresses = new List<EmailAddressDto>(),
                    MessageTemplateType = templateType,
                    TemplateModel = JsonConvert.SerializeObject(parameters)
                };
                if (Log.IsDebugEnabled)
                {
                    Log.Debug($"Sending email {emailMessageDto}");
                }
                Enqueue(emailMessageDto);
            }
        }

        private EmailAddressDto GetFromAddress(Site site)
        {
            return site.IsSmsportal 
                ? new EmailAddressDto("noreply@smsportal.com", "SMSPortal") 
                : !string.IsNullOrEmpty(site.EmailAddress) 
                    ? new EmailAddressDto(site.EmailAddress) 
                    : null;
        }

        private void Enqueue(EmailMessageDto message)
        {
            lock (channelLock)
            {
                if (channel == null)
                {
                    channel = channelManager.GetSendingChannelAsync(
                        JsonMessageFormatter.Instance, 
                        true, 
                        waitForConnections, 
                        tokenSource.Token).Result;
                }

                channel.SendMessage(routingKey, message);
            }
        }

        private List<EmailAddressDto> ToEmailAddressDtos(string emails)
        {
            var list = new List<EmailAddressDto>();
            var emailsList = emails.Split(DelimiterString.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (var email in emailsList)
            {
                var dto = new EmailAddressDto(email);
                list.Add(dto);
            }
            return list;
        }

        public void Dispose()
        {
            if (isDisposed) return;
            tokenSource.Cancel();
            isDisposed = true;
        }
    }
}
