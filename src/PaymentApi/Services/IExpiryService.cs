﻿namespace SmsPortal.Plutus.Services
{
    public interface IExpiryService
    {
        /// <summary>
        /// Start checking for expired invoices.
        /// </summary>
        void Start();

        /// <summary>
        /// Stop checking for expired invoices.
        /// </summary>
        void Stop();
    }
}