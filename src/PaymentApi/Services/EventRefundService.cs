using System;
using log4net;
using RabbitMQ.Client.Events;
using SmsPortal.Core.RabbitMQ;
using SmsPortal.Core.RabbitMQ.ChannelManager;
using SmsPortal.Core.RabbitMQ.Json;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;
using SmsPortal.Plutus.Repositories;
using Valkyrie.Contract;

namespace SmsPortal.Plutus.Services
{
    public class EventRefundService : IEventRefundService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(EventRefundService));
        private readonly ICreditAllocationRepository creditAllocRepo;
        private readonly IEventRepository eventRepo;
        private readonly IEmailService emailService;
        private readonly ISiteRepository siteRepo;
        private readonly IRefundAccountRepository refundAccRepo;
        private readonly IChannelManager channelManager;
        private readonly string rabbitQueueName;
        private IReceivingRabbitChannel receivingChannel;

        public EventRefundService(ICreditAllocationRepository creditAllocRepo,
                                  IEventRepository eventRepo,
                                  IEmailService emailService,
                                  ISiteRepository siteRepo,
                                  IRefundAccountRepository refundAccRepo,
                                  IChannelManager channelManager,
                                  string rabbitQueueName)
        {
            this.creditAllocRepo = creditAllocRepo;
            this.eventRepo = eventRepo;
            this.emailService = emailService;
            this.siteRepo = siteRepo;
            this.refundAccRepo = refundAccRepo;
            this.channelManager = channelManager;
            this.rabbitQueueName = rabbitQueueName;
        }

        private ReceiveResult OnRefundRequestReceived(EventCancelNotification cancel, BasicDeliverEventArgs args)
        {
            var refund = eventRepo.DiscountCancelledEventMessages(
                cancel.AccountId,
                cancel.EventId,
                cancel.CancelledMessageCount,
                cancel.CancelledMessageValue);

            if(refund == null)
            {
                log.Warn($"Ignoring attempt to refund {cancel.CancelledMessageCount} messages for invalid event {cancel.EventId} to account {cancel.AccountId}");
                return ReceiveResult.Success;
            }
            
            var cancellingAccount = refundAccRepo.Get(new AccountKey(cancel.AccountId, AccountType.SmsAccount));

            if(TryGetAccountToRefund(cancellingAccount, out var refundAccount))
            {
                var refundEventAllocation = new RefundEventAllocation();
                refundEventAllocation.AccountKey = refundAccount.AccountKey;
                refundEventAllocation.Credits = (int) cancel.CancelledMessageValue;
                refundEventAllocation.Currency = refundAccount.Currency;
                refundEventAllocation.RouteCountryId = cancellingAccount.RouteCountryId;
                creditAllocRepo.RefundEventAllocation(refundEventAllocation);

                if (refundAccount.Equals(cancellingAccount))
                {
                    emailService.SendCancelRefundEmail(refundAccount, cancellingAccount, refund, refundAccount.SiteId);
                }
            }

            return ReceiveResult.Success;
        }

        private bool TryGetAccountToRefund(RefundAccount cancellingAccount, out RefundAccount refundAccount)
        {
            
            if (!cancellingAccount.IsPostpaid)
            {
                refundAccount = cancellingAccount;
                return true;
            }

            var siteId = cancellingAccount.SiteId;
            if (!cancellingAccount.IsMaster)
            {
                var masterAccount = refundAccRepo.Get(new AccountKey(cancellingAccount.MasterAccountId, AccountType.SmsAccount));
                if (!masterAccount.IsPostpaid)
                {
                    refundAccount = masterAccount;
                    return true;
                }

                siteId = masterAccount.SiteId;
            }

            
            if (siteId != 0)
            {
                var site = siteRepo.Get(cancellingAccount.SiteId);
                var reseller = refundAccRepo.Get(site.AccountKey);
                if (!reseller.IsPostpaid)
                {
                    refundAccount = reseller;
                    return true;
                }
            }

            refundAccount = null;
            return false;
        }

        public void Start()
        {
            if (receivingChannel != null)
            {
                log.Warn($"Attempted to create receiver for ({receivingChannel.QueueName}) which already exists.");
                return;
            }

            try
            {
                receivingChannel = channelManager.GetReceivingChannel<EventCancelNotification>(
                    rabbitQueueName, 
                    OnRefundRequestReceived, 
                    JsonMessageFormatter.Instance);
                receivingChannel.StartReceiving();
            }
            catch (Exception e)
            {
                log.Exception("Couldn't start the Refund Service.", e);
            }
        }

        public void Stop()
        {
            if (receivingChannel.IsReceiving)
            {
                receivingChannel.StopReceivingAndWait(); 
            }
        }

        public void Dispose()
        {
            Stop();
        }
    }
}