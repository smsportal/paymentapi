﻿using System.Collections.Generic;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Services
{
    public interface IPostPaidInvoiceService
    {
        /// <summary>
        /// Retrieve the list of acceptable (supported) line item breakdown types for the specified invoice type 
        /// </summary>
        /// <param name="invoiceType">The invoice type</param>
        /// <param name="defaultBreakdownType">A variable to receive the default line item breakdown type</param>
        /// <returns>A collection of line item breakdown types</returns>
        IEnumerable<LineItemBreakdownType> AcceptableLineItemBreakdownTypes(InvoiceLineItemType invoiceType, out LineItemBreakdownType defaultBreakdownType);
    }
}