﻿using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Repositories;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Services
{
    public class PostPaidPaymentAccountService : IPostPaidPaymentAccountService
    {
        private readonly IPaymentAccountRepository paymentAccountRepository;
        private readonly ISiteRepository siteRepo;

        public PostPaidPaymentAccountService(
            IPaymentAccountRepository paymentAccountRepository,
            ISiteRepository siteRepo)
        {
            this.paymentAccountRepository = paymentAccountRepository;
            this.siteRepo = siteRepo;
        }

        private PaymentAccount CorrectBuysFromSite(ref PaymentAccount account)
        {
            if (!string.IsNullOrEmpty(account.ClientRefCode)) return account;

            var site = siteRepo.Get(account.BuysFromSite);
            account.ClientRefCode = site.GetPastelCode(account.CountryOfResidence);
            return account;
        }
        
        public PaymentAccount GetPaymentAccount(AccountKey accountKey)
        {
            if(!TryGetPaymentAccount(accountKey, out var account))
            {
                account = PaymentAccount.GetTemporary(accountKey);
            }
            
            return CorrectBuysFromSite(ref account);
        }
        
        public bool TryGetPaymentAccount(AccountKey accountKey, out PaymentAccount account)
        {
            if (!paymentAccountRepository.TryGetPaymentAccount(accountKey, out account)) return false;
            
            var site = siteRepo.Get(account.SiteId);
            if (site.IsBilledAsSmsportal)
            {
                CorrectBuysFromSite(ref account);
                return true;
            }

            var nonSmsPortalUserAccount = account;
            if (!paymentAccountRepository.TryGetPaymentAccount(new AccountKey(account.SiteId, AccountType.SiteId), out var resellerAccount)) return true;
            
            //Since it is an SMS account on a valid reseller site, bill the reseller instead of the SMS account 
            account = resellerAccount;
            //Copy the user-account specific settings that takes precedence over the reseller account settings
            account.SetAccountLink(nonSmsPortalUserAccount);
            account.SetShortCodePricing(nonSmsPortalUserAccount);
            
            CorrectBuysFromSite(ref account);
            return true;
        }
    }
}