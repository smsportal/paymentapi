﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using EvoPdf;
using iTextSharp.text.pdf;
using log4net;
using Metrics;
using RazorEngine.Templating;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Services
{
	public class PdfService : IPdfService, IDisposable
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(PdfService));
	    private readonly DirectoryInfo templateDir;
	    private const string ImagesSubPath = "images";
	    private const string FontsSubPath = "fonts";
	    private const string SmsPortalLogoFile = "smsportal.png";
	    private const string InvoiceTemplate = "Invoice.cshtml";
	    private const string QuoteTemplate = "Quote.cshtml";
	    private const string CostEstimateTemplate = "CostEstimate.cshtml";
	    private const string CreditNoteTemplate = "CreditNote.cshtml";
	    private readonly IRazorEngineService razor;
	    private readonly DocumentImages smsPortalLogo;
	    private readonly Timer htmlConvert;
	    private readonly Timer pdfConvert;

	    public PdfService(string templatePath)
		{
			templateDir = new DirectoryInfo(templatePath);
		    if (!templateDir.Exists)
		    {
		        throw new InvalidOperationException($"{templatePath} is not a valid and existing directory");
		    }
		    htmlConvert = Metric.Timer("Razor-Time", Unit.Items);
		    pdfConvert = Metric.Timer("PDF-Convert", Unit.Items);
		    smsPortalLogo = new DocumentImages
		    {
			    RootPath = templateDir.FullName,
			    ImagesSubPath = ImagesSubPath,
			    FontsSubPath = FontsSubPath,
			    LogoFilename = SmsPortalLogoFile
		    };
		    razor = RazorEngineService.Create();
		    razor.AddTemplate(InvoiceTemplate, File.ReadAllText(GetTemplateFile(InvoiceTemplate)));
		    razor.Compile(InvoiceTemplate, typeof(InvoiceRazorModel));
		    razor.AddTemplate(QuoteTemplate, File.ReadAllText(GetTemplateFile(QuoteTemplate)));
		    razor.Compile(QuoteTemplate, typeof(InvoiceRazorModel));
		    razor.AddTemplate(CostEstimateTemplate, File.ReadAllText(GetTemplateFile(CostEstimateTemplate)));
		    razor.Compile(CostEstimateTemplate, typeof(InvoiceRazorModel));
		    razor.AddTemplate(CreditNoteTemplate, File.ReadAllText(GetTemplateFile(CreditNoteTemplate)));
		    razor.Compile(CreditNoteTemplate, typeof(CreditNoteRazorModel));
		}

        public byte[] CreatePdf(Invoice invoice)
        {
            return CreatePdf(invoice, null);
        }

        public byte[] CreatePdf(Invoice invoice, PostPaidBillingCycle cycle)
        {
            return CreatePdf(invoice, cycle, DocumentType.Invoice);
        }

        public byte[] CreatePdf(Invoice invoice, PostPaidBillingCycle cycle, DocumentType docType)
        {
            var model = new InvoiceModel();
            model.ClientRefCode = invoice.PastelCode;
            model.IsPostPaid = (cycle != null);
            model.ShowBillingPeriod = (cycle != null);
            if (cycle != null)
            {
                model.PeriodBilledFromDate = cycle.CycleStartDate;
                model.PeriodBilledToDate = cycle.CycleEndDate;
                model.PeriodBilledDescription = cycle.ItemDescription;
            }
            model.IsInternational = invoice.IsInternational;
            model.ClientBillingDetails = invoice.BillingAddress;
            model.OrderNumber = invoice.PurchaseOrderNr;
            model.CustomerRefCode = invoice.CustomerRef;
            model.ReferenceNumber = invoice.InvoiceNr;
            model.Currency = invoice.Currency;
            if (invoice.ConversionRate == 1 || invoice.ConversionRate == 0)
            {
	            invoice.ConversionRate = null;
            }
            model.ConversionRate = invoice.ConversionRate?.ToString("#.##");
            foreach (var lineItem in invoice.GetLineItems())
            {
                model.LineItems.Add(new DocumentLineItemModel()
                {
                    Description = lineItem.Description,
                    Price = lineItem.UnitPrice,
                    Quantity = lineItem.Quantity,
                    Total = lineItem.LineTotal,
                    OptionalNote = string.IsNullOrEmpty(lineItem.OptionalNote) ? String.Empty : lineItem.OptionalNote
                });
            }
            model.SubTotal = invoice.SubTotal;
            model.TaxValue = invoice.TaxAmount;
            model.TaxPercentage = invoice.TaxPercentage;
            model.Total = invoice.Total;
            model.CreatedDate = invoice.Created;
            if (invoice.DueDate.HasValue)
            {
                model.DueDate = invoice.DueDate.Value;
            }

            string html;
            switch (docType)
            {
                case DocumentType.CostEstimate:
                    html = CreateHtml(new InvoiceRazorModel { Invoice = model }, CostEstimateTemplate, typeof(InvoiceRazorModel));
                    break;
                case DocumentType.Quote:
                    html = CreateHtml(new InvoiceRazorModel { Invoice = model }, QuoteTemplate, typeof(InvoiceRazorModel));
                    break;
                case DocumentType.Invoice:
                    html = CreateHtml(new InvoiceRazorModel { Invoice = model }, InvoiceTemplate, typeof(InvoiceRazorModel));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(docType), docType, null);
            }

            if (Log.IsDebugEnabled)
            {
	            Log.Debug($"HTML for {invoice.InvoiceNr}:\r\n\r\n{html}\r\n\r\n");
            }
            
			return CreatePdfBytesFromHtml(html);
		}

		public byte[] CreatePdf(CreditNoteModel creditNote)
		{
			var html = CreateHtml(new CreditNoteRazorModel { CreditNote = creditNote}, CreditNoteTemplate, typeof(CreditNoteRazorModel));
			return CreatePdfBytesFromHtml(html);
		}

		public HttpResponseMessage CreateResponse(byte[] bytes, string filename, HttpStatusCode statusCode)
		{
			var response = new HttpResponseMessage(statusCode);
			response.Content = new StreamContent(new MemoryStream(bytes));
			response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
			response.Content.Headers.ContentLength = bytes.Length;
			response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
			response.Content.Headers.ContentDisposition.FileName = filename;
			return response;
		}

	    private string CreateHtml(RazorModel model, string docType, Type modelType)
	    {
		    return htmlConvert.Time(() =>
		    {
			    model.Images = smsPortalLogo;
			    return razor.Run(docType, modelType, model);
		    });
	    }
        
		private byte[] CreatePdfBytesFromHtml(string html)
		{
			var htmlBytes = GetPdfConverter().GetPdfBytesFromHtmlString(html);
			byte[] pdfBytes;
			return pdfConvert.Time(() =>
			{
				using (var input = new MemoryStream(htmlBytes))
				using (var output = new MemoryStream())
				{
					var reader = new PdfReader(input);
					PdfEncryptor.Encrypt(reader, output, true, null, "abcdef" /* TODO Generate random owner password */,
						PdfWriter.ALLOW_PRINTING);
					pdfBytes = output.ToArray();
				}


				return pdfBytes;
			});
		}

		private PdfConverter GetPdfConverter()
		{
			try
			{
				var pdfConverter = new PdfConverter();
				pdfConverter.LicenseKey = "zkBTQVJSQVNSUEFUT1FBUlBPUFNPWFhYWA==";
				pdfConverter.HtmlViewerWidth = Convert.ToInt32(EvoPdf.UnitsConverter.PixelsToPoints(1100));
				pdfConverter.PdfDocumentOptions.BottomMargin = 10;
				pdfConverter.PdfDocumentOptions.TopMargin = 10;
				pdfConverter.PdfDocumentOptions.LeftMargin = 12;
				pdfConverter.PdfDocumentOptions.RightMargin = 12;
				pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
				pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
				pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
				pdfConverter.PdfDocumentOptions.FitWidth = true;
				pdfConverter.JavaScriptEnabled = true;
				pdfConverter.PdfDocumentOptions.AvoidImageBreak = true;
				pdfConverter.PdfDocumentOptions.AvoidTextBreak = true;
				pdfConverter.PdfDocumentOptions.EmbedFonts = true;

				string path;
				if (AppDomain.CurrentDomain.RelativeSearchPath == null)
				{
					path = AppDomain.CurrentDomain.BaseDirectory;
				}
				else
				{
					path = AppDomain.CurrentDomain.RelativeSearchPath;
				}

				pdfConverter.EvoInternalFileName = System.IO.Path.Combine(path, "evointernal.dat");

				return pdfConverter;
			}
			catch (Exception e)
			{
				Log.Exception("Error creating PdfConverter", e);
				throw;
			}
		}

	    private string GetTemplateFile(string template)
	    {
	        return templateDir.FullName + Path.DirectorySeparatorChar + template;
	    }

	    public void Dispose()
	    {
		    razor.Dispose();
	    }
	}
}
