﻿using System;
using System.Collections.Generic;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Models.Enums;

namespace SmsPortal.Plutus.Services
{
    public interface IShortCodeRentalInvoiceService : IPostPaidInvoiceService
    {
        /// <summary>
        /// Retrieve all proforma invoices for the short code billing
        /// </summary>
        /// <param name="accountKey">The type of account and the unique identifier of the account</param>
        /// <param name="invoiceDate">The date of the invoice</param>
        /// <param name="lineItemType">The invoice line item type</param>
        /// <returns>A collection of proforma invoices</returns>
        IEnumerable<ProformaInvoice> GetAllProformaInvoices(
            InvoiceLineItemType? lineItemType = null,
            AccountKey? accountKey = null,
            DateTime? invoiceDate = null);
        
        /// <summary>
        /// Retrieve a single proforma invoice for the short code billing related to a specific account
        /// </summary>
        /// <param name="accountKey">The type of account and the unique identifier of the account</param>
        /// <param name="invoiceDate">The date of the invoice</param>
        /// <param name="lineItemType">The invoice line item type</param>
        /// <returns>A collection of proforma invoices</returns>
        ProformaInvoice GetProformaInvoice(
            AccountKey accountKey,
            DateTime invoiceDate,
            InvoiceLineItemType lineItemType);
    }
}