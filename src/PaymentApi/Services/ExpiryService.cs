﻿using System;
using System.Timers;
using log4net;
using SmsPortal.Plutus.Models;
using SmsPortal.Plutus.Repositories;

namespace SmsPortal.Plutus.Services
{
    /// <summary>
    /// The service expiring overdue invoices.
    /// </summary>
    public class ExpiryService: IExpiryService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ExpiryService));
        private readonly ExpiryConfigurationSection config;
        private readonly IPaymentAccountRepository paymentAccountRepository;
        private readonly IInvoiceRepository invoiceRepository;
        private readonly ILegacyInvoiceRepository legacyInvoiceRepository;
        private readonly ICreditNoteRepository creditNoteRepo;
        private readonly IPdfService pdfService;
        private readonly IEmailService emailer;
        private readonly Timer timer;
        private bool isProcessing = false;

        /// <summary>
        /// Constructor
        /// </summary>
        public ExpiryService(ExpiryConfigurationSection config, 
            IPaymentAccountRepository paymentAccountRepository, 
            IInvoiceRepository invoiceRepository, 
            ILegacyInvoiceRepository legacyInvoiceRepository, 
            ICreditNoteRepository creditNoteRepo, 
            IPdfService pdfService, 
            IEmailService emailer)
        {
            this.config = config;
            this.invoiceRepository = invoiceRepository;
            this.legacyInvoiceRepository = legacyInvoiceRepository;
            this.creditNoteRepo = creditNoteRepo;
            this.pdfService = pdfService;
            this.emailer = emailer;
            this.paymentAccountRepository = paymentAccountRepository;
            timer = new Timer(config.Interval.TotalMilliseconds);
            timer.Elapsed += CheckForExpiry;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        /// <summary>
        /// Check for expired invoices.
        /// </summary>
        private void CheckForExpiry(object sender, ElapsedEventArgs e)
        {
            if (isProcessing) return;
            isProcessing = true;
            try
            {
                foreach (ExpiryConfigurationSection.ExpiryConfigurationItem expirySetting in config.ExpiryList)
                {
                    var list = invoiceRepository.List(new ListInvoicesRequest { Status = expirySetting.Status });
                    foreach (var invoice in list)
                    {
                        if (!invoice.DueDate.HasValue)
                        {
                            log.Debug($"{invoice.Status} Invoice {invoice.InvoiceNr} for {invoice.AccountName} has no due date, so it cannot expire");
                            continue;
                        }

                        var overdue = (DateTime.Now - invoice.DueDate.Value);
                        if (overdue < expirySetting.Overdue)
                        {
                            if (overdue.TotalMilliseconds > 0)
                            {
                                log.Debug($"{invoice.Status} Invoice {invoice.InvoiceNr} for {invoice.AccountName} is overdue by {overdue} based on a due date of {invoice.DueDate.Value}");
                            }
                            continue;
                        }

                        if (!invoice.TransactionId.HasValue)
                        {
                            log.Debug($"{invoice.Status} Invoice {invoice.InvoiceNr} for {invoice.AccountName} has no transaction id, so it cannot expire");
                            continue;
                        }

                        try
                        {
                            var paymentAccount = paymentAccountRepository.GetPaymentAccount(invoice.AccountKey);
                            var creditNote = CreateCreditNote(paymentAccount, invoice.TransactionId.Value);
                        }
                        catch (Exception expiryErr)
                        {
                            log.Exception("Expiry Service", expiryErr);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                log.Exception("Expiry Service", exception);
            }
            isProcessing = false;
        }

        private CreditNoteModel CreateCreditNote(PaymentAccount paymentAccount, Guid transactionId)
        {
            var creditNote = legacyInvoiceRepository.ExpireInvoice(paymentAccount, transactionId.ToString());
            var pdf = pdfService.CreatePdf(creditNote);
            creditNoteRepo.AddPdf(creditNote.CreditNoteId, pdf);
            log.Info($"Invoice {creditNote.InvoiceReferenceNumber} for {creditNote.AccountName} expired and Credit Note {creditNote.ReferenceNumber} created");
            emailer.SendInvoiceExpiredEmail(creditNote, pdf, paymentAccount.BuysFromSite);
            return creditNote;
        }

        /// <summary>
        /// Start checking for expired invoices.
        /// </summary>
        public void Start()
        {
            timer.Start();
        }

        /// <summary>
        /// Stop checking for expired invoices.
        /// </summary>
        public void Stop()
        {
            timer.Stop();
        }
    }
}
