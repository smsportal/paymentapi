﻿using System;
using System.Net.Http;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus.Services
{
    public interface IEmailService : IDisposable
    {

        /// <summary>
        /// Send credit allocation email.
        /// </summary>
        void SendCreditAllocationEmail(
            string contactName, 
            string balance, 
            string creditsLoaded, 
            string username,
            string destinationEmail,
            string country, 
            int siteId, 
            int? loginId = null);

        void SendPaymentAuthorizationEmail(PaymentAccount account, Payment payment, int siteId);

        void SendPaymentNotification(Payment payment, PaymentAccount account, string destination, int siteId);
        
        void SendCancelRefundEmail(
            RefundAccount refundAccount,
            RefundAccount cancellingAccount,
            EventRefundResult evt,
            int siteId);

        void SendQuoteCreatedEmail(Invoice invoice, 
            byte[] pdfBytes, 
            int siteId);

        void SendPaidInvoiceEmail(Invoice invoice, 
            string email, 
            byte[] pdfBytes, 
            int siteId);

        /// <summary>
        /// Send the email after an invoice has expired.
        /// </summary>
        void SendInvoiceExpiredEmail(CreditNoteModel creditNote, 
            byte[] pdfBytes, 
            int siteId);

        /// <summary>
        /// Send the email after a user has cancelled an invoice.
        /// </summary>
        void SendInvoiceCancelledEmail(CreditNoteModel creditNote, 
            byte[] pdfBytes, 
            int siteId);

        /// <summary>
        /// Send the email after an EFT invoice was created.
        /// </summary>
        void SendEftInvoiceCreatedEmail(PaymentAccount account, 
            Invoice invoice, 
            byte[] pdfBytes, 
            int siteId);

        /// <summary>
        /// Send the email after an unpaid post-paid invoice related to an event was created.
        /// </summary>
        void SendUnpaidShortCodeSetupInvoiceCreated(Invoice invoice, 
            string shortCodeDesc, 
            byte[] pdfBytes, 
            int siteId);

        /// <summary>
        /// Send the email after an unpaid post-paid invoice related to an event was created.
        /// </summary>
        void SendUnpaidCampaignInvoiceCreated(Invoice invoice, 
            PostPaidBillingCycle cmpgn, 
            string email, 
            byte[] pdfBytes, 
            int siteId);

        /// <summary>
        /// Send the email after an unpaid post-paid invoice was created.
        /// </summary>
        void SendUnpaidPostPaidInvoiceCreated(Invoice invoice, 
            byte[] pdfBytes, 
            int siteId);

        /// <summary>
        /// Send the email after a cost estimate was created.
        /// </summary>
        void SendCostEstimateCreated(Invoice invoice, 
            byte[] pdfBytes, 
            int siteId);

        /// <summary>
        /// Send the email after an unpaid pre-paid invoice was created.
        /// </summary>
        void SendUnpaidPrePaidInvoiceEmailCreated(Invoice invoice, 
            byte[] pdfBytes, 
            int siteId);

        void SendPaidInvoiceEmail(InvoiceModel invoice, 
            int siteId);
    }
}
