﻿using System;
using log4net;
using SmsPortal.Plutus.Models;

namespace SmsPortal.Plutus
{
    public static class LoggingExtensions
    {
        public static void PaymentDebug(this ILog log, string message, string gatewayName, string paymentDebug = null, string postbackDebug = null)
        {
            log.Debug(message.ToPaymentGatewayText(gatewayName, paymentDebug, postbackDebug));
        }
        
        public static void PaymentInfo(this ILog log, string message, string gatewayName, string paymentDebug = null, string postbackDebug = null)
        {
            log.Info(message.ToPaymentGatewayText(gatewayName, paymentDebug, postbackDebug));
        }
        
        public static void PaymentWarn(this ILog log, string message, string gatewayName, string paymentDebug = null, string postbackDebug = null)
        {
            log.Warn(message.ToPaymentGatewayText(gatewayName, paymentDebug, postbackDebug));
        }
        
        public static void PaymentNotProcessed(this ILog log, string gatewayName, string paymentDebug, string postbackDebug)
        {
            log.PaymentWarn("Payment not processed", gatewayName, paymentDebug, postbackDebug);
        }

        public static void PaymentException(this ILog log, string gatewayName, string paymentDebug, string postbackDebug, Exception ex)
        {
            log.Exception(
                ex?.GetType()?.Name ?? string.Empty,
                new PaymentGatewayException(gatewayName,
                    ex?.Message?.ToPaymentGatewayText(string.Empty, paymentDebug, postbackDebug),
                    ex));
        }

        private static string ToPaymentGatewayText(this string message, string gatewayName, string paymentDebug = null, string postbackDebug = null)
        {
            var paymentInfo =
                !string.IsNullOrEmpty(paymentDebug)
                    ? $"\r\nPayment Info: {paymentDebug}"
                    : string.Empty;
            var postbackInfo =
                !string.IsNullOrEmpty(postbackDebug)
                    ? $"\r\nData: {postbackDebug}"
                    : string.Empty;
            var msg =
                !string.IsNullOrEmpty(gatewayName) && !string.IsNullOrEmpty(message)
                    ? $"\r\n{gatewayName}: {message}"
                    : !string.IsNullOrEmpty(message)
                        ? message
                        : string.Empty;
            return $"{msg}{paymentInfo}{postbackInfo}".Trim();
        }

        public static void Exception(this ILog log, string situationTag, Exception ex)
        {
            var formattedSituationTag = 
                !string.IsNullOrEmpty(situationTag)
                    ? $"{situationTag.Trim()}\r\n"
                    : string.Empty;
            switch (ex)
            {
                case PaymentGatewayException e:
                    formattedSituationTag = $"{e.Gateway} payment exception.\r\n{formattedSituationTag}";
                    switch (e.Severity)
                    {
                        case IssueSeverity.Info:
                            log.Info($"{formattedSituationTag}{e.Message.ToPaymentGatewayText(e.Gateway)}");
                            break;
                        case IssueSeverity.Warn:
                            log.Warn($"{formattedSituationTag}{e.Message.ToPaymentGatewayText(e.Gateway)}");
                            break;
                        case IssueSeverity.Error:
                            log.Error($"{formattedSituationTag}{e.Message.ToPaymentGatewayText(e.Gateway)}");
                            break;
                        case IssueSeverity.Fatal:
                            log.Fatal($"{formattedSituationTag}{e.Message.ToPaymentGatewayText(e.Gateway)}");
                            break;
                        default:
                            //NOTE: 2 log entries since it is 2 separate issues
                            log.Fatal(new ArgumentOutOfRangeException(nameof(e.Severity)));
                            log.Error(formattedSituationTag,  ex);
                            break;
                    }
                    break;
                case InvalidStateException e:
                    switch (e.Severity)
                    {
                        case IssueSeverity.Info:
                            log.Info($"{formattedSituationTag}{e.Message}");
                            break;
                        case IssueSeverity.Warn:
                            log.Warn($"{formattedSituationTag}{e.Message}");
                            break;
                        case IssueSeverity.Error:
                            log.Error($"{formattedSituationTag}{e.Message}");
                            break;
                        case IssueSeverity.Fatal:
                            log.Fatal($"{formattedSituationTag}{e.Message}");
                            break;
                        default:
                            //NOTE: 2 log entries since it is 2 separate issues
                            log.Fatal(new ArgumentOutOfRangeException(nameof(e.Severity)));
                            log.Error(formattedSituationTag,  ex);
                            break;
                    }
                    break;
                case System.Configuration.ConfigurationErrorsException cfg:
                    log.Warn(
                        cfg.InnerException != null
                            ? $"{formattedSituationTag}{cfg.Message}\r\n{cfg.InnerException.Message}"
                            : $"{formattedSituationTag}{cfg.Message}");
                    break;
                default:
                    log.Error(formattedSituationTag,  ex);
                    break;
            }
        }
    }
}