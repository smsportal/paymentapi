﻿using log4net;

namespace SmsPortal.Plutus
{
	public static class ResultValidator
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(ResultValidator));
		//public static void ValidateResult(HttpRequestMessage request, Sending.Models.SendResponse sendResponse)
		//{
		//	if (!sendResponse.HasErrors)
		//	{
		//		return;
		//	}
		//	if (sendResponse.ContainsErrorCode(SendingErrorCodes.IncorrectScheduledDate))
		//	{
		//		var errorCollection = CreateErrorCollection(sendResponse, HttpStatusCode.BadRequest);
		//		var response = request.CreateResponse(HttpStatusCode.BadRequest, errorCollection);
		//		throw new HttpResponseException(response);
		//	}
		//	else if (sendResponse.ContainsErrorCode(SendingErrorCodes.InsufficientCredits))
		//	{
		//		var errorCollection = CreateErrorCollection(sendResponse, HttpStatusCode.Forbidden);
		//		var response = request.CreateResponse(HttpStatusCode.Forbidden, errorCollection);
		//		throw new HttpResponseException(response);
		//	}
		//	else if (sendResponse.ContainsErrorCode(SendingErrorCodes.EmptySend))
		//	{
		//		var errorCollection = CreateErrorCollection(sendResponse, HttpStatusCode.BadRequest);
		//		var response = request.CreateResponse(HttpStatusCode.BadRequest, errorCollection);
		//		throw new HttpResponseException(response);
		//	}
		//	else
		//	{
		//		var response = request.CreateResponse(HttpStatusCode.InternalServerError);
		//		log.Error($"Unexpected error code {sendResponse.ErrorMessages[0].ErrorCode}.");
		//		throw new HttpResponseException(response);
		//	}
		//}

		//public static void ValidateAuthenticationResult(HttpRequestMessage request, AuthorizationResult authorizationResult)
		//{
		//	if (!authorizationResult.HasErrors)
		//	{
		//		return;
		//	}
		//	if (authorizationResult.ContainsErrorCode(SecurityErrorCodes.AuthenticationHeaderMissing))
		//	{
		//		var errorCollection = CreateErrorCollection(authorizationResult, HttpStatusCode.BadRequest);
		//		var response = request.CreateResponse(HttpStatusCode.BadRequest, errorCollection);
		//		throw new HttpResponseException(response);
		//	}
		//	else if (authorizationResult.ContainsErrorCode(SecurityErrorCodes.AuthenticationHeaderInvalid))
		//	{
		//		var errorCollection = CreateErrorCollection(authorizationResult, HttpStatusCode.BadRequest);
		//		var response = request.CreateResponse(HttpStatusCode.BadRequest, errorCollection);
		//		throw new HttpResponseException(response);
		//	}
		//	else
		//	{
		//		var response = request.CreateResponse(HttpStatusCode.InternalServerError);
		//		log.Error($"Unexpected error code {authorizationResult.ErrorMessages[0].ErrorCode}.");
		//		throw new HttpResponseException(response);
		//	}
		//}

		//public static void ValidateBalanceResult(HttpRequestMessage request, BalanceResult balanceResult)
		//{
		//	if (!balanceResult.HasErrors)
		//	{
		//		return;
		//	}
		//	if (balanceResult.ContainsErrorCode(SendingErrorCodes.InternalServerError))
		//	{
		//		var errorCollection = CreateErrorCollection(balanceResult, HttpStatusCode.InternalServerError);
		//		var response = request.CreateResponse(HttpStatusCode.InternalServerError, errorCollection);
		//		throw new HttpResponseException(response);
		//	}
		//	else
		//	{
		//		var response = request.CreateResponse(HttpStatusCode.InternalServerError);
		//		log.Error($"Unexpected error code {balanceResult.ErrorMessages[0].ErrorCode}.");
		//		throw new HttpResponseException(response);
		//	}
		//}

		//public static void ValidateReplyRuleResult(HttpRequestMessage request, ReplyRuleResult replyRuleResult)
		//{
		//	if (!replyRuleResult.HasErrors)
		//	{
		//		return;
		//	}
		//	if (replyRuleResult.ContainsErrorCode(SendingErrorCodes.EntityNotFoundReplyRule))
		//	{
		//		var errorCollection = CreateErrorCollection(replyRuleResult, HttpStatusCode.BadRequest);
		//		var response = request.CreateResponse(HttpStatusCode.BadRequest, errorCollection);
		//		throw new HttpResponseException(response);
		//	}
		//	else
		//	{
		//		var response = request.CreateResponse(HttpStatusCode.InternalServerError);
		//		log.Error($"Unexpected error code {replyRuleResult.ErrorMessages[0].ErrorCode}.");
		//		throw new HttpResponseException(response);
		//	}
		//}

		//private static ErrorResponseMessageCollection<Sending.Models.ErrorResponseMessage> CreateErrorCollection(Sending.Models.Response response,
		//	HttpStatusCode statusCode)
		//{
		//	return new ErrorResponseMessageCollection<Sending.Models.ErrorResponseMessage>
		//	{
		//		StatusCode = (int)statusCode,
		//		Errors = response.ErrorMessages
		//	};
		//}

		//private static ErrorResponseMessageCollection<Security.Models.ErrorResponseMessage> CreateErrorCollection(Security.Models.Response response,
		//	HttpStatusCode statusCode)
		//{
		//	return new ErrorResponseMessageCollection<Security.Models.ErrorResponseMessage>
		//	{
		//		StatusCode = (int)statusCode,
		//		Errors = response.ErrorMessages
		//	};
		//}

	}
}
