﻿namespace SmsPortal.Plutus.Model
{
    public class UpdateInvoicePaymentNumberRequest
    {
		public int AccountId { get; set; }

		public string AccountType { get; set; }

		public string OrderNumber { get; set; }

		public string TransactionId { get; set; }
    }
}
