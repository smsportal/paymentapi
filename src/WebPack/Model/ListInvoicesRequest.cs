﻿using System;

namespace SmsPortal.Plutus.Models
{
	public class ListInvoicesRequest
	{
		/// <summary>
		/// The LoginId of the account the invoice belongs to.
		/// </summary>
		public int? LoginId { get; set; }

		/// <summary>
		/// Include invoices created after the From date.
		/// </summary>
		public DateTime? From { get; set; }

		/// <summary>
		/// Include invoices created before the To date.
		/// </summary>
		public DateTime? To { get; set; }

		/// <summary>
		/// The currency of the invoice.
		/// </summary>
		public string Currency { get; set; }

		/// <summary>
		/// Status of the invoice.
		/// </summary>
		public string Status { get; set; }

		/// <summary>
		/// The payment gateway used when the invoice was created.
		/// </summary>
		public string Gateway { get; set; }
	}
}
