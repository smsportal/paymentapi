namespace SmsPortal.Plutus.Model.Dto.Enums
{
    /// <summary>
    /// The filter types for post-paid billing actions 
    /// </summary>
    public static class PostPaidBillingCycleFilterType
    {
        /// <summary>
        /// All post-paid invoices for the billing cycle based on SMS usage
        /// </summary>
        public const string BulkSmsUsage = "STD";

        /// <summary>
        /// All post-paid invoices for the billing cycle based on reverse billed replies
        /// </summary>
        public const string ReverseBilledReplies = "MOR";

        /// <summary>
        /// All post-paid invoices for the billing cycle based on reverse billed short code messages
        /// </summary>
        public const string ReverseBilledShortCodeMessages = "RBSC";

        /// <summary>
        /// All post-paid invoices for the billing cycle based on dedicated short code rentals
        /// </summary>
        public const string DedicatedShortCodeRental = "SC";

        /// <summary>
        /// All post-paid invoices for the billing cycle based on shared short code rentals
        /// </summary>
        public const string SharedShortCodeKeywordRental = "SCKW";
    }
}