﻿namespace SmsPortal.Plutus.Model.Dto.Enums
{
    /// <summary>
    /// Identifier for the various payment methods provided by the payment gateways  
    /// </summary>
    public static class PaymentMethodType
    {
        /// <summary>
        /// A payment using a credit card. 
        /// </summary>
        public const string CreditCard = "CC";
        
        /// <summary>
        /// A payment via the PayPal payment gateway. 
        /// </summary>
        public const string PayPal = "PP";
        
        /// <summary>
        /// A payment via the SAGE / NetCash payment gateway.
        /// NetCash was previously known as SagePay. 
        /// </summary>
        public const string SagePay = "SAGEP";
    }
}