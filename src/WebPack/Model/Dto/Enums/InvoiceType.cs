namespace SmsPortal.Plutus.Model.Dto.Enums
{
    /// <summary>
    /// The classifications of different invoice types
    /// </summary>
    public static class InvoiceType
    {
        /// <summary>
        /// This may be either a post-paid or pre-paid invoice.
        /// Pre-paid invoices of this type buy credits used for bulk messaging,
        /// while post-paid invoices list the cost of bulk messages (MT) already sent. 
        /// </summary>
        public const string BulkSms = "Credits"; //0

        /// <summary>
        /// This can only be a post-paid invoice.
        /// Lists the cost of reverse billed reply messages (MO).
        /// </summary>
        public const string ReverseBilledReplySms = "RBReply"; //1

        /// <summary>
        /// This can only be a post-paid invoice.
        /// Lists the cost of reverse billed short code messages.
        /// </summary>
        public const string ReverseBilledShortCodeSms = "RBSC"; //2

        /// <summary>
        /// This can only be a post-paid invoice.
        /// Lists the cost of setting up a short code.
        /// </summary>
        public const string ShortCodeSetup = "SCSetup"; //3

        /// <summary>
        /// This can only be a post-paid invoice.
        /// Lists the annual cost of renting a keyword on a shared shortcode.
        /// </summary>
        public const string ShortCodeKeywordRental = "SharedSCRental"; //4

        /// <summary>
        /// This can only be a post-paid invoice.
        /// Lists the monthly cost of renting a dedicated short code.
        /// </summary>
        public const string ShortCodeRental = "DedicatedSCRental"; //5

        //RESERVED VALUE IS NOT AN INVOICE TYPE: TransactionFee = 6  
    }
}