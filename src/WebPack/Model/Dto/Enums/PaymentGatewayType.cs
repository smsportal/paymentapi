﻿namespace SmsPortal.Plutus.Model.Dto.Enums
{
    /// <summary>
    /// The supported payment gateways  
    /// </summary>
    public static class PaymentGatewayType
    {
        /// <summary>
        /// An electronic fund transfer. 
        /// </summary>
        public const string Eft = "EFT"; //0
        
        /// <summary>
        /// A payment via the PayPal payment gateway. 
        /// </summary>
        public const string PayPal = "PayPal"; //10
        
        /// <summary>
        /// A payment via the SAGE payment gateway.
        /// NetCash was previously known as SagePay. 
        /// </summary>
        public const string NetCash = "SagePay"; //20
        
        /// <summary>
        /// An electronic fund transfer via the SAGE payment gateway.
        /// NetCash was previously known as SagePay. 
        /// </summary>
        public const string SagePayEft = "SagePay-EFT"; //21

        /// <summary>
        /// A payment via the Stripe payment gateway. 
        /// </summary>
        public const string Stripe = "Stripe"; //30
    }
}