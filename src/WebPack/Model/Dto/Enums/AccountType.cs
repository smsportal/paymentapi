namespace SmsPortal.Plutus.Model.Dto.Enums
{
    /// <summary>
    /// Identification of the various types of accounts
    /// </summary>
    public static class AccountType
    {
        /// <summary>
        /// A user account to send messages, this could be either a master or sub-account
        /// </summary>
        public const string SmsAccount = "MA";

        /// <summary>
        /// A user account for a reseller
        /// </summary>
        public const string ResellerAccount = "RS";

        /// <summary>
        /// A billing account linked to a pastel code
        /// </summary>
        public const string PastelCode = "PC";
    }
}