﻿namespace SmsPortal.Plutus.Model.Dto.Enums
{
    /// <summary>
    /// Represents the different billing options we provide to our clients
    /// </summary>
    public static class BillingType
    {
        /// <summary>
        /// Manual invoices.
        /// The types of invoices will be handled as if they are per user invoices.
        /// </summary>
        public const string Manual = "MANUAL"; //-1,
        /// <summary>
        /// Invoices per ref code with line items per master account (with the sub-account rolled up).  
        /// </summary>
        public const string PerRefCode = "REFCODE"; //0,
        /// <summary>
        /// Invoices per master account with line items per account.
        /// </summary>
        public const string PerUser = "USER"; //1,
        /// <summary>
        /// Invoices per master account with line items per network (with the sub-account rolled up). 
        /// </summary>
        public const string Smpp = "SMPP"; //2,
        /// <summary>
        /// Invoices per bulk messaging event with a single line items for the event. 
        /// </summary>
        public const string PerCampaign = "CAMPAIGN"; //3,
        /// <summary>
        /// Invoices per master account with line items per cost centre (with the sub-account rolled up). 
        /// </summary>
        public const string PerCostCentre = "COSTCENTRE"; //4,
        /// <summary>
        /// Invoices per cost centre & campaign combination with line items
        /// per network as well as a single line for all the reverse billed replies. 
        /// </summary>
        public const string PerCostCentrePerCampaign = "CAMPAIGN-COSTCENTRE"; //5
    }
}
