﻿namespace SmsPortal.Plutus.Model.Dto.Enums
{
    public static class PriceType
    {
        public const string Scale = "scale";
        public const string Fixed = "fixed";
        public const string PriceList = "pricelist";
    }
}