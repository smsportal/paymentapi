﻿namespace SmsPortal.Plutus.Model.Dto
{
	public class LineItemDto
	{
		/// <summary>
		/// Price per item.
		/// </summary>
		public decimal UnitPrice { get; set; }

		/// <summary>
		/// Number of items.
		/// </summary>
		public int Quantity { get; set; }

		/// <summary>
		/// The total amount of the line item.
		/// </summary>
		public decimal LineTotal { get; set; }

		/// <summary>
		/// Description of the line item.
		/// </summary>
		public string Description { get; set; }
	}
}
