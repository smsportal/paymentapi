﻿using System;
using System.Collections.Generic;

namespace SmsPortal.Plutus.Model.Dto
{
	public class InvoiceDto
	{
		public List<LineItemDto> LineItems { get; set; }

		/// <summary>
		/// The unique ID assigned by the system to the invoice.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// The amount of the invoice before tax.
		/// </summary>
		public decimal SubTotal { get; set; }

		/// <summary>
		/// The percentage of tax that should be added to the subtotal amount.
		/// </summary>
		public decimal TaxPercentage { get; set; }

		/// <summary>
		/// The amount of tax applicable on the invoice.
		/// </summary>
		public decimal TaxAmount { get; set; }

		/// <summary>
		/// The total amount due on the invoice.
		/// </summary>
		public decimal Total { get; set; }

		/// <summary>
		/// The type of tax being paid, VAT, GST etc.
		/// </summary>
		public string TaxType { get; set; }

		/// <summary>
		/// A unique ID assigned by the system to the invoice using the site's accounting system's numbering system.
		/// </summary>
		public string InvoiceNr { get; set; }

		/// <summary>
		/// The code used to post the invoice to Pastel.
		/// </summary>
		public string PastelCode { get; set; }

		/// <summary>
		/// If the account making a purchase is a SMS account then this value will be set. Otherwise 0
		/// </summary>
		public int LoginId { get; set; }

		/// <summary>
		/// If the account making a purchase is a Reseller then this value will be set. Otherwise 0
		/// </summary>
		public int ResellerId { get; set; }

		/// <summary>
		/// The name of the account making paying the invoice.
		/// </summary>
		public string AccountName { get; set; }

		/// <summary>
		/// Optional value provided by the client for their accounting department's purposes.
		/// </summary>
		public string PurchaseOrderNr { get; set; }

		/// <summary>
		/// Contains the name of the Login who created the invoice or to whom the invoice will be sent.
		/// </summary>
		public string CustomerRef { get; set; }

		/// <summary>
		/// The currency that the invoice is denominated in.
		/// </summary>
		public string Currency { get; set; }

		/// <summary>
		/// The current status of the invoice. Defaults to Unpaid.
		/// </summary>
		public string Status { get; set; }

		/// <summary>
		/// Details to appear on the invoice.
		/// </summary>
		public BillingDetailsDto BillingAddress { get; set; }

		/// <summary>
		/// When the invoice was created.
		/// </summary>
		public DateTime Created { get; set; }

		/// <summary>
		/// The date by which the invoice should be paid if present.
		/// </summary>
		public DateTime DueDate { get; set; }
	}
}
