﻿namespace SmsPortal.Plutus.Model.Dto
{
	public class BillingDetailsDto
	{
		/// <summary>
		/// The name to appear on the invoice.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Contains client's address information
		/// </summary>
		public string Address1 { get; set; }

		/// <summary>
		/// Contains client's address information
		/// </summary>
		public string Address2 { get; set; }

		/// <summary>
		/// The city in which the client resides.
		/// </summary>
		public string City { get; set; }

		/// <summary>
		/// The client's state or province.
		/// </summary>
		public string StateProvince { get; set; }

		/// <summary>
		/// The client's country of residence.
		/// </summary>
		public string Country { get; set; }


		public int CountryId { get; set; }

		/// <summary>
		/// The client's zip code or postal address
		/// </summary>
		public string ZipCode { get; set; }

		/// <summary>
		/// The client's VAT number (optional)
		/// </summary>
		public string VatNumber { get; set; }

		/// <summary>
		/// The client's company registration number
		/// </summary>
		public string RegistrationNumber { get; set; }

		/// <summary>
		/// The email address where invoices should be sent.
		/// </summary>
		public string InvoiceEmail { get; set; }

		/// <summary>
		/// Client's optional order number.
		/// </summary>
		public string OrderNumber { get; set; }
	}
}
