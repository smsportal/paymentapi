using System;

namespace SmsPortal.Plutus.Model.Dto
{
    public class AccountDto
    {
        public int AccountId { get; set; }

        public string AccountType { get; set; }

        public string Name { get; set; }

        public bool Activated { get; set; }

        public int? BillingGroupId { get; set; }

        public DateTime Created { get; set; }
    }
}