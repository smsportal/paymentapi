namespace SmsPortal.Plutus.Model.Dto
{
    public enum LineItemBreakdownTypeDto
    {
        PerRefCode = 0,
        PerUser = 1,
        PerNetwork = 2,
        PerCampaign = 3,
        PerCostCentre = 4,
        PerCostCentrePerCampaign = 5
    }
}