﻿namespace SmsPortal.Plutus.Model.Dto
{
    public class PaymentGatewayDto
    {
        public int CountryId { get; set; }

        public string Gateway { get; set; } 

        public string Currency { get; set; }

        public bool Enabled { get; set; }

        public string MerchantId { get; set; }

        public decimal MinTransaction { get; set; }

        public decimal MaxTransaction { get; set; }

        public decimal TransactionFee { get; set; }
    }
}
