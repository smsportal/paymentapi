﻿namespace SmsPortal.Plutus.Model
{
    public class CreateBillingGroupRequest
    {        
        public string PastelCode { get; set; }
        
        public int? CompanyId { get; set; }
        
        public string CompanyName { get; set; }
        
        public string PriceType { get; set; } 
        
        public int? PriceListId { get; set; }

        public string PriceFixedCurrency { get; set; }
        
        public decimal? PriceFixedStd { get; set; }
        
        public decimal? PriceFixedRb { get; set; }
        
        public decimal? PriceFixedRbReply { get; set; }
        
        public string Address1 { get; set; }
        
        public string Address2 { get; set; }
        
        public string City { get; set; }
        
        public string StateProvince { get; set; }
        
        public string ZipPostalCode { get; set; }
        
        public int CountryId { get; set; }
        
        public string Email { get; set; }
        
        public string VatNr { get; set; }
        
        public string RegistrationNr { get; set; }
        
        public string ScSharedRentalBillingType { get; set; }
        
        public string ScDedicatedRentalBillingType { get; set; }
        
        public string ScRevBilledRentalBillingType { get; set; }
        
        public string MtBillingType { get; set; }
        
        public string MoBillingType { get; set; }
        
        public bool PostPaid { get; set; }
        
        public bool BillForNoNetworks { get; set; }
    }
}