﻿using System;

namespace SmsPortal.Plutus.Model
{
    public class CreateQuoteRequest
    {

        public int Quantity { get; set; }

        public decimal UnitPrice { get; set; }

        public int? RouteCountryId { get; set; }

        public string Currency { get; set; }

        public string AccountType { get; set; }
        public int AccountId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string OrderNumber { get; set; } = "";
    }
}
