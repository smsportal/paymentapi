﻿namespace SmsPortal.Plutus.Model
{
    public class CreatePostPaidEventInvoiceRequest
    {
        public string AccountType { get; set; }
        public int AccountId { get; set; }
        public long EventId { get; set; }
        public string OrderNumber { get; set; } = "";
        public string EmailAddress { get; set; } = "";
    }
}
