﻿using System;

namespace SmsPortal.Plutus.Model
{
	public class CreateShortCodeRentalInvoiceRequest
	{
		public int SCRental_BillingType { get; set; }
		public bool SCIsdedicated { get; set; }
		public int AccountId { get; set; }
		public string OrderNumber { get; set; }
		public long PeriodBilledFromDateTicks { get; set; }
		public long PeriodBilledToDateTicks { get; set; }
		public string Comment { get; set; }
		public string ShortCode { get; set; }
		public string Keyword { get; set; }
		public string Email { get; set; }
		public DateTime PeriodBilledFromDate => new DateTime(PeriodBilledFromDateTicks);
		public DateTime PeriodBilledToDate => new DateTime(PeriodBilledToDateTicks);
	}
}
