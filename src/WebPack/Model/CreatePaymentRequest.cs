﻿
namespace SmsPortal.Plutus.Model
{
    public class CreatePaymentRequest
    {
        /// <summary>
        /// The ID of the account performing the purchase
        /// </summary>
        public int LoginId { get; set; }

        /// <summary>
        /// The type of account performing the purchase. Only "MA" supported
        /// </summary>
        public string AccountType { get; set; }

        /// <summary>
        /// The currency of the UnitPrice.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// The number of SMS credits being purchased.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// The price of a single SMS credit
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// The gateway that the client has chosen to use to pay for the SMS credit purchase. Valid values "paypal", "sagepay" and "eft" and "instanteft"
        /// </summary>
        public string Gateway { get; set; }

        /// <summary>
        /// The name to appear on the invoice.
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// The ID of the RouteCountry for which credits are being purchased.
        /// </summary>
        public int? RouteCountryId { get; set; }

        // TODO The following fields below are unused by Plutus and are to be removed
        
        /// <summary>
        /// Optional value provided by the client for their accounting department's purposes.
        /// </summary>
        public string PurchaseOrderNr { get; set; }

        /// <summary>
        /// Contains client's address information
        /// </summary>
        public string Address1 { get; set; }
        
        /// <summary>
        /// Contains client's address information
        /// </summary>
        public string Address2 { get; set; }
        
        /// <summary>
        /// The city in which the client resides.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// The client's country of residence.
        /// </summary>
        public string Country { get; set; }
        
        public int CountryId { get; set; }

        /// <summary>
        /// The email address where invoices should be sent.
        /// </summary>
        public string InvoiceEmail { get; set; }

        /// <summary>
        /// The client's state or province.
        /// </summary>
        public string StateProvince { get; set; }

        /// <summary>
        /// The client's state or province.
        /// </summary>
        public string VatNumber { get; set; }

        /// <summary>
        /// The client's zip code or postal address
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// The client's company registration number
        /// </summary>
        public string RegistrationNumber { get; set; }

    }
}
