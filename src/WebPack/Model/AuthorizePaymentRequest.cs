﻿using System;

namespace SmsPortal.Plutus.Model
{
    public class AuthorizePaymentRequest
    {
	    public string AdminUsername { get; set; }

		public string AdminPassword { get; set; }

		public decimal Total { get; set; }

		public decimal VatAmount { get; set; }

		public decimal Credits { get; set; }

		public int PurchaseAccLoginId { get; set; }

		public DateTime CreatedDate { get; set; }

        public DateTime DueDate { get; set; }

		public string Currency { get; set; }

		public decimal TransactionFee { get; set; }

		public string TransactionId { get; set; }

		public string PaymentMethod { get; set; }

		public string AccountType { get; set; }

		public string PaymentReference { get; set; }

		public decimal Price => (Total - VatAmount - TransactionFee) / Credits;
    }
}
