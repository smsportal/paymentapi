﻿namespace SmsPortal.Plutus.Model
{
    public class CancelInvoiceRequest
    {
		public string InvoiceTransactionId { get; set; }

		public int AccountId { get; set; }

		public string AccountType { get; set; }
    }
}
