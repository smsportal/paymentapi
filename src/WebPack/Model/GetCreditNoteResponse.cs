﻿using SmsPortal.Plutus.Model.Dto;

namespace SmsPortal.Plutus.Model
{
	public class GetCreditNoteResponse : Response
	{
		public CreditNoteDto CreditNote { get; set; }
	}
}
