﻿using System;

namespace SmsPortal.Plutus.Model
{
	public class ListCreditNotesRequest
	{
		/// <summary>
		/// The LoginId of the account the credit note belongs to.
		/// </summary>
		public int? LoginId { get; set; }

		/// <summary>
		/// Include credit notes created after the From date.
		/// </summary>
		public DateTime? From { get; set; }

		/// <summary>
		/// Include credit notes created before the To date.
		/// </summary>
		public DateTime? To { get; set; }

		/// <summary>
		/// The currency of the credit note.
		/// </summary>
		public string Currency { get; set; }
	}
}
