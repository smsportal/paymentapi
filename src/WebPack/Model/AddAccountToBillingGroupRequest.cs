namespace SmsPortal.Plutus.Model
{
    public class AddAccountToBillingGroupRequest
    {
        public int Id { get; set; }

        public int AccountId { get; set; }
    }
}