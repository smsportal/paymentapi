using System.Collections.Generic;
using SmsPortal.Plutus.Model.Dto;

namespace SmsPortal.Plutus.Model
{
    public class ListCreditAllocationsResponse: Response
    {
        public List<CreditAllocationDto> CreditAllocations { get; set; }
        
    }
}