﻿namespace SmsPortal.Plutus.Model
{
	public class PdfResponse : Response
	{
		public byte[] PdfBytes { get; set; }
	}
}
