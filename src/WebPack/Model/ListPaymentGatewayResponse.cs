﻿using System.Collections.Generic;
using SmsPortal.Plutus.Model.Dto;

namespace SmsPortal.Plutus.Model
{
    /// <summary>
    /// Response object containing payment gateway details.
    /// </summary>
    public class ListPaymentGatewayResponse
    {
        /// <summary>
        /// List of payment gateways.
        /// </summary>
        public List<PaymentGatewayDto> PaymentGateways { get; set; }

        /// <summary>
        /// Creates and initializes a new <c>ListPaymentGatewayResponse</c> object.
        /// </summary>
        public ListPaymentGatewayResponse()
        {
            PaymentGateways = new List<PaymentGatewayDto>();
        }
    }
}
