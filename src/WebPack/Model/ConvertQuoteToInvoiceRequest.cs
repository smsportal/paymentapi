﻿namespace SmsPortal.Plutus.Model
{
    public class ConvertQuoteToInvoiceRequest
    {
        public int QuoteId { get; set; }
        public string OrderNumber { get; set; }
        public int AccountId { get; set; }
        public string AccountType { get; set; }
        public string Status { get; set; }
    }
}
