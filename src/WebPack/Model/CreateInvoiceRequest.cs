﻿namespace SmsPortal.Plutus.Model
{
    public class CreateInvoiceRequest
    {
        public int AccountId { get; set; }

        /// <summary>
        /// The type of account. Either master, subaccount or reseller. Defaults to master.
        /// </summary>
        public string AccountType { get; set; } = "MA";

        public int Quantity { get; set; }

        public decimal UnitPrice { get; set; }

        public string Gateway { get; set; }

        public string PurchaseOrderNumber { get; set; }
        
        public int? RouteCountryId { get; set; }
        
        public string Currency { get; set; }

        // TODO The following fields below are unused by Plutus and are to be removed
        
        public string Address1 { get; set; }
        
        public string Address2 { get; set; }
        
        public string City { get; set; }
        
        public string CompanyName { get; set; }
        
        public string CountryName { get; set; }
        
        public int CountryId { get; set; }
        
        public string InvoiceEmail { get; set; }
        
        public string StateProvince { get; set; }
        
        public string VatNumber { get; set; }
        
        public string ZipCode { get; set; }
        
        public string RegistrationNumber { get; set; }

    }
}
