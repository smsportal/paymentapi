﻿using System.Collections.Generic;
using SmsPortal.Plutus.Model.Dto;

namespace SmsPortal.Plutus.Model
{
    public class ListBillingGroupsResponse : Response
    {
        public List<BillingGroupDto> BillingGroups { get; set; } = new List<BillingGroupDto>();
    }
}