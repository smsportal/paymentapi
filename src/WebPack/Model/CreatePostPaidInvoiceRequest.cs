﻿using System;

namespace SmsPortal.Plutus.Model
{
    public class CreatePostPaidInvoiceRequest
    {
        public string FilterType { get; set; }
        public string AccountType { get; set; }
        public int AccountId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string OrderNumber { get; set; } = "";
    }
}
