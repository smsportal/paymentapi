﻿using SmsPortal.Plutus.Model.Dto;

namespace SmsPortal.Plutus.Model
{
    public class CreateBillingGroupResponse : Response
    {
        public BillingGroupDto BillingGroup { get; set; }
    }
}