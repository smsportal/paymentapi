﻿using System.Collections.Generic;
using SmsPortal.Plutus.Model.Dto;

namespace SmsPortal.Plutus.Model
{
	public class ListInvoicesResponse : Response
	{
		public List<InvoiceDto> Invoices { get; set; }
	}
}
