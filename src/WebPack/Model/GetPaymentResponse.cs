﻿namespace SmsPortal.Plutus.Model
{
    public class GetPaymentResponse
    {
        public decimal TaxAmount { get; set; }

        public string TaxType { get; set; }

        public decimal PaymentFee { get; set; }

        public decimal SubTotal { get; set; }

        public decimal Total { get; set; }

        public string Currency { get; set; }

        //TODO: consider returning whole invoice
        public string InvoiceNr { get; set; }

        public string TransactionId { get; set; }

        public string HtmlPaymentForm { get; set; }

        public string Status { get; set; }
    }
}
