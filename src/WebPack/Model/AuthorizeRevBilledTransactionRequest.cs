﻿using System;

namespace SmsPortal.Plutus.Model
{
	public class AuthorizeRevBilledTransactionRequest
	{
		public int LoginId { get; set; }

		public string EmailRecipients { get; set; }

		public string EmailSubject { get; set; }

		public string EmailBody { get; set; }

		public string Comment { get; set; }

		public long FromDateTicks { private get; set; }

		public long ToDateTicks { private get; set; }

		public string InvoiceTypeString { get; set; }

		public DateTime PeriodBilledFromDate => new DateTime(FromDateTicks);

		public DateTime PeriodBilledToDate => new DateTime(ToDateTicks);
	}
}
