﻿using SmsPortal.Plutus.Model.Dto;
using System.Collections.Generic;

namespace SmsPortal.Plutus.Model
{
    public class GetPostPaidBillingCycleProformaInvoiceListResponse
    {
        public string FilterType { get; set; }
        public List<InvoiceDto> Invoices { get; set; }
    }
}
