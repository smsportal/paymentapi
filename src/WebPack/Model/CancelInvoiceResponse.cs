﻿namespace SmsPortal.Plutus.Model
{
    public class CancelInvoiceResponse
    {
        public string CreditNoteReference { get; set; }
    }
}
