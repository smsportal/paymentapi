﻿using SmsPortal.Plutus.Model.Dto;

namespace SmsPortal.Plutus.Model
{
    public class CreateInvoiceResponse
    {
        public InvoiceDto Invoice { get; set; }
    }
}
