using System;

namespace SmsPortal.Plutus.Model
{
    public class StartBillingCycleRequest
    {
        public DateTime BillingCycleStart { get; set; }
    }
}