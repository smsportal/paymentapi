﻿using System;

namespace SmsPortal.Plutus.Models
{
	/// <summary>
	/// Request for bulk retrieval of quotes.
	/// </summary>
	public class ListQuotesRequest
	{
		/// <summary>
		/// Include invoices created after the From date.
		/// </summary>
		public DateTime? From { get; set; }

		/// <summary>
		/// Include invoices created before the To date.
		/// </summary>
		public DateTime? To { get; set; }

		/// <summary>
		/// The currency of the invoice.
		/// </summary>
		public string Reference { get; set; }
	}
}
