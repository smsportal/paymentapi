﻿namespace SmsPortal.Plutus.Model
{
    public class DeclinePostPaidInvoiceRequest : CreatePostPaidInvoiceRequest
    {
        public string Reason { get; set; }
    }
}
