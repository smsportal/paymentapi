﻿using SmsPortal.Plutus.Model.Dto;
using SmsPortal.Plutus.Model.Dto.Enums;

namespace SmsPortal.Plutus.Model
{
    public class GetBillingGroupResponse : Response
    {
        public BillingGroupDto BillingGroup { get; set; }
    }
}