﻿namespace SmsPortal.Plutus.Model
{
    public class CreateCreditNoteRequest
    {
		public long CreateDatemilliseconds1 { get; set; }

		public string InvoiceTransactionId { get; set; }

		public int AccountId { get; set; }

		public string AccountType { get; set; }

		public long CreateDatemilliseconds2 { get; set; }
	}
}
