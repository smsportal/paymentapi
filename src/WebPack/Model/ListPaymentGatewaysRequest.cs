﻿namespace SmsPortal.Plutus.Model
{
    /// <summary>
    /// Request object for getting all the PaymentGateways for a site.
    /// </summary>
    public class ListPaymentGatewaysRequest
    {
        /// <summary>
        /// The ID of the site.
        /// </summary>
        public int SiteId { get; set; }
    }
}
