﻿using System;

namespace SmsPortal.Plutus.Model
{
    public class CreatePostPaidShortCodeSetupInvoiceRequest
    {
        public string AccountType { get; set; }
        public int AccountId { get; set; }
        public string ShortCode { get; set; }
        public string Keyword { get; set; }
        public string ShortCodeUsername { get; set; }
        public DateTime SetupDate { get; set; }
        public string OrderNumber { get; set; }
        public string EmailAddress { get; set; }
        public long? DataId { get; set; }
    }
}
