﻿namespace SmsPortal.Plutus.v3.Model
{
    /// <summary>
    /// A request to start processing a payment  
    /// </summary>
    public class CreatePaymentRequest
    {
        /// <summary>
        /// The ID of the account performing the purchase.
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// The type of account performing the purchase.
        /// <seealso cref="SmsPortal.Plutus.Model.Dto.Enums.AccountType"/>
        /// </summary>
        public string AccountType { get; set; }

        /// <summary>
        /// The number of SMS credits being purchased.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// The price of a single SMS credit
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// The 3-letter ISO 4217 code for the currency of the unit price.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// The gateway that the client has chosen to use to pay for the SMS credit purchase.
        /// <seealso cref="SmsPortal.Plutus.Model.Dto.Enums.PaymentGatewayType"/>
        /// </summary>
        public string Gateway { get; set; }

        /// <summary>
        /// Optional value provided by the client for their accounting department's purposes.
        /// </summary>
        public string PurchaseOrderNr { get; set; }

        /// <summary>
        /// The ID of the RouteCountry for which credits are being purchased.
        /// </summary>
        public int RouteCountryId { get; set; }

        /// <summary>
        /// An optional email address to notify in addition to the email address configured on the bill address.
        /// </summary>
        public string NotificationEmailAddress { get; set; }
    }
}