﻿using System.Collections.Generic;

namespace SmsPortal.Plutus.v3.Model
{
    /// <summary>
    /// A response to the payment request.
    /// <seealso cref="SmsPortal.Plutus.v3.Model.CreatePaymentRequest"/> 
    /// </summary>
    public class CreatePaymentResponse
    {
        /// <summary>
        /// The ID of the account performing the purchase.
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// The type of account performing the purchase.
        /// <seealso cref="SmsPortal.Plutus.Model.Dto.Enums.AccountType"/>
        /// </summary>
        public string AccountType { get; set; }
        
        /// <summary>
        /// The SMS Portal internal unique identifier for the payment.
        /// </summary>
        public string PaymentId { get; set; }

        /// <summary>
        /// The invoice number associated with the payment.
        /// If not provided, then there is no invoice associated with the payment.   
        /// </summary>
        public string InvoiceNr { get; set; }

        /// <summary>
        /// The 3-letter ISO 4217 code for the payment currency.
        /// </summary>
        public string Currency { get; set; }
        
        /// <summary>
        /// A description of a the items purchased.
        /// </summary>
        public string UnitDescription { get; set; }
        
        /// <summary>
        /// The price of a single item (SMS credit).
        /// </summary>
        public decimal UnitPrice { get; set; }
        
        /// <summary>
        /// The quantity of items (SMS Credits) purchased.
        /// </summary>
        public decimal Quantity { get; set; }
        
        /// <summary>
        /// The transaction fees associated with the payment.
        /// </summary>
        public decimal TransactionFee { get; set; }
        
        /// <summary>
        /// The tax amount of the payment.
        /// </summary>
        public decimal TaxAmount { get; set; }
        
        /// <summary>
        /// The total amount of the payment.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// The variables required to complete the payment process from the customer's browser. 
        /// </summary>
        public IDictionary<string, string> Variables { get; set; }
    }
}