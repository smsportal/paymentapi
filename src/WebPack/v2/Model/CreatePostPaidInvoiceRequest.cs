using System;
using SmsPortal.Plutus.Model.Dto;

namespace SmsPortal.Plutus.v2.Model
{
    public class CreatePostPaidInvoiceRequest
    {
        public int Month { get; set; }
        
        public string ClientRefCode { get; set; }

        public bool DryRun { get; set; }
        
        // TODO Remove this in a future PR.
        // This field is no longer used in the request, but retrieved from the billing group.
        [Obsolete]
        public bool BillForNoNetworks { get; set; }
    }
}