using System.Collections.Generic;
using SmsPortal.Plutus.v2.Model.Dto;

namespace SmsPortal.Plutus.v2.Model
{
    /// <summary>
    /// The response to a <seealso cref="ListPaymentHistoryRequest"/> request.
    /// </summary>
    public class ListPaymentHistoryResponse
    {
        /// <summary>
        /// The list of payments.
        /// </summary>
        public List<PaymentDto> Payments { get; set; }
        public int PageNumber { get; set; }
        public int NumberOfPages { get; set; }
    }
}