namespace SmsPortal.Plutus.v2.Model
{
    public class CreatePaymentResponse
    {
        public string PaymentId { get; set; }

        public string HtmlForm { get; set; }

        public string InvoiceNr { get; set; }

        public decimal Total { get; set; }
    }
}