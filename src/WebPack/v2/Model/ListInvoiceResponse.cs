using System.Collections.Generic;
using SmsPortal.Plutus.Model.Dto;

namespace SmsPortal.Plutus.v2.Model
{
    public class ListInvoiceResponse
    {
        public List<InvoiceDto> Invoices { get; set; }

        public ListInvoiceResponse()
        {
            Invoices = new List<InvoiceDto>();
        }
    }
}