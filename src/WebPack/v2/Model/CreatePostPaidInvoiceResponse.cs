using System.Collections.Generic;
using SmsPortal.Plutus.Model;

namespace SmsPortal.Plutus.v2.Model
{
    public class CreatePostPaidInvoiceResponse: Response
    {
        public List<int> InvoiceIds { get; }

        public CreatePostPaidInvoiceResponse()
        {
            InvoiceIds = new List<int>();
        }
    }
}