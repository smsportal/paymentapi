namespace SmsPortal.Plutus.v2.Model.Dto.Enums
{
    /// <summary>
    /// Identification of the various payment statuses
    /// </summary>
    public static class PaymentStatus
    {
        /// <summary>
        /// The payment process was started.
        /// </summary>
        public const string Started = "STARTED";

        /// <summary>
        /// The payment process is in a faulted state, resulting in a failed payment.
        /// </summary>
        public const string Faulted = "FAULTED";

        /// <summary>
        /// The payment process was completed, but resulted in a failed payment.
        /// </summary>
        public const string Failed = "FAILED";
        
        /// <summary>
        /// The payment process was completed, resulting in a successful payment.
        /// </summary>
        public const string Completed = "COMPLETED";
    }
}