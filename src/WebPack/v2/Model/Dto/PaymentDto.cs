using System;

namespace SmsPortal.Plutus.v2.Model.Dto
{
    /// <summary>
    /// A Payment made by either a SMS Account or Reseller.  
    /// </summary>
    public class PaymentDto
    {
        /// <summary>
        /// The globally unique identifier assigned by the system to the payment transaction.
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// The unique numerical identifier assigned by the system to the invoice.
        /// <remarks>
        /// If not provided then the payment is not associated with an invoice. 
        /// </remarks> 
        /// </summary>
        public int? InvoiceId { get; set; }

        /// <summary>
        /// A unique ID assigned by the system to the invoice using the site's accounting system's numbering system.
        /// <remarks>
        /// If not provided then the payment is not associated with an invoice. 
        /// </remarks> 
        /// </summary>
        public string InvoiceNr { get; set; }

        /// <summary>
        /// The currency that the payment is denominated in.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// The amount of tax applicable on the payment.
        /// </summary>
        public decimal TaxAmount { get; set; }

        /// <summary>
        /// The total amount due on the payment.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// A description of the reason for the payment.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The payment gateway used.
        /// </summary>
        public string Gateway { get; set; }

        /// <summary>
        /// The unique identifier assigned by the payment gateway to the payment transaction. 
        /// </summary>
        public string GatewayTransactionId { get; set; }

        /// <summary>
        /// The <seealso cref="SmsPortal.Plutus.v2.Model.Dto.Enums.PaymentStatus"/> of the payment transaction.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// The date of the last <seealso cref="SmsPortal.Plutus.v2.Model.Dto.Enums.PaymentStatus"/> of the payment transaction.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The amount of credits purchased with the payment.
        /// </summary>
        public int? Credits { get; set; }
        
        /// <summary>
        /// The unique numerical identifier of the route country associated with the payment.
        /// <remarks>
        /// If not provided then the payment is not associated with a single route country. 
        /// </remarks> 
        /// </summary>
        public int? RouteCountryId { get; set; }
    }
}